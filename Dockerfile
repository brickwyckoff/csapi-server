from python:3.6

COPY . /src/

RUN pip install --requirement /src/requirements/dev.txt

# EXPOSE 80
EXPOSE 8001

ENV DJANGO_SETTINGS_MODULE csapi.settings.dev

WORKDIR /src

CMD gunicorn csapi.wsgi:application -b '0.0.0.0:8001' --reload
