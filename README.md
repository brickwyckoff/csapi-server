# README #

Basic seed project for python/django server in the CS API project

## Docker instructions

- To build the docker file, change to the project root and issue "docker build -t csapi ." 

- To run the container successfully you need to be running the DB in a local mysql instance on the standard 3306 port. You also can't be running any other web server on port 80.

- To run the image, issue:
`docker run -d -p 80:80  -v C:\Users\twyck\Code\CS\csapi-server:/src --add-host=dbserver:192.168.1.157 csapi`
...replacing the path to your codebase after the '-v' with your own (cannot use relative '.' addressing here, unfortunately) and replacing the IP with your host machine's IP.

This will get more robust over time, but just about works now. 


## Pre-built sample requests
```
POST /login/ HTTP/1.1
Host: 127.0.0.1
Content-Type: application/json
Cache-Control: no-cache

{
	"username": "twyckoff",
	"password": "mypasswordhere"
}
```

This will return a json body with a JWT token, as:

```
{
  "result": true,
  "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJjc2FwaS5kZW1vLmNvbSIsImF1ZCI6WyJ1cm46Y3NhcGkiLCJ1cm46Y3NhZG1pbiJdLCJpYXQiOjE0OTA3MTY2MjUsImp0aSI6NjI0LCJzeXN0ZW1fbmFtZSI6InR3eWNrb2ZmIiwiZW1haWwiOiJ0d3lja29mZkBicmlja3NpbXBsZS5jb20iLCJyb2xlcyI6eyJyb2xlIjoiYWRtaW5pc3RyYXRvciJ9fQ.t5247xhQ7_jxCVvaCA4EjfSLphqgOezgOKd9h6NH5lU"
}
```

...you can then create another request, passing the token in through the auth header, as:

```
GET /demo/verify HTTP/1.1
Host: 127.0.0.1
Content-Type: application/json
Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJjc2FwaS5kZW1vLmNvbSIsImF1ZCI6WyJ1cm46Y3NhcGkiLCJ1cm46Y3NhZG1pbiJdLCJpYXQiOjE0OTA3MTY2MjUsImp0aSI6NjI0LCJzeXN0ZW1fbmFtZSI6InR3eWNrb2ZmIiwiZW1haWwiOiJ0d3lja29mZkBicmlja3NpbXBsZS5jb20iLCJyb2xlcyI6eyJyb2xlIjoiYWRtaW5pc3RyYXRvciJ9fQ.t5247xhQ7_jxCVvaCA4EjfSLphqgOezgOKd9h6NH5lU
Cache-Control: no-cache
```

The above should simply echo back the contents of your JWT, it is for demo purposes only. 

Other, more RESTful EP requests:

```
GET /api/demdata/ HTTP/1.1
Host: 127.0.0.1
Content-Type: application/json
Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJjc2FwaS5kZW1vLmNvbSIsImF1ZCI6WyJ1cm46Y3NhcGkiLCJ1cm46Y3NhZG1pbiJdLCJpYXQiOjE0OTA3MTY2MjUsImp0aSI6NjI0LCJzeXN0ZW1fbmFtZSI6InR3eWNrb2ZmIiwiZW1haWwiOiJ0d3lja29mZkBicmlja3NpbXBsZS5jb20iLCJyb2xlcyI6eyJyb2xlIjoiYWRtaW5pc3RyYXRvciJ9fQ.t5247xhQ7_jxCVvaCA4EjfSLphqgOezgOKd9h6NH5lU
Cache-Control: no-cache
```

```
GET /api/demdatamanual/11 HTTP/1.1
Host: 127.0.0.1
Content-Type: application/json
Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJjc2FwaS5kZW1vLmNvbSIsImF1ZCI6WyJ1cm46Y3NhcGkiLCJ1cm46Y3NhZG1pbiJdLCJpYXQiOjE0OTA3MTY2MjUsImp0aSI6NjI0LCJzeXN0ZW1fbmFtZSI6InR3eWNrb2ZmIiwiZW1haWwiOiJ0d3lja29mZkBicmlja3NpbXBsZS5jb20iLCJyb2xlcyI6eyJyb2xlIjoiYWRtaW5pc3RyYXRvciJ9fQ.t5247xhQ7_jxCVvaCA4EjfSLphqgOezgOKd9h6NH5lU
Cache-Control: no-cache
```

March 29, 2017: Graphene querying added. Can now issue:

```
POST /graph/graphql/ HTTP/1.1
Host: 127.0.0.1
Content-Type: application/json
Authorization: Bearer Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJjc2FwaS5kZW1vLmNvbSIsImF1ZCI6WyJ1cm46Y3NhcGkiLCJ1cm46Y3NhZG1pbiJdLCJpYXQiOjE0OTA3MTYzMTQsImp0aSI6NjI0LCJzeXN0ZW1fbmFtZSI6InR3eWNrb2ZmIiwiZW1haWwiOiJ0d3lja29mZkBicmlja3NpbXBsZS5jb20iLCJyb2xlcyI6eyJyb2xlIjoiYWRtaW5pc3RyYXRvciJ9fQ.4QXYQjeUUbrJFXQIi_tLq1qsMXXoNNRRL1qu5FwxSo8
Cache-Control: no-cache

{
    "query":"{patients(mrn:1013){firstname, lastname, demdata {ptaddress1, ptcity, ptstate, ptzip}}}"
}
```