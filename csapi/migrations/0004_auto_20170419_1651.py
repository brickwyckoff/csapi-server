# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-04-19 20:51
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('csapi', '0003_auto_20170419_1642'),
    ]

    operations = [
        migrations.CreateModel(
            name='BatchEligCoverageDetail',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('insuranceTier', models.SmallIntegerField()),
                ('coverageCode', models.CharField(max_length=1)),
                ('inPlanNetwork', models.BinaryField(default=False)),
                ('benefitPercent', models.DecimalField(decimal_places=2, max_digits=3)),
                ('coverageLevel', models.CharField(max_length=16)),
                ('timePeriod', models.CharField(max_length=32)),
                ('benefitAmount', models.DecimalField(decimal_places=2, max_digits=6)),
            ],
            options={
                'db_table': 'BatchEligCoverageDetail',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='BatchEligHeader',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('status', models.CharField(max_length=16)),
                ('rejectionReason', models.CharField(max_length=100)),
                ('subscriberFirst', models.CharField(max_length=100)),
                ('subscriberLast', models.CharField(max_length=100)),
                ('subscriberMiddle', models.CharField(max_length=100)),
                ('subscriberGender', models.CharField(max_length=6)),
                ('subscriberDob', models.CharField(max_length=10)),
                ('subscriberAddress1', models.CharField(max_length=100)),
                ('subscriberAddress2', models.CharField(max_length=100)),
                ('subscriberCity', models.CharField(max_length=100)),
                ('subscriberState', models.CharField(max_length=100)),
                ('subscriberZip', models.CharField(max_length=10)),
                ('subscriberPrimaryId', models.CharField(max_length=32)),
                ('subscriberPrimaryactive', models.BinaryField(default=0)),
                ('subscriberPrimaryGroupNumber', models.CharField(max_length=32)),
                ('subscriberPrimaryGroupDescription', models.CharField(max_length=100)),
                ('subscriberPrimaryCompany', models.CharField(max_length=100)),
                ('subscriberPrimaryPlanDescription', models.CharField(max_length=100)),
                ('subscriberPrimaryBeginDate', models.CharField(max_length=10)),
                ('subscriberPrimaryEndDate', models.CharField(max_length=10)),
                ('batchId', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='csapi.BatchEligResult')),
            ],
            options={
                'db_table': 'batchEligHeader',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='BatchEligHeaderMessage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('message', models.CharField(max_length=256)),
                ('batchHeaderId', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='csapi.BatchEligHeader')),
            ],
            options={
                'db_table': 'batchEligHeaderMessage',
                'managed': True,
            },
        ),
        migrations.AddField(
            model_name='batcheligcoveragedetail',
            name='batchHeaderId',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='csapi.BatchEligHeader'),
        ),
    ]
