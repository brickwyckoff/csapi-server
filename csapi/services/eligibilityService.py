from rest_framework.views import APIView
from django.http import JsonResponse
from csapi.dbAccess.dbQueryThread import DbQueryThread
from csapi.core.apiCredentials import ApiCredentials
from csapi.core.cacheAccess import CacheAccess
from csapi.eligibility import pokitdokProvider, eligibilityApiProvider
from csapi.models.eligibilitycheckinfo import Eligibilitycheckinfo
import uuid

class EligibilityService (APIView):
    def __init__ (self):
        self.apiCredentials = ApiCredentials ()
        self.cache = CacheAccess ()
        self.cacheScope = "EligibilityService"
        self.cacheKey_DefaultProvider = 'DefaultProvider'
        if self.cache.getScopedByKey (self.cacheScope, self.cacheKey_DefaultProvider):
            self.defaultProvider = self.cache.getScopedByKey (self.cacheScope, self.cacheKey_DefaultProvider)[0]

    def getStartupQueryThreads (self):
        defaultProviderThread = DbQueryThread('SELECT code, firstName, lastName, NPI FROM providers WHERE isMedDirector=1 limit 1;'.format(id))

        return [{
            'scope': self.cacheScope,
            'key': self.cacheKey_DefaultProvider,
            'ThreadInstance': defaultProviderThread
        }]

    def post(self, request, format=None):
        id = request.data['mrn']
        insuranceType = request.data['insuranceType']
        result = self.refreshEligibility (insuranceType, id)
        return JsonResponse (result)

    def refreshEligibility (self, insuranceType, mrn=0, ssn = 0):
        data = self.retrieveData (mrn, insuranceType)

        # need to branch here, depending on configured source for E

        if (1 < data['eligibilitySourceCode']):
            provider = eligibilityApiProvider.Provider()
        else:
            provider = pokitdokProvider.Provider ()

        result = provider.checkEligibility (data)

        if not result:
            result = {
                "message": "Unable to complete the request"
            }
        else:
            self.saveResults (data, result)

        return  result


    def retrieveData (self, id, insuranceType):
        query = self.buildDemDataQuery (id, insuranceType)

        dbt1 = DbQueryThread (query)
        dbt1.start ()
        dbt1.join ()

        if not dbt1.resultObject:
            raise ValueError ("Unable to retrieve patient data.")

        usePatientProv = False
        textualType = self.getTextualInsuranceType (insuranceType)

        patientMrn = dbt1.resultObject[0]['mrn']
        patientId = dbt1.resultObject[0]['{0}InsCoPolicyNum'.format (textualType)]
        patientFirst = dbt1.resultObject[0]['{0}InsuredFirstName'.format (textualType)]
        patientLast = dbt1.resultObject[0]['{0}InsuredLastName'.format (textualType)]
        patientDob = dbt1.resultObject[0]['{0}InsuredDob'.format (textualType)]
        eligId = dbt1.resultObject[0]['eligReceiverId']
        provFirst = ''
        provLast = ''
        provNpi = ''

        if None != dbt1.resultObject[0]['provFirst']:
            usePatientProv = True
            provFirst = dbt1.resultObject[0]['provFirst']
            provLast = dbt1.resultObject[0]['provLast']
            provNpi = dbt1.resultObject[0]['NPI']

        if False == usePatientProv:
            provFirst = self.defaultProvider['firstName']
            provLast = self.defaultProvider['lastName']
            provNpi = self.defaultProvider['NPI']

        eligibilitySourceCode = dbt1.resultObject[0]['eligibilityCheck']

        patientData = {
            'mrn': patientMrn,
            'firstName': patientFirst,
            'lastName': patientLast,
            'policyNum': patientId,
            'dob': patientDob
        }

        provData = {
            'firstName': provFirst,
            'lastName': provLast,
            'npi': provNpi
        }

        return {
            'eligibilitySourceCode': eligibilitySourceCode,
            'patient': patientData,
            'provider': provData,
            'trading_partner_id': eligId,
        }

    def buildDemDataQuery (self, id, insuranceType):
        textualType = self.getTextualInsuranceType (insuranceType)

        return """
                    select 
                                p.mrn,
                                p.{0}InsuredFirstName,
                                p.{0}InsuredLastName,
                                p.{0}InsCoPolicyNum,
                                p.{0}InsuredDob,
                                p.mainDoctor, 
                                gp.firstName provFirst, 
                                gp.lastName provLast,
                                gp.NPI,
                                i.eligReceiverId,
                                i.eligibilityCheck,
                                i.company
                    
                    from 
                                demdata p
                                
                    left outer join	
                                insCoTable i
                        on		p.{0}InsCoName = i.company
                    
                                
                    left outer join	
                                providers gp
                        on		gp.code = p.mainDoctor
                    
                    
                    where
                                p.mrn = {1}
                                and i.eligReceiverId != ''
			""".format (textualType, id)

    def getTextualInsuranceType (self, insuranceType):
        textualType = 'primary'

        if 2 == insuranceType:
            textualType = 'secondary'
        elif 3 == insuranceType:
            textualType = 'tertiary'

        return textualType

    def saveResults (self, dbData, resultData):
        # goals:
        # 1 'header'-like record in eligibilityCheckInfo - not much there, really
        # (potentially) second record in the above table, IF there's a PCP node returned
        # multiple rows for the coverages - also in eligibilityCheckInfo:
        #   copayment, coinsurance, limitations, deductibles, outOfPocket

        batchId = self.getBatchId ()

        self.generateHeaderRow (batchId, dbData, resultData)
        self.generateCoverageRows (batchId, dbData, resultData)
        # rec.coverageLevel =
        # insert a rec for each check
        # insert a 2nd rec if result.coverageSummary.pcp == True
        # concat value of 'coverageSummary.messages' if present
        # loop over child rows to be created

    def generateHeaderRow (self, batchId, dbData, resultData):
        rec = Eligibilitycheckinfo ()
        rec.batchid = batchId
        rec.mrn = dbData['patient']['mrn']
        rec.insco = resultData['coverageSummary']['insCo']
        rec.subFirstName = resultData['patientData']['firstName']
        rec.subLastName = resultData['patientData']['lastName']
        rec.subPolicyNum = resultData['patientData']['policyNum']
        rec.eligInfo = ''   # todo: figure out mapping....
        if resultData and 'rejectionReason' in resultData:    # todo: verify
            rec.errorCode = resultData['rejectionReason']
        rec.save ()


    def generateCoverageRows (self, batchId, dbData, resultData):
        if None == resultData:
            return None

        if 'coverageDetails' not in resultData:
            return None

        covRoot = resultData['coverageDetails']
        covNodes = ['coinsurance', 'copay', 'deductibles', 'out_of_pocket', 'limitations']

        for currNode in covNodes:
            if currNode in covRoot:
                for cov in covRoot[currNode]:
                    rec = Eligibilitycheckinfo ()
                    rec.batchid = batchId
                    rec.mrn = dbData['patient']['mrn']
                    rec.insco = resultData['coverageSummary']['insCo']
                    rec.subFirstName = resultData['patientData']['firstName']
                    rec.subLastName = resultData['patientData']['lastName']
                    rec.subPolicyNum = resultData['patientData']['policyNum']
                    rec.eligInfo = cov['coverageCode']
                    rec.eligmessage = cov['messages'][:80]  if len (cov['messages']) > 80 else cov['messages']
                    rec.save ()

    def getBatchId (self):
        return uuid.uuid4()






