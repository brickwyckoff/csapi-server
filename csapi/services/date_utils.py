
import datetime
import math

class DateUtils:
    @staticmethod
    def getUnixNow ():
        return math.floor(datetime.datetime.now().timestamp())
