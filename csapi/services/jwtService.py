
import jwt
from calendar import timegm
from datetime import datetime, timedelta
import math
from django.conf import settings


from csapi.services.date_utils import DateUtils

class JwtService ():

    @staticmethod
    def decodeToken (tokendata, client_ip = None):
        if JwtService.getEnforceExpiration ():
            decoded = jwt.decode(tokendata, JwtService.getEncryptionKey(), algorithm='HS256',
                                 audience=JwtService.getApiAudience(), leeway=10, options = {"verify_exp": True})
        else:
            decoded = jwt.decode(tokendata, JwtService.getEncryptionKey(), algorithm='HS256',
                             audience=JwtService.getApiAudience(), options = {"verify_exp": False})

        if JwtService.getEnforceIpInToken():
            if None == client_ip:
                raise ValueError ('Client IP constistency is enabled but IP address was not supplied to decode method.')
            elif  (not "client_ip" in decoded) or (client_ip != decoded["client_ip"]):
                raise ValueError ('Client IP constistency is enabled. Token supplied was issued to a different IP address than the one currently in use.')

        return decoded

    @staticmethod
    def buildToken (dict, includeTimestamp = True):

        stdProps = {
            "iss": "csapi.demo.com",
            "aud": [JwtService.getApiAudience (), JwtService.getAdminAudience ()],
        }

        if includeTimestamp:
            stdProps.update ({"iat": DateUtils.getUnixNow()})

        if JwtService.getEnforceExpiration ():
            stdProps.update ({"exp": JwtService.getNewExpirationTime ()})

        if (JwtService.getEnforceIpInToken ()):
            if not "client_ip" in dict:
                raise ValueError('Client IP constistency is enabled but no IP address is available.')
            stdProps.update ({"client_ip": dict["client_ip"]})

        xlated = JwtService.translateUserData (dict)
        stdProps.update (xlated)

        return jwt.encode (stdProps, JwtService.getEncryptionKey (), algorithm='HS256')

    @staticmethod
    def encodeToken (data):
        return jwt.encode(data, JwtService.getEncryptionKey (), algorithm='HS256')

    @staticmethod
    def getEncryptionKey ():
        return "supersecretjwtkey-private"

    @staticmethod
    def getApiAudience ():
        return "{0}.csapi".format (JwtService.getUrnBase ())

    @staticmethod
    def getAdminAudience ():
        return "{0}.csadmin".format (JwtService.getUrnBase ())

    @staticmethod
    def translateUserData (userData):
        result = {}
        roles = {}

        if None == userData:
            return result

        if 'id' in userData:
            result.update({"jti": userData['id']})

        if 'username' in userData:
            result.update({"system_name": userData['username']})

        if 'email' in userData:
            result.update({"email": userData['email']})

        if 'accessLevel' in userData:
            role = JwtService.translateAccessLevel (userData['accessLevel'])
            result.update({"roles": [{"role": role}]})

        return result

    @staticmethod
    def translateAccessLevel (accessLevel):
        if 7 < accessLevel:
            return "reception"
        if 7 == accessLevel:
            return "reader"
        if 6 == accessLevel:
            return "ma"
        if 5 == accessLevel:
            return "counselor"
        if 4 == accessLevel:
            return "provider"
        if 3 == accessLevel:
            return "te_provider"
        if 2 == accessLevel:
            return "administrator"

    @staticmethod
    def getUrnBase ():
        return "urn:cleanslate.emr"

    @staticmethod
    def getTokenRefreshPeriod ():
        return JwtService.getTokenExpirationPeriod() - JwtService.getTokenSlidePeriod()

    @staticmethod
    def getTokenExpirationPeriod ():
        result = 30

        try:
            result = settings.JWT_TOKEN_VALIDITY_PERIOD

        except:
            pass

        return result

    @staticmethod
    def getTokenSlidePeriod ():
        result = 5

        try:
            result = settings.JWT_TOKEN_SLIDE_PERIOD
        except:
            pass

        return result

    @staticmethod
    def getNewExpirationTime ():
        result = datetime.utcnow ().timestamp () + (60 * JwtService.getTokenExpirationPeriod ())
        return JwtService.gmAdjustDate (result)
        # return math.floor (result)

    @staticmethod
    def getEnforceExpiration ():
        result = True

        try:
            result = settings.JWT_TOKEN_ENFORCE_EXPIRATION
        except:
            pass

        return result

    @staticmethod
    def getEnforceIpInToken ():
        result = True

        try:
            result = settings.JWT_TOKEN_ENFORCE_IP_CONSISTENCY
        except:
            pass

        return result

    @staticmethod
    def gmAdjustDate (utcDateValue):
        return math.floor (timegm(datetime.fromtimestamp (utcDateValue).utctimetuple ()))

    @staticmethod
    def gmAdjustDate (utcDateValue):
        return math.floor (timegm(datetime.fromtimestamp (utcDateValue).utctimetuple ()))

