
from csapi.login.login_user_model import user

class LoginService ():

    @staticmethod
    def login (username, password):
        try:
            u = user.getByUsername(username)
            if u.checkPassword(password):
                return  u

        except:
            pass

        return None

