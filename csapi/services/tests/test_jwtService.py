
from django.test import TestCase
from ..jwtService import JwtService

class JwtServiceTests (TestCase):
    urnBase = ""
    encryptionKey = ""

    def setUp(self):
        self.urnBase = JwtService.getUrnBase ()
        self.encryptionKey = JwtService.getEncryptionKey ()

    # <editor-fold desc="simple, scalar tests">

    def test_encryptionKeyHasntChanged (self):
        self.assertEqual("supersecretjwtkey-private", self.encryptionKey)

    def test_urnBaseHasntChanged (self):
        self.assertEqual("urn:cleanslate.emr", self.urnBase)

    def test_translateAccessLevel_receptionist(self):
        accessLeveL = 8
        expected = "reception"
        self.assertEquals(JwtService.translateAccessLevel(accessLeveL), expected)

    def test_translateAccessLevel_reader(self):
        accessLeveL = 7
        expected = "reader"
        self.assertEquals(JwtService.translateAccessLevel(accessLeveL), expected)

    def test_translateAccessLevel_ma(self):
        accessLeveL = 6
        expected = "ma"
        self.assertEquals(JwtService.translateAccessLevel(accessLeveL), expected)

    def test_translateAccessLevel_counselor(self):
        accessLeveL = 5
        expected = "counselor"
        self.assertEquals(JwtService.translateAccessLevel(accessLeveL), expected)

    def test_translateAccessLevel_provider(self):
        accessLeveL = 4
        expected = "provider"
        self.assertEquals(JwtService.translateAccessLevel(accessLeveL), expected)

    def test_translateAccessLevel_te_provider(self):
        accessLeveL = 3
        expected = "te_provider"
        self.assertEquals(JwtService.translateAccessLevel(accessLeveL), expected)

    def test_translateAccessLevel_administrator(self):
        accessLeveL = 2
        expected = "administrator"
        self.assertEquals(JwtService.translateAccessLevel(accessLeveL), expected)

    #</editor-fold>

    # <editor-fold desc="more complex tests">

    def test_translateUserDataTWyckoff (self):
        expected = {
          "jti": 624,
          "system_name": "twyckoff",
          "email": "twyckoff@bricksimple.com",
          "roles": [
              {"role": "reception"}
            ]
        }

        localDict = self.buildUserTWyckoff ()
        testValue = JwtService.translateUserData (localDict)
        self.assertEqual (testValue, expected)

    def test_buildToken (self):
        expected = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJjc2FwaS5kZW1vLmNvbSIsImF1ZCI6WyJ1cm46Y2xlYW5zbGF0ZS5lbXIuY3NhcGkiLCJ1cm46Y2xlYW5zbGF0ZS5lbXIuY3NhZG1pbiJdLCJqdGkiOjYyNCwic3lzdGVtX25hbWUiOiJ0d3lja29mZiIsImVtYWlsIjoidHd5Y2tvZmZAYnJpY2tzaW1wbGUuY29tIiwicm9sZXMiOlt7InJvbGUiOiJyZWNlcHRpb24ifV19.s6yCvCB_BjSQdCgq8VOy3qXuIp-PidwLjZAtqF9zwiU"

        localDict = self.buildUserTWyckoff()

        # need to strip the issue time value or this test will always fail
        if 'iss' in localDict:
            del localDict['iss']

        testValue = JwtService.buildToken(localDict, False)
        self.assertEqual(testValue.decode("utf-8"), expected)

    #</editor-fold>

    # <editor-fold desc="internal helper methods">

    def buildUserTWyckoff (self):
        return {
            "id": 624,
            "username": "twyckoff",
            "email": "twyckoff@bricksimple.com",
            "accessLevel": 8
        }

    #</editor-fold>
