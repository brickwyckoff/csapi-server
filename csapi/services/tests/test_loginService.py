
from django.test import TestCase
from ..loginService import LoginService

class LoginServiceTests (TestCase):

    def setUp(self):
        pass


    def test_noneReturnedIfUserNotFound (self):
        un = "thisuserdefinitelydoesntexist"
        pw = "thisisbynomeansmypassword"
        u = LoginService.login (un, pw)

        self.assertEqual(u, None)

    def test_noneReturnedIfBadPassword (self):
        un = "twyckoff"
        pw = "thisisbynomeansmypassword"
        u = LoginService.login (un, pw)

        self.assertEqual(u, None)

