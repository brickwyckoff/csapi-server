
from django.test import TestCase
import datetime
import math

from csapi.services.date_utils import DateUtils

class DateUtilsTests (TestCase):

    def setUp(self):
        pass

    def test_getUnixNow (self):
        #note that our function currently rounds down to the nearest second

        testPasses = False

        expected = math.floor(datetime.datetime.now().timestamp())
        value = DateUtils.getUnixNow ()

        # we have to tolerate a slight change between the time we set our date and the code being invoked
        newer = value >= expected
        diff = value - expected

        if diff >= 0:
            if diff < 2:
                testPasses = True

        self.assertTrue (testPasses)