"""
WSGI config for csapi project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.10/howto/deployment/wsgi/
"""

import os
from django.core.wsgi import get_wsgi_application
from csapi.core.cacheAccess import CacheAccess
from csapi.dbAccess.dbQueryThread import DbQueryThread
from csapi.services.eligibilityService import EligibilityService
import time

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "csapi.settings.local")

# do some initial caching - super gobals
threadList = []

print ("wsgi.py: Initializing cache...")

# retrieve some essential config data:
thread_DBName = DbQueryThread ("SELECT * FROM emrcommon.clientDatabaseNames where active = 1 and 'CleanSlate Centers' = clientName;")
threadList.append (thread_DBName)

# retrieve connection info for external API's
thread_ApiCreds = DbQueryThread ("SELECT * FROM emrcommon.resourceAddresses;")
threadList.append (thread_ApiCreds)

eThreads =  EligibilityService ().getStartupQueryThreads ()

if None != eThreads:
    for t in eThreads : threadList.append (t['ThreadInstance'])

for t in threadList : t.start ()

while any(t.is_alive() for t in threadList): time.sleep(.25)

# cache these results
CacheAccess ().upsertGlobalWithKey ('DbConfig', thread_DBName.resultObject)
CacheAccess ().upsertGlobalWithKey ('ApiCreds', thread_ApiCreds.resultObject)

for et in eThreads :
    for t in threadList:
        if et['ThreadInstance'] is t:
            CacheAccess ().upsertScopedWithKey (et['scope'], et['key'], t.resultObject)

application = get_wsgi_application()
