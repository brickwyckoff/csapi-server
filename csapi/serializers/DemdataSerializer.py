from rest_framework import serializers

from csapi.models.demdata import Demdata


class DemdataSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Demdata
        fields = ('url', 'ptfirstname', 'ptlastname', 'ptemail', 'ptaddress1', 'ptaddress2', 'ptcity', 'ptstate', 'ptzip')