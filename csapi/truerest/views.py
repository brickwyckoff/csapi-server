from rest_framework import viewsets

from csapi.models.demdata import Demdata
from csapi.serializers.DemdataSerializer import DemdataSerializer
from django.views.decorators.http import require_GET

# from django.views.decorators.http import require_GET
# from django.views.decorators.csrf import csrf_exempt

@require_GET
class DemdataViewSet(viewsets.ModelViewSet):
    queryset = Demdata.objects.all().order_by('mrn')
    serializer_class = DemdataSerializer

