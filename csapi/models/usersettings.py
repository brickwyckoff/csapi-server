from __future__ import unicode_literals

from django.db import models

class Usersettings(models.Model):
    id = models.BigAutoField(primary_key=True)
    username = models.CharField(max_length=30, blank=True, null=True)
    practicemanager = models.IntegerField(db_column='practiceManager')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'userSettings'


