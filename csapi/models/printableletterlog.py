from __future__ import unicode_literals

from django.db import models

class Printableletterlog(models.Model):
    id = models.BigAutoField(primary_key=True)
    mrn = models.BigIntegerField()
    user = models.CharField(max_length=35)
    printtime = models.DateTimeField(db_column='printTime')  # Field name made lowercase.
    letterid = models.BigIntegerField(db_column='letterId')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'printableLetterLog'


