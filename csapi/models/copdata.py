from __future__ import unicode_literals

from django.db import models

class Copdata(models.Model):
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    copay = models.FloatField(blank=True, null=True)
    paymenttype = models.IntegerField(db_column='paymentType', blank=True, null=True)  # Field name made lowercase.
    pharmacycopay = models.FloatField(db_column='pharmacyCopay', blank=True, null=True)  # Field name made lowercase.
    pharmacycopayconf = models.CharField(db_column='pharmacyCopayConf', max_length=20, blank=True, null=True)  # Field name made lowercase.
    extrapayment = models.FloatField(db_column='extraPayment', blank=True, null=True)  # Field name made lowercase.
    requiredamount = models.FloatField(db_column='requiredAmount', blank=True, null=True)  # Field name made lowercase.
    selfpay = models.FloatField(db_column='selfPay', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'copData'


