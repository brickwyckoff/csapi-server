from __future__ import unicode_literals

from django.db import models

class Signatureexp(models.Model):
    id = models.BigAutoField(primary_key=True)
    sigtype = models.CharField(db_column='sigType', max_length=20)  # Field name made lowercase.
    pttype = models.CharField(db_column='ptType', max_length=10)  # Field name made lowercase.
    visittype = models.CharField(db_column='visitType', max_length=10)  # Field name made lowercase.
    isnewpt = models.IntegerField(db_column='isNewPt')  # Field name made lowercase.
    sigtext = models.CharField(db_column='sigText', max_length=500)  # Field name made lowercase.
    isonsite = models.IntegerField(db_column='isOnSite', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'signatureExp'


