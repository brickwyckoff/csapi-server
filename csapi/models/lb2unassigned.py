from __future__ import unicode_literals

from django.db import models

class Lb2Unassigned(models.Model):
    fillerid = models.CharField(db_column='fillerId', max_length=22, blank=True, null=True)  # Field name made lowercase.
    messagecontrolid = models.CharField(db_column='messageControlId', max_length=22, blank=True, null=True)  # Field name made lowercase.
    firstname = models.CharField(db_column='firstName', max_length=50, blank=True, null=True)  # Field name made lowercase.
    lastname = models.CharField(db_column='lastName', max_length=50, blank=True, null=True)  # Field name made lowercase.
    dob = models.DateField(blank=True, null=True)
    isassigned = models.IntegerField(db_column='isAssigned', blank=True, null=True)  # Field name made lowercase.
    creationtime = models.DateTimeField(db_column='creationTime')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'lb2Unassigned'


