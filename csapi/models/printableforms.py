from __future__ import unicode_literals

from django.db import models

class Printableforms(models.Model):
    isactive = models.IntegerField(db_column='isActive')  # Field name made lowercase.
    formclass = models.IntegerField(db_column='formClass')  # Field name made lowercase.
    haschildren = models.IntegerField(db_column='hasChildren')  # Field name made lowercase.
    parentid = models.BigIntegerField(db_column='parentId')  # Field name made lowercase.
    pageorder = models.IntegerField(db_column='pageOrder')  # Field name made lowercase.
    formtype = models.CharField(db_column='formType', max_length=35, blank=True, null=True)  # Field name made lowercase.
    formname = models.CharField(db_column='formName', max_length=50)  # Field name made lowercase.
    notificationemails = models.CharField(db_column='notificationEmails', max_length=200, blank=True, null=True)  # Field name made lowercase.
    recipientname = models.CharField(db_column='recipientName', max_length=50)  # Field name made lowercase.
    recipientnamexy = models.CharField(db_column='recipientNameXY', max_length=10, blank=True, null=True)  # Field name made lowercase.
    recipientfax = models.CharField(db_column='recipientFax', max_length=11)  # Field name made lowercase.
    recipientfaxxy = models.CharField(db_column='recipientFaxXY', max_length=10, blank=True, null=True)  # Field name made lowercase.
    formduration = models.IntegerField(db_column='formDuration')  # Field name made lowercase.
    formfunction = models.CharField(db_column='formFunction', max_length=35, blank=True, null=True)  # Field name made lowercase.
    backgroundform = models.CharField(db_column='backgroundForm', max_length=35, blank=True, null=True)  # Field name made lowercase.
    formwidth = models.FloatField(db_column='formWidth', blank=True, null=True)  # Field name made lowercase.
    formheight = models.FloatField(db_column='formHeight', blank=True, null=True)  # Field name made lowercase.
    font = models.CharField(max_length=10, blank=True, null=True)
    fontsize = models.IntegerField(db_column='fontSize', blank=True, null=True)  # Field name made lowercase.
    fontcolor = models.CharField(db_column='fontColor', max_length=11, blank=True, null=True)  # Field name made lowercase.
    providernamexy = models.CharField(db_column='providerNameXY', max_length=10, blank=True, null=True)  # Field name made lowercase.
    providerfirstnamexy = models.CharField(db_column='providerFirstNameXY', max_length=10)  # Field name made lowercase.
    providerlastnamexy = models.CharField(db_column='providerLastNameXY', max_length=10)  # Field name made lowercase.
    providernpixy = models.CharField(db_column='providerNPIXY', max_length=10, blank=True, null=True)  # Field name made lowercase.
    providercontactxy = models.CharField(db_column='providerContactXY', max_length=10, blank=True, null=True)  # Field name made lowercase.
    providerphonexy = models.CharField(db_column='providerPhoneXY', max_length=50, blank=True, null=True)  # Field name made lowercase.
    providerphoneareaxy = models.CharField(db_column='providerPhoneAreaXY', max_length=10)  # Field name made lowercase.
    providerphonecityxy = models.CharField(db_column='providerPhoneCityXY', max_length=10)  # Field name made lowercase.
    providerphonenumxy = models.CharField(db_column='providerPhoneNumXY', max_length=10)  # Field name made lowercase.
    providerfaxxy = models.CharField(db_column='providerFaxXY', max_length=50, blank=True, null=True)  # Field name made lowercase.
    providerfax = models.CharField(db_column='providerFax', max_length=10)  # Field name made lowercase.
    providerfaxareaxy = models.CharField(db_column='providerFaxAreaXY', max_length=10)  # Field name made lowercase.
    providerfaxcityxy = models.CharField(db_column='providerFaxCityXY', max_length=10)  # Field name made lowercase.
    providerfaxnumxy = models.CharField(db_column='providerFaxNumXY', max_length=10)  # Field name made lowercase.
    providersignaturexy = models.CharField(db_column='providerSignatureXY', max_length=10, blank=True, null=True)  # Field name made lowercase.
    provideraddressxy = models.CharField(db_column='providerAddressXY', max_length=10, blank=True, null=True)  # Field name made lowercase.
    providercityxy = models.CharField(db_column='providerCityXY', max_length=10, blank=True, null=True)  # Field name made lowercase.
    providerstatexy = models.CharField(db_column='providerStateXY', max_length=10, blank=True, null=True)  # Field name made lowercase.
    providerzipxy = models.CharField(db_column='providerZipXY', max_length=10, blank=True, null=True)  # Field name made lowercase.
    ptnamexy = models.CharField(db_column='ptNameXY', max_length=10, blank=True, null=True)  # Field name made lowercase.
    ptfirstnamexy = models.CharField(db_column='ptFirstNameXY', max_length=10)  # Field name made lowercase.
    ptlastnamexy = models.CharField(db_column='ptLastNameXY', max_length=10)  # Field name made lowercase.
    ptdobxy = models.CharField(db_column='ptDobXY', max_length=10, blank=True, null=True)  # Field name made lowercase.
    ptgenderxy = models.CharField(db_column='ptGenderXY', max_length=10)  # Field name made lowercase.
    malebox = models.CharField(db_column='maleBox', max_length=20)  # Field name made lowercase.
    femalebox = models.CharField(db_column='femaleBox', max_length=20)  # Field name made lowercase.
    malecheckxy = models.CharField(db_column='maleCheckXY', max_length=10)  # Field name made lowercase.
    femalecheckxy = models.CharField(db_column='femaleCheckXY', max_length=10)  # Field name made lowercase.
    ptpolicynumxy = models.CharField(db_column='ptPolicyNumXY', max_length=10, blank=True, null=True)  # Field name made lowercase.
    priminsurednamexy = models.CharField(db_column='primInsuredNameXY', max_length=10)  # Field name made lowercase.
    priminsuredphonexy = models.CharField(db_column='primInsuredPhoneXY', max_length=10)  # Field name made lowercase.
    ptinscoxy = models.CharField(db_column='ptInsCoXY', max_length=10)  # Field name made lowercase.
    drugxy = models.CharField(db_column='drugXY', max_length=10, blank=True, null=True)  # Field name made lowercase.
    sigxy = models.CharField(db_column='sigXY', max_length=10, blank=True, null=True)  # Field name made lowercase.
    dxxy = models.CharField(db_column='dxXY', max_length=10, blank=True, null=True)  # Field name made lowercase.
    dxtextxy = models.CharField(db_column='dxTextXY', max_length=10)  # Field name made lowercase.
    dx = models.CharField(max_length=10, blank=True, null=True)
    durationxy = models.CharField(db_column='durationXY', max_length=10, blank=True, null=True)  # Field name made lowercase.
    duration = models.CharField(max_length=35, blank=True, null=True)
    printdatexy = models.CharField(db_column='printDateXY', max_length=50, blank=True, null=True)  # Field name made lowercase.
    defaultchecked = models.CharField(db_column='defaultChecked', max_length=150, blank=True, null=True)  # Field name made lowercase.
    newtxcheckxy = models.CharField(db_column='newTxCheckXY', max_length=10, blank=True, null=True)  # Field name made lowercase.
    conttxcheckxy = models.CharField(db_column='contTxCheckXY', max_length=10, blank=True, null=True)  # Field name made lowercase.
    txstartdatexy = models.CharField(db_column='txStartDateXY', max_length=10, blank=True, null=True)  # Field name made lowercase.
    utoxycheckxy = models.CharField(db_column='utoxYCheckXY', max_length=10, blank=True, null=True)  # Field name made lowercase.
    utoxncheckxy = models.CharField(db_column='utoxNCheckXY', max_length=10, blank=True, null=True)  # Field name made lowercase.
    utoxdatexy = models.CharField(db_column='utoxDateXY', max_length=10, blank=True, null=True)  # Field name made lowercase.
    defaultboxes = models.CharField(db_column='defaultBoxes', max_length=200)  # Field name made lowercase.
    specialtyxy = models.CharField(db_column='specialtyXY', max_length=10)  # Field name made lowercase.
    cptxy = models.CharField(db_column='cptXY', max_length=10)  # Field name made lowercase.
    cpt = models.CharField(max_length=10)
    senderxy = models.CharField(db_column='senderXY', max_length=10)  # Field name made lowercase.
    sendercellxy = models.CharField(db_column='sendercellXY', max_length=10)  # Field name made lowercase.
    senderfaxxy = models.CharField(db_column='senderFaxXY', max_length=10)  # Field name made lowercase.
    secondproviderxy = models.CharField(db_column='secondProviderXY', max_length=10)  # Field name made lowercase.
    secondprovidernpixy = models.CharField(db_column='secondProviderNPIXY', max_length=10)  # Field name made lowercase.
    secondproviderphonexy = models.CharField(db_column='secondProviderPhoneXY', max_length=10)  # Field name made lowercase.
    clinicnamexy = models.CharField(db_column='clinicNameXY', max_length=10)  # Field name made lowercase.
    clinicnpixy = models.CharField(db_column='clinicNPIXY', max_length=10)  # Field name made lowercase.
    visitnumreq = models.CharField(db_column='visitNumReq', max_length=10)  # Field name made lowercase.
    visitnumreqxy = models.CharField(db_column='visitNumReqXY', max_length=10)  # Field name made lowercase.
    fulladdressxy = models.CharField(db_column='fullAddressXY', max_length=10)  # Field name made lowercase.
    providerlocalityxy = models.CharField(db_column='providerLocalityXY', max_length=10)  # Field name made lowercase.
    deaxy = models.CharField(db_column='deaXY', max_length=10)  # Field name made lowercase.
    licensexy = models.CharField(db_column='licenseXY', max_length=10)  # Field name made lowercase.
    clinictaxidxy = models.CharField(db_column='clinicTaxIdXY', max_length=10)  # Field name made lowercase.
    ptphonexy = models.CharField(db_column='ptPhoneXY', max_length=10)  # Field name made lowercase.
    ptadd1xy = models.CharField(db_column='ptAdd1XY', max_length=10)  # Field name made lowercase.
    ptadd2xy = models.CharField(db_column='ptAdd2XY', max_length=10)  # Field name made lowercase.
    ptcityxy = models.CharField(db_column='ptCityXY', max_length=10)  # Field name made lowercase.
    ptstatexy = models.CharField(db_column='ptStateXY', max_length=10)  # Field name made lowercase.
    ptzipxy = models.CharField(db_column='ptZipXY', max_length=10)  # Field name made lowercase.
    ptfulladdressxy = models.CharField(db_column='ptFullAddressXY', max_length=10)  # Field name made lowercase.
    ptlocalityxy = models.CharField(db_column='ptLocalityXY', max_length=10)  # Field name made lowercase.
    qtyxy = models.CharField(db_column='qtyXY', max_length=10)  # Field name made lowercase.
    refillsxy = models.CharField(db_column='refillsXY', max_length=10)  # Field name made lowercase.
    visitnumsxy = models.CharField(db_column='visitNumsXY', max_length=10)  # Field name made lowercase.
    groupnamexy = models.CharField(db_column='groupNameXY', max_length=10)  # Field name made lowercase.
    pharmacyxy = models.CharField(db_column='pharmacyXY', max_length=10)  # Field name made lowercase.
    pharmacyphonexy = models.CharField(db_column='pharmacyPhoneXY', max_length=10)  # Field name made lowercase.
    visitnumxy = models.CharField(db_column='visitNumXY', max_length=10)  # Field name made lowercase.
    barcodexy = models.CharField(db_column='barCodeXY', max_length=10)  # Field name made lowercase.
    barcodeend = models.CharField(db_column='barCodeEnd', max_length=3)  # Field name made lowercase.
    txenddatexy = models.CharField(db_column='txEndDateXY', max_length=10, blank=True, null=True)  # Field name made lowercase.
    mrnxy = models.CharField(db_column='mrnXY', max_length=10, blank=True, null=True)  # Field name made lowercase.
    checkboxxy = models.CharField(db_column='checkboxXY', max_length=100, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'printableForms'


