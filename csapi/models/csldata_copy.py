from __future__ import unicode_literals

from django.db import models

class CsldataCopy(models.Model):
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    mildimprovement = models.IntegerField(db_column='mildImprovement', blank=True, null=True)  # Field name made lowercase.
    remainsstable = models.IntegerField(db_column='remainsStable', blank=True, null=True)  # Field name made lowercase.
    newproblem = models.IntegerField(db_column='newProblem', blank=True, null=True)  # Field name made lowercase.
    meetinggoals = models.IntegerField(db_column='meetingGoals', blank=True, null=True)  # Field name made lowercase.
    modimprovement = models.IntegerField(db_column='modImprovement', blank=True, null=True)  # Field name made lowercase.
    worsening = models.IntegerField(blank=True, null=True)
    completedtreatment = models.IntegerField(db_column='completedTreatment', blank=True, null=True)  # Field name made lowercase.
    setbacktreatment = models.IntegerField(db_column='setbackTreatment', blank=True, null=True)  # Field name made lowercase.
    majorimprovement = models.IntegerField(db_column='majorImprovement', blank=True, null=True)  # Field name made lowercase.
    noprogress = models.IntegerField(db_column='noProgress', blank=True, null=True)  # Field name made lowercase.
    overallnotes = models.CharField(db_column='overallNotes', max_length=500, blank=True, null=True)  # Field name made lowercase.
    orientedperson = models.IntegerField(db_column='orientedPerson', blank=True, null=True)  # Field name made lowercase.
    orientedplace = models.IntegerField(db_column='orientedPlace', blank=True, null=True)  # Field name made lowercase.
    orientedtime = models.IntegerField(db_column='orientedTime', blank=True, null=True)  # Field name made lowercase.
    suiciderisk = models.IntegerField(db_column='suicideRisk', blank=True, null=True)  # Field name made lowercase.
    appappropriate = models.IntegerField(db_column='appAppropriate', blank=True, null=True)  # Field name made lowercase.
    appdisheveled = models.IntegerField(db_column='appDisheveled', blank=True, null=True)  # Field name made lowercase.
    appprovocative = models.IntegerField(db_column='appProvocative', blank=True, null=True)  # Field name made lowercase.
    apptearful = models.IntegerField(db_column='appTearful', blank=True, null=True)  # Field name made lowercase.
    apptired = models.IntegerField(db_column='appTired', blank=True, null=True)  # Field name made lowercase.
    appunkempt = models.IntegerField(db_column='appUnkempt', blank=True, null=True)  # Field name made lowercase.
    appjaundice = models.IntegerField(db_column='appJaundice', blank=True, null=True)  # Field name made lowercase.
    appaches = models.IntegerField(db_column='appAches', blank=True, null=True)  # Field name made lowercase.
    apphealthy = models.IntegerField(db_column='appHealthy', blank=True, null=True)  # Field name made lowercase.
    attcooperative = models.IntegerField(db_column='attCooperative', blank=True, null=True)  # Field name made lowercase.
    attpositive = models.IntegerField(db_column='attPositive', blank=True, null=True)  # Field name made lowercase.
    attdemanding = models.IntegerField(db_column='attDemanding', blank=True, null=True)  # Field name made lowercase.
    attdisinterested = models.IntegerField(db_column='attDisinterested', blank=True, null=True)  # Field name made lowercase.
    attegocentric = models.IntegerField(db_column='attEgocentric', blank=True, null=True)  # Field name made lowercase.
    attevasive = models.IntegerField(db_column='attEvasive', blank=True, null=True)  # Field name made lowercase.
    attfrank = models.IntegerField(db_column='attFrank', blank=True, null=True)  # Field name made lowercase.
    attsuspicious = models.IntegerField(db_column='attSuspicious', blank=True, null=True)  # Field name made lowercase.
    attvain = models.IntegerField(db_column='attVain', blank=True, null=True)  # Field name made lowercase.
    attsensitive = models.IntegerField(db_column='attSensitive', blank=True, null=True)  # Field name made lowercase.
    attconfident = models.IntegerField(db_column='attConfident', blank=True, null=True)  # Field name made lowercase.
    attirritable = models.IntegerField(db_column='attIrritable', blank=True, null=True)  # Field name made lowercase.
    behaggressive = models.IntegerField(db_column='behAggressive', blank=True, null=True)  # Field name made lowercase.
    behcheerful = models.IntegerField(db_column='behCheerful', blank=True, null=True)  # Field name made lowercase.
    behhelpless = models.IntegerField(db_column='behHelpless', blank=True, null=True)  # Field name made lowercase.
    behempowered = models.IntegerField(db_column='behEmpowered', blank=True, null=True)  # Field name made lowercase.
    behnegativistic = models.IntegerField(db_column='behNegativistic', blank=True, null=True)  # Field name made lowercase.
    behreassurance = models.IntegerField(db_column='behReassurance', blank=True, null=True)  # Field name made lowercase.
    behsecretive = models.IntegerField(db_column='behSecretive', blank=True, null=True)  # Field name made lowercase.
    behflat = models.IntegerField(db_column='behFlat', blank=True, null=True)  # Field name made lowercase.
    behwithdrawn = models.IntegerField(db_column='behWithdrawn', blank=True, null=True)  # Field name made lowercase.
    behanxious = models.IntegerField(db_column='behAnxious', blank=True, null=True)  # Field name made lowercase.
    behdepressed = models.IntegerField(db_column='behDepressed', blank=True, null=True)  # Field name made lowercase.
    behguarded = models.IntegerField(db_column='behGuarded', blank=True, null=True)  # Field name made lowercase.
    behindecisive = models.IntegerField(db_column='behIndecisive', blank=True, null=True)  # Field name made lowercase.
    behdramatic = models.IntegerField(db_column='behDramatic', blank=True, null=True)  # Field name made lowercase.
    behcalm = models.IntegerField(db_column='behCalm', blank=True, null=True)  # Field name made lowercase.
    behdistracted = models.IntegerField(db_column='behDistracted', blank=True, null=True)  # Field name made lowercase.
    behelated = models.IntegerField(db_column='behElated', blank=True, null=True)  # Field name made lowercase.
    behhyperactive = models.IntegerField(db_column='behHyperactive', blank=True, null=True)  # Field name made lowercase.
    behhyperalert = models.IntegerField(db_column='behHyperalert', blank=True, null=True)  # Field name made lowercase.
    behlethargic = models.IntegerField(db_column='behLethargic', blank=True, null=True)  # Field name made lowercase.
    behrestless = models.IntegerField(db_column='behRestless', blank=True, null=True)  # Field name made lowercase.
    behagitated = models.IntegerField(db_column='behAgitated', blank=True, null=True)  # Field name made lowercase.
    behcraving = models.IntegerField(db_column='behCraving', blank=True, null=True)  # Field name made lowercase.
    assessmentnotes = models.CharField(db_column='assessmentNotes', max_length=700, blank=True, null=True)  # Field name made lowercase.
    progactivewithdrawal = models.IntegerField(db_column='progActiveWithdrawal', blank=True, null=True)  # Field name made lowercase.
    progfinancialissues = models.IntegerField(db_column='progFinancialIssues', blank=True, null=True)  # Field name made lowercase.
    progpillcount = models.IntegerField(db_column='progPillCount', blank=True, null=True)  # Field name made lowercase.
    progdiscabstinence = models.IntegerField(db_column='progDiscAbstinence', blank=True, null=True)  # Field name made lowercase.
    progdiscfamily = models.IntegerField(db_column='progDiscFamily', blank=True, null=True)  # Field name made lowercase.
    progdiscdiseasemodel = models.IntegerField(db_column='progDiscDiseaseModel', blank=True, null=True)  # Field name made lowercase.
    prognotactivewithdrawal = models.IntegerField(db_column='progNotActiveWithdrawal', blank=True, null=True)  # Field name made lowercase.
    proglegalissues = models.IntegerField(db_column='progLegalIssues', blank=True, null=True)  # Field name made lowercase.
    progattendingaa = models.IntegerField(db_column='progAttendingAA', blank=True, null=True)  # Field name made lowercase.
    progdiscltabstinence = models.IntegerField(db_column='progDiscLTAbstinence', blank=True, null=True)  # Field name made lowercase.
    progdiscfriends = models.IntegerField(db_column='progDiscFriends', blank=True, null=True)  # Field name made lowercase.
    progcompliant = models.IntegerField(db_column='progCompliant', blank=True, null=True)  # Field name made lowercase.
    proghousingissues = models.IntegerField(db_column='progHousingIssues', blank=True, null=True)  # Field name made lowercase.
    progsupportgroup = models.IntegerField(db_column='progSupportGroup', blank=True, null=True)  # Field name made lowercase.
    progdisctriggers = models.IntegerField(db_column='progDiscTriggers', blank=True, null=True)  # Field name made lowercase.
    progdiscwork = models.IntegerField(db_column='progDiscWork', blank=True, null=True)  # Field name made lowercase.
    progrelapsebehavior = models.IntegerField(db_column='progRelapseBehavior', blank=True, null=True)  # Field name made lowercase.
    progincreaseservices = models.IntegerField(db_column='progIncreaseServices', blank=True, null=True)  # Field name made lowercase.
    progindtreatment = models.IntegerField(db_column='progIndTreatment', blank=True, null=True)  # Field name made lowercase.
    progdisccoping = models.IntegerField(db_column='progDiscCoping', blank=True, null=True)  # Field name made lowercase.
    progdiscclinic = models.IntegerField(db_column='progDiscClinic', blank=True, null=True)  # Field name made lowercase.
    progcleanurine = models.IntegerField(db_column='progCleanUrine', blank=True, null=True)  # Field name made lowercase.
    progdirtyurine = models.IntegerField(db_column='progDirtyUrine', blank=True, null=True)  # Field name made lowercase.
    progdiscltbenefits = models.IntegerField(db_column='progDiscLTBenefits', blank=True, null=True)  # Field name made lowercase.
    progidentifiedsupports = models.IntegerField(db_column='progIdentifiedSupports', blank=True, null=True)  # Field name made lowercase.
    progdiscemotion = models.IntegerField(db_column='progDiscEmotion', blank=True, null=True)  # Field name made lowercase.
    progreleasesigned = models.IntegerField(db_column='progReleaseSigned', blank=True, null=True)  # Field name made lowercase.
    prognotes = models.CharField(db_column='progNotes', max_length=700, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'cslData_copy'


