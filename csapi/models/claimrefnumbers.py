from __future__ import unicode_literals

from django.db import models

class Claimrefnumbers(models.Model):
    payerid = models.CharField(db_column='payerId', max_length=15, blank=True, null=True)  # Field name made lowercase.
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    refnumber = models.CharField(db_column='refNumber', max_length=30)  # Field name made lowercase.
    creationtime = models.DateTimeField(db_column='creationTime')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'claimRefNumbers'


