from __future__ import unicode_literals

from django.db import models

class Labchargelog(models.Model):
    id = models.BigAutoField(primary_key=True)
    messagecontrolid = models.CharField(db_column='messageControlId', max_length=35)  # Field name made lowercase.
    cptcode = models.CharField(db_column='cptCode', max_length=10)  # Field name made lowercase.
    creationtime = models.DateTimeField(db_column='creationTime')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'labChargeLog'


