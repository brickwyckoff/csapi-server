from __future__ import unicode_literals

from django.db import models

class Customformslinks(models.Model):
    fromform = models.CharField(db_column='fromForm', max_length=35, blank=True, null=True)  # Field name made lowercase.
    fromfield = models.CharField(db_column='fromField', max_length=35, blank=True, null=True)  # Field name made lowercase.
    toform = models.CharField(db_column='toForm', max_length=35, blank=True, null=True)  # Field name made lowercase.
    tofield = models.CharField(db_column='toField', max_length=35, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'customFormsLinks'


