from __future__ import unicode_literals

from django.db import models

class Transcriptionpost(models.Model):
    id = models.BigAutoField(primary_key=True)
    transcription = models.TextField()
    visitdate = models.DateTimeField(db_column='visitDate')  # Field name made lowercase.
    dictationdate = models.DateTimeField(db_column='dictationDate')  # Field name made lowercase.
    transcriptiondate = models.DateTimeField(db_column='transcriptionDate')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'transcriptionPost'


