from __future__ import unicode_literals

from django.db import models

class Prescriptiongenlog(models.Model):
    rxlogid = models.BigIntegerField(db_column='rxLogId', blank=True, null=True)  # Field name made lowercase.
    mrn = models.BigIntegerField(blank=True, null=True)
    prescription = models.CharField(max_length=50, blank=True, null=True)
    freq = models.CharField(max_length=10, blank=True, null=True)
    route = models.CharField(max_length=10, blank=True, null=True)
    dayssupply = models.IntegerField(db_column='daysSupply', blank=True, null=True)  # Field name made lowercase.
    qty = models.IntegerField(blank=True, null=True)
    refills = models.IntegerField(blank=True, null=True)
    generationdate = models.DateTimeField(db_column='generationDate', blank=True, null=True)  # Field name made lowercase.
    printdate = models.DateTimeField(db_column='printDate', blank=True, null=True)  # Field name made lowercase.
    faxdate = models.DateTimeField(db_column='faxDate', blank=True, null=True)  # Field name made lowercase.
    callindate = models.DateTimeField(db_column='callInDate', blank=True, null=True)  # Field name made lowercase.
    faxto = models.CharField(db_column='faxTo', max_length=50, blank=True, null=True)  # Field name made lowercase.
    faxtonumber = models.BigIntegerField(db_column='faxToNumber', blank=True, null=True)  # Field name made lowercase.
    faxstatus = models.IntegerField(db_column='faxStatus', blank=True, null=True)  # Field name made lowercase.
    prescriber = models.CharField(max_length=5, blank=True, null=True)
    visitloc = models.IntegerField(db_column='visitLoc', blank=True, null=True)  # Field name made lowercase.
    user = models.CharField(max_length=35, blank=True, null=True)
    rximage = models.CharField(db_column='rxImage', max_length=100, blank=True, null=True)  # Field name made lowercase.
    filldate = models.DateTimeField(db_column='fillDate', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'prescriptionGenLog'


