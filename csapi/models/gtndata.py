from __future__ import unicode_literals

from django.db import models

class Gtndata(models.Model):
    id = models.BigAutoField(primary_key=True)
    visitnum = models.BigIntegerField(db_column='visitNum')  # Field name made lowercase.
    startingtime = models.CharField(db_column='startingTime', max_length=10)  # Field name made lowercase.
    active = models.IntegerField()
    endingtime = models.CharField(db_column='endingTime', max_length=10)  # Field name made lowercase.
    numberpresent = models.CharField(db_column='numberPresent', max_length=20)  # Field name made lowercase.
    variable = models.CharField(max_length=1)
    onlyresponsive = models.CharField(db_column='onlyResponsive', max_length=1)  # Field name made lowercase.
    minimal = models.CharField(max_length=1)
    withdrawn = models.CharField(max_length=1)
    expected = models.CharField(max_length=1)
    supportive = models.CharField(max_length=1)
    sharing = models.CharField(max_length=1)
    attentive = models.CharField(max_length=1)
    intrusive = models.CharField(max_length=1)
    monopolizing = models.CharField(max_length=1)
    resistant = models.CharField(max_length=1)
    otherquality = models.TextField(db_column='otherQuality')  # Field name made lowercase.
    normaleuth = models.CharField(db_column='normalEuth', max_length=1)  # Field name made lowercase.
    anxious = models.CharField(max_length=1)
    depressed = models.CharField(max_length=1)
    angry = models.CharField(max_length=1)
    euphoric = models.CharField(max_length=1)
    othermood = models.TextField(db_column='otherMood')  # Field name made lowercase.
    normalaffect = models.CharField(db_column='normalAffect', max_length=1)  # Field name made lowercase.
    intense = models.CharField(max_length=1)
    blunted = models.CharField(max_length=1)
    inappropriate = models.CharField(max_length=1)
    labile = models.CharField(max_length=1)
    otheraffect = models.TextField(db_column='otherAffect')  # Field name made lowercase.
    normalmental = models.CharField(db_column='normalMental', max_length=1)  # Field name made lowercase.
    aware = models.CharField(max_length=1)
    memory = models.CharField(max_length=1)
    dis = models.CharField(max_length=1)
    disorganized = models.CharField(max_length=1)
    confused = models.CharField(max_length=1)
    vigilant = models.CharField(max_length=1)
    delusional = models.CharField(max_length=1)
    halluc = models.CharField(max_length=1)
    othermental = models.TextField(db_column='otherMental')  # Field name made lowercase.
    deny = models.CharField(max_length=1)
    cannabis = models.CharField(max_length=1)
    cocaine = models.CharField(max_length=1)
    opioids = models.CharField(max_length=1)
    amph = models.CharField(max_length=1)
    benzo = models.CharField(max_length=1)
    etoh = models.CharField(max_length=1)
    otheruse = models.TextField(db_column='otherUse')  # Field name made lowercase.
    support = models.CharField(max_length=1)
    clear = models.CharField(max_length=1)
    edu = models.CharField(max_length=1)
    explore = models.CharField(max_length=1)
    solving = models.CharField(max_length=1)
    limit = models.CharField(max_length=1)
    roleplay = models.CharField(db_column='rolePlay', max_length=1)  # Field name made lowercase.
    motive = models.CharField(max_length=1)
    relapse = models.CharField(max_length=1)
    twelve = models.CharField(max_length=1)
    cog = models.CharField(max_length=1)
    relation = models.CharField(max_length=1)
    work = models.CharField(max_length=1)
    issues = models.CharField(max_length=1)
    parent = models.CharField(max_length=1)
    abuse = models.CharField(max_length=1)
    origin = models.CharField(max_length=1)
    sexual = models.CharField(max_length=1)
    otherthemes = models.TextField(db_column='otherThemes')  # Field name made lowercase.
    closing = models.TextField()

    class Meta:
        managed = False
        db_table = 'gtnData'


