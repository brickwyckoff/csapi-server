from __future__ import unicode_literals

from django.db import models

class Scriptstorelease(models.Model):
    id = models.BigAutoField(primary_key=True)
    mrn = models.BigIntegerField(blank=True, null=True)
    prescriptionlogid = models.BigIntegerField(db_column='prescriptionLogId', blank=True, null=True)  # Field name made lowercase.
    clinicloc = models.BigIntegerField(db_column='clinicLoc', blank=True, null=True)  # Field name made lowercase.
    ncpdpid = models.CharField(max_length=7, blank=True, null=True)
    donotfilldate = models.DateField(db_column='doNotFillDate', blank=True, null=True)  # Field name made lowercase.
    isreleased = models.IntegerField(db_column='isReleased')  # Field name made lowercase.
    releasetime = models.DateTimeField(db_column='releaseTime', blank=True, null=True)  # Field name made lowercase.
    releaseuser = models.CharField(db_column='releaseUser', max_length=30, blank=True, null=True)  # Field name made lowercase.
    activetime = models.DateTimeField(db_column='activeTime', blank=True, null=True)  # Field name made lowercase.
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'scriptsToRelease'


