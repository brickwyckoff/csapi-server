from __future__ import unicode_literals

from django.db import models

class Printableformsclasses(models.Model):
    id = models.BigAutoField(primary_key=True)
    classname = models.CharField(db_column='className', max_length=50)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'printableFormsClasses'


