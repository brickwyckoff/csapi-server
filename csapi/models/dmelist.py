from __future__ import unicode_literals

from django.db import models

class Dmelist(models.Model):
    dmename = models.CharField(db_column='dmeName', max_length=200, blank=True, null=True)  # Field name made lowercase.
    cptcode = models.CharField(db_column='cptCode', max_length=11, blank=True, null=True)  # Field name made lowercase.
    qtyinstock = models.IntegerField(db_column='qtyInStock', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'dmeList'


