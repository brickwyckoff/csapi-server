from __future__ import unicode_literals

from django.db import models

class Comdata(models.Model):
    mrn = models.BigIntegerField(blank=True, null=True)
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    chiefcomplaint = models.CharField(db_column='chiefComplaint', max_length=200, blank=True, null=True)  # Field name made lowercase.
    selectedtemplate = models.CharField(db_column='selectedTemplate', max_length=60, blank=True, null=True)  # Field name made lowercase.
    templateid = models.IntegerField(db_column='templateId', blank=True, null=True)  # Field name made lowercase.
    quicknotes = models.CharField(db_column='quickNotes', max_length=250, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'comData'


