from __future__ import unicode_literals

from django.db import models

class Textmsglog(models.Model):
    id = models.BigAutoField(primary_key=True)
    comnumber = models.CharField(db_column='comNumber', max_length=15)  # Field name made lowercase.
    mrn = models.BigIntegerField()
    textmessage = models.CharField(db_column='textMessage', max_length=200)  # Field name made lowercase.
    receivetime = models.DateTimeField(db_column='receiveTime')  # Field name made lowercase.
    isread = models.IntegerField(db_column='isRead')  # Field name made lowercase.
    tofrom = models.IntegerField(db_column='toFrom')  # Field name made lowercase.
    user = models.CharField(max_length=35)

    class Meta:
        managed = False
        db_table = 'textMsgLog'


