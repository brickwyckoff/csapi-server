from __future__ import unicode_literals

from django.db import models

class Accountingtempold(models.Model):
    id = models.BigAutoField(primary_key=True)
    visitnum = models.BigIntegerField(db_column='visitNum')  # Field name made lowercase.
    sessionid = models.CharField(db_column='sessionId', max_length=100)  # Field name made lowercase.
    postdate = models.DateField(db_column='postDate')  # Field name made lowercase.
    proccode = models.CharField(db_column='procCode', max_length=10)  # Field name made lowercase.
    insco = models.CharField(db_column='insCo', max_length=60)  # Field name made lowercase.
    charge = models.CharField(max_length=60)
    payment = models.CharField(max_length=60)
    credit = models.CharField(max_length=60)
    deductible = models.CharField(max_length=60)
    adjustment = models.CharField(max_length=60)
    coins = models.CharField(max_length=60)
    copay = models.CharField(max_length=60)
    pending = models.CharField(max_length=60)
    denial = models.CharField(max_length=60)

    class Meta:
        managed = False
        db_table = 'accountingTempOld'


