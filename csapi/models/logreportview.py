from __future__ import unicode_literals

from django.db import models

class Logreportview(models.Model):
    id = models.BigAutoField(primary_key=True)
    username = models.CharField(max_length=50, blank=True, null=True)
    report = models.CharField(max_length=100, blank=True, null=True)
    viewtime = models.DateTimeField(db_column='viewTime')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'logReportView'


