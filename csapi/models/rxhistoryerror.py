from __future__ import unicode_literals

from django.db import models

class Rxhistoryerror(models.Model):
    mrn = models.BigIntegerField(blank=True, null=True)
    errorcode = models.CharField(db_column='errorCode', max_length=10, blank=True, null=True)  # Field name made lowercase.
    descriptioncode = models.CharField(db_column='descriptionCode', max_length=10, blank=True, null=True)  # Field name made lowercase.
    descriptiontext = models.CharField(db_column='descriptionText', max_length=100, blank=True, null=True)  # Field name made lowercase.
    creationtime = models.DateTimeField(db_column='creationTime', blank=True, null=True)  # Field name made lowercase.
    payorname = models.CharField(db_column='payorName', max_length=100, blank=True, null=True)  # Field name made lowercase.
    memberid = models.CharField(db_column='memberId', max_length=100, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'rxHistoryError'


