from __future__ import unicode_literals

from django.db import models

class Eligibilityqueries(models.Model):
    user = models.CharField(max_length=60, blank=True, null=True)
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    checktime = models.DateTimeField(db_column='checkTime', blank=True, null=True)  # Field name made lowercase.
    insco = models.CharField(db_column='insCo', max_length=60, blank=True, null=True)  # Field name made lowercase.
    mrn = models.BigIntegerField()

    class Meta:
        managed = False
        db_table = 'eligibilityQueries'


