from __future__ import unicode_literals

from django.db import models

class Patientslink(models.Model):
    mrn = models.BigIntegerField(blank=True, null=True)
    mrnext = models.CharField(db_column='mrnExt', max_length=35, blank=True, null=True)  # Field name made lowercase.
    linkname = models.CharField(db_column='linkName', max_length=30, blank=True, null=True)  # Field name made lowercase.
    lastquerytime = models.DateTimeField(db_column='lastQueryTime', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'patientsLink'


