from __future__ import unicode_literals

from django.db import models

class Templates(models.Model):
    templatename = models.CharField(db_column='templateName', max_length=50, blank=True, null=True)  # Field name made lowercase.
    field = models.CharField(max_length=70, blank=True, null=True)
    showtext = models.CharField(db_column='showText', max_length=200, blank=True, null=True)  # Field name made lowercase.
    printvalue = models.CharField(db_column='printValue', max_length=600, blank=True, null=True)  # Field name made lowercase.
    type = models.CharField(max_length=15, blank=True, null=True)
    sectionheader = models.CharField(db_column='sectionHeader', max_length=100, blank=True, null=True)  # Field name made lowercase.
    subheader = models.CharField(db_column='subHeader', max_length=100, blank=True, null=True)  # Field name made lowercase.
    radioshow = models.IntegerField(db_column='radioShow', blank=True, null=True)  # Field name made lowercase.
    itemname = models.CharField(db_column='itemName', max_length=20, blank=True, null=True)  # Field name made lowercase.
    itemid = models.CharField(db_column='itemId', max_length=30, blank=True, null=True)  # Field name made lowercase.
    sectionordinal = models.IntegerField(db_column='sectionOrdinal', blank=True, null=True)  # Field name made lowercase.
    listordinal = models.IntegerField(db_column='listOrdinal', blank=True, null=True)  # Field name made lowercase.
    hpisection = models.CharField(db_column='hpiSection', max_length=30, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'templates'


