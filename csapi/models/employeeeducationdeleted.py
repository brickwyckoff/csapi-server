from __future__ import unicode_literals

from django.db import models

class Employeeeducationdeleted(models.Model):
    edtitle = models.CharField(db_column='edTitle', max_length=200, blank=True, null=True)  # Field name made lowercase.
    edmessage = models.TextField(db_column='edMessage', blank=True, null=True)  # Field name made lowercase.
    edvideo = models.CharField(db_column='edVideo', max_length=300, blank=True, null=True)  # Field name made lowercase.
    edfile = models.CharField(db_column='edFile', max_length=150, blank=True, null=True)  # Field name made lowercase.
    edactivedate = models.DateField(db_column='edActiveDate', blank=True, null=True)  # Field name made lowercase.
    edminviews = models.IntegerField(db_column='edMinViews', blank=True, null=True)  # Field name made lowercase.
    edactiveread = models.IntegerField(db_column='edActiveRead', blank=True, null=True)  # Field name made lowercase.
    edactiveuntilremoved = models.IntegerField(db_column='edActiveUntilRemoved', blank=True, null=True)  # Field name made lowercase.
    enteredby = models.CharField(db_column='enteredBy', max_length=35, blank=True, null=True)  # Field name made lowercase.
    enteredtime = models.DateTimeField(db_column='enteredTime', blank=True, null=True)  # Field name made lowercase.
    assignedusers = models.TextField(db_column='assignedUsers', blank=True, null=True)  # Field name made lowercase.
    assignedusertypes = models.TextField(db_column='assignedUserTypes', blank=True, null=True)  # Field name made lowercase.
    isactive = models.IntegerField(db_column='isActive', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'employeeEducationDeleted'


