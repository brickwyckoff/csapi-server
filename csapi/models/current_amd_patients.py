from __future__ import unicode_literals

from django.db import models
from .current_amd_visits import CurrentAmdVisits
class CurrentAmdPatients(models.Model):
    # Patient block
    mrn = models.BigIntegerField(db_column='mrn')  # Field name made lowercase.
    patient_name = models.CharField(db_column='patientName', max_length=100)
    ssn = models.CharField(db_column='ssn', max_length=10, null=True, blank=True)
    create_date = models.DateTimeField(db_column='createDate')
    change_date = models.DateTimeField(db_column='changeDate', null=True, blank=True)

    visit_num = models.ForeignKey(CurrentAmdVisits, to_field='visit_num')

    class Meta:
        db_table = 'currentAmdPatients'