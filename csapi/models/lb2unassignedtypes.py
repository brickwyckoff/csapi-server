from __future__ import unicode_literals

from django.db import models

class Lb2Unassignedtypes(models.Model):
    isassigned = models.IntegerField(db_column='isAssigned', blank=True, null=True)  # Field name made lowercase.
    description = models.CharField(max_length=30, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'lb2UnassignedTypes'


