from __future__ import unicode_literals

from django.db import models

class Loginfailure(models.Model):
    username = models.CharField(max_length=50)
    time = models.DateTimeField()
    ip = models.CharField(max_length=30)

    class Meta:
        managed = False
        db_table = 'loginFailure'


