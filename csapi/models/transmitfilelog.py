from __future__ import unicode_literals

from django.db import models

class Transmitfilelog(models.Model):
    fileid = models.IntegerField(db_column='fileId', blank=True, null=True)  # Field name made lowercase.
    transmitto = models.CharField(db_column='transmitTo', max_length=35, blank=True, null=True)  # Field name made lowercase.
    transmittime = models.DateTimeField(db_column='transmitTime', blank=True, null=True)  # Field name made lowercase.
    transmitby = models.CharField(db_column='transmitBy', max_length=35, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'transmitFileLog'


