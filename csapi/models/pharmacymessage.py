from __future__ import unicode_literals

from django.db import models

class Pharmacymessage(models.Model):
    controlnumber = models.CharField(db_column='controlNumber', max_length=19, blank=True, null=True)  # Field name made lowercase.
    message = models.CharField(max_length=264, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pharmacyMessage'


