from __future__ import unicode_literals

from django.db import models

class Diadata(models.Model):
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    diff1 = models.CharField(max_length=150, blank=True, null=True)
    diff2 = models.CharField(max_length=150, blank=True, null=True)
    diff3 = models.CharField(max_length=150, blank=True, null=True)
    diff4 = models.CharField(max_length=150, blank=True, null=True)
    final1 = models.CharField(max_length=150, blank=True, null=True)
    final2 = models.CharField(max_length=150, blank=True, null=True)
    final3 = models.CharField(max_length=150, blank=True, null=True)
    final4 = models.CharField(max_length=150, blank=True, null=True)
    icd1 = models.CharField(max_length=15, blank=True, null=True)
    icd2 = models.CharField(max_length=15, blank=True, null=True)
    icd3 = models.CharField(max_length=15, blank=True, null=True)
    icd4 = models.CharField(max_length=15, blank=True, null=True)
    final5 = models.CharField(max_length=150, blank=True, null=True)
    icd5 = models.CharField(max_length=15, blank=True, null=True)
    final6 = models.CharField(max_length=150, blank=True, null=True)
    icd6 = models.CharField(max_length=15, blank=True, null=True)
    final7 = models.CharField(max_length=150, blank=True, null=True)
    icd7 = models.CharField(max_length=15, blank=True, null=True)
    final8 = models.CharField(max_length=150, blank=True, null=True)
    icd8 = models.CharField(max_length=15, blank=True, null=True)
    snomed1 = models.CharField(max_length=15, blank=True, null=True)
    snomed2 = models.CharField(max_length=15, blank=True, null=True)
    snomed3 = models.CharField(max_length=15, blank=True, null=True)
    snomed4 = models.CharField(max_length=15, blank=True, null=True)
    snomed5 = models.CharField(max_length=15, blank=True, null=True)
    snomed6 = models.CharField(max_length=15, blank=True, null=True)
    snomed7 = models.CharField(max_length=15, blank=True, null=True)
    snomed8 = models.CharField(max_length=15, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'diaData'


