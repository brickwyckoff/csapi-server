from __future__ import unicode_literals

from django.db import models

class Ccdfilesunlinked(models.Model):
    fileid = models.CharField(db_column='fileId', max_length=200, blank=True, null=True)  # Field name made lowercase.
    firstname = models.CharField(db_column='firstName', max_length=35, blank=True, null=True)  # Field name made lowercase.
    lastname = models.CharField(db_column='lastName', max_length=35, blank=True, null=True)  # Field name made lowercase.
    dob = models.DateField(blank=True, null=True)
    gender = models.CharField(max_length=1, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ccdFilesUnlinked'


