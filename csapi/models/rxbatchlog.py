from __future__ import unicode_literals

from django.db import models

class Rxbatchlog(models.Model):
    id = models.BigAutoField(primary_key=True)
    prescriptionlogid = models.BigIntegerField(db_column='prescriptionLogId', blank=True, null=True)  # Field name made lowercase.
    batchtype = models.CharField(db_column='batchType', max_length=20, blank=True, null=True)  # Field name made lowercase.
    batchid = models.BigIntegerField(db_column='batchId', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'rxBatchLog'


