from __future__ import unicode_literals

from django.db import models

class Tabdisplayorder(models.Model):
    tabid = models.BigIntegerField(db_column='tabId', blank=True, null=True)  # Field name made lowercase.
    taborder = models.IntegerField(db_column='tabOrder', blank=True, null=True)  # Field name made lowercase.
    accesslevel = models.IntegerField(db_column='accessLevel', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'tabDisplayOrder'


