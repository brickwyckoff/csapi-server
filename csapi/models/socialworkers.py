from __future__ import unicode_literals

from django.db import models

class Socialworkers(models.Model):
    firstname = models.CharField(db_column='firstName', max_length=20, blank=True, null=True)  # Field name made lowercase.
    lastname = models.CharField(db_column='lastName', max_length=40, blank=True, null=True)  # Field name made lowercase.
    title = models.CharField(max_length=15, blank=True, null=True)
    code = models.CharField(max_length=20, blank=True, null=True)
    issup = models.IntegerField(db_column='isSup', blank=True, null=True)  # Field name made lowercase.
    signature = models.CharField(max_length=30, blank=True, null=True)
    gender = models.CharField(max_length=1, blank=True, null=True)
    npi = models.CharField(max_length=15)
    taxonomycode = models.CharField(db_column='taxonomyCode', max_length=15, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'socialWorkers'


