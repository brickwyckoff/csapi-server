from __future__ import unicode_literals

from django.db import models

class Rxhistorytemp(models.Model):
    mrn = models.BigIntegerField(blank=True, null=True)
    productname = models.CharField(db_column='productName', max_length=35, blank=True, null=True)  # Field name made lowercase.
    productid = models.CharField(db_column='productId', max_length=35, blank=True, null=True)  # Field name made lowercase.
    productidqual = models.CharField(db_column='productIdQual', max_length=3, blank=True, null=True)  # Field name made lowercase.
    dosage = models.CharField(max_length=3, blank=True, null=True)
    strength = models.CharField(max_length=70, blank=True, null=True)
    strengthunits = models.CharField(db_column='strengthUnits', max_length=3, blank=True, null=True)  # Field name made lowercase.
    productdbcode = models.CharField(db_column='productDbCode', max_length=35, blank=True, null=True)  # Field name made lowercase.
    productdbcodequal = models.CharField(db_column='productDbCodeQual', max_length=3, blank=True, null=True)  # Field name made lowercase.
    qtyqual = models.CharField(db_column='qtyQual', max_length=3, blank=True, null=True)  # Field name made lowercase.
    qtycodelistqual = models.CharField(db_column='qtyCodeListQual', max_length=10, blank=True, null=True)  # Field name made lowercase.
    qty = models.CharField(max_length=20, blank=True, null=True)
    directions = models.CharField(max_length=70, blank=True, null=True)
    daysupply = models.CharField(db_column='daySupply', max_length=10, blank=True, null=True)  # Field name made lowercase.
    lastfilldate = models.DateField(db_column='lastFillDate', blank=True, null=True)  # Field name made lowercase.
    writtendate = models.DateField(db_column='writtenDate', blank=True, null=True)  # Field name made lowercase.
    expirationdate = models.DateField(db_column='expirationDate', blank=True, null=True)  # Field name made lowercase.
    effectivedate = models.DateField(db_column='effectiveDate', blank=True, null=True)  # Field name made lowercase.
    enddate = models.DateField(db_column='endDate', blank=True, null=True)  # Field name made lowercase.
    substitution = models.CharField(max_length=3, blank=True, null=True)
    refillqual = models.CharField(db_column='refillQual', max_length=3, blank=True, null=True)  # Field name made lowercase.
    refillqty = models.CharField(db_column='refillQty', max_length=35, blank=True, null=True)  # Field name made lowercase.
    diagqual = models.CharField(db_column='diagQual', max_length=3, blank=True, null=True)  # Field name made lowercase.
    diag = models.CharField(max_length=17, blank=True, null=True)
    note = models.CharField(max_length=70, blank=True, null=True)
    pharmacyid = models.CharField(db_column='pharmacyId', max_length=35, blank=True, null=True)  # Field name made lowercase.
    prescriberdea = models.CharField(db_column='prescriberDea', max_length=10, blank=True, null=True)  # Field name made lowercase.
    prescribernpi = models.CharField(db_column='prescriberNpi', max_length=10, blank=True, null=True)  # Field name made lowercase.
    prescriberclinic = models.CharField(db_column='prescriberClinic', max_length=35, blank=True, null=True)  # Field name made lowercase.
    prescriberfirstname = models.CharField(db_column='prescriberFirstName', max_length=35, blank=True, null=True)  # Field name made lowercase.
    prescriberlastname = models.CharField(db_column='prescriberLastName', max_length=35, blank=True, null=True)  # Field name made lowercase.
    prescriberaddress1 = models.CharField(db_column='prescriberAddress1', max_length=35, blank=True, null=True)  # Field name made lowercase.
    prescriberaddress2 = models.CharField(db_column='prescriberAddress2', max_length=35, blank=True, null=True)  # Field name made lowercase.
    prescribercity = models.CharField(db_column='prescriberCity', max_length=35, blank=True, null=True)  # Field name made lowercase.
    prescriberstate = models.CharField(db_column='prescriberState', max_length=9, blank=True, null=True)  # Field name made lowercase.
    prescriberzip = models.CharField(db_column='prescriberZip', max_length=11, blank=True, null=True)  # Field name made lowercase.
    prescriberphone = models.CharField(db_column='prescriberPhone', max_length=80, blank=True, null=True)  # Field name made lowercase.
    prescriberphonequal = models.CharField(db_column='prescriberPhoneQual', max_length=3, blank=True, null=True)  # Field name made lowercase.
    creationtime = models.DateTimeField(db_column='creationTime', blank=True, null=True)  # Field name made lowercase.
    payorname = models.CharField(db_column='payorName', max_length=100, blank=True, null=True)  # Field name made lowercase.
    memberid = models.CharField(db_column='memberId', max_length=100, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'rxHistoryTemp'


