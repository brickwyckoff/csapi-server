from __future__ import unicode_literals

from django.db import models

class Providerstateinfochangelog(models.Model):
    id = models.BigAutoField(primary_key=True)
    code = models.CharField(max_length=20, blank=True, null=True)
    providerstateinfoid = models.BigIntegerField(db_column='providerStateInfoId', blank=True, null=True)  # Field name made lowercase.
    field = models.CharField(max_length=20, blank=True, null=True)
    oldvalue = models.CharField(db_column='oldValue', max_length=250, blank=True, null=True)  # Field name made lowercase.
    newvalue = models.CharField(db_column='newValue', max_length=250, blank=True, null=True)  # Field name made lowercase.
    user = models.CharField(max_length=30, blank=True, null=True)
    changetime = models.DateTimeField(db_column='changeTime')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'providerStateInfoChangeLog'


