from __future__ import unicode_literals

from django.db import models

class Mvadata(models.Model):
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    mva = models.IntegerField(blank=True, null=True)
    wc = models.IntegerField(blank=True, null=True)
    employer = models.CharField(max_length=100, blank=True, null=True)
    employerphone = models.BigIntegerField(db_column='employerPhone', blank=True, null=True)  # Field name made lowercase.
    wcinsurance = models.CharField(db_column='wcInsurance', max_length=100, blank=True, null=True)  # Field name made lowercase.
    wcinsurancephone = models.BigIntegerField(db_column='wcInsurancePhone', blank=True, null=True)  # Field name made lowercase.
    wcinsuranceaddress = models.CharField(db_column='wcInsuranceAddress', max_length=60, blank=True, null=True)  # Field name made lowercase.
    wcinsurancecity = models.CharField(db_column='wcInsuranceCity', max_length=60, blank=True, null=True)  # Field name made lowercase.
    wcinsurancestate = models.CharField(db_column='wcInsuranceState', max_length=2, blank=True, null=True)  # Field name made lowercase.
    wcinsurancezip = models.CharField(db_column='wcInsuranceZip', max_length=10, blank=True, null=True)  # Field name made lowercase.
    wcclaimnumber = models.CharField(db_column='wcClaimNumber', max_length=50, blank=True, null=True)  # Field name made lowercase.
    wcadjusterfirstname = models.CharField(db_column='wcAdjusterFirstName', max_length=50, blank=True, null=True)  # Field name made lowercase.
    wcadjusterlastname = models.CharField(db_column='wcAdjusterLastName', max_length=60, blank=True, null=True)  # Field name made lowercase.
    wcadjusterphone = models.BigIntegerField(db_column='wcAdjusterPhone', blank=True, null=True)  # Field name made lowercase.
    mvainsurance = models.CharField(db_column='mvaInsurance', max_length=100, blank=True, null=True)  # Field name made lowercase.
    mvainsurancephone = models.BigIntegerField(db_column='mvaInsurancePhone', blank=True, null=True)  # Field name made lowercase.
    mvainsuranceaddress = models.CharField(db_column='mvaInsuranceAddress', max_length=60, blank=True, null=True)  # Field name made lowercase.
    mvainsurancecity = models.CharField(db_column='mvaInsuranceCity', max_length=60, blank=True, null=True)  # Field name made lowercase.
    mvainsurancestate = models.CharField(db_column='mvaInsuranceState', max_length=2, blank=True, null=True)  # Field name made lowercase.
    mvainsurancezip = models.CharField(db_column='mvaInsuranceZip', max_length=10, blank=True, null=True)  # Field name made lowercase.
    mvaclaimnumber = models.CharField(db_column='mvaClaimNumber', max_length=50, blank=True, null=True)  # Field name made lowercase.
    mvacarownerfirstname = models.CharField(db_column='mvaCarOwnerFirstName', max_length=50, blank=True, null=True)  # Field name made lowercase.
    mvacarownerlastname = models.CharField(db_column='mvaCarOwnerLastName', max_length=60, blank=True, null=True)  # Field name made lowercase.
    mvastate = models.CharField(db_column='mvaState', max_length=2, blank=True, null=True)  # Field name made lowercase.
    mvawcnotes = models.CharField(db_column='mvaWcNotes', max_length=500, blank=True, null=True)  # Field name made lowercase.
    currentuser = models.CharField(db_column='currentUser', max_length=30, blank=True, null=True)  # Field name made lowercase.
    doi = models.CharField(max_length=30, blank=True, null=True)
    mrn = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'mvaData'


