from __future__ import unicode_literals

from django.db import models

class Cptdata(models.Model):
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    code1 = models.CharField(max_length=6, blank=True, null=True)
    qty1 = models.IntegerField(blank=True, null=True)
    code2 = models.CharField(max_length=6, blank=True, null=True)
    qty2 = models.IntegerField(blank=True, null=True)
    code3 = models.CharField(max_length=6, blank=True, null=True)
    qty3 = models.IntegerField(blank=True, null=True)
    code4 = models.CharField(max_length=6, blank=True, null=True)
    qty4 = models.IntegerField(blank=True, null=True)
    code5 = models.CharField(max_length=6, blank=True, null=True)
    qty5 = models.IntegerField(blank=True, null=True)
    code6 = models.CharField(max_length=6, blank=True, null=True)
    qty6 = models.IntegerField(blank=True, null=True)
    code7 = models.CharField(max_length=6, blank=True, null=True)
    qty7 = models.IntegerField(blank=True, null=True)
    code8 = models.CharField(max_length=6, blank=True, null=True)
    qty8 = models.IntegerField(blank=True, null=True)
    code9 = models.CharField(max_length=6, blank=True, null=True)
    qty9 = models.IntegerField(blank=True, null=True)
    code10 = models.CharField(max_length=6, blank=True, null=True)
    qty10 = models.CharField(max_length=2, blank=True, null=True)
    timespent = models.IntegerField(db_column='timeSpent', blank=True, null=True)  # Field name made lowercase.
    facetofacedoc = models.IntegerField(db_column='faceToFaceDoc', blank=True, null=True)  # Field name made lowercase.
    counseling1 = models.CharField(max_length=6, blank=True, null=True)
    mod1a = models.CharField(max_length=3, blank=True, null=True)
    mod2a = models.CharField(max_length=3, blank=True, null=True)
    mod3a = models.CharField(max_length=3, blank=True, null=True)
    mod4a = models.CharField(max_length=3, blank=True, null=True)
    mod5a = models.CharField(max_length=3, blank=True, null=True)
    mod6a = models.CharField(max_length=3, blank=True, null=True)
    mod7a = models.CharField(max_length=3, blank=True, null=True)
    mod8a = models.CharField(max_length=3, blank=True, null=True)
    mod9a = models.CharField(max_length=3, blank=True, null=True)
    mod10a = models.CharField(max_length=3, blank=True, null=True)
    mod1b = models.CharField(max_length=3, blank=True, null=True)
    mod2b = models.CharField(max_length=3, blank=True, null=True)
    mod3b = models.CharField(max_length=3, blank=True, null=True)
    mod4b = models.CharField(max_length=3, blank=True, null=True)
    mod5b = models.CharField(max_length=3, blank=True, null=True)
    mod6b = models.CharField(max_length=3, blank=True, null=True)
    mod7b = models.CharField(max_length=3, blank=True, null=True)
    mod8b = models.CharField(max_length=3, blank=True, null=True)
    mod9b = models.CharField(max_length=3, blank=True, null=True)
    mod10b = models.CharField(max_length=3, blank=True, null=True)
    link1 = models.CharField(max_length=8, blank=True, null=True)
    link2 = models.CharField(max_length=8, blank=True, null=True)
    link3 = models.CharField(max_length=8, blank=True, null=True)
    link4 = models.CharField(max_length=8, blank=True, null=True)
    link5 = models.CharField(max_length=8, blank=True, null=True)
    link6 = models.CharField(max_length=8, blank=True, null=True)
    link7 = models.CharField(max_length=8, blank=True, null=True)
    link8 = models.CharField(max_length=8, blank=True, null=True)
    link9 = models.CharField(max_length=8, blank=True, null=True)
    link10 = models.CharField(max_length=8, blank=True, null=True)
    incidentto = models.IntegerField(db_column='incidentTo', blank=True, null=True)  # Field name made lowercase.
    referralused = models.IntegerField(db_column='referralUsed')  # Field name made lowercase.
    ndc1 = models.CharField(max_length=15, blank=True, null=True)
    ndcnote1 = models.CharField(db_column='ndcNote1', max_length=200, blank=True, null=True)  # Field name made lowercase.
    ndc2 = models.CharField(max_length=15, blank=True, null=True)
    ndcnote2 = models.CharField(db_column='ndcNote2', max_length=200, blank=True, null=True)  # Field name made lowercase.
    ndc3 = models.CharField(max_length=15, blank=True, null=True)
    ndcnote3 = models.CharField(db_column='ndcNote3', max_length=200, blank=True, null=True)  # Field name made lowercase.
    ndc4 = models.CharField(max_length=15, blank=True, null=True)
    ndcnote4 = models.CharField(db_column='ndcNote4', max_length=200, blank=True, null=True)  # Field name made lowercase.
    ndc5 = models.CharField(max_length=15, blank=True, null=True)
    ndcnote5 = models.CharField(db_column='ndcNote5', max_length=200, blank=True, null=True)  # Field name made lowercase.
    ndc6 = models.CharField(max_length=15, blank=True, null=True)
    ndcnote6 = models.CharField(db_column='ndcNote6', max_length=200, blank=True, null=True)  # Field name made lowercase.
    ndc7 = models.CharField(max_length=15, blank=True, null=True)
    ndcnote7 = models.CharField(db_column='ndcNote7', max_length=200, blank=True, null=True)  # Field name made lowercase.
    ndc8 = models.CharField(max_length=15, blank=True, null=True)
    ndcnote8 = models.CharField(db_column='ndcNote8', max_length=200, blank=True, null=True)  # Field name made lowercase.
    ndc9 = models.CharField(max_length=15, blank=True, null=True)
    ndcnote9 = models.CharField(db_column='ndcNote9', max_length=200, blank=True, null=True)  # Field name made lowercase.
    ndc10 = models.CharField(max_length=15, blank=True, null=True)
    ndcnote10 = models.CharField(db_column='ndcNote10', max_length=200, blank=True, null=True)  # Field name made lowercase.
    fee1 = models.FloatField(blank=True, null=True)
    fee2 = models.FloatField(blank=True, null=True)
    fee3 = models.FloatField(blank=True, null=True)
    fee4 = models.FloatField(blank=True, null=True)
    fee5 = models.FloatField(blank=True, null=True)
    fee6 = models.FloatField(blank=True, null=True)
    fee7 = models.FloatField(blank=True, null=True)
    fee8 = models.FloatField(blank=True, null=True)
    fee9 = models.FloatField(blank=True, null=True)
    fee10 = models.FloatField(blank=True, null=True)
    hospitaladmitdate = models.DateField(db_column='hospitalAdmitDate', blank=True, null=True)  # Field name made lowercase.
    hospitaldischargedate = models.DateField(db_column='hospitalDischargeDate', blank=True, null=True)  # Field name made lowercase.
    addclaiminfo = models.CharField(db_column='addClaimInfo', max_length=80, blank=True, null=True)  # Field name made lowercase.
    patientstatuscode = models.CharField(db_column='patientStatusCode', max_length=5, blank=True, null=True)  # Field name made lowercase.
    statementstart = models.DateField(db_column='statementStart', blank=True, null=True)  # Field name made lowercase.
    statementend = models.DateField(db_column='statementEnd', blank=True, null=True)  # Field name made lowercase.
    revcode1 = models.CharField(db_column='revCode1', max_length=5, blank=True, null=True)  # Field name made lowercase.
    revcode2 = models.CharField(db_column='revCode2', max_length=5, blank=True, null=True)  # Field name made lowercase.
    revcode3 = models.CharField(db_column='revCode3', max_length=5, blank=True, null=True)  # Field name made lowercase.
    revcode4 = models.CharField(db_column='revCode4', max_length=5, blank=True, null=True)  # Field name made lowercase.
    revcode5 = models.CharField(db_column='revCode5', max_length=5, blank=True, null=True)  # Field name made lowercase.
    revcode6 = models.CharField(db_column='revCode6', max_length=5, blank=True, null=True)  # Field name made lowercase.
    revcode7 = models.CharField(db_column='revCode7', max_length=5, blank=True, null=True)  # Field name made lowercase.
    revcode8 = models.CharField(db_column='revCode8', max_length=5, blank=True, null=True)  # Field name made lowercase.
    revcode9 = models.CharField(db_column='revCode9', max_length=5, blank=True, null=True)  # Field name made lowercase.
    revcode10 = models.CharField(db_column='revCode10', max_length=5, blank=True, null=True)  # Field name made lowercase.
    revcode11 = models.CharField(db_column='revCode11', max_length=5, blank=True, null=True)  # Field name made lowercase.
    revcode12 = models.CharField(db_column='revCode12', max_length=5, blank=True, null=True)  # Field name made lowercase.
    revcode13 = models.CharField(db_column='revCode13', max_length=5, blank=True, null=True)  # Field name made lowercase.
    revcode14 = models.CharField(db_column='revCode14', max_length=5, blank=True, null=True)  # Field name made lowercase.
    revcode15 = models.CharField(db_column='revCode15', max_length=5, blank=True, null=True)  # Field name made lowercase.
    revcode16 = models.CharField(db_column='revCode16', max_length=5, blank=True, null=True)  # Field name made lowercase.
    revcode17 = models.CharField(db_column='revCode17', max_length=5, blank=True, null=True)  # Field name made lowercase.
    revcode18 = models.CharField(db_column='revCode18', max_length=5, blank=True, null=True)  # Field name made lowercase.
    revcode19 = models.CharField(db_column='revCode19', max_length=5, blank=True, null=True)  # Field name made lowercase.
    revcode20 = models.CharField(db_column='revCode20', max_length=5, blank=True, null=True)  # Field name made lowercase.
    dischargestatus = models.CharField(db_column='dischargeStatus', max_length=5, blank=True, null=True)  # Field name made lowercase.
    mod1c = models.CharField(max_length=3, blank=True, null=True)
    mod2c = models.CharField(max_length=3, blank=True, null=True)
    mod3c = models.CharField(max_length=3, blank=True, null=True)
    mod4c = models.CharField(max_length=3, blank=True, null=True)
    mod5c = models.CharField(max_length=3, blank=True, null=True)
    mod6c = models.CharField(max_length=3, blank=True, null=True)
    mod7c = models.CharField(max_length=3, blank=True, null=True)
    mod8c = models.CharField(max_length=3, blank=True, null=True)
    mod9c = models.CharField(max_length=3, blank=True, null=True)
    mod10c = models.CharField(max_length=3, blank=True, null=True)
    mod11c = models.CharField(max_length=3, blank=True, null=True)
    mod12c = models.CharField(max_length=3, blank=True, null=True)
    mod13c = models.CharField(max_length=3, blank=True, null=True)
    mod14c = models.CharField(max_length=3, blank=True, null=True)
    mod15c = models.CharField(max_length=3, blank=True, null=True)
    mod16c = models.CharField(max_length=3, blank=True, null=True)
    mod17c = models.CharField(max_length=3, blank=True, null=True)
    mod18c = models.CharField(max_length=3, blank=True, null=True)
    mod19c = models.CharField(max_length=3, blank=True, null=True)
    mod20c = models.CharField(max_length=3, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cptData'


