from __future__ import unicode_literals

from django.db import models

class Rvwdata(models.Model):
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    medhx = models.IntegerField(db_column='medHx', blank=True, null=True)  # Field name made lowercase.
    socialhx = models.IntegerField(db_column='socialHx', blank=True, null=True)  # Field name made lowercase.
    familyhx = models.IntegerField(db_column='familyHx', blank=True, null=True)  # Field name made lowercase.
    lastvisit = models.IntegerField(db_column='lastVisit', blank=True, null=True)  # Field name made lowercase.
    vitals = models.IntegerField(blank=True, null=True)
    ptreminders = models.IntegerField(db_column='ptReminders', blank=True, null=True)  # Field name made lowercase.
    utox = models.IntegerField(blank=True, null=True)
    drugscreenord = models.IntegerField(db_column='drugScreenOrd', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'rvwData'


