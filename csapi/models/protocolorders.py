from __future__ import unicode_literals

from django.db import models

class Protocolorders(models.Model):
    pttype = models.CharField(db_column='ptType', max_length=6, blank=True, null=True)  # Field name made lowercase.
    orderid = models.IntegerField(db_column='orderId', blank=True, null=True)  # Field name made lowercase.
    timefreq = models.IntegerField(db_column='timeFreq', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'protocolOrders'


