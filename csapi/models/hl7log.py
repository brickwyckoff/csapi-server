from __future__ import unicode_literals

from django.db import models

class Hl7Log(models.Model):
    sendingapplication = models.CharField(db_column='sendingApplication', max_length=180, blank=True, null=True)  # Field name made lowercase.
    sendingfacility = models.CharField(db_column='sendingFacility', max_length=180, blank=True, null=True)  # Field name made lowercase.
    receivingapplication = models.CharField(db_column='receivingApplication', max_length=180, blank=True, null=True)  # Field name made lowercase.
    receivingfacility = models.CharField(db_column='receivingFacility', max_length=180, blank=True, null=True)  # Field name made lowercase.
    messagetimestamp = models.DateTimeField(db_column='messageTimestamp', blank=True, null=True)  # Field name made lowercase.
    messagetype = models.CharField(db_column='messageType', max_length=7, blank=True, null=True)  # Field name made lowercase.
    messagecontrolid = models.CharField(db_column='messageControlId', max_length=20, blank=True, null=True)  # Field name made lowercase.
    processtimestamp = models.DateTimeField(db_column='processTimeStamp', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'hl7Log'


