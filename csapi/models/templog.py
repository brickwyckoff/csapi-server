from __future__ import unicode_literals

from django.db import models

class Templog(models.Model):
    postdata = models.TextField(db_column='postData', blank=True, null=True)  # Field name made lowercase.
    inserttime = models.DateTimeField(db_column='insertTime', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'tempLog'


