from __future__ import unicode_literals

from django.db import models

class Advancedmdcodes(models.Model):
    codetype = models.CharField(db_column='codeType', max_length=10, blank=True, null=True)  # Field name made lowercase.
    code = models.CharField(max_length=15, blank=True, null=True)
    advancedmdcode = models.CharField(db_column='advancedMdCode', max_length=15, blank=True, null=True)  # Field name made lowercase.
    visitloc = models.IntegerField(db_column='visitLoc')  # Field name made lowercase.
    isdefault = models.IntegerField(db_column='isDefault')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'advancedMdCodes'


