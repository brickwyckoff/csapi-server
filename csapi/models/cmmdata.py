from __future__ import unicode_literals

from django.db import models

class Cmmdata(models.Model):
    mrn = models.BigIntegerField(blank=True, null=True)
    commtype = models.CharField(db_column='commType', max_length=20, blank=True, null=True)  # Field name made lowercase.
    commmessage = models.CharField(db_column='commMessage', max_length=500, blank=True, null=True)  # Field name made lowercase.
    commdate = models.DateTimeField(db_column='commDate')  # Field name made lowercase.
    user = models.CharField(max_length=80, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cmmData'


