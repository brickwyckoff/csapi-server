from __future__ import unicode_literals

from django.db import models

class Printableletters(models.Model):
    id = models.BigAutoField(primary_key=True)
    lettername = models.CharField(db_column='letterName', max_length=80)  # Field name made lowercase.
    lettercontent = models.TextField(db_column='letterContent')  # Field name made lowercase.
    isactive = models.IntegerField(db_column='isActive')  # Field name made lowercase.
    infotoset = models.TextField(db_column='infoToSet', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'printableLetters'


