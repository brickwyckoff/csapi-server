from __future__ import unicode_literals

from django.db import models

class Pharmacyeligibilityerror(models.Model):
    mrn = models.BigIntegerField(blank=True, null=True)
    errorcode = models.CharField(db_column='errorCode', max_length=5, blank=True, null=True)  # Field name made lowercase.
    errortime = models.DateTimeField(db_column='errorTime', blank=True, null=True)  # Field name made lowercase.
    policyname = models.CharField(db_column='policyName', max_length=60, blank=True, null=True)  # Field name made lowercase.
    validtransaction = models.IntegerField(db_column='validTransaction', blank=True, null=True)  # Field name made lowercase.
    followup = models.CharField(db_column='followUp', max_length=1, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'pharmacyEligibilityError'


