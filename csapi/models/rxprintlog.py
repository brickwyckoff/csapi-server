from __future__ import unicode_literals

from django.db import models

class Rxprintlog(models.Model):
    mrn = models.BigIntegerField(blank=True, null=True)
    prescription = models.CharField(max_length=60, blank=True, null=True)
    freq = models.CharField(max_length=15, blank=True, null=True)
    qty = models.CharField(max_length=15, blank=True, null=True)
    route = models.CharField(max_length=15, blank=True, null=True)
    refills = models.CharField(max_length=15, blank=True, null=True)
    prescriber = models.CharField(max_length=10, blank=True, null=True)
    printtime = models.DateTimeField(db_column='printTime', blank=True, null=True)  # Field name made lowercase.
    pharmacy = models.BigIntegerField(blank=True, null=True)
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    isfax = models.IntegerField(db_column='isFax', blank=True, null=True)  # Field name made lowercase.
    filldate = models.DateField(db_column='fillDate', blank=True, null=True)  # Field name made lowercase.
    faxdate = models.DateTimeField(db_column='faxDate')  # Field name made lowercase.
    filldateoverride = models.DateField(db_column='fillDateOverride', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'rxPrintLog'


