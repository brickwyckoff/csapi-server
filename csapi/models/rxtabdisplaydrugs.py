from __future__ import unicode_literals

from django.db import models

class Rxtabdisplaydrugs(models.Model):
    drugid = models.CharField(db_column='drugId', max_length=10, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'rxTabDisplayDrugs'


