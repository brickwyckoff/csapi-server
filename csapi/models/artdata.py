from __future__ import unicode_literals

from django.db import models

class Artdata(models.Model):
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'artData'


