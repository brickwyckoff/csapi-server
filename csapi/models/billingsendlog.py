from __future__ import unicode_literals

from django.db import models

class Billingsendlog(models.Model):
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    resend = models.IntegerField(blank=True, null=True)
    user = models.CharField(max_length=35, blank=True, null=True)
    sendtime = models.DateTimeField(db_column='sendTime', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'billingSendLog'


