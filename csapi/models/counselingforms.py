from __future__ import unicode_literals

from django.db import models

class Counselingforms(models.Model):
    formname = models.CharField(db_column='formName', max_length=50, blank=True, null=True)  # Field name made lowercase.
    formdescription = models.CharField(db_column='formDescription', max_length=300, blank=True, null=True)  # Field name made lowercase.
    formurl = models.CharField(db_column='formUrl', max_length=50, blank=True, null=True)  # Field name made lowercase.
    active = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'counselingForms'


