from __future__ import unicode_literals

from django.db import models

class Pexdata(models.Model):
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    general = models.CharField(max_length=500, blank=True, null=True)
    eye = models.CharField(max_length=500, blank=True, null=True)
    ent = models.CharField(max_length=500, blank=True, null=True)
    neck = models.CharField(max_length=500, blank=True, null=True)
    pulm = models.CharField(max_length=500, blank=True, null=True)
    card = models.CharField(max_length=500, blank=True, null=True)
    abd = models.CharField(max_length=500, blank=True, null=True)
    ms = models.CharField(max_length=500, blank=True, null=True)
    neuro = models.CharField(max_length=500, blank=True, null=True)
    lymph = models.CharField(max_length=500, blank=True, null=True)
    psych = models.CharField(max_length=500, blank=True, null=True)
    other = models.CharField(max_length=500, blank=True, null=True)
    skin = models.CharField(max_length=500, blank=True, null=True)
    intoxication = models.CharField(max_length=500, blank=True, null=True)
    withdrawal = models.CharField(max_length=500, blank=True, null=True)
    mse = models.CharField(max_length=3500, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pexData'


