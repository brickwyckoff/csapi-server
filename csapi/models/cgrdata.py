from __future__ import unicode_literals

from django.db import models

class Cgrdata(models.Model):
    id = models.BigAutoField(primary_key=True)
    mrn = models.BigIntegerField(blank=True, null=True)
    groupnum = models.CharField(db_column='groupNum', max_length=5, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'cgrData'


