from __future__ import unicode_literals

from django.db import models

class Labcpt(models.Model):
    labname = models.CharField(db_column='labName', max_length=30, blank=True, null=True)  # Field name made lowercase.
    procedurecode = models.CharField(db_column='procedureCode', max_length=10)  # Field name made lowercase.
    procedureqty = models.IntegerField(db_column='procedureQty')  # Field name made lowercase.
    insco = models.CharField(db_column='insCo', max_length=60)  # Field name made lowercase.
    labsource = models.CharField(db_column='labSource', max_length=7)  # Field name made lowercase.
    labcode = models.CharField(db_column='labCode', max_length=30, blank=True, null=True)  # Field name made lowercase.
    enteronorder = models.IntegerField(db_column='enterOnOrder', blank=True, null=True)  # Field name made lowercase.
    testtype = models.CharField(db_column='testType', max_length=10, blank=True, null=True)  # Field name made lowercase.
    active = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'labCpt'


