from __future__ import unicode_literals

from django.db import models

class Reqfields(models.Model):
    fieldid = models.CharField(db_column='fieldId', max_length=60, blank=True, null=True)  # Field name made lowercase.
    fieldtype = models.CharField(db_column='fieldType', max_length=5, blank=True, null=True)  # Field name made lowercase.
    valuetype = models.CharField(db_column='valueType', max_length=10, blank=True, null=True)  # Field name made lowercase.
    ismedical = models.IntegerField(db_column='isMedical')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'reqFields'


