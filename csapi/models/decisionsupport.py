from __future__ import unicode_literals

from django.db import models

class Decisionsupport(models.Model):
    id = models.BigAutoField(primary_key=True)
    decisionrunlab = models.CharField(db_column='decisionRunLab', max_length=50)  # Field name made lowercase.
    decisionrunfreq = models.IntegerField(db_column='decisionRunFreq')  # Field name made lowercase.
    decisioncitation = models.CharField(db_column='decisionCitation', max_length=500)  # Field name made lowercase.
    decisiondeveloper = models.CharField(db_column='decisionDeveloper', max_length=500)  # Field name made lowercase.
    decisionfunding = models.CharField(db_column='decisionFunding', max_length=500)  # Field name made lowercase.
    decisionreleasedate = models.CharField(db_column='decisionReleaseDate', max_length=100)  # Field name made lowercase.
    decisionrevisondate = models.CharField(db_column='decisionRevisonDate', max_length=100)  # Field name made lowercase.
    crittype1 = models.CharField(db_column='critType1', max_length=100)  # Field name made lowercase.
    critvalue1 = models.CharField(db_column='critValue1', max_length=100)  # Field name made lowercase.
    crittype2 = models.CharField(db_column='critType2', max_length=100)  # Field name made lowercase.
    critvalue2 = models.CharField(db_column='critValue2', max_length=100)  # Field name made lowercase.
    crittype3 = models.CharField(db_column='critType3', max_length=100)  # Field name made lowercase.
    critvalue3 = models.CharField(db_column='critValue3', max_length=100)  # Field name made lowercase.
    crittype4 = models.CharField(db_column='critType4', max_length=100)  # Field name made lowercase.
    critvalue4 = models.CharField(db_column='critValue4', max_length=100)  # Field name made lowercase.
    crittype5 = models.CharField(db_column='critType5', max_length=100)  # Field name made lowercase.
    critvalue5 = models.CharField(db_column='critValue5', max_length=100)  # Field name made lowercase.
    crittype6 = models.CharField(db_column='critType6', max_length=100)  # Field name made lowercase.
    critvalue6 = models.CharField(db_column='critValue6', max_length=100)  # Field name made lowercase.
    crittype7 = models.CharField(db_column='critType7', max_length=100)  # Field name made lowercase.
    critvalue7 = models.CharField(db_column='critValue7', max_length=100)  # Field name made lowercase.
    crittype8 = models.CharField(db_column='critType8', max_length=100)  # Field name made lowercase.
    critvalue8 = models.CharField(db_column='critValue8', max_length=100)  # Field name made lowercase.
    crittype9 = models.CharField(db_column='critType9', max_length=100)  # Field name made lowercase.
    critvalue9 = models.CharField(db_column='critValue9', max_length=100)  # Field name made lowercase.
    crittype10 = models.CharField(db_column='critType10', max_length=100)  # Field name made lowercase.
    critvalue10 = models.CharField(db_column='critValue10', max_length=100)  # Field name made lowercase.
    user = models.CharField(max_length=35)
    addtime = models.DateTimeField(db_column='addTime')  # Field name made lowercase.
    isactive = models.IntegerField(db_column='isActive')  # Field name made lowercase.
    userlevel = models.CharField(db_column='userLevel', max_length=35)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'decisionSupport'


