from __future__ import unicode_literals

from django.db import models

class Visitsmerged(models.Model):
    mrn = models.BigIntegerField(blank=True, null=True)
    date = models.DateField(blank=True, null=True)
    visittime = models.TimeField(db_column='visitTime', blank=True, null=True)  # Field name made lowercase.
    provider = models.CharField(max_length=20, blank=True, null=True)
    supervisor = models.CharField(max_length=20, blank=True, null=True)
    socialworker = models.CharField(db_column='socialWorker', max_length=20, blank=True, null=True)  # Field name made lowercase.
    status = models.IntegerField(blank=True, null=True)
    changetime = models.DateTimeField(db_column='changeTime', blank=True, null=True)  # Field name made lowercase.
    visitloc = models.IntegerField(db_column='visitLoc', blank=True, null=True)  # Field name made lowercase.
    visittype = models.IntegerField(db_column='visitType', blank=True, null=True)  # Field name made lowercase.
    eligibilityverified = models.DateTimeField(db_column='eligibilityVerified')  # Field name made lowercase.
    eligibilitystatus = models.CharField(db_column='eligibilityStatus', max_length=2, blank=True, null=True)  # Field name made lowercase.
    cosigner = models.CharField(max_length=20, blank=True, null=True)
    labsupervisor = models.CharField(db_column='labSupervisor', max_length=20, blank=True, null=True)  # Field name made lowercase.
    visittimestamp = models.DateTimeField(db_column='visitTimestamp')  # Field name made lowercase.
    attester = models.CharField(max_length=20, blank=True, null=True)
    attestationtype = models.IntegerField(db_column='attestationType', blank=True, null=True)  # Field name made lowercase.
    nurse = models.CharField(max_length=20, blank=True, null=True)
    insuitedoc = models.CharField(db_column='inSuiteDoc', max_length=20, blank=True, null=True)  # Field name made lowercase.
    socialworkersup = models.CharField(db_column='socialWorkerSup', max_length=20, blank=True, null=True)  # Field name made lowercase.
    billinghold = models.CharField(db_column='billingHold', max_length=35, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'visitsMerged'


