from __future__ import unicode_literals

from django.db import models

class Customcounseling(models.Model):
    topcenter = models.CharField(db_column='topCenter', max_length=10, blank=True, null=True)  # Field name made lowercase.
    topright = models.CharField(db_column='topRight', max_length=10, blank=True, null=True)  # Field name made lowercase.
    bottomcenter = models.CharField(db_column='bottomCenter', max_length=10, blank=True, null=True)  # Field name made lowercase.
    bottomright = models.CharField(db_column='bottomRight', max_length=10, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'customCounseling'


