from __future__ import unicode_literals

from django.db import models

class Itxdata(models.Model):
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    problem1 = models.CharField(max_length=1000, blank=True, null=True)
    shortterm1 = models.CharField(db_column='shortTerm1', max_length=1000, blank=True, null=True)  # Field name made lowercase.
    shorttermtime1 = models.CharField(db_column='shortTermTime1', max_length=100, blank=True, null=True)  # Field name made lowercase.
    longterm1 = models.CharField(db_column='longTerm1', max_length=1000, blank=True, null=True)  # Field name made lowercase.
    longtermtime1 = models.CharField(db_column='longTermTime1', max_length=100, blank=True, null=True)  # Field name made lowercase.
    problem2 = models.CharField(max_length=1000, blank=True, null=True)
    shortterm2 = models.CharField(db_column='shortTerm2', max_length=1000, blank=True, null=True)  # Field name made lowercase.
    shorttermtime2 = models.CharField(db_column='shortTermTime2', max_length=100, blank=True, null=True)  # Field name made lowercase.
    longterm2 = models.CharField(db_column='longTerm2', max_length=1000, blank=True, null=True)  # Field name made lowercase.
    longtermtime2 = models.CharField(db_column='longTermTime2', max_length=100, blank=True, null=True)  # Field name made lowercase.
    intervention1 = models.CharField(max_length=1000, blank=True, null=True)
    intervention2 = models.CharField(max_length=1000, blank=True, null=True)
    problem3 = models.CharField(max_length=1000, blank=True, null=True)
    shortterm3 = models.CharField(db_column='shortTerm3', max_length=1000, blank=True, null=True)  # Field name made lowercase.
    shorttermtime3 = models.CharField(db_column='shortTermTime3', max_length=100, blank=True, null=True)  # Field name made lowercase.
    longterm3 = models.CharField(db_column='longTerm3', max_length=1000, blank=True, null=True)  # Field name made lowercase.
    longtermtime3 = models.CharField(db_column='longTermTime3', max_length=100, blank=True, null=True)  # Field name made lowercase.
    intervention3 = models.CharField(max_length=1000, blank=True, null=True)
    attest = models.IntegerField(blank=True, null=True)
    dateofplan = models.CharField(db_column='dateOfPlan', max_length=35, blank=True, null=True)  # Field name made lowercase.
    gaf = models.CharField(max_length=35, blank=True, null=True)
    clientstrengths = models.CharField(db_column='clientStrengths', max_length=800, blank=True, null=True)  # Field name made lowercase.
    clientabilities = models.CharField(db_column='clientAbilities', max_length=800, blank=True, null=True)  # Field name made lowercase.
    clientneeds = models.CharField(db_column='clientNeeds', max_length=800, blank=True, null=True)  # Field name made lowercase.
    clientpreferences = models.CharField(db_column='clientPreferences', max_length=800, blank=True, null=True)  # Field name made lowercase.
    objectives1 = models.CharField(max_length=800, blank=True, null=True)
    objectives2 = models.CharField(max_length=800, blank=True, null=True)
    objectives3 = models.CharField(max_length=800, blank=True, null=True)
    general = models.CharField(max_length=800, blank=True, null=True)
    dischargeplanning = models.CharField(db_column='dischargePlanning', max_length=800, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'itxData'


