from __future__ import unicode_literals

from django.db import models

class Labordernotes(models.Model):
    id = models.BigAutoField(primary_key=True)
    labcontrolid = models.BigIntegerField(db_column='labControlId', blank=True, null=True)  # Field name made lowercase.
    note = models.CharField(max_length=500, blank=True, null=True)
    notesource = models.CharField(db_column='noteSource', max_length=1, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'labOrderNotes'


