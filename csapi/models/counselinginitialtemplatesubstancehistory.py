from __future__ import unicode_literals

from django.db import models

class Counselinginitialtemplatesubstancehistory(models.Model):
    datafield = models.CharField(db_column='dataField', max_length=60, blank=True, null=True)  # Field name made lowercase.
    displaytext = models.CharField(db_column='displayText', max_length=500, blank=True, null=True)  # Field name made lowercase.
    active = models.IntegerField(blank=True, null=True)
    listorder = models.IntegerField(db_column='listOrder', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'counselingInitialTemplateSubstanceHistory'


