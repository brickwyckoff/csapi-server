from __future__ import unicode_literals

from django.db import models

class Caldata(models.Model):
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    unitqty = models.IntegerField(db_column='unitQty', blank=True, null=True)  # Field name made lowercase.
    timeunit = models.CharField(db_column='timeUnit', max_length=10, blank=True, null=True)  # Field name made lowercase.
    scheduled = models.IntegerField(blank=True, null=True)
    changetime = models.DateTimeField(db_column='changeTime', blank=True, null=True)  # Field name made lowercase.
    caldbid = models.BigIntegerField(db_column='calDbId', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'calData'


