from __future__ import unicode_literals

from django.db import models

class Aapdataimported(models.Model):
    mrn = models.BigIntegerField(blank=True, null=True)
    assessandplan = models.CharField(db_column='assessAndPlan', max_length=2000, blank=True, null=True)  # Field name made lowercase.
    futurecaregoals = models.CharField(db_column='futureCareGoals', max_length=2000, blank=True, null=True)  # Field name made lowercase.
    futurecareinstructions = models.CharField(db_column='futureCareInstructions', max_length=2000, blank=True, null=True)  # Field name made lowercase.
    batchid = models.BigIntegerField(db_column='batchId')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'aapDataImported'


