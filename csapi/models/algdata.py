from __future__ import unicode_literals

from django.db import models

class Algdata(models.Model):
    mrn = models.BigIntegerField(blank=True, null=True)
    allergy = models.CharField(max_length=200, blank=True, null=True)
    creationtime = models.DateTimeField(db_column='creationTime', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'algData'


