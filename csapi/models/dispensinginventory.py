from __future__ import unicode_literals

from django.db import models

class Dispensinginventory(models.Model):
    id = models.BigAutoField(primary_key=True)
    bottleid = models.CharField(db_column='bottleId', max_length=50)  # Field name made lowercase.
    clinicid = models.BigIntegerField(db_column='clinicId')  # Field name made lowercase.
    drugid = models.CharField(db_column='drugId', max_length=10)  # Field name made lowercase.
    tradeid = models.IntegerField(db_column='tradeId')  # Field name made lowercase.
    genericbrand = models.CharField(db_column='genericBrand', max_length=2)  # Field name made lowercase.
    rxotc = models.CharField(db_column='rxOtc', max_length=6)  # Field name made lowercase.
    doseform = models.IntegerField(db_column='doseForm')  # Field name made lowercase.
    route = models.IntegerField()
    strength = models.IntegerField()
    originalamount = models.FloatField(db_column='originalAmount')  # Field name made lowercase.
    currentamount = models.FloatField(db_column='currentAmount')  # Field name made lowercase.
    addedby = models.CharField(db_column='addedBy', max_length=35)  # Field name made lowercase.
    addeddate = models.DateTimeField(db_column='addedDate')  # Field name made lowercase.
    activedate = models.DateTimeField(db_column='activeDate')  # Field name made lowercase.
    isactive = models.IntegerField(db_column='isActive')  # Field name made lowercase.
    isondeck = models.IntegerField(db_column='isOnDeck')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'dispensingInventory'


