from __future__ import unicode_literals

from django.db import models

class Mrcdata(models.Model):
    id = models.BigAutoField(primary_key=True)
    visitnum = models.BigIntegerField(db_column='visitNum')  # Field name made lowercase.
    refillsremaining = models.IntegerField(db_column='refillsRemaining')  # Field name made lowercase.
    qtyremaining = models.IntegerField(db_column='qtyRemaining')  # Field name made lowercase.
    user = models.CharField(max_length=35)
    creationtime = models.DateTimeField(db_column='creationTime', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'mrcData'


