from __future__ import unicode_literals

from django.db import models

class Rxfreqconversion(models.Model):
    textfreq = models.CharField(db_column='textFreq', max_length=15, blank=True, null=True)  # Field name made lowercase.
    numfreq = models.FloatField(db_column='numFreq', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'rxFreqConversion'


