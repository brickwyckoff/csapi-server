from __future__ import unicode_literals

from django.db import models

class Providerrestrictions(models.Model):
    providersfield = models.CharField(db_column='providersField', max_length=50, blank=True, null=True)  # Field name made lowercase.
    providersfieldvalue = models.CharField(db_column='providersFieldValue', max_length=50, blank=True, null=True)  # Field name made lowercase.
    blockeddemfield = models.CharField(db_column='blockedDemField', max_length=50, blank=True, null=True)  # Field name made lowercase.
    blockeddemfieldterms = models.CharField(db_column='blockedDemFieldTerms', max_length=200, blank=True, null=True)  # Field name made lowercase.
    blockedroles = models.CharField(db_column='blockedRoles', max_length=100, blank=True, null=True)  # Field name made lowercase.
    blockedreason = models.CharField(db_column='blockedReason', max_length=200, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'providerRestrictions'


