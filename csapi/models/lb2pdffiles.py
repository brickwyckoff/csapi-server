from __future__ import unicode_literals

from django.db import models

class Lb2Pdffiles(models.Model):
    labid = models.CharField(db_column='labId', max_length=50, blank=True, null=True)  # Field name made lowercase.
    filename = models.CharField(db_column='fileName', max_length=100, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'lb2PdfFiles'


