from __future__ import unicode_literals

from django.db import models

class Attendingnoteoptions(models.Model):
    id = models.BigAutoField(primary_key=True)
    attendingnotetext = models.CharField(db_column='attendingNoteText', max_length=1200)  # Field name made lowercase.
    active = models.IntegerField()
    displayorder = models.IntegerField(db_column='displayOrder', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'attendingNoteOptions'


