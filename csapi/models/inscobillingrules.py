from __future__ import unicode_literals

from django.db import models

class Inscobillingrules(models.Model):
    inscoid = models.BigIntegerField(db_column='insCoId', blank=True, null=True)  # Field name made lowercase.
    usepa = models.IntegerField(db_column='usePa', blank=True, null=True)  # Field name made lowercase.
    usenp = models.IntegerField(db_column='useNp', blank=True, null=True)  # Field name made lowercase.
    pamod = models.CharField(db_column='paMod', max_length=3, blank=True, null=True)  # Field name made lowercase.
    npmod = models.CharField(db_column='npMod', max_length=3, blank=True, null=True)  # Field name made lowercase.
    incidenttobilling = models.IntegerField(db_column='incidentToBilling', blank=True, null=True)  # Field name made lowercase.
    billundercosigner = models.IntegerField(db_column='billUnderCosigner', blank=True, null=True)  # Field name made lowercase.
    billunderservicingprovider = models.IntegerField(db_column='billUnderServicingProvider', blank=True, null=True)  # Field name made lowercase.
    billundermid = models.IntegerField(db_column='billUnderMid', blank=True, null=True)  # Field name made lowercase.
    billunderservicinginitial = models.IntegerField(db_column='billUnderServicingInitial', blank=True, null=True)  # Field name made lowercase.
    billunderservicingfollowup = models.IntegerField(db_column='billUnderServicingFollowUp', blank=True, null=True)  # Field name made lowercase.
    prependdxf10orf11 = models.CharField(db_column='prependDxF10orF11', max_length=10, blank=True, null=True)  # Field name made lowercase.
    prependdxf10 = models.CharField(db_column='prependDxF10', max_length=10, blank=True, null=True)  # Field name made lowercase.
    prependdxf11 = models.CharField(db_column='prependDxF11', max_length=10, blank=True, null=True)  # Field name made lowercase.
    switchemforrejoin = models.IntegerField(db_column='switchEmForRejoin', blank=True, null=True)  # Field name made lowercase.
    defaultchargeamt = models.FloatField(db_column='defaultChargeAmt', blank=True, null=True)  # Field name made lowercase.
    defaultchargecptexclude = models.CharField(db_column='defaultChargeCptExclude', max_length=80, blank=True, null=True)  # Field name made lowercase.
    prependdxf10uds = models.CharField(db_column='prependDxF10Uds', max_length=10, blank=True, null=True)  # Field name made lowercase.
    prependdxf11uds = models.CharField(db_column='prependDxF11Uds', max_length=10, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'insCoBillingRules'


