
from django.db import models
from csapi.models.patients import Patients
from csapi.models.current_amd_visits import CurrentAmdVisits

class BatchEligResult (models.Model):
    id = models.AutoField (auto_created=True, primary_key=True, serialize=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    status = models.CharField (max_length=16)

    patient_mrn = models.ForeignKey(Patients, to_field='mrn')
    visit =  models.ForeignKey(CurrentAmdVisits, to_field='visit_num')

    class Meta:
        db_table = 'batchEligResult'
        managed = True
