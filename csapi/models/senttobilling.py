from __future__ import unicode_literals

from django.db import models

class Senttobilling(models.Model):
    dos = models.DateField(blank=True, null=True)
    user = models.CharField(max_length=60, blank=True, null=True)
    sendtime = models.DateTimeField(db_column='sendTime', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'sentToBilling'


