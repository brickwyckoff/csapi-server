from __future__ import unicode_literals

from django.db import models

class Prescriptionpatients(models.Model):
    mrn = models.BigIntegerField(blank=True, null=True)
    activerx = models.BigIntegerField(db_column='activeRx')  # Field name made lowercase.
    pendingrx = models.BigIntegerField(db_column='pendingRx')  # Field name made lowercase.
    activeoutdate = models.DateField(db_column='activeOutDate')  # Field name made lowercase.
    pendingoutdate = models.DateField(db_column='pendingOutDate')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'prescriptionPatients'


