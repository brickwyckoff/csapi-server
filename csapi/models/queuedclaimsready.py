from __future__ import unicode_literals

from django.db import models

class Queuedclaimsready(models.Model):
    id = models.BigAutoField(primary_key=True)
    visitnum = models.BigIntegerField(db_column='visitNum')  # Field name made lowercase.
    sendtype = models.IntegerField(db_column='sendType')  # Field name made lowercase.
    claimtype = models.CharField(db_column='claimType', max_length=1)  # Field name made lowercase.
    creationtime = models.DateTimeField(db_column='creationTime')  # Field name made lowercase.
    usepriorauth1 = models.IntegerField(db_column='usePriorAuth1', blank=True, null=True)  # Field name made lowercase.
    usepriorauth2 = models.IntegerField(db_column='usePriorAuth2', blank=True, null=True)  # Field name made lowercase.
    usepriorauth3 = models.IntegerField(db_column='usePriorAuth3', blank=True, null=True)  # Field name made lowercase.
    usepriorauth4 = models.IntegerField(db_column='usePriorAuth4', blank=True, null=True)  # Field name made lowercase.
    npi = models.CharField(max_length=15, blank=True, null=True)
    pos = models.CharField(max_length=5, blank=True, null=True)
    claimentitytype = models.CharField(db_column='claimEntityType', max_length=5, blank=True, null=True)  # Field name made lowercase.
    billfrom = models.CharField(db_column='billFrom', max_length=2, blank=True, null=True)  # Field name made lowercase.
    claimfiletype = models.CharField(db_column='claimFileType', max_length=1, blank=True, null=True)  # Field name made lowercase.
    billingprovider = models.CharField(db_column='billingProvider', max_length=20, blank=True, null=True)  # Field name made lowercase.
    labbillingprovider = models.CharField(db_column='labBillingProvider', max_length=20, blank=True, null=True)  # Field name made lowercase.
    inscorefnum = models.CharField(db_column='insCoRefNum', max_length=80, blank=True, null=True)  # Field name made lowercase.
    batchnumber = models.CharField(db_column='batchNumber', max_length=30, blank=True, null=True)  # Field name made lowercase.
    approvedbatch = models.CharField(db_column='approvedBatch', max_length=30, blank=True, null=True)  # Field name made lowercase.
    islabclaim = models.IntegerField(db_column='isLabClaim', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'queuedClaimsReady'


