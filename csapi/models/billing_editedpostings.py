from __future__ import unicode_literals

from django.db import models

class BillingEditedpostings(models.Model):
    originalid = models.BigIntegerField(db_column='originalId', blank=True, null=True)  # Field name made lowercase.
    originalamt = models.FloatField(db_column='originalAmt')  # Field name made lowercase.
    originalenteredby = models.CharField(db_column='originalEnteredBy', max_length=30)  # Field name made lowercase.
    originaltime = models.DateTimeField(db_column='originalTime')  # Field name made lowercase.
    creationtime = models.DateTimeField(db_column='creationTime')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'billing_editedPostings'


