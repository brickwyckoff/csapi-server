from __future__ import unicode_literals

from django.db import models

class Vitdata(models.Model):
    temp = models.FloatField(blank=True, null=True)
    bp = models.CharField(max_length=7, blank=True, null=True)
    bps = models.IntegerField(db_column='bpS')  # Field name made lowercase.
    bpd = models.IntegerField(db_column='bpD')  # Field name made lowercase.
    pulse = models.IntegerField(blank=True, null=True)
    rr = models.IntegerField(blank=True, null=True)
    oxsat = models.IntegerField(db_column='oxSat', blank=True, null=True)  # Field name made lowercase.
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    heightft = models.IntegerField(db_column='heightFt', blank=True, null=True)  # Field name made lowercase.
    heightin = models.FloatField(db_column='heightIn', blank=True, null=True)  # Field name made lowercase.
    weight = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'vitData'


