from __future__ import unicode_literals

from django.db import models

class Csidata(models.Model):
    mrn = models.BigIntegerField(blank=True, null=True)
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    precipitants = models.CharField(max_length=500, blank=True, null=True)
    treatmenthistory = models.CharField(db_column='treatmentHistory', max_length=500, blank=True, null=True)  # Field name made lowercase.
    lifesituation = models.CharField(db_column='lifeSituation', max_length=500, blank=True, null=True)  # Field name made lowercase.
    patienthistory = models.CharField(db_column='patientHistory', max_length=1000, blank=True, null=True)  # Field name made lowercase.
    familyhistory = models.CharField(db_column='familyHistory', max_length=500, blank=True, null=True)  # Field name made lowercase.
    suiciderisk = models.CharField(db_column='suicideRisk', max_length=500, blank=True, null=True)  # Field name made lowercase.
    violenthistory = models.CharField(db_column='violentHistory', max_length=500, blank=True, null=True)  # Field name made lowercase.
    supportnetwork = models.CharField(db_column='supportNetwork', max_length=500, blank=True, null=True)  # Field name made lowercase.
    substancehistory = models.CharField(db_column='substanceHistory', max_length=500, blank=True, null=True)  # Field name made lowercase.
    medicalproblems = models.CharField(db_column='medicalProblems', max_length=500, blank=True, null=True)  # Field name made lowercase.
    obstacles = models.CharField(max_length=500, blank=True, null=True)
    clientskills = models.CharField(db_column='clientSkills', max_length=500, blank=True, null=True)  # Field name made lowercase.
    mentalhealth = models.CharField(db_column='mentalHealth', max_length=500, blank=True, null=True)  # Field name made lowercase.
    additionalnotes = models.CharField(db_column='additionalNotes', max_length=500, blank=True, null=True)  # Field name made lowercase.
    treatmentproblem = models.IntegerField(db_column='treatmentProblem', blank=True, null=True)  # Field name made lowercase.
    treatmentobjective = models.IntegerField(db_column='treatmentObjective', blank=True, null=True)  # Field name made lowercase.
    treatmentintervention = models.IntegerField(db_column='treatmentIntervention', blank=True, null=True)  # Field name made lowercase.
    additionaltreatmentproblem = models.CharField(db_column='additionalTreatmentProblem', max_length=500, blank=True, null=True)  # Field name made lowercase.
    additionaltreatmentobjective = models.CharField(db_column='additionalTreatmentObjective', max_length=500, blank=True, null=True)  # Field name made lowercase.
    additionaltreatmentintervention = models.CharField(db_column='additionalTreatmentIntervention', max_length=500, blank=True, null=True)  # Field name made lowercase.
    substancetreatmenthistory = models.CharField(db_column='substanceTreatmentHistory', max_length=500, blank=True, null=True)  # Field name made lowercase.
    substanceabusecurrent = models.CharField(db_column='substanceAbuseCurrent', max_length=500, blank=True, null=True)  # Field name made lowercase.
    physicalwithdrawal = models.CharField(db_column='physicalWithdrawal', max_length=500, blank=True, null=True)  # Field name made lowercase.
    sobrietyhistory = models.CharField(db_column='sobrietyHistory', max_length=500, blank=True, null=True)  # Field name made lowercase.
    familyhistorysubstance = models.CharField(db_column='familyHistorySubstance', max_length=500, blank=True, null=True)  # Field name made lowercase.
    diagnosticsummary = models.CharField(db_column='diagnosticSummary', max_length=500, blank=True, null=True)  # Field name made lowercase.
    financialsummary = models.CharField(db_column='financialSummary', max_length=500, blank=True, null=True)  # Field name made lowercase.
    leisureactivities = models.CharField(db_column='leisureActivities', max_length=500, blank=True, null=True)  # Field name made lowercase.
    vocationalhistory = models.CharField(db_column='vocationalHistory', max_length=500, blank=True, null=True)  # Field name made lowercase.
    educationalhistory = models.CharField(db_column='educationalHistory', max_length=500, blank=True, null=True)  # Field name made lowercase.
    alcoholfirstage = models.CharField(db_column='alcoholFirstAge', max_length=20, blank=True, null=True)  # Field name made lowercase.
    alcohollastuse = models.CharField(db_column='alcoholLastUse', max_length=20, blank=True, null=True)  # Field name made lowercase.
    alcoholusefreq = models.CharField(db_column='alcoholUseFreq', max_length=20, blank=True, null=True)  # Field name made lowercase.
    alcoholroute = models.CharField(db_column='alcoholRoute', max_length=20, blank=True, null=True)  # Field name made lowercase.
    marijuanafirstage = models.CharField(db_column='marijuanaFirstAge', max_length=20, blank=True, null=True)  # Field name made lowercase.
    marijuanalastuse = models.CharField(db_column='marijuanaLastUse', max_length=20, blank=True, null=True)  # Field name made lowercase.
    marijuanausefreq = models.CharField(db_column='marijuanaUseFreq', max_length=20, blank=True, null=True)  # Field name made lowercase.
    marijuanaroute = models.CharField(db_column='marijuanaRoute', max_length=20, blank=True, null=True)  # Field name made lowercase.
    cocainefirstage = models.CharField(db_column='cocaineFirstAge', max_length=20, blank=True, null=True)  # Field name made lowercase.
    cocainelastuse = models.CharField(db_column='cocaineLastUse', max_length=20, blank=True, null=True)  # Field name made lowercase.
    cocaineusefreq = models.CharField(db_column='cocaineUseFreq', max_length=20, blank=True, null=True)  # Field name made lowercase.
    cocaineroute = models.CharField(db_column='cocaineRoute', max_length=20, blank=True, null=True)  # Field name made lowercase.
    opiatesfirstage = models.CharField(db_column='opiatesFirstAge', max_length=20, blank=True, null=True)  # Field name made lowercase.
    opiateslastuse = models.CharField(db_column='opiatesLastUse', max_length=20, blank=True, null=True)  # Field name made lowercase.
    opiatesusefreq = models.CharField(db_column='opiatesUseFreq', max_length=20, blank=True, null=True)  # Field name made lowercase.
    opiatesroute = models.CharField(db_column='opiatesRoute', max_length=20, blank=True, null=True)  # Field name made lowercase.
    heroinfirstage = models.CharField(db_column='heroinFirstAge', max_length=20, blank=True, null=True)  # Field name made lowercase.
    heroinlastuse = models.CharField(db_column='heroinLastUse', max_length=20, blank=True, null=True)  # Field name made lowercase.
    heroinusefreq = models.CharField(db_column='heroinUseFreq', max_length=20, blank=True, null=True)  # Field name made lowercase.
    heroinroute = models.CharField(db_column='heroinRoute', max_length=20, blank=True, null=True)  # Field name made lowercase.
    methfirstage = models.CharField(db_column='methFirstAge', max_length=20, blank=True, null=True)  # Field name made lowercase.
    methlastuse = models.CharField(db_column='methLastUse', max_length=20, blank=True, null=True)  # Field name made lowercase.
    methusefreq = models.CharField(db_column='methUseFreq', max_length=20, blank=True, null=True)  # Field name made lowercase.
    methroute = models.CharField(db_column='methRoute', max_length=20, blank=True, null=True)  # Field name made lowercase.
    benzosfirstage = models.CharField(db_column='benzosFirstAge', max_length=20, blank=True, null=True)  # Field name made lowercase.
    benzoslastuse = models.CharField(db_column='benzosLastUse', max_length=20, blank=True, null=True)  # Field name made lowercase.
    benzosusefreq = models.CharField(db_column='benzosUseFreq', max_length=20, blank=True, null=True)  # Field name made lowercase.
    benzosroute = models.CharField(db_column='benzosRoute', max_length=20, blank=True, null=True)  # Field name made lowercase.
    hallucinogensfirstage = models.CharField(db_column='hallucinogensFirstAge', max_length=20, blank=True, null=True)  # Field name made lowercase.
    hallucinogenslastuse = models.CharField(db_column='hallucinogensLastUse', max_length=20, blank=True, null=True)  # Field name made lowercase.
    hallucinogensusefreq = models.CharField(db_column='hallucinogensUseFreq', max_length=20, blank=True, null=True)  # Field name made lowercase.
    hallucinogensroute = models.CharField(db_column='hallucinogensRoute', max_length=20, blank=True, null=True)  # Field name made lowercase.
    suboxonefirstage = models.CharField(db_column='suboxoneFirstAge', max_length=20, blank=True, null=True)  # Field name made lowercase.
    suboxonelastuse = models.CharField(db_column='suboxoneLastUse', max_length=20, blank=True, null=True)  # Field name made lowercase.
    suboxoneusefreq = models.CharField(db_column='suboxoneUseFreq', max_length=20, blank=True, null=True)  # Field name made lowercase.
    suboxoneroute = models.CharField(db_column='suboxoneRoute', max_length=20, blank=True, null=True)  # Field name made lowercase.
    methadonefirstage = models.CharField(db_column='methadoneFirstAge', max_length=20, blank=True, null=True)  # Field name made lowercase.
    methadonelastuse = models.CharField(db_column='methadoneLastUse', max_length=20, blank=True, null=True)  # Field name made lowercase.
    methadoneusefreq = models.CharField(db_column='methadoneUseFreq', max_length=20, blank=True, null=True)  # Field name made lowercase.
    methadoneroute = models.CharField(db_column='methadoneRoute', max_length=20, blank=True, null=True)  # Field name made lowercase.
    pcpfirstage = models.CharField(db_column='pcpFirstAge', max_length=20, blank=True, null=True)  # Field name made lowercase.
    pcplastuse = models.CharField(db_column='pcpLastUse', max_length=20, blank=True, null=True)  # Field name made lowercase.
    pcpusefreq = models.CharField(db_column='pcpUseFreq', max_length=20, blank=True, null=True)  # Field name made lowercase.
    pcproute = models.CharField(db_column='pcpRoute', max_length=20, blank=True, null=True)  # Field name made lowercase.
    clubfirstage = models.CharField(db_column='clubFirstAge', max_length=20, blank=True, null=True)  # Field name made lowercase.
    clublastuse = models.CharField(db_column='clubLastUse', max_length=20, blank=True, null=True)  # Field name made lowercase.
    clubusefreq = models.CharField(db_column='clubUseFreq', max_length=20, blank=True, null=True)  # Field name made lowercase.
    clubroute = models.CharField(db_column='clubRoute', max_length=20, blank=True, null=True)  # Field name made lowercase.
    stimulantsfirstage = models.CharField(db_column='stimulantsFirstAge', max_length=20, blank=True, null=True)  # Field name made lowercase.
    stimulantslastuse = models.CharField(db_column='stimulantsLastUse', max_length=20, blank=True, null=True)  # Field name made lowercase.
    stimulantsusefreq = models.CharField(db_column='stimulantsUseFreq', max_length=20, blank=True, null=True)  # Field name made lowercase.
    stimulantsroute = models.CharField(db_column='stimulantsRoute', max_length=20, blank=True, null=True)  # Field name made lowercase.
    inhalantsfirstage = models.CharField(db_column='inhalantsFirstAge', max_length=20, blank=True, null=True)  # Field name made lowercase.
    inhalantslastuse = models.CharField(db_column='inhalantsLastUse', max_length=20, blank=True, null=True)  # Field name made lowercase.
    inhalantsusefreq = models.CharField(db_column='inhalantsUseFreq', max_length=20, blank=True, null=True)  # Field name made lowercase.
    inhalantsroute = models.CharField(db_column='inhalantsRoute', max_length=20, blank=True, null=True)  # Field name made lowercase.
    otcfirstage = models.CharField(db_column='otcFirstAge', max_length=20, blank=True, null=True)  # Field name made lowercase.
    otclastuse = models.CharField(db_column='otcLastUse', max_length=20, blank=True, null=True)  # Field name made lowercase.
    otcusefreq = models.CharField(db_column='otcUseFreq', max_length=20, blank=True, null=True)  # Field name made lowercase.
    otcroute = models.CharField(db_column='otcRoute', max_length=20, blank=True, null=True)  # Field name made lowercase.
    barbituratesfirstage = models.CharField(db_column='barbituratesFirstAge', max_length=20, blank=True, null=True)  # Field name made lowercase.
    barbiturateslastuse = models.CharField(db_column='barbituratesLastUse', max_length=20, blank=True, null=True)  # Field name made lowercase.
    barbituratesusefreq = models.CharField(db_column='barbituratesUseFreq', max_length=20, blank=True, null=True)  # Field name made lowercase.
    barbituratesroute = models.CharField(db_column='barbituratesRoute', max_length=20, blank=True, null=True)  # Field name made lowercase.
    tranquilizersfirstage = models.CharField(db_column='tranquilizersFirstAge', max_length=20, blank=True, null=True)  # Field name made lowercase.
    tranquilizerslastuse = models.CharField(db_column='tranquilizersLastUse', max_length=20, blank=True, null=True)  # Field name made lowercase.
    tranquilizersusefreq = models.CharField(db_column='tranquilizersUseFreq', max_length=20, blank=True, null=True)  # Field name made lowercase.
    tranquilizersroute = models.CharField(db_column='tranquilizersRoute', max_length=20, blank=True, null=True)  # Field name made lowercase.
    sedativesfirstage = models.CharField(db_column='sedativesFirstAge', max_length=20, blank=True, null=True)  # Field name made lowercase.
    sedativeslastuse = models.CharField(db_column='sedativesLastUse', max_length=20, blank=True, null=True)  # Field name made lowercase.
    sedativesusefreq = models.CharField(db_column='sedativesUseFreq', max_length=20, blank=True, null=True)  # Field name made lowercase.
    sedativesroute = models.CharField(db_column='sedativesRoute', max_length=20, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'csiData'


