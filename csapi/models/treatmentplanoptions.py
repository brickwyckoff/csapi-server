from __future__ import unicode_literals

from django.db import models

class Treatmentplanoptions(models.Model):
    optionid = models.CharField(db_column='optionId', max_length=20, blank=True, null=True)  # Field name made lowercase.
    optiontext = models.CharField(db_column='optionText', max_length=1500, blank=True, null=True)  # Field name made lowercase.
    expandedtext = models.CharField(db_column='expandedText', max_length=5000, blank=True, null=True)  # Field name made lowercase.
    pdfheight = models.FloatField(db_column='pdfHeight', blank=True, null=True)  # Field name made lowercase.
    additionaloption = models.CharField(db_column='additionalOption', max_length=100, blank=True, null=True)  # Field name made lowercase.
    isactive = models.IntegerField(db_column='isActive')  # Field name made lowercase.
    activedate = models.DateField(db_column='activeDate', blank=True, null=True)  # Field name made lowercase.
    inactivedate = models.DateField(db_column='inactiveDate', blank=True, null=True)  # Field name made lowercase.
    pttype = models.CharField(db_column='ptType', max_length=3, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'treatmentPlanOptions'


