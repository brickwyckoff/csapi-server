from __future__ import unicode_literals

from django.db import models

class Lb2Notesarchived(models.Model):
    mrn = models.BigIntegerField(blank=True, null=True)
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    messagecontrolid = models.CharField(db_column='messageControlId', max_length=22, blank=True, null=True)  # Field name made lowercase.
    labid = models.CharField(db_column='labId', max_length=50, blank=True, null=True)  # Field name made lowercase.
    note = models.CharField(max_length=50000, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'lb2NotesArchived'


