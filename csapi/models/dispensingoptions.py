from __future__ import unicode_literals

from django.db import models

class Dispensingoptions(models.Model):
    id = models.BigAutoField(primary_key=True)
    drugid = models.CharField(db_column='drugId', max_length=10)  # Field name made lowercase.
    tradeid = models.IntegerField(db_column='tradeId')  # Field name made lowercase.
    strength = models.IntegerField()
    genericbrand = models.CharField(db_column='genericBrand', max_length=2)  # Field name made lowercase.
    rxotc = models.CharField(db_column='rxOtc', max_length=6)  # Field name made lowercase.
    doseform = models.IntegerField(db_column='doseForm')  # Field name made lowercase.
    route = models.IntegerField()
    isactive = models.IntegerField(db_column='isActive')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'dispensingOptions'


