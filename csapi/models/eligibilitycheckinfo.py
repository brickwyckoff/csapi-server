from __future__ import unicode_literals

from django.db import models

class Eligibilitycheckinfo(models.Model):
    id = models.BigAutoField(primary_key=True)
    batchid = models.CharField(db_column='batchId', max_length=80)  # Field name made lowercase.
    creationtime = models.DateTimeField(db_column='creationTime')  # Field name made lowercase.
    mrn = models.BigIntegerField()
    insco = models.CharField(db_column='insCo', max_length=80)  # Field name made lowercase.
    subfirstname = models.CharField(db_column='subFirstName', max_length=80)  # Field name made lowercase.
    sublastname = models.CharField(db_column='subLastName', max_length=80)  # Field name made lowercase.
    subpolicynum = models.CharField(db_column='subPolicyNum', max_length=80)  # Field name made lowercase.
    eliginfo = models.CharField(db_column='eligInfo', max_length=20)  # Field name made lowercase.
    errorcode = models.CharField(db_column='errorCode', max_length=15)  # Field name made lowercase.
    coveragelevel = models.CharField(db_column='coverageLevel', max_length=80)  # Field name made lowercase.
    servicetype = models.CharField(db_column='serviceType', max_length=80)  # Field name made lowercase.
    insurancetype = models.CharField(db_column='insuranceType', max_length=80)  # Field name made lowercase.
    plancoverage = models.CharField(db_column='planCoverage', max_length=80)  # Field name made lowercase.
    eligtime = models.CharField(db_column='eligTime', max_length=80)  # Field name made lowercase.
    benefitamount = models.CharField(db_column='benefitAmount', max_length=80)  # Field name made lowercase.
    benefitpercent = models.CharField(db_column='benefitPercent', max_length=80)  # Field name made lowercase.
    benefitqtytype = models.CharField(db_column='benefitQtyType', max_length=80)  # Field name made lowercase.
    benefitqty = models.CharField(db_column='benefitQty', max_length=80)  # Field name made lowercase.
    eligmessage = models.CharField(db_column='eligMessage', max_length=80)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'eligibilitycheckinfo'
        app_label = 'csapi.models'
