from __future__ import unicode_literals

from django.db import models

class Commonallergyclass(models.Model):
    classid = models.IntegerField(db_column='classId', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'commonAllergyClass'


