from __future__ import unicode_literals

from django.db import models

class Activecosigner(models.Model):
    clinicid = models.IntegerField(db_column='clinicId', blank=True, null=True)  # Field name made lowercase.
    providercode = models.CharField(db_column='providerCode', max_length=10, blank=True, null=True)  # Field name made lowercase.
    validendtime = models.DateTimeField(db_column='validEndTime', blank=True, null=True)  # Field name made lowercase.
    validstarttime = models.DateTimeField(db_column='validStartTime')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'activeCosigner'


