from __future__ import unicode_literals

from django.db import models

class Searchablefields(models.Model):
    searchtype = models.CharField(db_column='searchType', max_length=15, blank=True, null=True)  # Field name made lowercase.
    searchtable = models.CharField(db_column='searchTable', max_length=35, blank=True, null=True)  # Field name made lowercase.
    searchfields = models.CharField(db_column='searchFields', max_length=500, blank=True, null=True)  # Field name made lowercase.
    isactive = models.IntegerField(db_column='isActive', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'searchableFields'


