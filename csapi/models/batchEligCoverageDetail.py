from django.db import models
from csapi.models.batchEligHeader import BatchEligHeader

class BatchEligCoverageDetail (models.Model):
    class Meta:
        db_table = 'BatchEligCoverageDetail'
        managed = True

    id = models.AutoField (auto_created=True, primary_key=True, serialize=False)
    created_at = models.DateTimeField (auto_now_add=True)
    updated_at = models.DateTimeField (auto_now=True)
    insuranceTier = models.SmallIntegerField ()
    coverageCode  = models.CharField (max_length=1)
    inPlanNetwork = models.BinaryField (default=False)
    benefitPercent = models.DecimalField (max_digits=3, decimal_places=2)
    coverageLevel = models.CharField (max_length=16)
    # authorizationRequired =
    timePeriod = models.CharField (max_length=32)
    benefitAmount = models.DecimalField(max_digits=6, decimal_places=2)

    batchHeader = models.ForeignKey(BatchEligHeader, to_field='id')


