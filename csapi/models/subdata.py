from __future__ import unicode_literals

from django.db import models

class Subdata(models.Model):
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    alt = models.IntegerField(blank=True, null=True)
    ast = models.IntegerField(blank=True, null=True)
    pregnant = models.IntegerField(blank=True, null=True)
    creatinine = models.CharField(max_length=10, blank=True, null=True)
    venipuncture = models.IntegerField(blank=True, null=True)
    strike = models.IntegerField(blank=True, null=True)
    resetstrikes = models.IntegerField(db_column='resetStrikes', blank=True, null=True)  # Field name made lowercase.
    rapidflu = models.IntegerField(db_column='rapidFlu', blank=True, null=True)  # Field name made lowercase.
    rapidfluqc = models.IntegerField(db_column='rapidFluQc', blank=True, null=True)  # Field name made lowercase.
    rapidstrep = models.IntegerField(db_column='rapidStrep', blank=True, null=True)  # Field name made lowercase.
    rapidstrepqc = models.IntegerField(db_column='rapidStrepQc', blank=True, null=True)  # Field name made lowercase.
    hemoccult = models.IntegerField()
    hemoccultqc = models.IntegerField(db_column='hemoccultQc')  # Field name made lowercase.
    leuk = models.CharField(max_length=30, blank=True, null=True)
    nitrite = models.CharField(max_length=30, blank=True, null=True)
    urobil = models.CharField(max_length=30, blank=True, null=True)
    protein = models.CharField(max_length=30, blank=True, null=True)
    ph = models.CharField(max_length=30, blank=True, null=True)
    blood = models.CharField(max_length=30, blank=True, null=True)
    specgravity = models.CharField(db_column='specGravity', max_length=30, blank=True, null=True)  # Field name made lowercase.
    ketone = models.CharField(max_length=30, blank=True, null=True)
    bilirubin = models.CharField(max_length=30, blank=True, null=True)
    glucose = models.CharField(max_length=30, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'subData'


