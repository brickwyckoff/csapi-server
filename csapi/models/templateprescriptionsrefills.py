from __future__ import unicode_literals

from django.db import models

class Templateprescriptionsrefills(models.Model):
    prescriptionclass = models.CharField(db_column='prescriptionClass', max_length=20, blank=True, null=True)  # Field name made lowercase.
    refills = models.CharField(max_length=30, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'templatePrescriptionsRefills'


