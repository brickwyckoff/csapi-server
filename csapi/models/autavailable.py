from __future__ import unicode_literals

from django.db import models

class Autavailable(models.Model):
    mrn = models.BigIntegerField(blank=True, null=True)
    prescription = models.CharField(max_length=60, blank=True, null=True)
    freq = models.CharField(max_length=15, blank=True, null=True)
    qty = models.CharField(max_length=15, blank=True, null=True)
    route = models.CharField(max_length=15, blank=True, null=True)
    refills = models.CharField(max_length=15, blank=True, null=True)
    signature = models.CharField(max_length=30, blank=True, null=True)
    signtime = models.DateTimeField(db_column='signTime', blank=True, null=True)  # Field name made lowercase.
    useddate = models.DateTimeField(db_column='usedDate', blank=True, null=True)  # Field name made lowercase.
    usedvisitnum = models.BigIntegerField(db_column='usedVisitNum', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'autAvailable'


