from __future__ import unicode_literals

from django.db import models

class Pharmacyeligibilitychange(models.Model):
    id = models.IntegerField(blank=True, null=True)
    controlnumber = models.CharField(db_column='controlNumber', max_length=19, blank=True, null=True)  # Field name made lowercase.
    mrn = models.BigIntegerField(blank=True, null=True)
    firstname = models.CharField(db_column='firstName', max_length=60, blank=True, null=True)  # Field name made lowercase.
    lastname = models.CharField(db_column='lastName', max_length=60, blank=True, null=True)  # Field name made lowercase.
    middlename = models.CharField(db_column='middleName', max_length=60, blank=True, null=True)  # Field name made lowercase.
    dob = models.DateField(blank=True, null=True)
    address1 = models.CharField(max_length=60, blank=True, null=True)
    address2 = models.CharField(max_length=60, blank=True, null=True)
    city = models.CharField(max_length=60, blank=True, null=True)
    state = models.CharField(max_length=2, blank=True, null=True)
    zip = models.CharField(max_length=10, blank=True, null=True)
    gender = models.CharField(max_length=1, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pharmacyEligibilityChange'


