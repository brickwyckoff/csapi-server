from __future__ import unicode_literals

from django.db import models

class Reports(models.Model):
    display = models.CharField(max_length=70, blank=True, null=True)
    description = models.CharField(max_length=200, blank=True, null=True)
    filename = models.CharField(db_column='fileName', max_length=200, blank=True, null=True)  # Field name made lowercase.
    active = models.IntegerField(blank=True, null=True)
    accesslevel = models.CharField(db_column='accessLevel', max_length=3, blank=True, null=True)  # Field name made lowercase.
    listorder = models.IntegerField(db_column='listOrder', blank=True, null=True)  # Field name made lowercase.
    assignmentfields = models.CharField(db_column='assignmentFields', max_length=500, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'reports'


