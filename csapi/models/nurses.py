from __future__ import unicode_literals

from django.db import models

class Nurses(models.Model):
    id = models.BigAutoField(primary_key=True)
    firstname = models.CharField(db_column='firstName', max_length=20, blank=True, null=True)  # Field name made lowercase.
    lastname = models.CharField(db_column='lastName', max_length=40, blank=True, null=True)  # Field name made lowercase.
    title = models.CharField(max_length=3, blank=True, null=True)
    gender = models.CharField(max_length=1, blank=True, null=True)
    code = models.CharField(max_length=2, blank=True, null=True)
    mainclinic = models.IntegerField(db_column='mainClinic', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'nurses'


