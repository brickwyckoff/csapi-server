from __future__ import unicode_literals

from django.db import models

class Providerstateinfo(models.Model):
    id = models.BigAutoField(primary_key=True)
    code = models.CharField(max_length=20, blank=True, null=True)
    state = models.CharField(max_length=2, blank=True, null=True)
    medicallicense = models.CharField(db_column='medicalLicense', max_length=35, blank=True, null=True)  # Field name made lowercase.
    deanumber = models.CharField(db_column='deaNumber', max_length=20, blank=True, null=True)  # Field name made lowercase.
    xdeanumber = models.CharField(db_column='xdeaNumber', max_length=20, blank=True, null=True)  # Field name made lowercase.
    pmpusername = models.CharField(db_column='pmpUsername', max_length=50, blank=True, null=True)  # Field name made lowercase.
    pmppassword = models.CharField(db_column='pmpPassword', max_length=80, blank=True, null=True)  # Field name made lowercase.
    updatetime = models.DateTimeField(db_column='updateTime')  # Field name made lowercase.
    specialty = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'providerStateInfo'


