from __future__ import unicode_literals

from django.db import models

class Lastquerytime(models.Model):
    clientname = models.CharField(db_column='clientName', max_length=35, blank=True, null=True)  # Field name made lowercase.
    querytime = models.DateTimeField(db_column='queryTime', blank=True, null=True)  # Field name made lowercase.
    querytimelocal = models.DateTimeField(db_column='queryTimeLocal', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'lastQueryTime'


