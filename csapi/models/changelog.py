from __future__ import unicode_literals

from django.db import models

class Changelog(models.Model):
    field = models.CharField(max_length=50, blank=True, null=True)
    fielddisplay = models.CharField(db_column='fieldDisplay', max_length=50, blank=True, null=True)  # Field name made lowercase.
    oldvalue = models.CharField(db_column='oldValue', max_length=500, blank=True, null=True)  # Field name made lowercase.
    newvalue = models.CharField(db_column='newValue', max_length=500, blank=True, null=True)  # Field name made lowercase.
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    mrn = models.BigIntegerField(blank=True, null=True)
    user = models.CharField(max_length=50, blank=True, null=True)
    changetime = models.DateTimeField(db_column='changeTime', blank=True, null=True)  # Field name made lowercase.
    actiontype = models.CharField(db_column='actionType', max_length=2)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'changeLog'


