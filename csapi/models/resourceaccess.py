from __future__ import unicode_literals

from django.db import models

class Resourceaccess(models.Model):
    resourcename = models.CharField(db_column='resourceName', max_length=30, blank=True, null=True)  # Field name made lowercase.
    resourceusername = models.CharField(db_column='resourceUsername', max_length=60, blank=True, null=True)  # Field name made lowercase.
    resourcepassword = models.CharField(db_column='resourcePassword', max_length=60, blank=True, null=True)  # Field name made lowercase.
    resourceversion = models.CharField(db_column='resourceVersion', max_length=30, blank=True, null=True)  # Field name made lowercase.
    resourceother = models.CharField(db_column='resourceOther', max_length=30, blank=True, null=True)  # Field name made lowercase.
    resourcetoken = models.CharField(db_column='resourceToken', max_length=300, blank=True, null=True)  # Field name made lowercase.
    clinicid = models.BigIntegerField(db_column='clinicId', blank=True, null=True)  # Field name made lowercase.
    resourceurl = models.CharField(db_column='resourceUrl', max_length=100, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'resourceAccess'


