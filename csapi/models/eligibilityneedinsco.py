from __future__ import unicode_literals

from django.db import models

class Eligibilityneedinsco(models.Model):
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    insco = models.CharField(db_column='insCo', max_length=60, blank=True, null=True)  # Field name made lowercase.
    creationtime = models.DateTimeField(db_column='creationTime', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'eligibilityNeedInsCo'


