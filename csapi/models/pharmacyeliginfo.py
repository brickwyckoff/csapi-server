from __future__ import unicode_literals

from django.db import models

class Pharmacyeliginfo(models.Model):
    controlnumber = models.CharField(db_column='controlNumber', max_length=19, blank=True, null=True)  # Field name made lowercase.
    relatedto = models.CharField(db_column='relatedTo', max_length=5, blank=True, null=True)  # Field name made lowercase.
    benefitstatus = models.CharField(db_column='benefitStatus', max_length=2, blank=True, null=True)  # Field name made lowercase.
    servicetype = models.CharField(db_column='serviceType', max_length=2, blank=True, null=True)  # Field name made lowercase.
    insurancetype = models.CharField(db_column='insuranceType', max_length=3, blank=True, null=True)  # Field name made lowercase.
    plandescription = models.CharField(db_column='planDescription', max_length=50, blank=True, null=True)  # Field name made lowercase.
    monetaryamount = models.CharField(db_column='monetaryAmount', max_length=18, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'pharmacyEligInfo'


