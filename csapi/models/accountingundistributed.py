from __future__ import unicode_literals

from django.db import models

class Accountingundistributed(models.Model):
    mrn = models.BigIntegerField(blank=True, null=True)
    undistamt = models.FloatField(db_column='undistAmt', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'accountingUndistributed'


