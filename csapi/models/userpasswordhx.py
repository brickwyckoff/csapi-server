from __future__ import unicode_literals

from django.db import models

class Userpasswordhx(models.Model):
    id = models.BigAutoField(primary_key=True)
    username = models.CharField(max_length=30, blank=True, null=True)
    password = models.CharField(max_length=80, blank=True, null=True)
    createtime = models.DateTimeField(db_column='createTime')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'userPasswordHx'


