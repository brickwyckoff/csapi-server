from __future__ import unicode_literals

from django.db import models

class Labresultnotes(models.Model):
    id = models.BigAutoField(primary_key=True)
    labcode = models.CharField(db_column='labCode', max_length=20, blank=True, null=True)  # Field name made lowercase.
    note = models.CharField(max_length=500, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'labResultNotes'


