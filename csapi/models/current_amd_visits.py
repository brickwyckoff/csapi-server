from django.db import models

class CurrentAmdVisits(models.Model):
    # Visit block
    visit_num = models.BigIntegerField(db_column='visitNum', unique=True)  # Field name made lowercase.
    duration = models.IntegerField(db_column='duration')
    color = models.CharField(db_column='color' ,max_length=50)

    class Meta:
        db_table = 'currentAmdVisits'
    def __str__(self):
        return "{0}".format(self.visitnum)