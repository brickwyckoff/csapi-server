from __future__ import unicode_literals

from django.db import models

class Labnotessend(models.Model):
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    lablocationcode = models.CharField(db_column='labLocationCode', max_length=30, blank=True, null=True)  # Field name made lowercase.
    notesource = models.CharField(db_column='noteSource', max_length=1, blank=True, null=True)  # Field name made lowercase.
    note = models.CharField(max_length=500, blank=True, null=True)
    issent = models.IntegerField(db_column='isSent', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'labNotesSend'


