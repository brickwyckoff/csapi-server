from __future__ import unicode_literals

from django.db import models

class Qtxdata(models.Model):
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    dateinital = models.CharField(db_column='dateInital', max_length=100, blank=True, null=True)  # Field name made lowercase.
    problem1 = models.CharField(max_length=1000, blank=True, null=True)
    goal1 = models.CharField(max_length=1000, blank=True, null=True)
    progress1 = models.CharField(max_length=1000, blank=True, null=True)
    newgoal1 = models.CharField(db_column='newGoal1', max_length=1000, blank=True, null=True)  # Field name made lowercase.
    goalapprop1 = models.IntegerField(db_column='goalApprop1', blank=True, null=True)  # Field name made lowercase.
    problem2 = models.CharField(max_length=1000, blank=True, null=True)
    goal2 = models.CharField(max_length=1000, blank=True, null=True)
    progress2 = models.CharField(max_length=1000, blank=True, null=True)
    newgoal2 = models.CharField(db_column='newGoal2', max_length=1000, blank=True, null=True)  # Field name made lowercase.
    goalapprop2 = models.IntegerField(db_column='goalApprop2', blank=True, null=True)  # Field name made lowercase.
    problem3 = models.CharField(max_length=1000, blank=True, null=True)
    goal3 = models.CharField(max_length=1000, blank=True, null=True)
    progress3 = models.CharField(max_length=1000, blank=True, null=True)
    newgoal3 = models.CharField(db_column='newGoal3', max_length=1000, blank=True, null=True)  # Field name made lowercase.
    goalapprop3 = models.IntegerField(db_column='goalApprop3', blank=True, null=True)  # Field name made lowercase.
    attest = models.IntegerField(blank=True, null=True)
    dateofplan = models.CharField(db_column='dateOfPlan', max_length=15, blank=True, null=True)  # Field name made lowercase.
    gaf = models.CharField(max_length=35, blank=True, null=True)
    plantype = models.CharField(db_column='planType', max_length=60, blank=True, null=True)  # Field name made lowercase.
    supervisor = models.CharField(max_length=60, blank=True, null=True)
    supervisorreviewdate = models.CharField(db_column='supervisorReviewDate', max_length=15, blank=True, null=True)  # Field name made lowercase.
    dateofplan90 = models.CharField(db_column='dateOfPlan90', max_length=15, blank=True, null=True)  # Field name made lowercase.
    gaf90 = models.CharField(max_length=35, blank=True, null=True)
    plantype90 = models.CharField(db_column='planType90', max_length=60, blank=True, null=True)  # Field name made lowercase.
    problem190 = models.CharField(max_length=1000, blank=True, null=True)
    goal190 = models.CharField(max_length=1000, blank=True, null=True)
    newgoal190 = models.CharField(db_column='newGoal190', max_length=1000, blank=True, null=True)  # Field name made lowercase.
    progress190 = models.CharField(max_length=1000, blank=True, null=True)
    problem290 = models.CharField(max_length=1000, blank=True, null=True)
    goal290 = models.CharField(max_length=1000, blank=True, null=True)
    newgoal290 = models.CharField(db_column='newGoal290', max_length=1000, blank=True, null=True)  # Field name made lowercase.
    progress290 = models.CharField(max_length=1000, blank=True, null=True)
    problem390 = models.CharField(max_length=1000, blank=True, null=True)
    goal390 = models.CharField(max_length=1000, blank=True, null=True)
    newgoal390 = models.CharField(db_column='newGoal390', max_length=1000, blank=True, null=True)  # Field name made lowercase.
    progress390 = models.CharField(max_length=1000, blank=True, null=True)
    attest90 = models.IntegerField(blank=True, null=True)
    supervisor90 = models.CharField(max_length=60, blank=True, null=True)
    supervisorreviewdate90 = models.CharField(db_column='supervisorReviewDate90', max_length=15, blank=True, null=True)  # Field name made lowercase.
    dateofplan180 = models.CharField(db_column='dateOfPlan180', max_length=15, blank=True, null=True)  # Field name made lowercase.
    gaf180 = models.CharField(max_length=35, blank=True, null=True)
    plantype180 = models.CharField(db_column='planType180', max_length=60, blank=True, null=True)  # Field name made lowercase.
    problem1180 = models.CharField(max_length=1000, blank=True, null=True)
    goal1180 = models.CharField(max_length=1000, blank=True, null=True)
    newgoal1180 = models.CharField(db_column='newGoal1180', max_length=1000, blank=True, null=True)  # Field name made lowercase.
    progress1180 = models.CharField(max_length=1000, blank=True, null=True)
    problem2180 = models.CharField(max_length=1000, blank=True, null=True)
    goal2180 = models.CharField(max_length=1000, blank=True, null=True)
    newgoal2180 = models.CharField(db_column='newGoal2180', max_length=1000, blank=True, null=True)  # Field name made lowercase.
    progress2180 = models.CharField(max_length=1000, blank=True, null=True)
    problem3180 = models.CharField(max_length=1000, blank=True, null=True)
    goal3180 = models.CharField(max_length=1000, blank=True, null=True)
    newgoal3180 = models.CharField(db_column='newGoal3180', max_length=1000, blank=True, null=True)  # Field name made lowercase.
    progress3180 = models.CharField(max_length=1000, blank=True, null=True)
    attest180 = models.IntegerField(blank=True, null=True)
    supervisor180 = models.CharField(max_length=60, blank=True, null=True)
    supervisorreviewdate180 = models.CharField(db_column='supervisorReviewDate180', max_length=15, blank=True, null=True)  # Field name made lowercase.
    dateofplan270 = models.CharField(db_column='dateOfPlan270', max_length=15, blank=True, null=True)  # Field name made lowercase.
    gaf270 = models.CharField(max_length=35, blank=True, null=True)
    plantype270 = models.CharField(db_column='planType270', max_length=60, blank=True, null=True)  # Field name made lowercase.
    problem1270 = models.CharField(max_length=1000, blank=True, null=True)
    goal1270 = models.CharField(max_length=1000, blank=True, null=True)
    newgoal1270 = models.CharField(db_column='newGoal1270', max_length=1000, blank=True, null=True)  # Field name made lowercase.
    progress1270 = models.CharField(max_length=1000, blank=True, null=True)
    problem2270 = models.CharField(max_length=1000, blank=True, null=True)
    goal2270 = models.CharField(max_length=1000, blank=True, null=True)
    newgoal2270 = models.CharField(db_column='newGoal2270', max_length=1000, blank=True, null=True)  # Field name made lowercase.
    progress2270 = models.CharField(max_length=1000, blank=True, null=True)
    problem3270 = models.CharField(max_length=1000, blank=True, null=True)
    goal3270 = models.CharField(max_length=1000, blank=True, null=True)
    newgoal3270 = models.CharField(db_column='newGoal3270', max_length=1000, blank=True, null=True)  # Field name made lowercase.
    progress3270 = models.CharField(max_length=1000, blank=True, null=True)
    attest270 = models.IntegerField(blank=True, null=True)
    supervisor270 = models.CharField(max_length=60, blank=True, null=True)
    supervisorreviewdate270 = models.CharField(db_column='supervisorReviewDate270', max_length=15, blank=True, null=True)  # Field name made lowercase.
    dateofplan360 = models.CharField(db_column='dateOfPlan360', max_length=15, blank=True, null=True)  # Field name made lowercase.
    gaf360 = models.CharField(max_length=35, blank=True, null=True)
    plantype360 = models.CharField(db_column='planType360', max_length=60, blank=True, null=True)  # Field name made lowercase.
    problem1360 = models.CharField(max_length=1000, blank=True, null=True)
    goal1360 = models.CharField(max_length=1000, blank=True, null=True)
    newgoal1360 = models.CharField(db_column='newGoal1360', max_length=1000, blank=True, null=True)  # Field name made lowercase.
    progress1360 = models.CharField(max_length=1000, blank=True, null=True)
    problem2360 = models.CharField(max_length=1000, blank=True, null=True)
    goal2360 = models.CharField(max_length=1000, blank=True, null=True)
    newgoal2360 = models.CharField(db_column='newGoal2360', max_length=1000, blank=True, null=True)  # Field name made lowercase.
    progress2360 = models.CharField(max_length=1000, blank=True, null=True)
    problem3360 = models.CharField(max_length=1000, blank=True, null=True)
    goal3360 = models.CharField(max_length=1000, blank=True, null=True)
    newgoal3360 = models.CharField(db_column='newGoal3360', max_length=1000, blank=True, null=True)  # Field name made lowercase.
    progress3360 = models.CharField(max_length=1000, blank=True, null=True)
    attest360 = models.IntegerField(blank=True, null=True)
    supervisor360 = models.CharField(max_length=60, blank=True, null=True)
    supervisorreviewdate360 = models.CharField(db_column='supervisorReviewDate360', max_length=15, blank=True, null=True)  # Field name made lowercase.
    clientsig90 = models.CharField(db_column='clientSig90', max_length=100)  # Field name made lowercase.
    clientsigdate90 = models.CharField(db_column='clientSigDate90', max_length=100)  # Field name made lowercase.
    clientsig180 = models.CharField(db_column='clientSig180', max_length=100)  # Field name made lowercase.
    clientsigdate180 = models.CharField(db_column='clientSigDate180', max_length=100)  # Field name made lowercase.
    clientsig270 = models.CharField(db_column='clientSig270', max_length=100)  # Field name made lowercase.
    clientsigdate270 = models.CharField(db_column='clientSigDate270', max_length=100)  # Field name made lowercase.
    clientsig360 = models.CharField(db_column='clientSig360', max_length=100)  # Field name made lowercase.
    clientsigdate360 = models.CharField(db_column='clientSigDate360', max_length=100)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'qtxData'


