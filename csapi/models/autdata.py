from __future__ import unicode_literals

from django.db import models

class Autdata(models.Model):
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    prescription1 = models.CharField(max_length=60, blank=True, null=True)
    freq1 = models.CharField(max_length=15, blank=True, null=True)
    qty1 = models.CharField(max_length=15, blank=True, null=True)
    route1 = models.CharField(max_length=15, blank=True, null=True)
    refills1 = models.CharField(max_length=15, blank=True, null=True)
    signature1 = models.CharField(max_length=30, blank=True, null=True)
    signtime1 = models.DateTimeField(db_column='signTime1', blank=True, null=True)  # Field name made lowercase.
    prescription2 = models.CharField(max_length=60, blank=True, null=True)
    freq2 = models.CharField(max_length=15, blank=True, null=True)
    qty2 = models.CharField(max_length=15, blank=True, null=True)
    route2 = models.CharField(max_length=15, blank=True, null=True)
    refills2 = models.CharField(max_length=15, blank=True, null=True)
    signature2 = models.CharField(max_length=30, blank=True, null=True)
    rxsupervisor = models.CharField(db_column='rxSupervisor', max_length=2, blank=True, null=True)  # Field name made lowercase.
    printed1 = models.IntegerField()
    printed2 = models.IntegerField()
    pharmacy1 = models.BigIntegerField()
    pharmacy2 = models.BigIntegerField()
    callin1 = models.CharField(db_column='callIn1', max_length=10)  # Field name made lowercase.
    callin2 = models.CharField(db_column='callIn2', max_length=10)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'autData'


