from __future__ import unicode_literals

from django.db import models

class Pexdefaults(models.Model):
    field = models.CharField(max_length=10, blank=True, null=True)
    defaultvalue = models.CharField(db_column='defaultValue', max_length=500, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'pexDefaults'


