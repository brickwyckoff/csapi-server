from __future__ import unicode_literals

from django.db import models

class Dispensinginventorynotes(models.Model):
    id = models.BigAutoField(primary_key=True)
    dispensinginventoryid = models.BigIntegerField(db_column='dispensingInventoryId')  # Field name made lowercase.
    note = models.TextField()
    newamount = models.FloatField(db_column='newAmount')  # Field name made lowercase.
    user = models.CharField(max_length=35)
    entrytime = models.DateTimeField(db_column='entryTime')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'dispensingInventoryNotes'


