from __future__ import unicode_literals

from django.db import models

class Counselorpartners(models.Model):
    counselorname = models.CharField(db_column='counselorName', max_length=60, blank=True, null=True)  # Field name made lowercase.
    databasename = models.CharField(db_column='databaseName', max_length=30, blank=True, null=True)  # Field name made lowercase.
    shareinsurancedemographics = models.IntegerField(db_column='shareInsuranceDemographics')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'counselorPartners'


