from __future__ import unicode_literals

from django.db import models

class Lb2Dataarchived(models.Model):
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    mrn = models.BigIntegerField(blank=True, null=True)
    messagecontrolid = models.CharField(db_column='messageControlId', max_length=22, blank=True, null=True)  # Field name made lowercase.
    parentorderid = models.CharField(db_column='parentOrderId', max_length=22, blank=True, null=True)  # Field name made lowercase.
    subheaderid = models.CharField(db_column='subHeaderId', max_length=15, blank=True, null=True)  # Field name made lowercase.
    labid = models.CharField(db_column='labId', max_length=50, blank=True, null=True)  # Field name made lowercase.
    labname = models.CharField(db_column='labName', max_length=200, blank=True, null=True)  # Field name made lowercase.
    labdate = models.DateTimeField(db_column='labDate', blank=True, null=True)  # Field name made lowercase.
    labresult = models.CharField(db_column='labResult', max_length=72, blank=True, null=True)  # Field name made lowercase.
    labresultnum = models.FloatField(db_column='labResultNum')  # Field name made lowercase.
    labunit = models.CharField(db_column='labUnit', max_length=50, blank=True, null=True)  # Field name made lowercase.
    labreferencerange = models.CharField(db_column='labReferenceRange', max_length=100, blank=True, null=True)  # Field name made lowercase.
    abnormalflag = models.CharField(db_column='abnormalFlag', max_length=2, blank=True, null=True)  # Field name made lowercase.
    orderedbyid = models.CharField(db_column='orderedById', max_length=100, blank=True, null=True)  # Field name made lowercase.
    orderedbyname = models.CharField(db_column='orderedByName', max_length=100, blank=True, null=True)  # Field name made lowercase.
    analysedat = models.CharField(db_column='analysedAt', max_length=100, blank=True, null=True)  # Field name made lowercase.
    analysedby = models.CharField(db_column='analysedBy', max_length=15, blank=True, null=True)  # Field name made lowercase.
    analysedatname = models.CharField(db_column='analysedAtName', max_length=60)  # Field name made lowercase.
    analysedataddress1 = models.CharField(db_column='analysedAtAddress1', max_length=60)  # Field name made lowercase.
    analysedataddress2 = models.CharField(db_column='analysedAtAddress2', max_length=60)  # Field name made lowercase.
    analysedatcity = models.CharField(db_column='analysedAtCity', max_length=60)  # Field name made lowercase.
    analysedatstate = models.CharField(db_column='analysedAtState', max_length=2)  # Field name made lowercase.
    analysedatzip = models.CharField(db_column='analysedAtZip', max_length=10)  # Field name made lowercase.
    analysedatmedicaldirector = models.CharField(db_column='analysedAtMedicalDirector', max_length=80)  # Field name made lowercase.
    origlabresult = models.CharField(db_column='origLabResult', max_length=72)  # Field name made lowercase.
    labresultcode = models.CharField(db_column='labResultCode', max_length=72)  # Field name made lowercase.
    labresultcodingsystem = models.CharField(db_column='labResultCodingSystem', max_length=72)  # Field name made lowercase.
    loinccode = models.CharField(db_column='loincCode', max_length=15)  # Field name made lowercase.
    observationsubid = models.CharField(db_column='observationSubId', max_length=20)  # Field name made lowercase.
    observationparentid = models.CharField(db_column='observationParentId', max_length=20)  # Field name made lowercase.
    analysedatphone = models.BigIntegerField(db_column='analysedAtPhone', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'lb2DataArchived'


