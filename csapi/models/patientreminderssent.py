from __future__ import unicode_literals

from django.db import models

class Patientreminderssent(models.Model):
    mrn = models.BigIntegerField(blank=True, null=True)
    reviewedby = models.CharField(db_column='reviewedBy', max_length=35, blank=True, null=True)  # Field name made lowercase.
    timereviewed = models.DateTimeField(db_column='timeReviewed', blank=True, null=True)  # Field name made lowercase.
    labloc = models.CharField(db_column='labLoc', max_length=15)  # Field name made lowercase.
    labcode = models.CharField(db_column='labCode', max_length=15)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'patientRemindersSent'


