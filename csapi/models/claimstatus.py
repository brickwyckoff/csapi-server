from __future__ import unicode_literals

from django.db import models

class Claimstatus(models.Model):
    id = models.BigAutoField(primary_key=True)
    visitnum = models.BigIntegerField(db_column='visitNum')  # Field name made lowercase.
    claimdate = models.DateTimeField(db_column='claimDate')  # Field name made lowercase.
    batchcontrolnum = models.CharField(db_column='batchControlNum', max_length=35)  # Field name made lowercase.
    batchstatus = models.CharField(db_column='batchStatus', max_length=1)  # Field name made lowercase.
    batchstatusdate = models.DateTimeField(db_column='batchStatusDate')  # Field name made lowercase.
    setcontrolnum = models.CharField(db_column='setControlNum', max_length=35)  # Field name made lowercase.
    claimstatus = models.CharField(db_column='claimStatus', max_length=2)  # Field name made lowercase.
    claimstatusamt = models.CharField(db_column='claimStatusAmt', max_length=15)  # Field name made lowercase.
    claimstatuscodes = models.CharField(db_column='claimStatusCodes', max_length=185)  # Field name made lowercase.
    claimstatusdate = models.DateTimeField(db_column='claimStatusDate')  # Field name made lowercase.
    eobdate = models.DateTimeField(db_column='eobDate')  # Field name made lowercase.
    claimsuffix = models.CharField(db_column='claimSuffix', max_length=5, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'claimStatus'


