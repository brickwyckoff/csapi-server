from __future__ import unicode_literals

from django.db import models

class Laborderlogpendingrecent(models.Model):
    mrn = models.BigIntegerField(blank=True, null=True)
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    labcontrolid = models.CharField(db_column='labControlId', max_length=60, blank=True, null=True)  # Field name made lowercase.
    labcode = models.CharField(db_column='labCode', max_length=15, blank=True, null=True)  # Field name made lowercase.
    sentto = models.CharField(db_column='sentTo', max_length=15, blank=True, null=True)  # Field name made lowercase.
    sentby = models.CharField(db_column='sentBy', max_length=50, blank=True, null=True)  # Field name made lowercase.
    creationtime = models.DateTimeField(db_column='creationTime', blank=True, null=True)  # Field name made lowercase.
    datereviewed = models.DateTimeField(db_column='dateReviewed', blank=True, null=True)  # Field name made lowercase.
    orderentered = models.IntegerField(db_column='orderEntered', blank=True, null=True)  # Field name made lowercase.
    enteredby = models.CharField(db_column='enteredBy', max_length=35, blank=True, null=True)  # Field name made lowercase.
    entertime = models.DateTimeField(db_column='enterTime', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'labOrderLogPendingRecent'


