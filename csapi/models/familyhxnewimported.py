from __future__ import unicode_literals

from django.db import models

class Familyhxnewimported(models.Model):
    mrn = models.BigIntegerField(blank=True, null=True)
    code = models.CharField(max_length=11, blank=True, null=True)
    altcode = models.CharField(db_column='altCode', max_length=10)  # Field name made lowercase.
    codetable = models.CharField(db_column='codeTable', max_length=20, blank=True, null=True)  # Field name made lowercase.
    freetext = models.CharField(db_column='freeText', max_length=80, blank=True, null=True)  # Field name made lowercase.
    startdate = models.DateField(db_column='startDate', blank=True, null=True)  # Field name made lowercase.
    enddate = models.DateField(db_column='endDate', blank=True, null=True)  # Field name made lowercase.
    isactive = models.IntegerField(db_column='isActive', blank=True, null=True)  # Field name made lowercase.
    isresolved = models.IntegerField(db_column='isResolved', blank=True, null=True)  # Field name made lowercase.
    updatedate = models.DateTimeField(db_column='updateDate')  # Field name made lowercase.
    relation = models.CharField(max_length=30)
    onsetage = models.IntegerField(db_column='onsetAge', blank=True, null=True)  # Field name made lowercase.
    importstatus = models.IntegerField(db_column='importStatus')  # Field name made lowercase.
    batchid = models.BigIntegerField(db_column='batchId')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'familyHxNewImported'


