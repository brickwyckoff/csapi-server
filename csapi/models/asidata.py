from __future__ import unicode_literals

from django.db import models

class Asidata(models.Model):
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    medhospitalized = models.CharField(db_column='medHospitalized', max_length=100, blank=True, null=True)  # Field name made lowercase.
    medlasthospitalized = models.CharField(db_column='medLastHospitalized', max_length=100, blank=True, null=True)  # Field name made lowercase.
    medchronic = models.IntegerField(db_column='medChronic', blank=True, null=True)  # Field name made lowercase.
    medchronicspecify = models.CharField(db_column='medChronicSpecify', max_length=1000, blank=True, null=True)  # Field name made lowercase.
    medtakingmeds = models.IntegerField(db_column='medTakingMeds', blank=True, null=True)  # Field name made lowercase.
    meddisabilitypension = models.IntegerField(db_column='medDisabilityPension', blank=True, null=True)  # Field name made lowercase.
    meddisabilitypensionspecify = models.CharField(db_column='medDisabilityPensionSpecify', max_length=1000, blank=True, null=True)  # Field name made lowercase.
    medproblemdays = models.CharField(db_column='medProblemDays', max_length=100, blank=True, null=True)  # Field name made lowercase.
    medproblembothered = models.CharField(db_column='medProblemBothered', max_length=100, blank=True, null=True)  # Field name made lowercase.
    medproblemimportant = models.CharField(db_column='medProblemImportant', max_length=100, blank=True, null=True)  # Field name made lowercase.
    medproblemneed = models.CharField(db_column='medProblemNeed', max_length=100, blank=True, null=True)  # Field name made lowercase.
    meddistortmisrep = models.IntegerField(db_column='medDistortMisrep', blank=True, null=True)  # Field name made lowercase.
    meddistortmisunderstand = models.IntegerField(db_column='medDistortMisunderstand', blank=True, null=True)  # Field name made lowercase.
    medcomments = models.CharField(db_column='medComments', max_length=1000, blank=True, null=True)  # Field name made lowercase.
    empedcompleted = models.CharField(db_column='empEdCompleted', max_length=100, blank=True, null=True)  # Field name made lowercase.
    emptrainingcompleted = models.CharField(db_column='empTrainingCompleted', max_length=100, blank=True, null=True)  # Field name made lowercase.
    emptechskill = models.IntegerField(db_column='empTechSkill', blank=True, null=True)  # Field name made lowercase.
    emptechskillspecify = models.CharField(db_column='empTechSkillSpecify', max_length=1000, blank=True, null=True)  # Field name made lowercase.
    empdriverslicense = models.IntegerField(db_column='empDriversLicense', blank=True, null=True)  # Field name made lowercase.
    empauto = models.IntegerField(db_column='empAuto', blank=True, null=True)  # Field name made lowercase.
    emplongestjob = models.CharField(db_column='empLongestJob', max_length=100, blank=True, null=True)  # Field name made lowercase.
    empusualjob = models.CharField(db_column='empUsualJob', max_length=1000, blank=True, null=True)  # Field name made lowercase.
    empcontrib = models.IntegerField(db_column='empContrib', blank=True, null=True)  # Field name made lowercase.
    empcontribmajority = models.IntegerField(db_column='empContribMajority', blank=True, null=True)  # Field name made lowercase.
    emppattern = models.CharField(db_column='empPattern', max_length=100, blank=True, null=True)  # Field name made lowercase.
    empdayspaid = models.CharField(db_column='empDaysPaid', max_length=100, blank=True, null=True)  # Field name made lowercase.
    empemploymentincome = models.CharField(db_column='empEmploymentIncome', max_length=100, blank=True, null=True)  # Field name made lowercase.
    empunemploymentincome = models.CharField(db_column='empUnemploymentIncome', max_length=100, blank=True, null=True)  # Field name made lowercase.
    empdpaincome = models.CharField(db_column='empDPAIncome', max_length=100, blank=True, null=True)  # Field name made lowercase.
    emppensionincome = models.CharField(db_column='empPensionIncome', max_length=100, blank=True, null=True)  # Field name made lowercase.
    empfamilyincome = models.CharField(db_column='empFamilyIncome', max_length=100, blank=True, null=True)  # Field name made lowercase.
    empillegalincome = models.CharField(db_column='empIllegalIncome', max_length=100, blank=True, null=True)  # Field name made lowercase.
    empdependants = models.CharField(db_column='empDependants', max_length=100, blank=True, null=True)  # Field name made lowercase.
    empproblems = models.CharField(db_column='empProblems', max_length=100, blank=True, null=True)  # Field name made lowercase.
    empproblemstroubled = models.CharField(db_column='empProblemsTroubled', max_length=100, blank=True, null=True)  # Field name made lowercase.
    empproblemscounseling = models.CharField(db_column='empProblemsCounseling', max_length=100, blank=True, null=True)  # Field name made lowercase.
    empcounselingrating = models.CharField(db_column='empCounselingRating', max_length=100, blank=True, null=True)  # Field name made lowercase.
    empdistortmisrep = models.IntegerField(db_column='empDistortMisrep', blank=True, null=True)  # Field name made lowercase.
    empdistortmisunderstand = models.IntegerField(db_column='empDistortMisunderstand', blank=True, null=True)  # Field name made lowercase.
    drugalcoholany = models.CharField(db_column='drugAlcoholAny', max_length=100, blank=True, null=True)  # Field name made lowercase.
    drugalcoholintox = models.CharField(db_column='drugAlcoholIntox', max_length=100, blank=True, null=True)  # Field name made lowercase.
    drugheroin = models.CharField(db_column='drugHeroin', max_length=100, blank=True, null=True)  # Field name made lowercase.
    drugmethadone = models.CharField(db_column='drugMethadone', max_length=100, blank=True, null=True)  # Field name made lowercase.
    drugotherop = models.CharField(db_column='drugOtherOp', max_length=100, blank=True, null=True)  # Field name made lowercase.
    drugbarb = models.CharField(db_column='drugBarb', max_length=100, blank=True, null=True)  # Field name made lowercase.
    drugothersed = models.CharField(db_column='drugOtherSed', max_length=100, blank=True, null=True)  # Field name made lowercase.
    drugcocaine = models.CharField(db_column='drugCocaine', max_length=100, blank=True, null=True)  # Field name made lowercase.
    drugamph = models.CharField(db_column='drugAmph', max_length=100, blank=True, null=True)  # Field name made lowercase.
    drugcannabis = models.CharField(db_column='drugCannabis', max_length=100, blank=True, null=True)  # Field name made lowercase.
    drughallucinogens = models.CharField(db_column='drugHallucinogens', max_length=100, blank=True, null=True)  # Field name made lowercase.
    druginhalants = models.CharField(db_column='drugInhalants', max_length=100, blank=True, null=True)  # Field name made lowercase.
    drugmultsubs = models.CharField(db_column='drugMultSubs', max_length=100, blank=True, null=True)  # Field name made lowercase.
    drugmajorproblem = models.CharField(db_column='drugMajorProblem', max_length=100, blank=True, null=True)  # Field name made lowercase.
    druglastabs = models.CharField(db_column='drugLastAbs', max_length=100, blank=True, null=True)  # Field name made lowercase.
    druglastabsend = models.CharField(db_column='drugLastAbsEnd', max_length=100, blank=True, null=True)  # Field name made lowercase.
    drughowmanydt = models.CharField(db_column='drugHowManyDt', max_length=100, blank=True, null=True)  # Field name made lowercase.
    drughowmanyod = models.CharField(db_column='drugHowManyOd', max_length=100, blank=True, null=True)  # Field name made lowercase.
    drughowmanyalcabusetx = models.CharField(db_column='drugHowManyAlcAbuseTx', max_length=100, blank=True, null=True)  # Field name made lowercase.
    drughowmanydrugabusetx = models.CharField(db_column='drugHowManyDrugAbuseTx', max_length=100, blank=True, null=True)  # Field name made lowercase.
    drughowmanyalcdetox = models.CharField(db_column='drugHowManyAlcDetox', max_length=100, blank=True, null=True)  # Field name made lowercase.
    drughowmanydrugdetox = models.CharField(db_column='drugHowManyDrugDetox', max_length=100, blank=True, null=True)  # Field name made lowercase.
    drugalcspending = models.CharField(db_column='drugAlcSpending', max_length=100, blank=True, null=True)  # Field name made lowercase.
    drugdrugspending = models.CharField(db_column='drugDrugSpending', max_length=100, blank=True, null=True)  # Field name made lowercase.
    drugoutpatienttx = models.CharField(db_column='drugOutpatientTx', max_length=100, blank=True, null=True)  # Field name made lowercase.
    drugalcprobdays = models.CharField(db_column='drugAlcProbDays', max_length=100, blank=True, null=True)  # Field name made lowercase.
    drugdrugprobdays = models.CharField(db_column='drugDrugProbDays', max_length=100, blank=True, null=True)  # Field name made lowercase.
    drugalctroubled = models.CharField(db_column='drugAlcTroubled', max_length=100, blank=True, null=True)  # Field name made lowercase.
    drugdrugtroubled = models.CharField(db_column='drugDrugTroubled', max_length=100, blank=True, null=True)  # Field name made lowercase.
    drugalctximportance = models.CharField(db_column='drugAlcTxImportance', max_length=100, blank=True, null=True)  # Field name made lowercase.
    drugdrugtximportance = models.CharField(db_column='drugDrugTxImportance', max_length=100, blank=True, null=True)  # Field name made lowercase.
    drugalctxneed = models.CharField(db_column='drugAlcTxNeed', max_length=100, blank=True, null=True)  # Field name made lowercase.
    drugdrugtxneed = models.CharField(db_column='drugDrugTxNeed', max_length=100, blank=True, null=True)  # Field name made lowercase.
    drugdistortmisrep = models.IntegerField(db_column='drugDistortMisrep', blank=True, null=True)  # Field name made lowercase.
    drugdistortmisunderstand = models.IntegerField(db_column='drugDistortMisunderstand', blank=True, null=True)  # Field name made lowercase.
    legalcrimjustice = models.IntegerField(db_column='legalCrimJustice', blank=True, null=True)  # Field name made lowercase.
    legalprobation = models.IntegerField(db_column='legalProbation', blank=True, null=True)  # Field name made lowercase.
    legalshoplifting = models.CharField(db_column='legalShoplifting', max_length=100, blank=True, null=True)  # Field name made lowercase.
    legalparoleviolation = models.CharField(db_column='legalParoleViolation', max_length=100, blank=True, null=True)  # Field name made lowercase.
    legaldrugs = models.CharField(db_column='legalDrugs', max_length=100, blank=True, null=True)  # Field name made lowercase.
    legalforgery = models.CharField(db_column='legalForgery', max_length=100, blank=True, null=True)  # Field name made lowercase.
    legalweapons = models.CharField(db_column='legalWeapons', max_length=100, blank=True, null=True)  # Field name made lowercase.
    legalburglary = models.CharField(db_column='legalBurglary', max_length=100, blank=True, null=True)  # Field name made lowercase.
    legalrobbery = models.CharField(db_column='legalRobbery', max_length=100, blank=True, null=True)  # Field name made lowercase.
    legalassault = models.CharField(db_column='legalAssault', max_length=100, blank=True, null=True)  # Field name made lowercase.
    legalarson = models.CharField(db_column='legalArson', max_length=100, blank=True, null=True)  # Field name made lowercase.
    legalrape = models.CharField(db_column='legalRape', max_length=100, blank=True, null=True)  # Field name made lowercase.
    legalhomicide = models.CharField(db_column='legalHomicide', max_length=100, blank=True, null=True)  # Field name made lowercase.
    legalprostitution = models.CharField(db_column='legalProstitution', max_length=100, blank=True, null=True)  # Field name made lowercase.
    legalcontempt = models.CharField(db_column='legalContempt', max_length=100, blank=True, null=True)  # Field name made lowercase.
    legalothercharge = models.CharField(db_column='legalOtherCharge', max_length=100, blank=True, null=True)  # Field name made lowercase.
    legalconvictions = models.CharField(db_column='legalConvictions', max_length=100, blank=True, null=True)  # Field name made lowercase.
    legalvagrancy = models.CharField(db_column='legalVagrancy', max_length=100, blank=True, null=True)  # Field name made lowercase.
    legaldwi = models.CharField(db_column='legalDWI', max_length=100, blank=True, null=True)  # Field name made lowercase.
    legalmajordriving = models.CharField(db_column='legalMajorDriving', max_length=100, blank=True, null=True)  # Field name made lowercase.
    legalincarceration = models.CharField(db_column='legalIncarceration', max_length=100, blank=True, null=True)  # Field name made lowercase.
    legalincarcerationlast = models.CharField(db_column='legalIncarcerationLast', max_length=100, blank=True, null=True)  # Field name made lowercase.
    legalincarcerationreason = models.CharField(db_column='legalIncarcerationReason', max_length=100, blank=True, null=True)  # Field name made lowercase.
    legalawaitingcharges = models.IntegerField(db_column='legalAwaitingCharges', blank=True, null=True)  # Field name made lowercase.
    legalawaitingchargesfor = models.CharField(db_column='legalAwaitingChargesFor', max_length=100, blank=True, null=True)  # Field name made lowercase.
    legaldaysdetained = models.CharField(db_column='legalDaysDetained', max_length=100, blank=True, null=True)  # Field name made lowercase.
    legaldaysprofit = models.CharField(db_column='legalDaysProfit', max_length=100, blank=True, null=True)  # Field name made lowercase.
    legalserious = models.CharField(db_column='legalSerious', max_length=100, blank=True, null=True)  # Field name made lowercase.
    legalimportance = models.CharField(db_column='legalImportance', max_length=100, blank=True, null=True)  # Field name made lowercase.
    legalneed = models.CharField(db_column='legalNeed', max_length=100, blank=True, null=True)  # Field name made lowercase.
    legaldistortmisrep = models.IntegerField(db_column='legalDistortMisrep', blank=True, null=True)  # Field name made lowercase.
    legaldistortmisunderstand = models.IntegerField(db_column='legalDistortMisunderstand', blank=True, null=True)  # Field name made lowercase.
    familymaritalstatus = models.CharField(db_column='familyMaritalStatus', max_length=100, blank=True, null=True)  # Field name made lowercase.
    familymaritalstatuslength = models.CharField(db_column='familyMaritalStatusLength', max_length=100, blank=True, null=True)  # Field name made lowercase.
    familymaritalsatisfied = models.CharField(db_column='familyMaritalSatisfied', max_length=100, blank=True, null=True)  # Field name made lowercase.
    familyliving = models.CharField(db_column='familyLiving', max_length=100, blank=True, null=True)  # Field name made lowercase.
    familylivingsince = models.CharField(db_column='familyLivingSince', max_length=100, blank=True, null=True)  # Field name made lowercase.
    familylivingsatisfied = models.CharField(db_column='familyLivingSatisfied', max_length=100, blank=True, null=True)  # Field name made lowercase.
    familylivingwithalcproblem = models.IntegerField(db_column='familyLivingWithAlcProblem', blank=True, null=True)  # Field name made lowercase.
    familylivingwithnonrxdrugs = models.IntegerField(db_column='familyLivingWithNonRxDrugs', blank=True, null=True)  # Field name made lowercase.
    familyfreetime = models.CharField(db_column='familyFreeTime', max_length=100, blank=True, null=True)  # Field name made lowercase.
    familyfreetimesatisfied = models.CharField(db_column='familyFreeTimeSatisfied', max_length=100, blank=True, null=True)  # Field name made lowercase.
    familyclosefriends = models.CharField(db_column='familyCloseFriends', max_length=100, blank=True, null=True)  # Field name made lowercase.
    familyrelationmother = models.CharField(db_column='familyRelationMother', max_length=100, blank=True, null=True)  # Field name made lowercase.
    familyrelationfather = models.CharField(db_column='familyRelationFather', max_length=100, blank=True, null=True)  # Field name made lowercase.
    familyrelationsibling = models.CharField(db_column='familyRelationSibling', max_length=100, blank=True, null=True)  # Field name made lowercase.
    familyrelationspouse = models.CharField(db_column='familyRelationSpouse', max_length=100, blank=True, null=True)  # Field name made lowercase.
    familyrelationchildren = models.CharField(db_column='familyRelationChildren', max_length=100, blank=True, null=True)  # Field name made lowercase.
    familyrelationfriends = models.CharField(db_column='familyRelationFriends', max_length=100, blank=True, null=True)  # Field name made lowercase.
    familyprobmother30 = models.IntegerField(db_column='familyProbMother30', blank=True, null=True)  # Field name made lowercase.
    familyprobmotherlife = models.IntegerField(db_column='familyProbMotherLife', blank=True, null=True)  # Field name made lowercase.
    familyprobfather30 = models.IntegerField(db_column='familyProbFather30', blank=True, null=True)  # Field name made lowercase.
    familyprobfatherlife = models.IntegerField(db_column='familyProbFatherLife', blank=True, null=True)  # Field name made lowercase.
    familyprobsiblings30 = models.IntegerField(db_column='familyProbSiblings30', blank=True, null=True)  # Field name made lowercase.
    familyprobsiblingslife = models.IntegerField(db_column='familyProbSiblingsLife', blank=True, null=True)  # Field name made lowercase.
    familyprobspouse30 = models.IntegerField(db_column='familyProbSpouse30', blank=True, null=True)  # Field name made lowercase.
    familyprobspouselife = models.IntegerField(db_column='familyProbSpouseLife', blank=True, null=True)  # Field name made lowercase.
    familyprobchildren30 = models.IntegerField(db_column='familyProbChildren30', blank=True, null=True)  # Field name made lowercase.
    familyprobchildrenlife = models.IntegerField(db_column='familyProbChildrenLife', blank=True, null=True)  # Field name made lowercase.
    familyprobother30 = models.IntegerField(db_column='familyProbOther30', blank=True, null=True)  # Field name made lowercase.
    familyprobotherlife = models.IntegerField(db_column='familyProbOtherLife', blank=True, null=True)  # Field name made lowercase.
    familyprobfriends30 = models.IntegerField(db_column='familyProbFriends30', blank=True, null=True)  # Field name made lowercase.
    familyprobfriendslife = models.IntegerField(db_column='familyProbFriendsLife', blank=True, null=True)  # Field name made lowercase.
    familyprobneighbors30 = models.IntegerField(db_column='familyProbNeighbors30', blank=True, null=True)  # Field name made lowercase.
    familyprobneighborslife = models.IntegerField(db_column='familyProbNeighborsLife', blank=True, null=True)  # Field name made lowercase.
    familyprobcoworkers30 = models.IntegerField(db_column='familyProbCoworkers30', blank=True, null=True)  # Field name made lowercase.
    familyprobcoworkerslife = models.IntegerField(db_column='familyProbCoworkersLife', blank=True, null=True)  # Field name made lowercase.
    familyabuseemotional30 = models.IntegerField(db_column='familyAbuseEmotional30', blank=True, null=True)  # Field name made lowercase.
    familyabuseemotionallife = models.IntegerField(db_column='familyAbuseEmotionalLife', blank=True, null=True)  # Field name made lowercase.
    familyabusephysical30 = models.IntegerField(db_column='familyAbusePhysical30', blank=True, null=True)  # Field name made lowercase.
    familyabusephysicallife = models.IntegerField(db_column='familyAbusePhysicalLife', blank=True, null=True)  # Field name made lowercase.
    familyabusesexual30 = models.IntegerField(db_column='familyAbuseSexual30', blank=True, null=True)  # Field name made lowercase.
    familyabusesexuallife = models.IntegerField(db_column='familyAbuseSexualLife', blank=True, null=True)  # Field name made lowercase.
    familyconflicts = models.CharField(db_column='familyConflicts', max_length=100, blank=True, null=True)  # Field name made lowercase.
    familyconflictsother = models.CharField(db_column='familyConflictsOther', max_length=100, blank=True, null=True)  # Field name made lowercase.
    familytroubledfamily = models.CharField(db_column='familyTroubledFamily', max_length=100, blank=True, null=True)  # Field name made lowercase.
    familytroubledsocial = models.CharField(db_column='familyTroubledSocial', max_length=100, blank=True, null=True)  # Field name made lowercase.
    familyfamilyimportance = models.CharField(db_column='familyFamilyImportance', max_length=100, blank=True, null=True)  # Field name made lowercase.
    familysocialimportance = models.CharField(db_column='familySocialImportance', max_length=100, blank=True, null=True)  # Field name made lowercase.
    familycounselingneed = models.CharField(db_column='familyCounselingNeed', max_length=100, blank=True, null=True)  # Field name made lowercase.
    familydistortmisrep = models.IntegerField(db_column='familyDistortMisrep', blank=True, null=True)  # Field name made lowercase.
    familydistortmisunderstand = models.IntegerField(db_column='familyDistortMisunderstand', blank=True, null=True)  # Field name made lowercase.
    psychhospitaltx = models.CharField(db_column='psychHospitalTx', max_length=100, blank=True, null=True)  # Field name made lowercase.
    psychoutpatienttx = models.CharField(db_column='psychOutpatientTx', max_length=100, blank=True, null=True)  # Field name made lowercase.
    psychpension = models.IntegerField(db_column='psychPension', blank=True, null=True)  # Field name made lowercase.
    psychdepression30 = models.IntegerField(db_column='psychDepression30', blank=True, null=True)  # Field name made lowercase.
    psychdepressionlife = models.IntegerField(db_column='psychDepressionLife', blank=True, null=True)  # Field name made lowercase.
    psychanxiety30 = models.IntegerField(db_column='psychAnxiety30', blank=True, null=True)  # Field name made lowercase.
    psychanxietylife = models.IntegerField(db_column='psychAnxietyLife', blank=True, null=True)  # Field name made lowercase.
    psychhallucinations30 = models.IntegerField(db_column='psychHallucinations30', blank=True, null=True)  # Field name made lowercase.
    psychhallucinationslife = models.IntegerField(db_column='psychHallucinationsLife', blank=True, null=True)  # Field name made lowercase.
    psychunderstanding30 = models.IntegerField(db_column='psychUnderstanding30', blank=True, null=True)  # Field name made lowercase.
    psychunderstandinglife = models.IntegerField(db_column='psychUnderstandingLife', blank=True, null=True)  # Field name made lowercase.
    psychviolent30 = models.IntegerField(db_column='psychViolent30', blank=True, null=True)  # Field name made lowercase.
    psychviolentlife = models.IntegerField(db_column='psychViolentLife', blank=True, null=True)  # Field name made lowercase.
    psychsuicide30 = models.IntegerField(db_column='psychSuicide30', blank=True, null=True)  # Field name made lowercase.
    psychsuicidelife = models.IntegerField(db_column='psychSuicideLife', blank=True, null=True)  # Field name made lowercase.
    psychsuicideattempted30 = models.IntegerField(db_column='psychSuicideAttempted30', blank=True, null=True)  # Field name made lowercase.
    psychsuicideattemptedlife = models.IntegerField(db_column='psychSuicideAttemptedLife', blank=True, null=True)  # Field name made lowercase.
    psychrx30 = models.IntegerField(db_column='psychRx30', blank=True, null=True)  # Field name made lowercase.
    psychrxlife = models.IntegerField(db_column='psychRxLife', blank=True, null=True)  # Field name made lowercase.
    psychproblemscount = models.CharField(db_column='psychProblemsCount', max_length=100, blank=True, null=True)  # Field name made lowercase.
    psychproblemstroubled = models.CharField(db_column='psychProblemsTroubled', max_length=100, blank=True, null=True)  # Field name made lowercase.
    psychproblemsimportance = models.CharField(db_column='psychProblemsImportance', max_length=100, blank=True, null=True)  # Field name made lowercase.
    psychdepressed = models.IntegerField(db_column='psychDepressed', blank=True, null=True)  # Field name made lowercase.
    psychhostile = models.IntegerField(db_column='psychHostile', blank=True, null=True)  # Field name made lowercase.
    psychanxious = models.IntegerField(db_column='psychAnxious', blank=True, null=True)  # Field name made lowercase.
    psychreality = models.IntegerField(db_column='psychReality', blank=True, null=True)  # Field name made lowercase.
    psychcomprehending = models.IntegerField(db_column='psychComprehending', blank=True, null=True)  # Field name made lowercase.
    psychsuicidal = models.IntegerField(db_column='psychSuicidal', blank=True, null=True)  # Field name made lowercase.
    psychneed = models.CharField(db_column='psychNeed', max_length=100, blank=True, null=True)  # Field name made lowercase.
    psychdistortmisrep = models.IntegerField(db_column='psychDistortMisrep', blank=True, null=True)  # Field name made lowercase.
    psychdistortmisunderstand = models.IntegerField(db_column='psychDistortMisunderstand', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'asiData'


