from __future__ import unicode_literals

from django.db import models

class Vtkdata(models.Model):
    id = models.BigAutoField(primary_key=True)
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    redstatus = models.IntegerField(db_column='redStatus', blank=True, null=True)  # Field name made lowercase.
    legalreporting = models.IntegerField(db_column='legalReporting')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'vtkData'


