from __future__ import unicode_literals

from django.db import models

class Hcfaprintsettings(models.Model):
    id = models.BigAutoField(primary_key=True)
    username = models.CharField(max_length=35)
    horizshift = models.FloatField(db_column='horizShift')  # Field name made lowercase.
    vertshift = models.FloatField(db_column='vertShift')  # Field name made lowercase.
    vertoffset = models.FloatField(db_column='vertOffset')  # Field name made lowercase.
    changetime = models.DateTimeField(db_column='changeTime')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'hcfaPrintSettings'


