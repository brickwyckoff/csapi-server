from __future__ import unicode_literals

from django.db import models

class Familyhxmerged(models.Model):
    mrn = models.IntegerField(blank=True, null=True)
    hxid = models.BigIntegerField(db_column='hxId', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'familyHxMerged'


