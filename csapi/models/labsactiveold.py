from __future__ import unicode_literals

from django.db import models

class Labsactiveold(models.Model):
    code = models.CharField(max_length=30)

    class Meta:
        managed = False
        db_table = 'labsActiveOld'


