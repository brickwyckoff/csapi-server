from __future__ import unicode_literals

from django.db import models

class Sigdata(models.Model):
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    provider = models.CharField(max_length=50, blank=True, null=True)
    providertime = models.DateTimeField(db_column='providerTime', blank=True, null=True)  # Field name made lowercase.
    supervisor = models.CharField(max_length=50, blank=True, null=True)
    supervisortime = models.DateTimeField(db_column='supervisorTime', blank=True, null=True)  # Field name made lowercase.
    socialworker = models.CharField(db_column='socialWorker', max_length=50, blank=True, null=True)  # Field name made lowercase.
    socialworkertime = models.DateTimeField(db_column='socialWorkerTime', blank=True, null=True)  # Field name made lowercase.
    cosigner = models.CharField(max_length=50, blank=True, null=True)
    cosignertime = models.DateTimeField(db_column='cosignerTime', blank=True, null=True)  # Field name made lowercase.
    labsupervisor = models.CharField(db_column='labSupervisor', max_length=50, blank=True, null=True)  # Field name made lowercase.
    labsupervisortime = models.DateTimeField(db_column='labSupervisorTime')  # Field name made lowercase.
    supervisoronsite = models.IntegerField(db_column='supervisorOnSite', blank=True, null=True)  # Field name made lowercase.
    attester = models.CharField(max_length=50, blank=True, null=True)
    attestertime = models.DateTimeField(db_column='attesterTime')  # Field name made lowercase.
    nurse = models.CharField(max_length=50, blank=True, null=True)
    nursetime = models.DateTimeField(db_column='nurseTime', blank=True, null=True)  # Field name made lowercase.
    insuite = models.CharField(db_column='inSuite', max_length=50, blank=True, null=True)  # Field name made lowercase.
    insuitetime = models.DateTimeField(db_column='inSuiteTime', blank=True, null=True)  # Field name made lowercase.
    socialworkersup = models.CharField(db_column='socialWorkerSup', max_length=50, blank=True, null=True)  # Field name made lowercase.
    socialworkersuptime = models.DateTimeField(db_column='socialWorkerSupTime')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'sigData'


