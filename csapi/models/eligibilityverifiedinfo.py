from __future__ import unicode_literals

from django.db import models

class Eligibilityverifiedinfo(models.Model):
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    firstname = models.CharField(db_column='firstName', max_length=60, blank=True, null=True)  # Field name made lowercase.
    lastname = models.CharField(db_column='lastName', max_length=60, blank=True, null=True)  # Field name made lowercase.
    address1 = models.CharField(max_length=60, blank=True, null=True)
    address2 = models.CharField(max_length=60, blank=True, null=True)
    city = models.CharField(max_length=60, blank=True, null=True)
    state = models.CharField(max_length=2, blank=True, null=True)
    zip = models.CharField(max_length=10, blank=True, null=True)
    dob = models.DateField(blank=True, null=True)
    gender = models.CharField(max_length=1, blank=True, null=True)
    insco = models.CharField(db_column='insCo', max_length=60, blank=True, null=True)  # Field name made lowercase.
    policynum = models.CharField(db_column='policyNum', max_length=20, blank=True, null=True)  # Field name made lowercase.
    creationtime = models.DateTimeField(db_column='creationTime', blank=True, null=True)  # Field name made lowercase.
    depfirstname = models.CharField(db_column='depFirstName', max_length=60)  # Field name made lowercase.
    deplastname = models.CharField(db_column='depLastName', max_length=60)  # Field name made lowercase.
    depaddress1 = models.CharField(db_column='depAddress1', max_length=60)  # Field name made lowercase.
    depaddress2 = models.CharField(db_column='depAddress2', max_length=60)  # Field name made lowercase.
    depcity = models.CharField(db_column='depCity', max_length=60)  # Field name made lowercase.
    depstate = models.CharField(db_column='depState', max_length=2)  # Field name made lowercase.
    depzip = models.CharField(db_column='depZip', max_length=11)  # Field name made lowercase.
    depdob = models.DateField(db_column='depDob')  # Field name made lowercase.
    depgender = models.CharField(db_column='depGender', max_length=1)  # Field name made lowercase.
    otherpayer = models.CharField(db_column='otherPayer', max_length=60)  # Field name made lowercase.
    controlnumber = models.CharField(db_column='controlNumber', max_length=20)  # Field name made lowercase.
    verifiedid = models.BigIntegerField(db_column='verifiedId')  # Field name made lowercase.
    otherpayerpolicynum = models.CharField(db_column='otherPayerPolicyNum', max_length=40)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'eligibilityVerifiedInfo'


