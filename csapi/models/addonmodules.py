from __future__ import unicode_literals

from django.db import models

class Addonmodules(models.Model):
    name = models.CharField(max_length=150, blank=True, null=True)
    file = models.CharField(max_length=150, blank=True, null=True)
    active = models.IntegerField(blank=True, null=True)
    ordinal = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'addOnModules'


