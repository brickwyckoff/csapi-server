from __future__ import unicode_literals

from django.db import models

class Alertmessages(models.Model):
    mrn = models.BigIntegerField(blank=True, null=True)
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    message = models.CharField(max_length=500, blank=True, null=True)
    isviewed = models.IntegerField(db_column='isViewed', blank=True, null=True)  # Field name made lowercase.
    accesslevel = models.IntegerField(db_column='accessLevel', blank=True, null=True)  # Field name made lowercase.
    creationtime = models.DateTimeField(db_column='creationTime', blank=True, null=True)  # Field name made lowercase.
    dismissedby = models.CharField(db_column='dismissedBy', max_length=60, blank=True, null=True)  # Field name made lowercase.
    dismissedtime = models.DateTimeField(db_column='dismissedTime', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'alertMessages'


