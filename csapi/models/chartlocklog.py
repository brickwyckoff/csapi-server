from __future__ import unicode_literals

from django.db import models

class Chartlocklog(models.Model):
    id = models.BigAutoField(primary_key=True)
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    user = models.CharField(max_length=30, blank=True, null=True)
    lockmessage = models.CharField(db_column='lockMessage', max_length=100, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'chartLockLog'


