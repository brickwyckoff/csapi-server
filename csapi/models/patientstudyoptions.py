from __future__ import unicode_literals

from django.db import models

class Patientstudyoptions(models.Model):
    studyname = models.CharField(db_column='studyName', max_length=100, blank=True, null=True)  # Field name made lowercase.
    isactive = models.IntegerField(db_column='isActive', blank=True, null=True)  # Field name made lowercase.
    barcolor = models.CharField(db_column='barColor', max_length=6, blank=True, null=True)  # Field name made lowercase.
    displayonreport = models.IntegerField(db_column='displayOnReport', blank=True, null=True)  # Field name made lowercase.
    performeval = models.IntegerField(db_column='performEval', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'patientStudyOptions'


