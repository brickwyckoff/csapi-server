from __future__ import unicode_literals

from django.db import models

class Aftercare(models.Model):
    filename = models.CharField(db_column='fileName', max_length=150, blank=True, null=True)  # Field name made lowercase.
    sheetname = models.CharField(db_column='sheetName', max_length=150, blank=True, null=True)  # Field name made lowercase.
    keyterms = models.CharField(db_column='keyTerms', max_length=500, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'aftercare'


