from __future__ import unicode_literals

from django.db import models

class Patientremindersuse(models.Model):
    reminderid = models.IntegerField(db_column='reminderId', blank=True, null=True)  # Field name made lowercase.
    reminderfreq = models.IntegerField(db_column='reminderFreq', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'patientRemindersUse'


