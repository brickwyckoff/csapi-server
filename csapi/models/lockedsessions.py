from __future__ import unicode_literals

from django.db import models

class Lockedsessions(models.Model):
    sessionid = models.CharField(db_column='sessionId', max_length=100, blank=True, null=True)  # Field name made lowercase.
    user = models.CharField(max_length=60, blank=True, null=True)
    creationtime = models.DateTimeField(db_column='creationTime', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'lockedSessions'


