from __future__ import unicode_literals

from django.db import models

class Logview(models.Model):
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    mrn = models.BigIntegerField(blank=True, null=True)
    user = models.CharField(max_length=30, blank=True, null=True)
    timein = models.DateTimeField(db_column='timeIn', blank=True, null=True)  # Field name made lowercase.
    timeout = models.DateTimeField(db_column='timeOut', blank=True, null=True)  # Field name made lowercase.
    ip = models.CharField(max_length=15)

    class Meta:
        managed = False
        db_table = 'logView'


