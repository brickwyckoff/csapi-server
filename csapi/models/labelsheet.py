from __future__ import unicode_literals

from django.db import models

class Labelsheet(models.Model):
    id = models.BigAutoField(primary_key=True)
    labelcode = models.CharField(db_column='labelCode', max_length=4)  # Field name made lowercase.
    labeldesc = models.CharField(db_column='labelDesc', max_length=50)  # Field name made lowercase.
    isactive = models.IntegerField(db_column='isActive')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'labelSheet'


