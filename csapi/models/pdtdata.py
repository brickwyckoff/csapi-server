from __future__ import unicode_literals

from django.db import models

class Pdtdata(models.Model):
    id = models.BigAutoField(primary_key=True)
    overallfunctioning = models.IntegerField(db_column='overallFunctioning')  # Field name made lowercase.
    visitnum = models.BigIntegerField(db_column='visitNum')  # Field name made lowercase.
    painlevel = models.IntegerField(db_column='painLevel')  # Field name made lowercase.
    weeklypainlevel = models.IntegerField(db_column='weeklyPainLevel')  # Field name made lowercase.
    painpercent = models.CharField(db_column='painPercent', max_length=35)  # Field name made lowercase.
    painreliefdifference = models.CharField(db_column='painReliefDifference', max_length=2)  # Field name made lowercase.
    currentregimen = models.IntegerField(db_column='currentRegimen')  # Field name made lowercase.
    clinicallysigrelief = models.IntegerField(db_column='clinicallySigRelief')  # Field name made lowercase.
    sleeppatterns = models.IntegerField(db_column='sleepPatterns')  # Field name made lowercase.
    mood = models.IntegerField()
    socialrelationships = models.IntegerField(db_column='socialRelationships')  # Field name made lowercase.
    familyrelationships = models.IntegerField(db_column='familyRelationships')  # Field name made lowercase.
    physicalfunctioning = models.IntegerField(db_column='physicalFunctioning')  # Field name made lowercase.
    sideeffects = models.IntegerField(db_column='sideEffects')  # Field name made lowercase.
    nausea = models.IntegerField()
    vomiting = models.IntegerField()
    constipation = models.IntegerField()
    itching = models.IntegerField()
    mental = models.IntegerField()
    sweating = models.IntegerField()
    fatigue = models.IntegerField()
    drowsiness = models.IntegerField()
    other = models.TextField()
    purpose = models.IntegerField()
    negmood = models.IntegerField(db_column='negMood')  # Field name made lowercase.
    intoxicated = models.IntegerField()
    unkempt = models.IntegerField()
    accident = models.IntegerField()
    frequentrenew = models.IntegerField(db_column='frequentRenew')  # Field name made lowercase.
    increaseddose = models.IntegerField(db_column='increasedDose')  # Field name made lowercase.
    reportsstolen = models.IntegerField(db_column='reportsStolen')  # Field name made lowercase.
    attemptsother = models.IntegerField(db_column='attemptsOther')  # Field name made lowercase.
    routechange = models.IntegerField(db_column='routeChange')  # Field name made lowercase.
    painmed = models.IntegerField(db_column='painMed')  # Field name made lowercase.
    namedrop = models.IntegerField(db_column='nameDrop')  # Field name made lowercase.
    victim = models.IntegerField()
    arrested = models.IntegerField()
    hoarding = models.IntegerField()
    abusedrugs = models.IntegerField(db_column='abuseDrugs')  # Field name made lowercase.
    streetculture = models.IntegerField(db_column='streetCulture')  # Field name made lowercase.
    otheraberrant = models.TextField(db_column='otherAberrant')  # Field name made lowercase.
    opioid = models.IntegerField()
    assessmentcomments = models.TextField(db_column='assessmentComments')  # Field name made lowercase.
    continue_field = models.IntegerField(db_column='continue')  # Field renamed because it was a Python reserved word.
    adjustdose = models.IntegerField(db_column='adjustDose')  # Field name made lowercase.
    switch = models.IntegerField()
    adjust = models.IntegerField()
    taper = models.IntegerField()
    additional = models.TextField()

    class Meta:
        managed = False
        db_table = 'pdtData'


