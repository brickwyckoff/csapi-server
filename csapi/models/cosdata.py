from __future__ import unicode_literals

from django.db import models

class Cosdata(models.Model):
    cosigner = models.CharField(max_length=20, blank=True, null=True)
    clinicloc = models.IntegerField(db_column='clinicLoc', blank=True, null=True)  # Field name made lowercase.
    day = models.IntegerField(blank=True, null=True)
    starttime = models.TimeField(db_column='startTime', blank=True, null=True)  # Field name made lowercase.
    endtime = models.TimeField(db_column='endTime', blank=True, null=True)  # Field name made lowercase.
    date = models.DateField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cosData'


