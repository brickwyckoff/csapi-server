from __future__ import unicode_literals

from django.db import models

class Printableformslog(models.Model):
    id = models.BigAutoField(primary_key=True)
    mrn = models.BigIntegerField(blank=True, null=True)
    formtype = models.CharField(db_column='formType', max_length=35, blank=True, null=True)  # Field name made lowercase.
    creationtime = models.DateTimeField(db_column='creationTime', blank=True, null=True)  # Field name made lowercase.
    sendmethod = models.CharField(db_column='sendMethod', max_length=1, blank=True, null=True)  # Field name made lowercase.
    user = models.CharField(max_length=35, blank=True, null=True)
    filename = models.CharField(db_column='fileName', max_length=50)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'printableFormsLog'


