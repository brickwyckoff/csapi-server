from __future__ import unicode_literals

from django.db import models

class Schedule(models.Model):
    mrn = models.BigIntegerField(blank=True, null=True)
    visitdate = models.DateTimeField(db_column='visitDate', blank=True, null=True)  # Field name made lowercase.
    reason = models.CharField(max_length=150, blank=True, null=True)
    user = models.CharField(max_length=50, blank=True, null=True)
    changetime = models.DateTimeField(db_column='changeTime', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'schedule'


