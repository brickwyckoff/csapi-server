from __future__ import unicode_literals

from django.db import models

class Erxservicelevels(models.Model):
    bitvalue = models.IntegerField(db_column='bitValue', blank=True, null=True)  # Field name made lowercase.
    specialtydescription = models.CharField(db_column='specialtyDescription', max_length=50, blank=True, null=True)  # Field name made lowercase.
    isactive = models.IntegerField(db_column='isActive', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'erxServiceLevels'


