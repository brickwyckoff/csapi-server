from __future__ import unicode_literals

from django.db import models

class Languagesactive(models.Model):
    id = models.BigAutoField(primary_key=True)
    alphathreecode = models.CharField(db_column='alphaThreeCode', max_length=3)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'languagesActive'


