from __future__ import unicode_literals

from django.db import models

class Commonmedications(models.Model):
    typecode = models.CharField(db_column='typeCode', max_length=15, blank=True, null=True)  # Field name made lowercase.
    drugid = models.CharField(db_column='drugId', max_length=10, blank=True, null=True)  # Field name made lowercase.
    tradeid = models.IntegerField(db_column='tradeId', blank=True, null=True)  # Field name made lowercase.
    doseform = models.IntegerField(db_column='doseForm', blank=True, null=True)  # Field name made lowercase.
    route = models.IntegerField(blank=True, null=True)
    strength = models.IntegerField(blank=True, null=True)
    freetext = models.CharField(db_column='freeText', max_length=80, blank=True, null=True)  # Field name made lowercase.
    genericbrand = models.CharField(db_column='genericBrand', max_length=1, blank=True, null=True)  # Field name made lowercase.
    rxotc = models.CharField(db_column='rxOtc', max_length=6, blank=True, null=True)  # Field name made lowercase.
    cvxcode = models.CharField(db_column='cvxCode', max_length=5, blank=True, null=True)  # Field name made lowercase.
    freq = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'commonMedications'


