from django.db import models
from csapi.models.batchEligResult import BatchEligResult

class BatchEligHeader (models.Model):
    class Meta:
        db_table = 'batchEligHeader'
        managed = True

    id = models.AutoField (auto_created=True, primary_key=True, serialize=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    # active = models.BinaryField (default=0)
    status = models.CharField (max_length=16)
    rejectionReason = models.CharField (max_length=100)
    subscriberFirst = models.CharField (max_length=100)
    subscriberLast = models.CharField (max_length=100)
    subscriberMiddle = models.CharField (max_length=100)
    subscriberGender = models.CharField (max_length=6)
    subscriberDob = models.CharField (max_length=10)
    subscriberAddress1 = models.CharField (max_length=100)
    subscriberAddress2 = models.CharField (max_length=100)
    subscriberCity = models.CharField (max_length=100)
    subscriberState = models.CharField (max_length=100)
    subscriberZip = models.CharField (max_length=10)
    subscriberPrimaryId = models.CharField (max_length=32)
    subscriberPrimaryactive = models.BinaryField (default=0)
    subscriberPrimaryGroupNumber = models.CharField (max_length=32)
    subscriberPrimaryGroupDescription = models.CharField (max_length=100)
    subscriberPrimaryCompany = models.CharField (max_length=100)
    subscriberPrimaryPlanDescription = models.CharField (max_length=100)
    subscriberPrimaryBeginDate = models.CharField (max_length=10)
    subscriberPrimaryEndDate = models.CharField (max_length=10)

    batch = models.ForeignKey(BatchEligResult, to_field='id')


