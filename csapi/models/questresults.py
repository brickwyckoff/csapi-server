from __future__ import unicode_literals

from django.db import models

class Questresults(models.Model):
    filename = models.CharField(db_column='fileName', max_length=60, blank=True, null=True)  # Field name made lowercase.
    downloadtime = models.DateTimeField(db_column='downloadTime', blank=True, null=True)  # Field name made lowercase.
    processed = models.IntegerField(blank=True, null=True)
    sentreceipt = models.IntegerField(db_column='sentReceipt', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'questResults'


