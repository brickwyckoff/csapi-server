from __future__ import unicode_literals

from django.db import models

class Checkupdatelog(models.Model):
    mrn = models.BigIntegerField(blank=True, null=True)
    checktime = models.DateTimeField(db_column='checkTime', blank=True, null=True)  # Field name made lowercase.
    checktype = models.CharField(db_column='checkType', max_length=15, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'checkUpdateLog'


