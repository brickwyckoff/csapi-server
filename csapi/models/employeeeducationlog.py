from __future__ import unicode_literals

from django.db import models

class Employeeeducationlog(models.Model):
    id = models.BigAutoField(primary_key=True)
    edid = models.BigIntegerField(db_column='edId', blank=True, null=True)  # Field name made lowercase.
    user = models.CharField(max_length=35, blank=True, null=True)
    viewtime = models.DateTimeField(db_column='viewTime', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'employeeEducationLog'


