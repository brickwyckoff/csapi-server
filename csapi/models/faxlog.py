from __future__ import unicode_literals

from django.db import models

class Faxlog(models.Model):
    id = models.BigAutoField(primary_key=True)
    faxid = models.CharField(db_column='faxId', max_length=60)  # Field name made lowercase.
    faxstatus = models.IntegerField(db_column='faxStatus')  # Field name made lowercase.
    faxstatuscode = models.CharField(db_column='faxStatusCode', max_length=15)  # Field name made lowercase.
    faxstatusdesc = models.CharField(db_column='faxStatusDesc', max_length=100)  # Field name made lowercase.
    faxerrorcode = models.CharField(db_column='faxErrorCode', max_length=15)  # Field name made lowercase.
    faxto = models.CharField(db_column='faxTo', max_length=100)  # Field name made lowercase.
    faxtonumber = models.CharField(db_column='faxToNumber', max_length=11)  # Field name made lowercase.
    faxtime = models.DateTimeField(db_column='faxTime')  # Field name made lowercase.
    faxfile = models.CharField(db_column='faxFile', max_length=150)  # Field name made lowercase.
    logtable = models.CharField(db_column='logTable', max_length=50)  # Field name made lowercase.
    logid = models.BigIntegerField(db_column='logId')  # Field name made lowercase.
    originalfaxid = models.BigIntegerField(db_column='originalFaxId')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'faxLog'


