from __future__ import unicode_literals

from django.db import models

class Cnslogdata(models.Model):
    date = models.DateTimeField()
    mrn = models.IntegerField(blank=True, null=True)
    visitnum = models.IntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    counselor = models.CharField(max_length=30, blank=True, null=True)
    note = models.CharField(max_length=500, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cnsLogData'


