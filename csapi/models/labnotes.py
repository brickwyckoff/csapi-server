from __future__ import unicode_literals

from django.db import models

class Labnotes(models.Model):
    mrn = models.BigIntegerField(blank=True, null=True)
    note = models.CharField(max_length=1000, blank=True, null=True)
    user = models.CharField(max_length=50, blank=True, null=True)
    creationtime = models.DateTimeField(db_column='creationTime', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'labNotes'


