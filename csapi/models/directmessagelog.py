from __future__ import unicode_literals

from django.db import models

class Directmessagelog(models.Model):
    id = models.BigAutoField(primary_key=True)
    messageto = models.CharField(db_column='messageTo', max_length=150)  # Field name made lowercase.
    messagefrom = models.CharField(db_column='messageFrom', max_length=150)  # Field name made lowercase.
    messagesubject = models.CharField(db_column='messageSubject', max_length=150)  # Field name made lowercase.
    messagebody = models.CharField(db_column='messageBody', max_length=5000)  # Field name made lowercase.
    filename = models.CharField(db_column='fileName', max_length=200)  # Field name made lowercase.
    attachmentfilename = models.CharField(db_column='attachmentFileName', max_length=150)  # Field name made lowercase.
    isread = models.IntegerField(db_column='isRead')  # Field name made lowercase.
    creationtime = models.DateTimeField(db_column='creationTime')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'directMessageLog'


