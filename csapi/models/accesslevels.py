from __future__ import unicode_literals

from django.db import models

class Accesslevels(models.Model):
    levelcode = models.IntegerField(db_column='levelCode', blank=True, null=True)  # Field name made lowercase.
    leveldescription = models.CharField(db_column='levelDescription', max_length=25, blank=True, null=True)  # Field name made lowercase.
    defaulttab = models.IntegerField(db_column='defaultTab', blank=True, null=True)  # Field name made lowercase.
    remoteaccess = models.IntegerField(db_column='remoteAccess', blank=True, null=True)  # Field name made lowercase.
    reportsonly = models.IntegerField(db_column='reportsOnly', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'accessLevels'


