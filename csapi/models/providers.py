from __future__ import unicode_literals

from django.db import models

class Providers(models.Model):
    firstname = models.CharField(db_column='firstName', max_length=20, blank=True, null=True)  # Field name made lowercase.
    lastname = models.CharField(db_column='lastName', max_length=40, blank=True, null=True)  # Field name made lowercase.
    title = models.CharField(max_length=3, blank=True, null=True)
    gender = models.CharField(max_length=1, blank=True, null=True)
    malicense = models.CharField(db_column='MALicense', max_length=6, blank=True, null=True)  # Field name made lowercase.
    feddea = models.CharField(db_column='fedDEA', max_length=20, blank=True, null=True)  # Field name made lowercase.
    madea = models.CharField(db_column='MADEA', max_length=12, blank=True, null=True)  # Field name made lowercase.
    npi = models.CharField(db_column='NPI', max_length=12, blank=True, null=True)  # Field name made lowercase.
    code = models.CharField(max_length=20, blank=True, null=True)
    supcode = models.CharField(db_column='supCode', max_length=2, blank=True, null=True)  # Field name made lowercase.
    issup = models.IntegerField(db_column='isSup', blank=True, null=True)  # Field name made lowercase.
    reqsup = models.IntegerField(db_column='reqSup', blank=True, null=True)  # Field name made lowercase.
    cosigner = models.CharField(max_length=2, blank=True, null=True)
    issub = models.IntegerField(db_column='isSub', blank=True, null=True)  # Field name made lowercase.
    signature = models.CharField(max_length=30, blank=True, null=True)
    amaspecialty = models.CharField(db_column='amaSpecialty', max_length=3, blank=True, null=True)  # Field name made lowercase.
    deaspecialty = models.CharField(db_column='deaSpecialty', max_length=3, blank=True, null=True)  # Field name made lowercase.
    eprescriber = models.IntegerField(db_column='ePrescriber', blank=True, null=True)  # Field name made lowercase.
    surescriptsmailbox = models.CharField(db_column='sureScriptsMailbox', max_length=10, blank=True, null=True)  # Field name made lowercase.
    surescriptsspi = models.CharField(db_column='sureScriptsSpi', max_length=20, blank=True, null=True)  # Field name made lowercase.
    rxsup = models.IntegerField(db_column='rxSup', blank=True, null=True)  # Field name made lowercase.
    norxsup = models.IntegerField(db_column='noRxSup', blank=True, null=True)  # Field name made lowercase.
    maxsuboxonepatients = models.IntegerField(db_column='maxSuboxonePatients', blank=True, null=True)  # Field name made lowercase.
    xdea = models.CharField(db_column='XDEA', max_length=20)  # Field name made lowercase.
    numauthrefills = models.IntegerField(db_column='numAuthRefills')  # Field name made lowercase.
    mainclinic = models.IntegerField(db_column='mainClinic')  # Field name made lowercase.
    cosignexpirationtime = models.IntegerField(db_column='cosignExpirationTime')  # Field name made lowercase.
    labsup = models.IntegerField(db_column='labSup', blank=True, null=True)  # Field name made lowercase.
    middlename = models.CharField(db_column='middleName', max_length=35)  # Field name made lowercase.
    feddea2 = models.CharField(db_column='fedDEA2', max_length=20, blank=True, null=True)  # Field name made lowercase.
    txplansupervisor = models.CharField(db_column='txPlanSupervisor', max_length=2)  # Field name made lowercase.
    taxonomycode = models.CharField(db_column='taxonomyCode', max_length=15, blank=True, null=True)  # Field name made lowercase.
    ispsych = models.IntegerField(db_column='isPsych')  # Field name made lowercase.
    signcode = models.CharField(db_column='signCode', max_length=15, blank=True, null=True)  # Field name made lowercase.
    ismeddirector = models.IntegerField(db_column='isMedDirector', blank=True, null=True)  # Field name made lowercase.
    iscertaddiction = models.IntegerField(db_column='isCertAddiction', blank=True, null=True)  # Field name made lowercase.
    iscertpsych = models.IntegerField(db_column='isCertPsych', blank=True, null=True)  # Field name made lowercase.
    basepay = models.CharField(db_column='basePay', max_length=500, blank=True, null=True)  # Field name made lowercase.
    revenuepercent = models.DecimalField(db_column='revenuePercent', max_digits=5, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    dob = models.DateField(blank=True, null=True)
    blockedtreatmentlevels = models.CharField(db_column='blockedTreatmentLevels', max_length=100, blank=True, null=True)  # Field name made lowercase.
    blockedvisittypes = models.CharField(db_column='blockedVisitTypes', max_length=100, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'providers'


