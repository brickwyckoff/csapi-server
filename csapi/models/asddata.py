from __future__ import unicode_literals

from django.db import models

class Asddata(models.Model):
    id = models.BigAutoField(primary_key=True)
    visitnum = models.BigIntegerField(db_column='visitNum')  # Field name made lowercase.
    medicalproblems = models.TextField(db_column='medicalProblems')  # Field name made lowercase.
    hospitalized = models.TextField()
    currentrx = models.TextField(db_column='currentRx')  # Field name made lowercase.
    surgeries = models.TextField()
    chronicpain = models.TextField(db_column='chronicPain')  # Field name made lowercase.
    pregnant = models.TextField()
    disability = models.TextField()
    smoking = models.TextField()
    injection = models.TextField()
    hivheptest = models.TextField(db_column='hivHepTest')  # Field name made lowercase.
    hiv = models.IntegerField()
    hep = models.IntegerField()
    allergies = models.TextField()
    painpill = models.TextField(db_column='painPill')  # Field name made lowercase.
    energyincrease = models.TextField(db_column='energyIncrease')  # Field name made lowercase.
    abuse = models.TextField()
    age = models.TextField()
    lifedrugs = models.TextField(db_column='lifeDrugs')  # Field name made lowercase.
    daysdrugs = models.TextField(db_column='daysDrugs')  # Field name made lowercase.
    snort = models.TextField()
    majorproblem = models.TextField(db_column='majorProblem')  # Field name made lowercase.
    detox = models.TextField()
    over = models.TextField()
    rehab = models.TextField()
    drugfree = models.TextField(db_column='drugFree')  # Field name made lowercase.
    feel = models.TextField()
    firstpill = models.TextField(db_column='firstPill')  # Field name made lowercase.
    desz = models.TextField()
    law = models.TextField()
    charges = models.TextField()
    jail = models.TextField()
    pending = models.TextField()
    ged = models.TextField()
    current = models.TextField()
    kind = models.TextField()
    goingback = models.TextField(db_column='goingBack')  # Field name made lowercase.
    futureplans = models.TextField(db_column='futurePlans')  # Field name made lowercase.
    car = models.TextField()
    married = models.TextField()
    child = models.TextField()
    livingelse = models.TextField(db_column='livingElse')  # Field name made lowercase.
    household = models.TextField()
    doesanyone = models.TextField(db_column='doesAnyone')  # Field name made lowercase.
    abusive = models.TextField()
    parents = models.TextField()
    goodrelation = models.TextField(db_column='goodRelation')  # Field name made lowercase.
    parentsusers = models.TextField(db_column='parentsUsers')  # Field name made lowercase.
    questionabuse = models.TextField(db_column='questionAbuse')  # Field name made lowercase.
    whyhow = models.TextField(db_column='whyHow')  # Field name made lowercase.
    haveare = models.TextField(db_column='haveAre')  # Field name made lowercase.
    depressionsuicide = models.TextField(db_column='depressionSuicide')  # Field name made lowercase.
    pastrx = models.TextField(db_column='pastRx')  # Field name made lowercase.
    gen = models.TextField()
    heent = models.TextField()
    lung = models.TextField()
    heart = models.TextField()
    abs = models.TextField()
    skin = models.TextField()
    psych = models.TextField()
    neuro = models.TextField()
    cond = models.TextField()
    subab = models.TextField(db_column='subAb')  # Field name made lowercase.
    leg = models.TextField()
    emped = models.TextField(db_column='empEd')  # Field name made lowercase.
    fsc = models.TextField()
    psycom = models.TextField(db_column='psyCom')  # Field name made lowercase.
    psysoc = models.TextField(db_column='psySoc')  # Field name made lowercase.
    medplan = models.TextField(db_column='medPlan')  # Field name made lowercase.
    masmed = models.TextField(db_column='masMed')  # Field name made lowercase.
    masdrug = models.TextField(db_column='masDrug')  # Field name made lowercase.
    masleg = models.TextField(db_column='masLeg')  # Field name made lowercase.
    mased = models.TextField(db_column='masEd')  # Field name made lowercase.
    masfam = models.TextField(db_column='masFam')  # Field name made lowercase.
    maspsy = models.TextField(db_column='masPsy')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'asdData'


