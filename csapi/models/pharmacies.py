from __future__ import unicode_literals

from django.db import models

class Pharmacies(models.Model):
    name = models.CharField(max_length=60, blank=True, null=True)
    address1 = models.CharField(max_length=60, blank=True, null=True)
    address2 = models.CharField(max_length=60, blank=True, null=True)
    city = models.CharField(max_length=60, blank=True, null=True)
    state = models.CharField(max_length=2, blank=True, null=True)
    zip = models.CharField(max_length=10, blank=True, null=True)
    fax = models.BigIntegerField(blank=True, null=True)
    phone = models.BigIntegerField(blank=True, null=True)
    closestclinic = models.IntegerField(db_column='closestClinic', blank=True, null=True)  # Field name made lowercase.
    pharmsortorder = models.IntegerField(db_column='pharmSortOrder', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'pharmacies'


