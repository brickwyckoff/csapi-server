from __future__ import unicode_literals

from django.db import models

class Inscotable(models.Model):
    company = models.CharField(max_length=100, blank=True, null=True)
    address1 = models.CharField(max_length=60, blank=True, null=True)
    address2 = models.CharField(max_length=60, blank=True, null=True)
    city = models.CharField(max_length=60, blank=True, null=True)
    state = models.CharField(max_length=2, blank=True, null=True)
    zip = models.CharField(max_length=10, blank=True, null=True)
    payorid = models.CharField(db_column='payorId', max_length=10, blank=True, null=True)  # Field name made lowercase.
    payoridqual = models.CharField(db_column='payorIdQual', max_length=2)  # Field name made lowercase.
    eligibilitycheck = models.IntegerField(db_column='eligibilityCheck', blank=True, null=True)  # Field name made lowercase.
    companyconverted = models.CharField(db_column='companyConverted', max_length=50)  # Field name made lowercase.
    phone = models.CharField(max_length=15)
    fax = models.CharField(max_length=15)
    eligibilityinsconame = models.CharField(db_column='eligibilityInsCoName', max_length=35)  # Field name made lowercase.
    eligversion = models.CharField(db_column='eligVersion', max_length=20)  # Field name made lowercase.
    eligusageindicator = models.CharField(db_column='eligUsageIndicator', max_length=1)  # Field name made lowercase.
    eligsubmitterheaderidqual = models.CharField(db_column='eligSubmitterHeaderIdQual', max_length=2)  # Field name made lowercase.
    eligsubmitterheaderid = models.CharField(db_column='eligSubmitterHeaderId', max_length=35)  # Field name made lowercase.
    eligreceiverentitycode = models.CharField(db_column='eligReceiverEntityCode', max_length=2)  # Field name made lowercase.
    eligreceivername = models.CharField(db_column='eligReceiverName', max_length=35)  # Field name made lowercase.
    eligreceiveridqual = models.CharField(db_column='eligReceiverIdQual', max_length=2)  # Field name made lowercase.
    eligreceiverid = models.CharField(db_column='eligReceiverId', max_length=35)  # Field name made lowercase.
    eligreceiverheaderidqual = models.CharField(db_column='eligReceiverHeaderIdQual', max_length=2)  # Field name made lowercase.
    eligreceiverheaderid = models.CharField(db_column='eligReceiverHeaderId', max_length=35)  # Field name made lowercase.
    eligforceself = models.IntegerField(db_column='eligForceSelf')  # Field name made lowercase.
    pamod = models.CharField(db_column='paMod', max_length=5, blank=True, null=True)  # Field name made lowercase.
    carriertype = models.IntegerField(db_column='carrierType', blank=True, null=True)  # Field name made lowercase.
    billnoprovider = models.IntegerField(db_column='billNoProvider', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'insCoTable'


