from __future__ import unicode_literals

from django.db import models

class Payproscharges(models.Model):
    mrn = models.BigIntegerField(blank=True, null=True)
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    transactionid = models.BigIntegerField(db_column='transactionId', blank=True, null=True)  # Field name made lowercase.
    referenceid = models.BigIntegerField(db_column='referenceId', blank=True, null=True)  # Field name made lowercase.
    chargeamt = models.FloatField(db_column='chargeAmt', blank=True, null=True)  # Field name made lowercase.
    chargetype = models.CharField(db_column='chargeType', max_length=15, blank=True, null=True)  # Field name made lowercase.
    responsecode = models.CharField(db_column='responseCode', max_length=15, blank=True, null=True)  # Field name made lowercase.
    responsetext = models.CharField(db_column='responseText', max_length=200, blank=True, null=True)  # Field name made lowercase.
    bankapprovalcode = models.CharField(db_column='bankApprovalCode', max_length=15, blank=True, null=True)  # Field name made lowercase.
    banktransactionid = models.CharField(db_column='bankTransactionId', max_length=100, blank=True, null=True)  # Field name made lowercase.
    ccnum = models.CharField(db_column='ccNum', max_length=20, blank=True, null=True)  # Field name made lowercase.
    ccexpdate = models.CharField(db_column='ccExpDate', max_length=10, blank=True, null=True)  # Field name made lowercase.
    user = models.CharField(max_length=35, blank=True, null=True)
    txtime = models.DateTimeField(db_column='txTime', blank=True, null=True)  # Field name made lowercase.
    isvoided = models.IntegerField(db_column='isVoided', blank=True, null=True)  # Field name made lowercase.
    accounttype = models.CharField(db_column='accountType', max_length=3, blank=True, null=True)  # Field name made lowercase.
    accountclass = models.CharField(db_column='accountClass', max_length=3, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'payProsCharges'


