from __future__ import unicode_literals

from django.db import models

class Sharedpatients(models.Model):
    mrn = models.BigIntegerField(blank=True, null=True)
    childdb = models.CharField(db_column='childDb', max_length=30, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'sharedPatients'


