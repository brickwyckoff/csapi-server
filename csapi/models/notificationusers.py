from __future__ import unicode_literals

from django.db import models

class Notificationusers(models.Model):
    username = models.CharField(max_length=35, blank=True, null=True)
    cellphone = models.CharField(db_column='cellPhone', max_length=10, blank=True, null=True)  # Field name made lowercase.
    email = models.CharField(max_length=80, blank=True, null=True)
    notificationtype = models.CharField(db_column='notificationType', max_length=35, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'notificationUsers'


