from __future__ import unicode_literals

from django.db import models

class Lockablebuttons(models.Model):
    buttonid = models.CharField(db_column='buttonId', max_length=60, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'lockableButtons'


