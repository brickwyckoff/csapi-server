from __future__ import unicode_literals

from django.db import models

class Accountingold(models.Model):
    type = models.CharField(max_length=60, blank=True, null=True)
    claimamt = models.FloatField(db_column='claimAmt', blank=True, null=True)  # Field name made lowercase.
    creationtime = models.DateTimeField(db_column='creationTime')  # Field name made lowercase.
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    firstname = models.CharField(db_column='firstName', max_length=30, blank=True, null=True)  # Field name made lowercase.
    lastname = models.CharField(db_column='lastName', max_length=50, blank=True, null=True)  # Field name made lowercase.
    mrn = models.BigIntegerField(blank=True, null=True)
    chargecredit = models.CharField(db_column='chargeCredit', max_length=3, blank=True, null=True)  # Field name made lowercase.
    code = models.CharField(max_length=5, blank=True, null=True)
    codetype = models.CharField(db_column='codeType', max_length=2)  # Field name made lowercase.
    enteredby = models.CharField(db_column='enteredBy', max_length=30)  # Field name made lowercase.
    visitloc = models.CharField(db_column='visitLoc', max_length=15)  # Field name made lowercase.
    controlnumber = models.CharField(db_column='controlNumber', max_length=30)  # Field name made lowercase.
    payerid = models.CharField(db_column='payerId', max_length=30)  # Field name made lowercase.
    eobdate = models.DateField(db_column='eobDate')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'accountingOld'


