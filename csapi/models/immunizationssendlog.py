from __future__ import unicode_literals

from django.db import models

class Immunizationssendlog(models.Model):
    id = models.BigAutoField(primary_key=True)
    medorderlogid = models.BigIntegerField(db_column='medOrderLogId', blank=True, null=True)  # Field name made lowercase.
    sentdate = models.DateTimeField(db_column='sentDate', blank=True, null=True)  # Field name made lowercase.
    sentby = models.CharField(db_column='sentBy', max_length=35, blank=True, null=True)  # Field name made lowercase.
    senttoid = models.BigIntegerField(db_column='sentToId', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'immunizationsSendLog'


