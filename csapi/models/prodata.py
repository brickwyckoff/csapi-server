from __future__ import unicode_literals

from django.db import models

class Prodata(models.Model):
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    length = models.CharField(max_length=10, blank=True, null=True)
    location = models.CharField(max_length=60, blank=True, null=True)
    descpartialthickness = models.IntegerField(db_column='descPartialThickness', blank=True, null=True)  # Field name made lowercase.
    descfullthickness = models.IntegerField(db_column='descFullThickness', blank=True, null=True)  # Field name made lowercase.
    desclinear = models.IntegerField(db_column='descLinear', blank=True, null=True)  # Field name made lowercase.
    desccurved = models.IntegerField(db_column='descCurved', blank=True, null=True)  # Field name made lowercase.
    descstellate = models.IntegerField(db_column='descStellate', blank=True, null=True)  # Field name made lowercase.
    aneslet = models.IntegerField(db_column='anesLet', blank=True, null=True)  # Field name made lowercase.
    aneslidocaine = models.IntegerField(db_column='anesLidocaine', blank=True, null=True)  # Field name made lowercase.
    anesepi = models.IntegerField(db_column='anesEpi', blank=True, null=True)  # Field name made lowercase.
    anesbicarb = models.IntegerField(db_column='anesBicarb', blank=True, null=True)  # Field name made lowercase.
    anesdigitalblock = models.IntegerField(db_column='anesDigitalBlock', blank=True, null=True)  # Field name made lowercase.
    prepsteriletech = models.IntegerField(db_column='prepSterileTech', blank=True, null=True)  # Field name made lowercase.
    prepirrigated = models.IntegerField(db_column='prepIrrigated', blank=True, null=True)  # Field name made lowercase.
    prepexplored = models.IntegerField(db_column='prepExplored', blank=True, null=True)  # Field name made lowercase.
    prepdebrided = models.IntegerField(db_column='prepDebrided', blank=True, null=True)  # Field name made lowercase.
    preprevised = models.IntegerField(db_column='prepRevised', blank=True, null=True)  # Field name made lowercase.
    foreignbody = models.CharField(db_column='foreignBody', max_length=100, blank=True, null=True)  # Field name made lowercase.
    compfracture = models.IntegerField(db_column='compFracture', blank=True, null=True)  # Field name made lowercase.
    comptendon = models.IntegerField(db_column='compTendon', blank=True, null=True)  # Field name made lowercase.
    compligament = models.IntegerField(db_column='compLigament', blank=True, null=True)  # Field name made lowercase.
    compmuscle = models.IntegerField(db_column='compMuscle', blank=True, null=True)  # Field name made lowercase.
    compnerve = models.IntegerField(db_column='compNerve', blank=True, null=True)  # Field name made lowercase.
    skinsuturenum = models.CharField(db_column='skinSutureNum', max_length=3, blank=True, null=True)  # Field name made lowercase.
    skinsuturegauge = models.CharField(db_column='skinSutureGauge', max_length=3, blank=True, null=True)  # Field name made lowercase.
    skinnylon = models.IntegerField(db_column='skinNylon', blank=True, null=True)  # Field name made lowercase.
    skinprolene = models.IntegerField(db_column='skinProlene', blank=True, null=True)  # Field name made lowercase.
    skindexon = models.IntegerField(db_column='skinDexon', blank=True, null=True)  # Field name made lowercase.
    skinvicryl = models.IntegerField(db_column='skinVicryl', blank=True, null=True)  # Field name made lowercase.
    subsuturenum = models.CharField(db_column='subSutureNum', max_length=3, blank=True, null=True)  # Field name made lowercase.
    subsuturegauge = models.CharField(db_column='subSutureGauge', max_length=3, blank=True, null=True)  # Field name made lowercase.
    subdexon = models.IntegerField(db_column='subDexon', blank=True, null=True)  # Field name made lowercase.
    subvicryl = models.IntegerField(db_column='subVicryl', blank=True, null=True)  # Field name made lowercase.
    dermabond = models.IntegerField(blank=True, null=True)
    staples = models.CharField(max_length=3, blank=True, null=True)
    comments = models.CharField(max_length=600, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'proData'


