from __future__ import unicode_literals

from django.db import models

class Patienteducation(models.Model):
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    docid = models.CharField(db_column='docId', max_length=25, blank=True, null=True)  # Field name made lowercase.
    doctype = models.CharField(db_column='docType', max_length=15, blank=True, null=True)  # Field name made lowercase.
    doctitle = models.CharField(db_column='docTitle', max_length=200)  # Field name made lowercase.
    doclink = models.CharField(db_column='docLink', max_length=1000, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'patientEducation'


