from __future__ import unicode_literals

from django.db import models

class Counselinginitiallog(models.Model):
    id = models.BigAutoField(primary_key=True)
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    user = models.CharField(max_length=35, blank=True, null=True)
    creationtime = models.DateTimeField(db_column='creationTime', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'counselingInitialLog'


