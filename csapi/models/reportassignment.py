from __future__ import unicode_literals

from django.db import models

class Reportassignment(models.Model):
    username = models.CharField(max_length=20, blank=True, null=True)
    insurance = models.CharField(max_length=200, blank=True, null=True)
    clinicid = models.CharField(db_column='clinicId', max_length=100, blank=True, null=True)  # Field name made lowercase.
    visitstartdate = models.DateField(db_column='visitStartDate', blank=True, null=True)  # Field name made lowercase.
    visitenddate = models.DateField(db_column='visitEndDate', blank=True, null=True)  # Field name made lowercase.
    denialstartdate = models.DateField(db_column='denialStartDate', blank=True, null=True)  # Field name made lowercase.
    denialenddate = models.DateField(db_column='denialEndDate', blank=True, null=True)  # Field name made lowercase.
    cptcode = models.CharField(db_column='cptCode', max_length=100, blank=True, null=True)  # Field name made lowercase.
    lastnamestart = models.CharField(db_column='lastNameStart', max_length=6, blank=True, null=True)  # Field name made lowercase.
    lastnameend = models.CharField(db_column='lastNameEnd', max_length=6, blank=True, null=True)  # Field name made lowercase.
    reporttype = models.CharField(db_column='reportType', max_length=20, blank=True, null=True)  # Field name made lowercase.
    reportid = models.BigIntegerField(db_column='reportId', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'reportAssignment'


