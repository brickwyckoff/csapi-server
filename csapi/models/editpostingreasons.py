from __future__ import unicode_literals

from django.db import models

class Editpostingreasons(models.Model):
    id = models.BigAutoField(primary_key=True)
    reason = models.CharField(max_length=100)
    active = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'editPostingReasons'


