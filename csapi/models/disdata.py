from __future__ import unicode_literals

from django.db import models

class Disdata(models.Model):
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    discharge = models.CharField(max_length=1500, blank=True, null=True)
    instructionpacket1 = models.CharField(db_column='instructionPacket1', max_length=20, blank=True, null=True)  # Field name made lowercase.
    instructionpacket2 = models.CharField(db_column='instructionPacket2', max_length=20, blank=True, null=True)  # Field name made lowercase.
    instructionpacket3 = models.CharField(db_column='instructionPacket3', max_length=20, blank=True, null=True)  # Field name made lowercase.
    summarytopt = models.IntegerField(db_column='summaryToPt')  # Field name made lowercase.
    summarytopt1 = models.IntegerField(db_column='summaryToPt1')  # Field name made lowercase.
    ptrequestedehealthinfo = models.IntegerField(db_column='ptRequestedEHealthInfo')  # Field name made lowercase.
    ehealthinfosent = models.IntegerField(db_column='eHealthInfoSent')  # Field name made lowercase.
    pttransferedout = models.IntegerField(db_column='ptTransferedOut')  # Field name made lowercase.
    esummaryout = models.IntegerField(db_column='eSummaryOut')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'disData'


