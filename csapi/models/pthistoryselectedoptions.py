from __future__ import unicode_literals

from django.db import models

class Pthistoryselectedoptions(models.Model):
    optionid = models.IntegerField(db_column='optionId', blank=True, null=True)  # Field name made lowercase.
    optionorder = models.IntegerField(db_column='optionOrder', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ptHistorySelectedOptions'


