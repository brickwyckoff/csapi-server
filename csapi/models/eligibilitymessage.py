from __future__ import unicode_literals

from django.db import models

class Eligibilitymessage(models.Model):
    verifiedid = models.BigIntegerField(db_column='verifiedId', blank=True, null=True)  # Field name made lowercase.
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    plandescription = models.CharField(db_column='planDescription', max_length=80, blank=True, null=True)  # Field name made lowercase.
    message = models.CharField(max_length=264, blank=True, null=True)
    ebid = models.BigIntegerField(db_column='ebId', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'eligibilityMessage'


