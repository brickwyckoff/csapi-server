from __future__ import unicode_literals

from django.db import models

class Encounternotedx(models.Model):
    diagcode = models.CharField(db_column='diagCode', max_length=10, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'encounterNoteDx'


