from __future__ import unicode_literals

from django.db import models

class Postingcodes(models.Model):
    code = models.CharField(max_length=15, blank=True, null=True)
    description = models.CharField(max_length=150, blank=True, null=True)
    codegroup = models.CharField(db_column='codeGroup', max_length=35, blank=True, null=True)  # Field name made lowercase.
    bucket = models.CharField(max_length=35, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'postingCodes'


