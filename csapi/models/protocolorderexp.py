from __future__ import unicode_literals

from django.db import models

class Protocolorderexp(models.Model):
    id = models.BigAutoField(primary_key=True)
    orderid = models.BigIntegerField(db_column='orderId', blank=True, null=True)  # Field name made lowercase.
    orderexp = models.CharField(db_column='orderExp', max_length=1000, blank=True, null=True)  # Field name made lowercase.
    expstartdate = models.DateField(db_column='expStartDate', blank=True, null=True)  # Field name made lowercase.
    expenddate = models.DateField(db_column='expEndDate', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'protocolOrderExp'


