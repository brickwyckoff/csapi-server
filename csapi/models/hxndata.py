from __future__ import unicode_literals

from django.db import models

class Hxndata(models.Model):
    mrn = models.BigIntegerField(blank=True, null=True)
    pasthistorynotes = models.TextField(db_column='pastHistoryNotes', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'hxnData'


