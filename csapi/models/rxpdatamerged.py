from __future__ import unicode_literals

from django.db import models

class Rxpdatamerged(models.Model):
    mrn = models.BigIntegerField(blank=True, null=True)
    refillsleft = models.IntegerField(db_column='refillsLeft', blank=True, null=True)  # Field name made lowercase.
    prescription = models.CharField(max_length=60, blank=True, null=True)
    freq = models.CharField(max_length=15, blank=True, null=True)
    qty = models.CharField(max_length=15, blank=True, null=True)
    route = models.CharField(max_length=15, blank=True, null=True)
    refills = models.CharField(max_length=15, blank=True, null=True)
    lastprescriber = models.CharField(db_column='lastPrescriber', max_length=10, blank=True, null=True)  # Field name made lowercase.
    lastprescriptiontime = models.DateTimeField(db_column='lastPrescriptionTime', blank=True, null=True)  # Field name made lowercase.
    lastprinttime = models.DateTimeField(db_column='lastPrintTime', blank=True, null=True)  # Field name made lowercase.
    filldateoverride = models.DateField(db_column='fillDateOverride', blank=True, null=True)  # Field name made lowercase.
    lastfilldate = models.DateField(db_column='lastFillDate', blank=True, null=True)  # Field name made lowercase.
    nextfilldate = models.DateField(db_column='nextFillDate', blank=True, null=True)  # Field name made lowercase.
    stratusrxid = models.BigIntegerField(db_column='stratusRxId', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'rxpDataMerged'


