from __future__ import unicode_literals

from django.db import models

class Tctdata(models.Model):
    id = models.BigAutoField(primary_key=True)
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    itemid = models.IntegerField(db_column='itemId', blank=True, null=True)  # Field name made lowercase.
    groupid = models.CharField(db_column='groupId', max_length=12, blank=True, null=True)  # Field name made lowercase.
    creationtime = models.DateTimeField(db_column='creationTime')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'tctData'


