from __future__ import unicode_literals

from django.db import models

class Importedmedrec(models.Model):
    id = models.BigAutoField(primary_key=True)
    mrn = models.BigIntegerField()
    sourcename = models.CharField(db_column='sourceName', max_length=50)  # Field name made lowercase.
    importtime = models.DateTimeField(db_column='importTime')  # Field name made lowercase.
    isack = models.IntegerField(db_column='isAck')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'importedMedRec'


