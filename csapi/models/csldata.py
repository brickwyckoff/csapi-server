from __future__ import unicode_literals

from django.db import models

class Csldata(models.Model):
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    orientedperson = models.IntegerField(db_column='orientedPerson', blank=True, null=True)  # Field name made lowercase.
    orientedplace = models.IntegerField(db_column='orientedPlace', blank=True, null=True)  # Field name made lowercase.
    orientedtime = models.IntegerField(db_column='orientedTime', blank=True, null=True)  # Field name made lowercase.
    treatmentnote = models.CharField(db_column='treatmentNote', max_length=10000, blank=True, null=True)  # Field name made lowercase.
    activewithdrawal = models.IntegerField(db_column='activeWithdrawal', blank=True, null=True)  # Field name made lowercase.
    financialissues = models.IntegerField(db_column='financialIssues', blank=True, null=True)  # Field name made lowercase.
    discabstinence = models.IntegerField(db_column='discAbstinence', blank=True, null=True)  # Field name made lowercase.
    discfamily = models.IntegerField(db_column='discFamily', blank=True, null=True)  # Field name made lowercase.
    notactivewithdrawal = models.IntegerField(db_column='notActiveWithdrawal', blank=True, null=True)  # Field name made lowercase.
    legalissues = models.IntegerField(db_column='legalIssues', blank=True, null=True)  # Field name made lowercase.
    attendingaa = models.IntegerField(db_column='attendingAA', blank=True, null=True)  # Field name made lowercase.
    discfriends = models.IntegerField(db_column='discFriends', blank=True, null=True)  # Field name made lowercase.
    housingissues = models.IntegerField(db_column='housingIssues', blank=True, null=True)  # Field name made lowercase.
    disctriggers = models.IntegerField(db_column='discTriggers', blank=True, null=True)  # Field name made lowercase.
    discwork = models.IntegerField(db_column='discWork', blank=True, null=True)  # Field name made lowercase.
    increaseservices = models.IntegerField(db_column='increaseServices', blank=True, null=True)  # Field name made lowercase.
    indtreatment = models.IntegerField(db_column='indTreatment', blank=True, null=True)  # Field name made lowercase.
    disccoping = models.IntegerField(db_column='discCoping', blank=True, null=True)  # Field name made lowercase.
    identifiedsupports = models.IntegerField(db_column='identifiedSupports', blank=True, null=True)  # Field name made lowercase.
    discemotion = models.IntegerField(db_column='discEmotion', blank=True, null=True)  # Field name made lowercase.
    discbenefits = models.IntegerField(db_column='discBenefits', blank=True, null=True)  # Field name made lowercase.
    discharmreduction = models.IntegerField(db_column='discHarmReduction', blank=True, null=True)  # Field name made lowercase.
    discabsopiates = models.IntegerField(db_column='discAbsOpiates', blank=True, null=True)  # Field name made lowercase.
    releasesigned = models.IntegerField(db_column='releaseSigned', blank=True, null=True)  # Field name made lowercase.
    referredto = models.CharField(db_column='referredTo', max_length=100, blank=True, null=True)  # Field name made lowercase.
    happy = models.IntegerField(blank=True, null=True)
    confident = models.IntegerField(blank=True, null=True)
    cheerful = models.IntegerField(blank=True, null=True)
    worried = models.IntegerField(blank=True, null=True)
    isolated = models.IntegerField(blank=True, null=True)
    determined = models.IntegerField(blank=True, null=True)
    frustrated = models.IntegerField(blank=True, null=True)
    indifferent = models.IntegerField(blank=True, null=True)
    withdrawn = models.IntegerField(blank=True, null=True)
    supported = models.IntegerField(blank=True, null=True)
    empowered = models.IntegerField(blank=True, null=True)
    fearful = models.IntegerField(blank=True, null=True)
    guilty = models.IntegerField(blank=True, null=True)
    anger = models.IntegerField(blank=True, null=True)
    disappointed = models.IntegerField(blank=True, null=True)
    anxious = models.IntegerField(blank=True, null=True)
    depressed = models.IntegerField(blank=True, null=True)
    improvement = models.IntegerField(blank=True, null=True)
    lonely = models.IntegerField(blank=True, null=True)
    relieved = models.IntegerField(blank=True, null=True)
    cptcode = models.CharField(db_column='cptCode', max_length=10, blank=True, null=True)  # Field name made lowercase.
    cptqty = models.CharField(db_column='cptQty', max_length=5, blank=True, null=True)  # Field name made lowercase.
    axisi = models.CharField(db_column='axisI', max_length=100, blank=True, null=True)  # Field name made lowercase.
    axisii = models.CharField(db_column='axisII', max_length=100, blank=True, null=True)  # Field name made lowercase.
    axisiii = models.CharField(db_column='axisIII', max_length=100, blank=True, null=True)  # Field name made lowercase.
    axisiv = models.CharField(db_column='axisIV', max_length=100, blank=True, null=True)  # Field name made lowercase.
    axisvgaf = models.CharField(db_column='axisVGaf', max_length=100, blank=True, null=True)  # Field name made lowercase.
    axisvgafhighest = models.CharField(db_column='axisVGafHighest', max_length=100, blank=True, null=True)  # Field name made lowercase.
    axisidx2 = models.CharField(db_column='axisIDx2', max_length=20, blank=True, null=True)  # Field name made lowercase.
    cptmod1 = models.CharField(db_column='cptMod1', max_length=3)  # Field name made lowercase.
    cptmod2 = models.CharField(db_column='cptMod2', max_length=3)  # Field name made lowercase.
    timein = models.CharField(db_column='timeIn', max_length=35, blank=True, null=True)  # Field name made lowercase.
    timeout = models.CharField(db_column='timeOut', max_length=35, blank=True, null=True)  # Field name made lowercase.
    orientedsituation = models.IntegerField(db_column='orientedSituation', blank=True, null=True)  # Field name made lowercase.
    cptcode2 = models.CharField(db_column='cptCode2', max_length=10, blank=True, null=True)  # Field name made lowercase.
    cptqty2 = models.CharField(db_column='cptQty2', max_length=5, blank=True, null=True)  # Field name made lowercase.
    cpt2mod1 = models.CharField(db_column='cpt2Mod1', max_length=3, blank=True, null=True)  # Field name made lowercase.
    cpt2mod2 = models.CharField(db_column='cpt2Mod2', max_length=3, blank=True, null=True)  # Field name made lowercase.
    followup = models.CharField(db_column='followUp', max_length=1500, blank=True, null=True)  # Field name made lowercase.
    treatmentnotecmplt = models.IntegerField(db_column='treatmentNoteCmplt')  # Field name made lowercase.
    fee1 = models.FloatField(blank=True, null=True)
    fee2 = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cslData'


