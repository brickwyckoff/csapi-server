from __future__ import unicode_literals

from django.db import models

class Mspformmeta(models.Model):
    categoryname = models.CharField(db_column='categoryName', max_length=100, blank=True, null=True)  # Field name made lowercase.
    datafield = models.CharField(db_column='dataField', max_length=100, blank=True, null=True)  # Field name made lowercase.
    displaytext = models.CharField(db_column='displayText', max_length=200, blank=True, null=True)  # Field name made lowercase.
    pageordinal = models.IntegerField(db_column='pageOrdinal', blank=True, null=True)  # Field name made lowercase.
    listordinal = models.IntegerField(db_column='listOrdinal', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'mspFormMeta'


