from __future__ import unicode_literals

from django.db import models

class Emrfields(models.Model):
    field = models.CharField(max_length=50, blank=True, null=True)
    section = models.CharField(max_length=3, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'emrFields'


