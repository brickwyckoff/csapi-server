from __future__ import unicode_literals

from django.db import models

class EwcFees(models.Model):
    code = models.CharField(max_length=9, blank=True, null=True)
    fee = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ewc_fees'


