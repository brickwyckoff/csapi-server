from __future__ import unicode_literals

from django.db import models

class Alerttypes(models.Model):
    alertname = models.CharField(db_column='alertName', max_length=50, blank=True, null=True)  # Field name made lowercase.
    active = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'alertTypes'


