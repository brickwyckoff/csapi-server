from __future__ import unicode_literals

from django.db import models

class Meddatanew(models.Model):
    mrn = models.BigIntegerField(blank=True, null=True)
    drugid = models.CharField(db_column='drugId', max_length=10, blank=True, null=True)  # Field name made lowercase.
    tradeid = models.CharField(db_column='tradeId', max_length=11, blank=True, null=True)  # Field name made lowercase.
    doseform = models.CharField(db_column='doseForm', max_length=11, blank=True, null=True)  # Field name made lowercase.
    route = models.CharField(max_length=11, blank=True, null=True)
    strength = models.CharField(max_length=11, blank=True, null=True)
    startdate = models.DateField(db_column='startDate', blank=True, null=True)  # Field name made lowercase.
    enddate = models.DateField(db_column='endDate', blank=True, null=True)  # Field name made lowercase.
    freetext = models.CharField(db_column='freeText', max_length=80, blank=True, null=True)  # Field name made lowercase.
    genericbrand = models.CharField(db_column='genericBrand', max_length=1, blank=True, null=True)  # Field name made lowercase.
    rxotc = models.CharField(db_column='rxOtc', max_length=6, blank=True, null=True)  # Field name made lowercase.
    freq = models.CharField(max_length=11, blank=True, null=True)
    isactive = models.IntegerField(db_column='isActive', blank=True, null=True)  # Field name made lowercase.
    drugclass = models.CharField(db_column='drugClass', max_length=11, blank=True, null=True)  # Field name made lowercase.
    dose = models.CharField(max_length=15, blank=True, null=True)
    doseunit = models.CharField(db_column='doseUnit', max_length=15, blank=True, null=True)  # Field name made lowercase.
    updatedate = models.DateTimeField(db_column='updateDate')  # Field name made lowercase.
    rxnorm = models.CharField(db_column='rxNorm', max_length=15)  # Field name made lowercase.
    importtype = models.CharField(db_column='importType', max_length=15)  # Field name made lowercase.
    infosource = models.CharField(db_column='infoSource', max_length=100)  # Field name made lowercase.
    numperdose = models.CharField(db_column='numPerDose', max_length=30)  # Field name made lowercase.
    numperdoseunit = models.CharField(db_column='numPerDoseUnit', max_length=30)  # Field name made lowercase.
    clinicalroute = models.CharField(db_column='clinicalRoute', max_length=30)  # Field name made lowercase.
    sigtext = models.CharField(db_column='sigText', max_length=400)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'medDataNew'


