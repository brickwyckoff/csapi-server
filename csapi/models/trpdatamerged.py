from __future__ import unicode_literals

from django.db import models

class Trpdatamerged(models.Model):
    mrn = models.BigIntegerField(blank=True, null=True)
    verifydate = models.DateField(db_column='verifyDate', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'trpDataMerged'


