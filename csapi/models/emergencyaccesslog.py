from __future__ import unicode_literals

from django.db import models

class Emergencyaccesslog(models.Model):
    mrn = models.BigIntegerField(blank=True, null=True)
    user = models.CharField(max_length=35, blank=True, null=True)
    invoketime = models.DateTimeField(db_column='invokeTime', blank=True, null=True)  # Field name made lowercase.
    invokeip = models.CharField(db_column='invokeIp', max_length=30, blank=True, null=True)  # Field name made lowercase.
    validdate = models.DateField(db_column='validDate', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'emergencyAccessLog'


