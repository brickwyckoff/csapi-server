from __future__ import unicode_literals

from django.db import models

class Treatmentlevels(models.Model):
    levelname = models.CharField(db_column='levelName', max_length=35, blank=True, null=True)  # Field name made lowercase.
    barcolor = models.CharField(db_column='barColor', max_length=6, blank=True, null=True)  # Field name made lowercase.
    displayorder = models.IntegerField(db_column='displayOrder', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'treatmentLevels'


