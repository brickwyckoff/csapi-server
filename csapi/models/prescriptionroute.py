from __future__ import unicode_literals

from django.db import models

class Prescriptionroute(models.Model):
    code = models.CharField(max_length=6, blank=True, null=True)
    meaning = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'prescriptionRoute'


