from __future__ import unicode_literals

from django.db import models

class Labresultlogarchived(models.Model):
    mrn = models.BigIntegerField(blank=True, null=True)
    labcontrolid = models.CharField(db_column='labControlId', max_length=60, blank=True, null=True)  # Field name made lowercase.
    creationtime = models.DateTimeField(db_column='creationTime', blank=True, null=True)  # Field name made lowercase.
    datereviewed = models.DateTimeField(db_column='dateReviewed', blank=True, null=True)  # Field name made lowercase.
    reviewedby = models.CharField(db_column='reviewedBy', max_length=35)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'labResultLogArchived'


