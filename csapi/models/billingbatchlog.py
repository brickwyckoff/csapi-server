from __future__ import unicode_literals

from django.db import models

class Billingbatchlog(models.Model):
    id = models.BigAutoField(primary_key=True)
    type = models.CharField(max_length=30, blank=True, null=True)
    username = models.CharField(max_length=30, blank=True, null=True)
    entrytime = models.DateTimeField(db_column='entryTime', blank=True, null=True)  # Field name made lowercase.
    externalbatchid = models.BigIntegerField(db_column='externalBatchId', blank=True, null=True)  # Field name made lowercase.
    batchstatus = models.CharField(db_column='batchStatus', max_length=35, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'billingBatchLog'


