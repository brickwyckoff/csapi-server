from __future__ import unicode_literals

from django.db import models

class Pcpfaxsent(models.Model):
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    user = models.CharField(max_length=60, blank=True, null=True)
    sentto = models.BigIntegerField(db_column='sentTo', blank=True, null=True)  # Field name made lowercase.
    creationtime = models.DateTimeField(db_column='creationTime', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'pcpFaxSent'


