from __future__ import unicode_literals

from django.db import models

class Portalinfo(models.Model):
    id = models.BigAutoField(primary_key=True)
    accesslevel = models.CharField(db_column='accessLevel', max_length=5)  # Field name made lowercase.
    databaseaccess = models.CharField(db_column='databaseAccess', max_length=100)  # Field name made lowercase.
    codedowloadfile = models.CharField(db_column='codeDowloadFile', max_length=100)  # Field name made lowercase.
    mostrecent = models.CharField(db_column='mostRecent', max_length=20)  # Field name made lowercase.
    downloadalldem = models.IntegerField(db_column='downloadAllDem')  # Field name made lowercase.
    labcodedowloadfile = models.CharField(db_column='labCodeDowloadFile', max_length=100)  # Field name made lowercase.
    demdowloadfile = models.CharField(db_column='demDowloadFile', max_length=50, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'portalInfo'


