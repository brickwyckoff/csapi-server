from django.db import models
from csapi.models.batchEligCoverageDetail import BatchEligCoverageDetail

class BatchEligCoverageDetailMessage (models.Model):
    class Meta:
        db_table = 'batchEligCoverageDetailMessage'
        managed = True

    id = models.AutoField (auto_created=True, primary_key=True, serialize=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    message = models.CharField (max_length=256)

    batchCoverageDetail = models.ForeignKey(BatchEligCoverageDetail, to_field='id')


