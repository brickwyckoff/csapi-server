from __future__ import unicode_literals

from django.db import models

class Hptdata(models.Model):
    id = models.BigAutoField(primary_key=True)
    mrn = models.BigIntegerField()
    callername = models.TextField(db_column='callerName')  # Field name made lowercase.
    birthday = models.TextField(db_column='birthDay')  # Field name made lowercase.
    contactnumber = models.TextField(db_column='contactNumber')  # Field name made lowercase.
    staffcontactinitial = models.TextField(db_column='staffContactInitial')  # Field name made lowercase.
    scheduledphoneinterview = models.TextField(db_column='scheduledPhoneInterview')  # Field name made lowercase.
    address = models.TextField()
    citystate = models.TextField(db_column='cityState')  # Field name made lowercase.
    zipcode = models.TextField(db_column='zipCode')  # Field name made lowercase.
    alternatenumber = models.TextField(db_column='alternateNumber')  # Field name made lowercase.
    bestnumber = models.TextField(db_column='bestNumber')  # Field name made lowercase.
    okay = models.IntegerField()
    curuse = models.CharField(db_column='curUse', max_length=1000)  # Field name made lowercase.
    length = models.TextField()
    difcli = models.TextField(db_column='difCli')  # Field name made lowercase.
    everbeen = models.IntegerField(db_column='everBeen')  # Field name made lowercase.
    howlong = models.TextField(db_column='howLong')  # Field name made lowercase.
    preg = models.IntegerField()
    couldbe = models.IntegerField(db_column='couldBe')  # Field name made lowercase.
    posdep = models.IntegerField(db_column='posDep')  # Field name made lowercase.
    posanx = models.IntegerField(db_column='posAnx')  # Field name made lowercase.
    posptsd = models.IntegerField(db_column='posPtsd')  # Field name made lowercase.
    employed = models.CharField(max_length=1000)
    moodswings = models.CharField(db_column='moodSwings', max_length=1000)  # Field name made lowercase.
    struggle = models.CharField(max_length=1000)
    harm = models.CharField(max_length=1000)
    suicide = models.CharField(max_length=1000)
    sucinfo = models.CharField(db_column='sucInfo', max_length=1000)  # Field name made lowercase.
    selfharm = models.CharField(max_length=1000)
    trig = models.CharField(max_length=1000)
    unemployed = models.CharField(max_length=1000)
    transport = models.CharField(max_length=1000)
    insure = models.IntegerField()
    whoinsure = models.CharField(db_column='whoInsure', max_length=1000)  # Field name made lowercase.
    mat = models.IntegerField()
    howdid = models.CharField(db_column='howDid', max_length=1000)  # Field name made lowercase.
    additional = models.CharField(max_length=1000)
    nameof = models.CharField(db_column='nameOf', max_length=1000)  # Field name made lowercase.
    amounts = models.CharField(max_length=1000)
    datesof = models.CharField(db_column='datesOf', max_length=1000)  # Field name made lowercase.
    routesof = models.CharField(db_column='routesOf', max_length=1000)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'hptData'


