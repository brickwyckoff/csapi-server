from __future__ import unicode_literals

from django.db import models

class Lastsyncvalue(models.Model):
    mrn = models.BigIntegerField(blank=True, null=True)
    field = models.CharField(max_length=50, blank=True, null=True)
    value = models.CharField(max_length=150, blank=True, null=True)
    synctime = models.DateTimeField(db_column='syncTime', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'lastSyncValue'


