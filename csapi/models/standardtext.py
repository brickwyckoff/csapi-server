from __future__ import unicode_literals

from django.db import models

class Standardtext(models.Model):
    texttype = models.CharField(db_column='textType', max_length=15, blank=True, null=True)  # Field name made lowercase.
    pttype = models.CharField(db_column='ptType', max_length=3, blank=True, null=True)  # Field name made lowercase.
    visittype = models.IntegerField(db_column='visitType', blank=True, null=True)  # Field name made lowercase.
    textvalue = models.CharField(db_column='textValue', max_length=500, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'standardText'


