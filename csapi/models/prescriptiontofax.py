from __future__ import unicode_literals

from django.db import models

class Prescriptiontofax(models.Model):
    id = models.BigAutoField(primary_key=True)
    prescriptionlogid = models.BigIntegerField(db_column='prescriptionLogId')  # Field name made lowercase.
    faxdate = models.DateField(db_column='faxDate')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'prescriptionToFax'


