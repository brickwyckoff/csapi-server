from __future__ import unicode_literals

from django.db import models

class Customlabalerts(models.Model):
    labloc = models.CharField(db_column='labLoc', max_length=30, blank=True, null=True)  # Field name made lowercase.
    labcode = models.CharField(db_column='labCode', max_length=20, blank=True, null=True)  # Field name made lowercase.
    checkvalue = models.CharField(db_column='checkValue', max_length=30, blank=True, null=True)  # Field name made lowercase.
    checktype = models.CharField(db_column='checkType', max_length=30, blank=True, null=True)  # Field name made lowercase.
    alertmessage = models.CharField(db_column='alertMessage', max_length=300, blank=True, null=True)  # Field name made lowercase.
    isactive = models.IntegerField(db_column='isActive', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'customLabAlerts'


