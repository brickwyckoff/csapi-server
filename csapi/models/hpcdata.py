from __future__ import unicode_literals

from django.db import models

class Hpcdata(models.Model):
    id = models.BigAutoField(primary_key=True)
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    hepcpositive = models.IntegerField(db_column='hepCPositive', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'hpcData'


