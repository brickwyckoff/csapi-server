from __future__ import unicode_literals

from django.db import models

class Eobcheckinfo(models.Model):
    id = models.BigAutoField(primary_key=True)
    checkamt = models.FloatField(db_column='checkAmt')  # Field name made lowercase.
    checkdate = models.DateField(db_column='checkDate')  # Field name made lowercase.
    checknumber = models.CharField(db_column='checkNumber', max_length=50)  # Field name made lowercase.
    checktype = models.CharField(db_column='checkType', max_length=15)  # Field name made lowercase.
    batchnumber = models.CharField(db_column='batchNumber', max_length=80, blank=True, null=True)  # Field name made lowercase.
    enteredbatchamount = models.FloatField(db_column='enteredBatchAmount', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'eobCheckInfo'


