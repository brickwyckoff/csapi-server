from __future__ import unicode_literals

from django.db import models

class Cc2Data(models.Model):
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    custom1 = models.IntegerField(blank=True, null=True)
    custom2 = models.IntegerField(blank=True, null=True)
    custom3 = models.IntegerField(blank=True, null=True)
    custom4 = models.IntegerField(blank=True, null=True)
    custom5 = models.IntegerField(blank=True, null=True)
    custom6 = models.IntegerField(blank=True, null=True)
    custom7 = models.IntegerField(blank=True, null=True)
    custom8 = models.IntegerField(blank=True, null=True)
    custom9 = models.IntegerField(blank=True, null=True)
    custom10 = models.IntegerField(blank=True, null=True)
    custom11 = models.IntegerField(blank=True, null=True)
    custom12 = models.IntegerField(blank=True, null=True)
    custom13 = models.IntegerField(blank=True, null=True)
    custom14 = models.IntegerField(blank=True, null=True)
    custom15 = models.IntegerField(blank=True, null=True)
    custom16 = models.IntegerField(blank=True, null=True)
    custom17 = models.IntegerField(blank=True, null=True)
    custom18 = models.IntegerField(blank=True, null=True)
    custom19 = models.IntegerField(blank=True, null=True)
    custom20 = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cc2Data'


