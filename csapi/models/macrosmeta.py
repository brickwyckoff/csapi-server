from __future__ import unicode_literals

from django.db import models

class Macrosmeta(models.Model):
    user = models.CharField(max_length=35, blank=True, null=True)
    macroname = models.CharField(db_column='macroName', max_length=50, blank=True, null=True)  # Field name made lowercase.
    isactive = models.IntegerField(db_column='isActive', blank=True, null=True)  # Field name made lowercase.
    displayorder = models.IntegerField(db_column='displayOrder', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'macrosMeta'


