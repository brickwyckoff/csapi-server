from __future__ import unicode_literals

from django.db import models

class Templatetable(models.Model):
    keyword = models.CharField(max_length=60, blank=True, null=True)
    templatedisplayname = models.CharField(db_column='templateDisplayName', max_length=60, blank=True, null=True)  # Field name made lowercase.
    templatename = models.CharField(db_column='templateName', max_length=60, blank=True, null=True)  # Field name made lowercase.
    uselastdxbutton = models.IntegerField(db_column='useLastDxButton', blank=True, null=True)  # Field name made lowercase.
    uselastrxbutton = models.IntegerField(db_column='useLastRxButton', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'templateTable'


