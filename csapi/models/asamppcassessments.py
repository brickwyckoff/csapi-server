from __future__ import unicode_literals

from django.db import models

class Asamppcassessments(models.Model):
    mrn = models.BigIntegerField(blank=True, null=True)
    assessmentid = models.CharField(db_column='assessmentId', max_length=35, blank=True, null=True)  # Field name made lowercase.
    creationtime = models.DateTimeField(db_column='creationTime', blank=True, null=True)  # Field name made lowercase.
    asamcreationtime = models.DateTimeField(db_column='asamCreationTime', blank=True, null=True)  # Field name made lowercase.
    updatetime = models.DateTimeField(db_column='updateTime', blank=True, null=True)  # Field name made lowercase.
    completiontime = models.DateTimeField(db_column='completionTime', blank=True, null=True)  # Field name made lowercase.
    allvisited = models.IntegerField(db_column='allVisited', blank=True, null=True)  # Field name made lowercase.
    iscomplete = models.IntegerField(db_column='isComplete', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'asamPpcAssessments'


