from __future__ import unicode_literals

from django.db import models

class Cdsdata(models.Model):
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    intakedate = models.CharField(db_column='intakeDate', max_length=100, blank=True, null=True)  # Field name made lowercase.
    dischargedate = models.CharField(db_column='dischargeDate', max_length=100, blank=True, null=True)  # Field name made lowercase.
    intakegaf = models.CharField(db_column='intakeGAF', max_length=100, blank=True, null=True)  # Field name made lowercase.
    dischargegaf = models.CharField(db_column='dischargeGAF', max_length=100, blank=True, null=True)  # Field name made lowercase.
    dischargereason = models.CharField(db_column='dischargeReason', max_length=1000, blank=True, null=True)  # Field name made lowercase.
    initialdx = models.CharField(db_column='initialDx', max_length=1000, blank=True, null=True)  # Field name made lowercase.
    txgoals = models.CharField(db_column='txGoals', max_length=1000, blank=True, null=True)  # Field name made lowercase.
    interventions = models.CharField(max_length=1000, blank=True, null=True)
    progress = models.CharField(max_length=1000, blank=True, null=True)
    recommendations = models.CharField(max_length=1000, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cdsData'


