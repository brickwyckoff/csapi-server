from __future__ import unicode_literals

from django.db import models

class Noteschangelog(models.Model):
    noteid = models.BigIntegerField(db_column='noteId', blank=True, null=True)  # Field name made lowercase.
    oldnote = models.CharField(db_column='oldNote', max_length=400, blank=True, null=True)  # Field name made lowercase.
    newnote = models.CharField(db_column='newNote', max_length=400, blank=True, null=True)  # Field name made lowercase.
    notetype = models.CharField(db_column='noteType', max_length=2, blank=True, null=True)  # Field name made lowercase.
    user = models.CharField(max_length=50, blank=True, null=True)
    changetime = models.DateTimeField(db_column='changeTime', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'notesChangeLog'


