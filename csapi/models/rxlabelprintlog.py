from __future__ import unicode_literals

from django.db import models

class Rxlabelprintlog(models.Model):
    id = models.BigAutoField(primary_key=True)
    mrn = models.BigIntegerField()
    rxid = models.BigIntegerField(db_column='rxId')  # Field name made lowercase.
    user = models.CharField(max_length=35)
    printtime = models.DateTimeField(db_column='printTime')  # Field name made lowercase.
    visitloc = models.IntegerField(db_column='visitLoc')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'rxLabelPrintLog'


