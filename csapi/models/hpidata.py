from __future__ import unicode_literals

from django.db import models

class Hpidata(models.Model):
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    history = models.CharField(max_length=9000, blank=True, null=True)
    mvawc = models.IntegerField(db_column='mvaWc', blank=True, null=True)  # Field name made lowercase.
    triage = models.CharField(max_length=5000, blank=True, null=True)
    attendingnote = models.CharField(db_column='attendingNote', max_length=1500, blank=True, null=True)  # Field name made lowercase.
    currenttreatmentlevel = models.IntegerField(db_column='currentTreatmentLevel', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'hpiData'


