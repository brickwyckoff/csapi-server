from __future__ import unicode_literals

from django.db import models

class Commonhistory(models.Model):
    typecode = models.CharField(db_column='typeCode', max_length=15, blank=True, null=True)  # Field name made lowercase.
    freetext = models.CharField(db_column='freeText', max_length=80, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'commonHistory'


