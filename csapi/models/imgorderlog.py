from __future__ import unicode_literals

from django.db import models

class Imgorderlog(models.Model):
    mrn = models.BigIntegerField(blank=True, null=True)
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    orderedby = models.CharField(db_column='orderedBy', max_length=35, blank=True, null=True)  # Field name made lowercase.
    ordertime = models.DateTimeField(db_column='orderTime', blank=True, null=True)  # Field name made lowercase.
    filledby = models.CharField(db_column='filledBy', max_length=35, blank=True, null=True)  # Field name made lowercase.
    filledtime = models.DateTimeField(db_column='filledTime', blank=True, null=True)  # Field name made lowercase.
    imagingordertest = models.CharField(db_column='imagingOrderTest', max_length=300, blank=True, null=True)  # Field name made lowercase.
    imagingorderlocation = models.CharField(db_column='imagingOrderLocation', max_length=300, blank=True, null=True)  # Field name made lowercase.
    imagingorderreads = models.IntegerField(db_column='imagingOrderReads', blank=True, null=True)  # Field name made lowercase.
    imagingorderindication = models.CharField(db_column='imagingOrderIndication', max_length=50, blank=True, null=True)  # Field name made lowercase.
    otherinfo = models.CharField(db_column='otherInfo', max_length=300)  # Field name made lowercase.
    loinc = models.CharField(max_length=15)

    class Meta:
        managed = False
        db_table = 'imgOrderLog'


