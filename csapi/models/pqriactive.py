from __future__ import unicode_literals

from django.db import models

class Pqriactive(models.Model):
    pqriid = models.IntegerField(db_column='pqriId', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'pqriActive'


