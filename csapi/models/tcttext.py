from __future__ import unicode_literals

from django.db import models

class Tcttext(models.Model):
    id = models.BigAutoField(primary_key=True)
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    itemtext = models.CharField(db_column='itemText', max_length=1000, blank=True, null=True)  # Field name made lowercase.
    itemtype = models.CharField(db_column='itemType', max_length=4, blank=True, null=True)  # Field name made lowercase.
    groupid = models.CharField(db_column='groupId', max_length=12, blank=True, null=True)  # Field name made lowercase.
    creationtime = models.DateTimeField(db_column='creationTime', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'tctText'


