from __future__ import unicode_literals

from django.db import models

class Msvdata(models.Model):
    id = models.BigAutoField(primary_key=True)
    visitnum = models.BigIntegerField(db_column='visitNum')  # Field name made lowercase.
    callresult = models.CharField(db_column='callResult', max_length=21)  # Field name made lowercase.
    calldone = models.IntegerField(db_column='callDone')  # Field name made lowercase.
    rxcancelled = models.IntegerField(db_column='rxCancelled')  # Field name made lowercase.
    rxearlypickup = models.IntegerField(db_column='rxEarlyPickup')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'msvData'


