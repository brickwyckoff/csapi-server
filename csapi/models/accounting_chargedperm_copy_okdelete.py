from __future__ import unicode_literals

from django.db import models

class AccountingChargedpermCopyOkdelete(models.Model):
    type = models.CharField(max_length=60, blank=True, null=True)
    claimamt = models.FloatField(db_column='claimAmt', blank=True, null=True)  # Field name made lowercase.
    creationtime = models.DateTimeField(db_column='creationTime')  # Field name made lowercase.
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    mrn = models.BigIntegerField(blank=True, null=True)
    chargecredit = models.CharField(db_column='chargeCredit', max_length=15, blank=True, null=True)  # Field name made lowercase.
    code = models.CharField(max_length=5, blank=True, null=True)
    procqty = models.CharField(db_column='procQty', max_length=3, blank=True, null=True)  # Field name made lowercase.
    proccodetype = models.CharField(db_column='procCodeType', max_length=2, blank=True, null=True)  # Field name made lowercase.
    codetype = models.CharField(db_column='codeType', max_length=2)  # Field name made lowercase.
    enteredby = models.CharField(db_column='enteredBy', max_length=30)  # Field name made lowercase.
    controlnumber = models.CharField(db_column='controlNumber', max_length=30)  # Field name made lowercase.
    inscorefnum = models.CharField(db_column='insCoRefNum', max_length=35)  # Field name made lowercase.
    payerid = models.CharField(db_column='payerId', max_length=30)  # Field name made lowercase.
    eobdate = models.DateField(db_column='eobDate')  # Field name made lowercase.
    postdate = models.DateField(db_column='postDate')  # Field name made lowercase.
    processid = models.CharField(db_column='processId', max_length=23, blank=True, null=True)  # Field name made lowercase.
    policynum = models.CharField(db_column='policyNum', max_length=35)  # Field name made lowercase.
    setcontrolnumber = models.CharField(db_column='setControlNumber', max_length=30, blank=True, null=True)  # Field name made lowercase.
    npi = models.CharField(max_length=15, blank=True, null=True)
    firstname = models.CharField(db_column='firstName', max_length=50, blank=True, null=True)  # Field name made lowercase.
    lastname = models.CharField(db_column='lastName', max_length=50, blank=True, null=True)  # Field name made lowercase.
    visitdate = models.DateField(db_column='visitDate', blank=True, null=True)  # Field name made lowercase.
    batchnumber = models.CharField(db_column='batchNumber', max_length=80, blank=True, null=True)  # Field name made lowercase.
    billfrom = models.CharField(db_column='billFrom', max_length=3, blank=True, null=True)  # Field name made lowercase.
    pos = models.CharField(max_length=3, blank=True, null=True)
    claimentitytype = models.CharField(db_column='claimEntityType', max_length=5, blank=True, null=True)  # Field name made lowercase.
    billingprovider = models.CharField(db_column='billingProvider', max_length=20, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'accounting_chargedPerm_copy_okDelete'


