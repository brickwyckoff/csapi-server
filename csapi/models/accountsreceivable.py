from __future__ import unicode_literals

from django.db import models

class Accountsreceivable(models.Model):
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    insco = models.CharField(db_column='insCo', max_length=60)  # Field name made lowercase.
    amount = models.FloatField()
    creationtime = models.DateTimeField(db_column='creationTime')  # Field name made lowercase.
    haspr = models.IntegerField(db_column='hasPr', blank=True, null=True)  # Field name made lowercase.
    charge = models.FloatField(blank=True, null=True)
    adjustment = models.FloatField(blank=True, null=True)
    insbalance = models.FloatField(db_column='insBalance', blank=True, null=True)  # Field name made lowercase.
    inspayment = models.FloatField(db_column='insPayment', blank=True, null=True)  # Field name made lowercase.
    ptbalance = models.FloatField(db_column='ptBalance', blank=True, null=True)  # Field name made lowercase.
    ptpayment = models.FloatField(db_column='ptPayment', blank=True, null=True)  # Field name made lowercase.
    agingtime = models.DateTimeField(db_column='agingTime', blank=True, null=True)  # Field name made lowercase.
    code = models.CharField(max_length=5, blank=True, null=True)
    pendingresponse = models.IntegerField(db_column='pendingResponse')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'accountsReceivable'


