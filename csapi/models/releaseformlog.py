from __future__ import unicode_literals

from django.db import models

class Releaseformlog(models.Model):
    id = models.BigAutoField(primary_key=True)
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    user = models.CharField(max_length=30, blank=True, null=True)
    inforeleased = models.CharField(db_column='infoReleased', max_length=200, blank=True, null=True)  # Field name made lowercase.
    releaseexpiration = models.DateField(db_column='releaseExpiration', blank=True, null=True)  # Field name made lowercase.
    releasepurpose = models.CharField(db_column='releasePurpose', max_length=200, blank=True, null=True)  # Field name made lowercase.
    releasedate = models.DateTimeField(db_column='releaseDate', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'releaseFormLog'


