from __future__ import unicode_literals

from django.db import models

class Adminoptions(models.Model):
    display = models.CharField(max_length=100, blank=True, null=True)
    description = models.CharField(max_length=200, blank=True, null=True)
    filename = models.CharField(db_column='fileName', max_length=200, blank=True, null=True)  # Field name made lowercase.
    active = models.IntegerField(blank=True, null=True)
    accesslevel = models.IntegerField(db_column='accessLevel', blank=True, null=True)  # Field name made lowercase.
    windowdimensions = models.CharField(db_column='windowDimensions', max_length=100, blank=True, null=True)  # Field name made lowercase.
    licenselevels = models.CharField(db_column='licenseLevels', max_length=500, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'adminOptions'


