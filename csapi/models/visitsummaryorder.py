from __future__ import unicode_literals

from django.db import models

class Visitsummaryorder(models.Model):
    id = models.BigAutoField(primary_key=True)
    arrayindex = models.CharField(db_column='arrayIndex', max_length=35)  # Field name made lowercase.
    displayorder = models.IntegerField(db_column='displayOrder')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'visitSummaryOrder'


