from __future__ import unicode_literals

from django.db import models

class Rosdata(models.Model):
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    fever = models.IntegerField(blank=True, null=True)
    chills = models.IntegerField(blank=True, null=True)
    vision = models.IntegerField(blank=True, null=True)
    discharge = models.IntegerField(blank=True, null=True)
    sorethroat = models.IntegerField(db_column='soreThroat', blank=True, null=True)  # Field name made lowercase.
    epistaxis = models.IntegerField(blank=True, null=True)
    cp = models.IntegerField()
    angina = models.IntegerField(blank=True, null=True)
    edema = models.IntegerField(blank=True, null=True)
    cough = models.IntegerField(blank=True, null=True)
    sputum = models.IntegerField(blank=True, null=True)
    dyspnea = models.IntegerField(blank=True, null=True)
    myalgias = models.IntegerField(blank=True, null=True)
    backpain = models.IntegerField(db_column='backPain', blank=True, null=True)  # Field name made lowercase.
    neckpain = models.IntegerField(db_column='neckPain', blank=True, null=True)  # Field name made lowercase.
    ap = models.IntegerField(blank=True, null=True)
    nausea = models.IntegerField(blank=True, null=True)
    vomiting = models.IntegerField(blank=True, null=True)
    diarrhea = models.IntegerField(blank=True, null=True)
    blackstool = models.IntegerField(db_column='blackStool', blank=True, null=True)  # Field name made lowercase.
    brbpr = models.IntegerField(blank=True, null=True)
    constipation = models.IntegerField(blank=True, null=True)
    rash = models.IntegerField(blank=True, null=True)
    pruritis = models.IntegerField(blank=True, null=True)
    bleeding = models.IntegerField(blank=True, null=True)
    bruising = models.IntegerField(blank=True, null=True)
    dysuria = models.IntegerField(blank=True, null=True)
    urgency = models.IntegerField(blank=True, null=True)
    frequency = models.IntegerField(blank=True, null=True)
    vagdc = models.IntegerField(db_column='vagDc', blank=True, null=True)  # Field name made lowercase.
    testpain = models.IntegerField(db_column='testPain', blank=True, null=True)  # Field name made lowercase.
    depression = models.IntegerField(blank=True, null=True)
    sihi = models.IntegerField(db_column='siHi', blank=True, null=True)  # Field name made lowercase.
    ha = models.IntegerField(blank=True, null=True)
    loc = models.IntegerField(blank=True, null=True)
    paresthesias = models.IntegerField(blank=True, null=True)
    hives = models.IntegerField(blank=True, null=True)
    immunocompromised = models.IntegerField(blank=True, null=True)
    reviewedandneg = models.IntegerField(db_column='reviewedAndNeg', blank=True, null=True)  # Field name made lowercase.
    fatigue = models.IntegerField(blank=True, null=True)
    insomnia = models.IntegerField(blank=True, null=True)
    sweats = models.IntegerField(blank=True, null=True)
    anxiety = models.IntegerField(blank=True, null=True)
    bonepain = models.IntegerField(db_column='bonePain', blank=True, null=True)  # Field name made lowercase.
    restlessness = models.IntegerField(blank=True, null=True)
    craving = models.IntegerField(blank=True, null=True)
    sob = models.IntegerField(blank=True, null=True)
    diaphoresis = models.IntegerField(blank=True, null=True)
    hotcold = models.IntegerField(db_column='hotCold', blank=True, null=True)  # Field name made lowercase.
    genpain = models.IntegerField(db_column='genPain', blank=True, null=True)  # Field name made lowercase.
    nightsweats = models.IntegerField(db_column='nightSweats', blank=True, null=True)  # Field name made lowercase.
    cankersores = models.IntegerField(db_column='cankerSores', blank=True, null=True)  # Field name made lowercase.
    diffvoiding = models.IntegerField(db_column='diffVoiding', blank=True, null=True)  # Field name made lowercase.
    sexdys = models.IntegerField(db_column='sexDys', blank=True, null=True)  # Field name made lowercase.
    legcramps = models.IntegerField(db_column='legCramps', blank=True, null=True)  # Field name made lowercase.
    restlesslegs = models.IntegerField(db_column='restlessLegs', blank=True, null=True)  # Field name made lowercase.
    tiredness = models.IntegerField(blank=True, null=True)
    toothache = models.IntegerField(blank=True, null=True)
    dizziness = models.IntegerField(blank=True, null=True)
    decreasedapp = models.IntegerField(db_column='decreasedApp', blank=True, null=True)  # Field name made lowercase.
    musclecramps = models.IntegerField(db_column='muscleCramps', blank=True, null=True)  # Field name made lowercase.
    painfuljoints = models.IntegerField(db_column='painfulJoints', blank=True, null=True)  # Field name made lowercase.
    coldsx = models.IntegerField(db_column='coldSx', blank=True, null=True)  # Field name made lowercase.
    troublesleeping = models.IntegerField(db_column='troubleSleeping', blank=True, null=True)  # Field name made lowercase.
    hotflashes = models.IntegerField(db_column='hotFlashes', blank=True, null=True)  # Field name made lowercase.
    bodyaches = models.IntegerField(db_column='bodyAches', blank=True, null=True)  # Field name made lowercase.
    diffurination = models.IntegerField(db_column='diffUrination', blank=True, null=True)  # Field name made lowercase.
    concentration = models.IntegerField()
    memory = models.IntegerField()
    irritability = models.IntegerField()
    hopelessness = models.IntegerField()
    legal = models.IntegerField()
    feeling = models.IntegerField()
    other = models.CharField(max_length=1000, blank=True, null=True)
    exerintol = models.IntegerField(db_column='exerIntol', blank=True, null=True)  # Field name made lowercase.
    weightchange = models.IntegerField(db_column='weightChange', blank=True, null=True)  # Field name made lowercase.
    blurredvision = models.IntegerField(db_column='blurredVision', blank=True, null=True)  # Field name made lowercase.
    dryeyes = models.IntegerField(db_column='dryEyes', blank=True, null=True)  # Field name made lowercase.
    wateryeyes = models.IntegerField(db_column='wateryEyes', blank=True, null=True)  # Field name made lowercase.
    irritationeyes = models.IntegerField(db_column='irritationEyes', blank=True, null=True)  # Field name made lowercase.
    earpain = models.IntegerField(db_column='earPain', blank=True, null=True)  # Field name made lowercase.
    diffhearing = models.IntegerField(db_column='diffHearing', blank=True, null=True)  # Field name made lowercase.
    freqnosebleeds = models.IntegerField(db_column='freqNosebleeds', blank=True, null=True)  # Field name made lowercase.
    stuffynose = models.IntegerField(db_column='stuffyNose', blank=True, null=True)  # Field name made lowercase.
    runnynose = models.IntegerField(db_column='runnyNose', blank=True, null=True)  # Field name made lowercase.
    drymouth = models.IntegerField(db_column='dryMouth', blank=True, null=True)  # Field name made lowercase.
    toothabn = models.IntegerField(db_column='toothAbn', blank=True, null=True)  # Field name made lowercase.
    oralabn = models.IntegerField(db_column='oralAbn', blank=True, null=True)  # Field name made lowercase.
    bleedinggums = models.IntegerField(db_column='bleedingGums', blank=True, null=True)  # Field name made lowercase.
    snoring = models.IntegerField(blank=True, null=True)
    mouthulcer = models.IntegerField(db_column='mouthUlcer', blank=True, null=True)  # Field name made lowercase.
    palpatations = models.IntegerField(blank=True, null=True)
    heartmurmur = models.IntegerField(db_column='heartMurmur', blank=True, null=True)  # Field name made lowercase.
    armpain = models.IntegerField(db_column='armPain', blank=True, null=True)  # Field name made lowercase.
    sobwalking = models.IntegerField(db_column='sobWalking', blank=True, null=True)  # Field name made lowercase.
    soblying = models.IntegerField(db_column='sobLying', blank=True, null=True)  # Field name made lowercase.
    lightheadedstanding = models.IntegerField(db_column='lightHeadedStanding', blank=True, null=True)  # Field name made lowercase.
    drycough = models.IntegerField(db_column='dryCough', blank=True, null=True)  # Field name made lowercase.
    prodcough = models.IntegerField(db_column='prodCough', blank=True, null=True)  # Field name made lowercase.
    wheezing = models.IntegerField(blank=True, null=True)
    coughingblood = models.IntegerField(db_column='coughingBlood', blank=True, null=True)  # Field name made lowercase.
    diffbreathing = models.IntegerField(db_column='diffBreathing', blank=True, null=True)  # Field name made lowercase.
    appchange = models.IntegerField(db_column='appChange', blank=True, null=True)  # Field name made lowercase.
    urinarylosscontrol = models.IntegerField(db_column='urinaryLossControl', blank=True, null=True)  # Field name made lowercase.
    diffurinating = models.IntegerField(db_column='diffUrinating', blank=True, null=True)  # Field name made lowercase.
    bloodurine = models.IntegerField(db_column='bloodUrine', blank=True, null=True)  # Field name made lowercase.
    incbladderempty = models.IntegerField(db_column='incBladderEmpty', blank=True, null=True)  # Field name made lowercase.
    muscleaches = models.IntegerField(db_column='muscleAches', blank=True, null=True)  # Field name made lowercase.
    muscleweakness = models.IntegerField(db_column='muscleWeakness', blank=True, null=True)  # Field name made lowercase.
    jointpain = models.IntegerField(db_column='jointPain', blank=True, null=True)  # Field name made lowercase.
    jaundice = models.IntegerField(blank=True, null=True)
    dryskin = models.IntegerField(db_column='drySkin', blank=True, null=True)  # Field name made lowercase.
    skinlesions = models.IntegerField(db_column='skinLesions', blank=True, null=True)  # Field name made lowercase.
    skinitching = models.IntegerField(db_column='skinItching', blank=True, null=True)  # Field name made lowercase.
    seizures = models.IntegerField(blank=True, null=True)
    freqha = models.IntegerField(db_column='freqHa', blank=True, null=True)  # Field name made lowercase.
    migraines = models.IntegerField(blank=True, null=True)
    numbness = models.IntegerField(blank=True, null=True)
    prickling = models.IntegerField(blank=True, null=True)
    difffallasleep = models.IntegerField(db_column='diffFallAsleep', blank=True, null=True)  # Field name made lowercase.
    diffstayasleep = models.IntegerField(db_column='diffStayAsleep', blank=True, null=True)  # Field name made lowercase.
    narcaddiction = models.IntegerField(db_column='narcAddiction', blank=True, null=True)  # Field name made lowercase.
    increasedthirst = models.IntegerField(db_column='increasedThirst', blank=True, null=True)  # Field name made lowercase.
    hairloss = models.IntegerField(db_column='hairLoss', blank=True, null=True)  # Field name made lowercase.
    hairgrowth = models.IntegerField(db_column='hairGrowth', blank=True, null=True)  # Field name made lowercase.
    coldint = models.IntegerField(db_column='coldInt', blank=True, null=True)  # Field name made lowercase.
    swollenglands = models.IntegerField(db_column='swollenGlands', blank=True, null=True)  # Field name made lowercase.
    easybruising = models.IntegerField(db_column='easyBruising', blank=True, null=True)  # Field name made lowercase.
    excessbleeding = models.IntegerField(db_column='excessBleeding', blank=True, null=True)  # Field name made lowercase.
    freqsneezing = models.IntegerField(db_column='freqSneezing', blank=True, null=True)  # Field name made lowercase.
    sinuspressure = models.IntegerField(db_column='sinusPressure', blank=True, null=True)  # Field name made lowercase.
    itching = models.IntegerField(blank=True, null=True)
    eyepain = models.IntegerField(db_column='eyePain', blank=True, null=True)  # Field name made lowercase.
    dicharge = models.IntegerField(blank=True, null=True)
    descreasedvision = models.IntegerField(db_column='descreasedVision', blank=True, null=True)  # Field name made lowercase.
    tinnitus = models.IntegerField(blank=True, null=True)
    bloodynose = models.IntegerField(db_column='bloodyNose', blank=True, null=True)  # Field name made lowercase.
    hearingloss = models.IntegerField(db_column='hearingLoss', blank=True, null=True)  # Field name made lowercase.
    sinusitus = models.IntegerField(blank=True, null=True)
    hemoptysis = models.IntegerField(blank=True, null=True)
    pleurisy = models.IntegerField(blank=True, null=True)
    pnd = models.IntegerField(blank=True, null=True)
    orthopnea = models.IntegerField(blank=True, null=True)
    syncope = models.IntegerField(blank=True, null=True)
    hematemesis = models.IntegerField(blank=True, null=True)
    melena = models.IntegerField(blank=True, null=True)
    hematuria = models.IntegerField(blank=True, null=True)
    hesistancy = models.IntegerField(blank=True, null=True)
    incontinence = models.IntegerField(blank=True, null=True)
    uti = models.IntegerField(blank=True, null=True)
    arthralgia = models.IntegerField(blank=True, null=True)
    jointswelling = models.IntegerField(db_column='jointSwelling', blank=True, null=True)  # Field name made lowercase.
    nsaid = models.IntegerField(blank=True, null=True)
    sores = models.IntegerField(blank=True, null=True)
    nailchanges = models.IntegerField(db_column='nailChanges', blank=True, null=True)  # Field name made lowercase.
    skinthickening = models.IntegerField(db_column='skinThickening', blank=True, null=True)  # Field name made lowercase.
    ataxia = models.IntegerField(blank=True, null=True)
    tremors = models.IntegerField(blank=True, null=True)
    vertigo = models.IntegerField(blank=True, null=True)
    excessthirst = models.IntegerField(db_column='excessThirst', blank=True, null=True)  # Field name made lowercase.
    polyuria = models.IntegerField(blank=True, null=True)
    hotint = models.IntegerField(db_column='hotInt', blank=True, null=True)  # Field name made lowercase.
    goiter = models.IntegerField(blank=True, null=True)
    antidepressants = models.IntegerField(db_column='antiDepressants', blank=True, null=True)  # Field name made lowercase.
    alcabuse = models.IntegerField(db_column='alcAbuse', blank=True, null=True)  # Field name made lowercase.
    drugabuse = models.IntegerField(db_column='drugAbuse', blank=True, null=True)  # Field name made lowercase.
    bleedingdiathesis = models.IntegerField(db_column='bleedingDiathesis', blank=True, null=True)  # Field name made lowercase.
    bloodclots = models.IntegerField(db_column='bloodClots', blank=True, null=True)  # Field name made lowercase.
    lymphedema = models.IntegerField(blank=True, null=True)
    allergicrhinitis = models.IntegerField(db_column='allergicRhinitis', blank=True, null=True)  # Field name made lowercase.
    hayfever = models.IntegerField(db_column='hayFever', blank=True, null=True)  # Field name made lowercase.
    asthma = models.IntegerField(blank=True, null=True)
    positiveppd = models.IntegerField(db_column='positivePPD', blank=True, null=True)  # Field name made lowercase.
    mentalillness = models.IntegerField(db_column='mentalIllness', blank=True, null=True)  # Field name made lowercase.
    weightloss = models.IntegerField(db_column='weightLoss', blank=True, null=True)  # Field name made lowercase.
    mouthlesions = models.IntegerField(db_column='mouthLesions', blank=True, null=True)  # Field name made lowercase.
    rinorrhea = models.IntegerField(blank=True, null=True)
    abcess = models.IntegerField(blank=True, null=True)
    trackmarks = models.IntegerField(db_column='trackMarks', blank=True, null=True)  # Field name made lowercase.
    mouth_lesions = models.IntegerField(db_column='mouth lesions')  # Field renamed to remove unsuitable characters.
    track_marks = models.IntegerField(db_column='track marks')  # Field renamed to remove unsuitable characters.
    acidref = models.IntegerField(db_column='acidRef')  # Field name made lowercase.
    bladdercontrol = models.IntegerField(db_column='bladderControl')  # Field name made lowercase.
    bladderinf = models.IntegerField(db_column='bladderInf')  # Field name made lowercase.
    bowelcont = models.IntegerField(db_column='bowelCont')  # Field name made lowercase.
    darkstool = models.IntegerField(db_column='darkStool')  # Field name made lowercase.
    dentalprobs = models.IntegerField(db_column='dentalProbs')  # Field name made lowercase.
    depressedmood = models.IntegerField(db_column='depressedMood')  # Field name made lowercase.
    difficultysleeping = models.IntegerField(db_column='difficultySleeping')  # Field name made lowercase.
    diffswallowing = models.IntegerField(db_column='diffSwallowing')  # Field name made lowercase.
    dry = models.IntegerField()
    earaches = models.IntegerField()
    excesssweats = models.IntegerField(db_column='excessSweats')  # Field name made lowercase.
    faint = models.IntegerField()
    fainting = models.IntegerField()
    feelinganx = models.IntegerField(db_column='feelingAnx')  # Field name made lowercase.
    fractures = models.IntegerField()
    glassescontacts = models.IntegerField(db_column='glassesContacts')  # Field name made lowercase.
    hearingprobs = models.IntegerField(db_column='hearingProbs')  # Field name made lowercase.
    heatcold = models.IntegerField(db_column='heatCold')  # Field name made lowercase.
    hep = models.IntegerField()
    highbp = models.IntegerField(db_column='highBP')  # Field name made lowercase.
    hiv = models.IntegerField()
    instability = models.IntegerField()
    irrheartbeat = models.IntegerField(db_column='irrHeartbeat')  # Field name made lowercase.
    itchiness = models.IntegerField()
    jointstiff = models.IntegerField(db_column='jointStiff')  # Field name made lowercase.
    jointswell = models.IntegerField(db_column='jointSwell')  # Field name made lowercase.
    lightheaded = models.IntegerField()
    limb = models.IntegerField()
    memoryloss = models.IntegerField(db_column='memoryLoss')  # Field name made lowercase.
    moles = models.IntegerField()
    musclespasms = models.IntegerField(db_column='muscleSpasms')  # Field name made lowercase.
    nosebleeds = models.IntegerField()
    numbting = models.IntegerField(db_column='numbTing')  # Field name made lowercase.
    painurine = models.IntegerField(db_column='painUrine')  # Field name made lowercase.
    recsorethroat = models.IntegerField(db_column='recSoreThroat')  # Field name made lowercase.
    ringingears = models.IntegerField(db_column='ringingEars')  # Field name made lowercase.
    senstolight = models.IntegerField(db_column='sensToLight')  # Field name made lowercase.
    sexdrive = models.IntegerField(db_column='sexDrive')  # Field name made lowercase.
    shortbreathexert = models.IntegerField(db_column='shortBreathExert')  # Field name made lowercase.
    shortbreathrest = models.IntegerField(db_column='shortBreathRest')  # Field name made lowercase.
    sinusprobs = models.IntegerField(db_column='sinusProbs')  # Field name made lowercase.
    stressprobs = models.IntegerField(db_column='stressProbs')  # Field name made lowercase.
    swellingfeet = models.IntegerField(db_column='swellingFeet')  # Field name made lowercase.
    swollenlymph = models.IntegerField(db_column='swollenLymph')  # Field name made lowercase.
    tb = models.IntegerField()
    vomappearance = models.IntegerField(db_column='vomAppearance')  # Field name made lowercase.
    weakness = models.IntegerField()
    weightchg = models.IntegerField(db_column='weightChg')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'rosData'


