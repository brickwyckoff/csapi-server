from __future__ import unicode_literals

from django.db import models

class Clinicstatus(models.Model):
    status = models.CharField(max_length=100, blank=True, null=True)
    code = models.IntegerField(blank=True, null=True)
    displayorder = models.IntegerField(db_column='displayOrder', blank=True, null=True)  # Field name made lowercase.
    clinicgroup = models.BigIntegerField(db_column='clinicGroup', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'clinicStatus'


