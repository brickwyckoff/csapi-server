from __future__ import unicode_literals

from django.db import models

class Immediaterxavailable(models.Model):
    id = models.BigAutoField(primary_key=True)
    code = models.CharField(max_length=15)
    remaining = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'immediateRxAvailable'


