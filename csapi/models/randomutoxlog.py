from __future__ import unicode_literals

from django.db import models

class Randomutoxlog(models.Model):
    id = models.BigAutoField(primary_key=True)
    mrn = models.BigIntegerField(blank=True, null=True)
    clinicid = models.BigIntegerField(db_column='clinicId', blank=True, null=True)  # Field name made lowercase.
    randomdate = models.DateField(db_column='randomDate', blank=True, null=True)  # Field name made lowercase.
    calledin = models.IntegerField(db_column='calledIn')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'randomUtoxLog'


