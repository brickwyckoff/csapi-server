from __future__ import unicode_literals

from django.db import models

class Maodata(models.Model):
    mrn = models.BigIntegerField(blank=True, null=True)
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    user = models.CharField(max_length=80, blank=True, null=True)
    entrydate = models.DateTimeField(db_column='entryDate')  # Field name made lowercase.
    section = models.CharField(max_length=10, blank=True, null=True)
    medication = models.CharField(max_length=200, blank=True, null=True)
    dosage = models.CharField(max_length=50, blank=True, null=True)
    observation = models.CharField(max_length=1500, blank=True, null=True)
    condition = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'maoData'


