from __future__ import unicode_literals

from django.db import models

class Mobilelabview(models.Model):
    labcode = models.CharField(db_column='labCode', max_length=15, blank=True, null=True)  # Field name made lowercase.
    viewnumber = models.IntegerField(db_column='viewNumber', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'mobileLabView'


