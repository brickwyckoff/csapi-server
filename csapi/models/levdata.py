from __future__ import unicode_literals

from django.db import models

class Levdata(models.Model):
    id = models.BigAutoField()
    visitnum = models.BigIntegerField(db_column='visitNum', primary_key=True)  # Field name made lowercase.
    treatmentlevel = models.IntegerField(db_column='treatmentLevel', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'levData'


