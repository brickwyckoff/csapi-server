from __future__ import unicode_literals

from django.db import models

class DeniedclaimsCopy5Oktodelete(models.Model):
    payerid = models.CharField(db_column='payerId', max_length=15, blank=True, null=True)  # Field name made lowercase.
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    company = models.CharField(max_length=50, blank=True, null=True)
    returndate = models.DateField(db_column='returnDate', blank=True, null=True)  # Field name made lowercase.
    user = models.CharField(max_length=30, blank=True, null=True)
    creationtime = models.DateTimeField(db_column='creationTime', blank=True, null=True)  # Field name made lowercase.
    denyroute = models.IntegerField(db_column='denyRoute', blank=True, null=True)  # Field name made lowercase.
    denyreason = models.CharField(db_column='denyReason', max_length=500)  # Field name made lowercase.
    proccode = models.CharField(db_column='procCode', max_length=10, blank=True, null=True)  # Field name made lowercase.
    payortrackingnum = models.CharField(db_column='payorTrackingNum', max_length=35, blank=True, null=True)  # Field name made lowercase.
    claimstatuscodes = models.CharField(db_column='claimStatusCodes', max_length=185, blank=True, null=True)  # Field name made lowercase.
    claimsuffix = models.CharField(db_column='claimSuffix', max_length=5, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'deniedClaims_copy5OkToDelete'


