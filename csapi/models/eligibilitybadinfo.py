from __future__ import unicode_literals

from django.db import models

class Eligibilitybadinfo(models.Model):
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    badmessage = models.CharField(db_column='badMessage', max_length=100, blank=True, null=True)  # Field name made lowercase.
    creationtime = models.DateTimeField(db_column='creationTime', blank=True, null=True)  # Field name made lowercase.
    controlnum = models.CharField(db_column='controlNum', max_length=35)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'eligibilityBadInfo'


