from __future__ import unicode_literals

from django.db import models

class Providerinsurancecredentials(models.Model):
    id = models.BigAutoField(primary_key=True)
    provider = models.CharField(max_length=35, blank=True, null=True)
    insurance = models.CharField(max_length=80, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'providerInsuranceCredentials'


