from __future__ import unicode_literals

from django.db import models

class Settingsinfo(models.Model):
    id = models.BigAutoField(primary_key=True)
    fieldname = models.CharField(db_column='fieldName', max_length=80)  # Field name made lowercase.
    shortdescription = models.CharField(db_column='shortDescription', max_length=50)  # Field name made lowercase.
    fielddescription = models.CharField(db_column='fieldDescription', max_length=500)  # Field name made lowercase.
    accesslevel = models.IntegerField(db_column='accessLevel')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'settingsInfo'


