from __future__ import unicode_literals

from django.db import models

class Rxdashboard(models.Model):
    id = models.BigAutoField(primary_key=True)
    mrn = models.BigIntegerField(blank=True, null=True)
    rxid = models.BigIntegerField(db_column='rxId', blank=True, null=True)  # Field name made lowercase.
    lastvisitdate = models.DateField(db_column='lastVisitDate', blank=True, null=True)  # Field name made lowercase.
    lastvisitnum = models.BigIntegerField(db_column='lastVisitNum', blank=True, null=True)  # Field name made lowercase.
    treatmentlevel = models.CharField(db_column='treatmentLevel', max_length=20, blank=True, null=True)  # Field name made lowercase.
    clinic = models.BigIntegerField(blank=True, null=True)
    maindoctor = models.CharField(db_column='mainDoctor', max_length=2, blank=True, null=True)  # Field name made lowercase.
    prescriber = models.CharField(max_length=2, blank=True, null=True)
    medication = models.CharField(max_length=20, blank=True, null=True)
    prescription = models.CharField(max_length=100, blank=True, null=True)
    ncpdpid = models.CharField(max_length=8, blank=True, null=True)
    status = models.CharField(max_length=50, blank=True, null=True)
    rxdate = models.DateField(db_column='rxDate', blank=True, null=True)  # Field name made lowercase.
    rxtype = models.CharField(db_column='rxType', max_length=20, blank=True, null=True)  # Field name made lowercase.
    isactive = models.IntegerField(db_column='isActive')  # Field name made lowercase.
    iscurrentpending = models.IntegerField(db_column='isCurrentPending')  # Field name made lowercase.
    iscancelled = models.IntegerField(db_column='isCancelled', blank=True, null=True)  # Field name made lowercase.
    activeoutdate = models.DateField(db_column='activeOutDate', blank=True, null=True)  # Field name made lowercase.
    pendingprintqueuedate = models.DateField(db_column='pendingPrintQueueDate', blank=True, null=True)  # Field name made lowercase.
    pendingstatus = models.CharField(db_column='pendingStatus', max_length=50, blank=True, null=True)  # Field name made lowercase.
    missinginfo = models.CharField(db_column='missingInfo', max_length=300, blank=True, null=True)  # Field name made lowercase.
    currentissues = models.CharField(db_column='currentIssues', max_length=500, blank=True, null=True)  # Field name made lowercase.
    printbatch = models.BigIntegerField(db_column='printBatch', blank=True, null=True)  # Field name made lowercase.
    scanbatch = models.BigIntegerField(db_column='scanBatch', blank=True, null=True)  # Field name made lowercase.
    donotfilldate = models.DateField(db_column='doNotFillDate', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'rxDashboard'


