from __future__ import unicode_literals

from django.db import models

class Customformstables(models.Model):
    id = models.BigAutoField(primary_key=True)
    formname = models.CharField(db_column='formName', max_length=500)  # Field name made lowercase.
    tablename = models.CharField(db_column='tableName', max_length=7)  # Field name made lowercase.
    indextype = models.IntegerField(db_column='indexType')  # Field name made lowercase.
    createdby = models.CharField(db_column='createdBy', max_length=35)  # Field name made lowercase.
    creationtime = models.DateTimeField(db_column='creationTime')  # Field name made lowercase.
    isactive = models.IntegerField(db_column='isActive')  # Field name made lowercase.
    formlocation = models.CharField(db_column='formLocation', max_length=15)  # Field name made lowercase.
    syncfields = models.TextField(db_column='syncFields', blank=True, null=True)  # Field name made lowercase.
    formnamecode = models.CharField(db_column='formNameCode', max_length=35, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'customFormsTables'


