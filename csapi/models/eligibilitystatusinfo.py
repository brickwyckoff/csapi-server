from __future__ import unicode_literals

from django.db import models

class Eligibilitystatusinfo(models.Model):
    verifiedid = models.BigIntegerField(db_column='verifiedId', blank=True, null=True)  # Field name made lowercase.
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    insco = models.CharField(db_column='insCo', max_length=60, blank=True, null=True)  # Field name made lowercase.
    status = models.CharField(max_length=3, blank=True, null=True)
    coveragelevel = models.CharField(db_column='coverageLevel', max_length=60, blank=True, null=True)  # Field name made lowercase.
    servicetype = models.CharField(db_column='serviceType', max_length=60, blank=True, null=True)  # Field name made lowercase.
    insurancetype = models.CharField(db_column='insuranceType', max_length=3, blank=True, null=True)  # Field name made lowercase.
    plandescription = models.CharField(db_column='planDescription', max_length=100, blank=True, null=True)  # Field name made lowercase.
    creationtime = models.DateTimeField(db_column='creationTime', blank=True, null=True)  # Field name made lowercase.
    monetaryamount = models.CharField(db_column='monetaryAmount', max_length=18, blank=True, null=True)  # Field name made lowercase.
    percent = models.CharField(max_length=10, blank=True, null=True)
    quantityqual = models.CharField(db_column='quantityQual', max_length=2, blank=True, null=True)  # Field name made lowercase.
    quantity = models.CharField(max_length=15, blank=True, null=True)
    authorization = models.CharField(max_length=1, blank=True, null=True)
    inplan = models.CharField(db_column='inPlan', max_length=1, blank=True, null=True)  # Field name made lowercase.
    timeperiodqual = models.CharField(db_column='timePeriodQual', max_length=2, blank=True, null=True)  # Field name made lowercase.
    ebid = models.BigIntegerField(db_column='ebId', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'eligibilityStatusInfo'


