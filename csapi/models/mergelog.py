from __future__ import unicode_literals

from django.db import models

class Mergelog(models.Model):
    keepmrn = models.BigIntegerField(db_column='keepMrn', blank=True, null=True)  # Field name made lowercase.
    mergemrn = models.BigIntegerField(db_column='mergeMrn', blank=True, null=True)  # Field name made lowercase.
    user = models.CharField(max_length=60, blank=True, null=True)
    mergetime = models.DateTimeField(db_column='mergeTime', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'mergeLog'


