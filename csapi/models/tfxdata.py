from __future__ import unicode_literals

from django.db import models

class Tfxdata(models.Model):
    id = models.BigAutoField(primary_key=True)
    visitnum = models.BigIntegerField(db_column='visitNum')  # Field name made lowercase.
    medrecin = models.IntegerField(db_column='medRecIn')  # Field name made lowercase.
    pttransferedin = models.IntegerField(db_column='ptTransferedIn')  # Field name made lowercase.
    sumcare = models.IntegerField(db_column='sumCare')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'tfxData'


