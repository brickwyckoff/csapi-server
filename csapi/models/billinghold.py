from __future__ import unicode_literals

from django.db import models

class Billinghold(models.Model):
    id = models.BigAutoField(primary_key=True)
    mrn = models.BigIntegerField(blank=True, null=True)
    visitdate = models.DateField(db_column='visitDate', blank=True, null=True)  # Field name made lowercase.
    holdtime = models.DateTimeField(db_column='holdTime', blank=True, null=True)  # Field name made lowercase.
    status = models.IntegerField(blank=True, null=True)
    releasedby = models.CharField(db_column='releasedBy', max_length=30, blank=True, null=True)  # Field name made lowercase.
    holdreason = models.CharField(db_column='holdReason', max_length=60, blank=True, null=True)  # Field name made lowercase.
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    releasedtime = models.DateTimeField(db_column='releasedTime')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'billingHold'


