from __future__ import unicode_literals

from django.db import models

class Accountingnotes(models.Model):
    notes = models.CharField(max_length=500, blank=True, null=True)
    creationtime = models.DateTimeField(db_column='creationTime')  # Field name made lowercase.
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    user = models.CharField(max_length=30)
    mrn = models.BigIntegerField()

    class Meta:
        managed = False
        db_table = 'accountingNotes'


