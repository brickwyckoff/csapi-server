from __future__ import unicode_literals

from django.db import models

class Statrxauth(models.Model):
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    prescription = models.CharField(max_length=60, blank=True, null=True)
    freq = models.CharField(max_length=20, blank=True, null=True)
    qty = models.CharField(max_length=7, blank=True, null=True)
    route = models.CharField(max_length=20, blank=True, null=True)
    refills = models.CharField(max_length=7, blank=True, null=True)
    pharmacyid = models.IntegerField(db_column='pharmacyId', blank=True, null=True)  # Field name made lowercase.
    requestedby = models.CharField(db_column='requestedBy', max_length=30, blank=True, null=True)  # Field name made lowercase.
    requestedtime = models.DateTimeField(db_column='requestedTime', blank=True, null=True)  # Field name made lowercase.
    authorizedby = models.CharField(db_column='authorizedBy', max_length=30, blank=True, null=True)  # Field name made lowercase.
    authorizedtime = models.DateTimeField(db_column='authorizedTime', blank=True, null=True)  # Field name made lowercase.
    imid = models.CharField(db_column='imId', max_length=300, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'statRxAuth'


