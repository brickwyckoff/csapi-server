from __future__ import unicode_literals

from django.db import models

class Roomnumbers(models.Model):
    roomnumber = models.CharField(db_column='roomNumber', max_length=5, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'roomNumbers'


