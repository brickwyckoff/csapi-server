from __future__ import unicode_literals

from django.db import models

class Loginhistory(models.Model):
    username = models.CharField(max_length=50)
    time = models.DateTimeField()
    ip = models.CharField(max_length=30)
    valid = models.IntegerField()
    serverip = models.CharField(db_column='serverIp', max_length=30, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'loginHistory'


