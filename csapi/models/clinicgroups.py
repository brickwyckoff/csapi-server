from __future__ import unicode_literals

from django.db import models

class Clinicgroups(models.Model):
    groupname = models.CharField(db_column='groupName', max_length=60, blank=True, null=True)  # Field name made lowercase.
    advancedmdlinkname = models.CharField(db_column='advancedMdLinkName', max_length=50, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'clinicGroups'


