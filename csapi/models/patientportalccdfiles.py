from __future__ import unicode_literals

from django.db import models

class Patientportalccdfiles(models.Model):
    id = models.BigAutoField(primary_key=True)
    mrn = models.BigIntegerField(blank=True, null=True)
    ccdfilename = models.CharField(db_column='ccdFileName', max_length=50, blank=True, null=True)  # Field name made lowercase.
    generatedtime = models.DateTimeField(db_column='generatedTime', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'patientPortalCcdFiles'


