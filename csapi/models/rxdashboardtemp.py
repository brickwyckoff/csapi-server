from __future__ import unicode_literals

from django.db import models

class Rxdashboardtemp(models.Model):
    id = models.BigAutoField(primary_key=True)
    insertquery = models.TextField(db_column='insertQuery')  # Field name made lowercase.
    prescription = models.TextField()
    mrn = models.TextField()
    rxid = models.TextField(db_column='rxId')  # Field name made lowercase.
    numperdose = models.TextField(db_column='numPerDose')  # Field name made lowercase.
    freq = models.TextField()
    route = models.TextField()
    qty = models.TextField()
    refills = models.TextField()
    prescriber = models.TextField()

    class Meta:
        managed = False
        db_table = 'rxDashboardTemp'


