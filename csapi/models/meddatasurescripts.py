from __future__ import unicode_literals

from django.db import models

class Meddatasurescripts(models.Model):
    mrn = models.BigIntegerField(blank=True, null=True)
    ndc = models.CharField(max_length=11, blank=True, null=True)
    drugid = models.CharField(db_column='drugId', max_length=10, blank=True, null=True)  # Field name made lowercase.
    tradeid = models.IntegerField(db_column='tradeId', blank=True, null=True)  # Field name made lowercase.
    doseform = models.IntegerField(db_column='doseForm', blank=True, null=True)  # Field name made lowercase.
    route = models.IntegerField(blank=True, null=True)
    strength = models.IntegerField(blank=True, null=True)
    startdate = models.DateField(db_column='startDate', blank=True, null=True)  # Field name made lowercase.
    enddate = models.DateField(db_column='endDate', blank=True, null=True)  # Field name made lowercase.
    freetext = models.CharField(db_column='freeText', max_length=80, blank=True, null=True)  # Field name made lowercase.
    genericbrand = models.CharField(db_column='genericBrand', max_length=1, blank=True, null=True)  # Field name made lowercase.
    rxotc = models.CharField(db_column='rxOtc', max_length=6, blank=True, null=True)  # Field name made lowercase.
    reviewed = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'medDataSurescripts'


