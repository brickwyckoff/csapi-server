from __future__ import unicode_literals

from django.db import models

class Jaidata(models.Model):
    mrn = models.BigIntegerField(blank=True, null=True)
    visitnum = models.IntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    visitdate = models.DateField(db_column='visitDate', blank=True, null=True)  # Field name made lowercase.
    evalintervalid = models.IntegerField(db_column='evalIntervalId', blank=True, null=True)  # Field name made lowercase.
    injectiondate = models.DateField(db_column='injectionDate', blank=True, null=True)  # Field name made lowercase.
    thccraving = models.IntegerField(db_column='thcCraving', blank=True, null=True)  # Field name made lowercase.
    coccraving = models.IntegerField(db_column='cocCraving', blank=True, null=True)  # Field name made lowercase.
    ampcraving = models.IntegerField(db_column='ampCraving', blank=True, null=True)  # Field name made lowercase.
    pcpcraving = models.IntegerField(db_column='pcpCraving', blank=True, null=True)  # Field name made lowercase.
    opicraving = models.IntegerField(db_column='opiCraving', blank=True, null=True)  # Field name made lowercase.
    metcraving = models.IntegerField(db_column='metCraving', blank=True, null=True)  # Field name made lowercase.
    barcraving = models.IntegerField(db_column='barCraving', blank=True, null=True)  # Field name made lowercase.
    bzocraving = models.IntegerField(db_column='bzoCraving', blank=True, null=True)  # Field name made lowercase.
    mtdcraving = models.IntegerField(db_column='mtdCraving', blank=True, null=True)  # Field name made lowercase.
    oxycraving = models.IntegerField(db_column='oxyCraving', blank=True, null=True)  # Field name made lowercase.
    thcuse = models.IntegerField(db_column='thcUse', blank=True, null=True)  # Field name made lowercase.
    cocuse = models.IntegerField(db_column='cocUse', blank=True, null=True)  # Field name made lowercase.
    ampuse = models.IntegerField(db_column='ampUse', blank=True, null=True)  # Field name made lowercase.
    pcpuse = models.IntegerField(db_column='pcpUse', blank=True, null=True)  # Field name made lowercase.
    opiuse = models.IntegerField(db_column='opiUse', blank=True, null=True)  # Field name made lowercase.
    metuse = models.IntegerField(db_column='metUse', blank=True, null=True)  # Field name made lowercase.
    baruse = models.IntegerField(db_column='barUse', blank=True, null=True)  # Field name made lowercase.
    bzouse = models.IntegerField(db_column='bzoUse', blank=True, null=True)  # Field name made lowercase.
    mtduse = models.IntegerField(db_column='mtdUse', blank=True, null=True)  # Field name made lowercase.
    oxyuse = models.IntegerField(db_column='oxyUse', blank=True, null=True)  # Field name made lowercase.
    alcuse = models.IntegerField(db_column='alcUse', blank=True, null=True)  # Field name made lowercase.
    tobuse = models.IntegerField(db_column='tobUse', blank=True, null=True)  # Field name made lowercase.
    alcusedays = models.IntegerField(db_column='alcUseDays', blank=True, null=True)  # Field name made lowercase.
    tobusedays = models.IntegerField(db_column='tobUseDays', blank=True, null=True)  # Field name made lowercase.
    thcusedays = models.IntegerField(db_column='thcUseDays', blank=True, null=True)  # Field name made lowercase.
    cocusedays = models.IntegerField(db_column='cocUseDays', blank=True, null=True)  # Field name made lowercase.
    ampusedays = models.IntegerField(db_column='ampUseDays', blank=True, null=True)  # Field name made lowercase.
    pcpusedays = models.IntegerField(db_column='pcpUseDays', blank=True, null=True)  # Field name made lowercase.
    opiusedays = models.IntegerField(db_column='opiUseDays', blank=True, null=True)  # Field name made lowercase.
    metusedays = models.IntegerField(db_column='metUseDays', blank=True, null=True)  # Field name made lowercase.
    barusedays = models.IntegerField(db_column='barUseDays', blank=True, null=True)  # Field name made lowercase.
    bzousedays = models.IntegerField(db_column='bzoUseDays', blank=True, null=True)  # Field name made lowercase.
    mtdusedays = models.IntegerField(db_column='mtdUseDays', blank=True, null=True)  # Field name made lowercase.
    oxyusedays = models.IntegerField(db_column='oxyUseDays', blank=True, null=True)  # Field name made lowercase.
    othuse = models.CharField(db_column='othUse', max_length=100, blank=True, null=True)  # Field name made lowercase.
    relapsedate = models.DateField(db_column='relapseDate', blank=True, null=True)  # Field name made lowercase.
    overdosedate1 = models.DateField(db_column='overdoseDate1', blank=True, null=True)  # Field name made lowercase.
    overdosedate2 = models.DateField(db_column='overdoseDate2', blank=True, null=True)  # Field name made lowercase.
    overdosedate3 = models.DateField(db_column='overdoseDate3', blank=True, null=True)  # Field name made lowercase.
    overdosedate4 = models.DateField(db_column='overdoseDate4', blank=True, null=True)  # Field name made lowercase.
    opioverdosedate1 = models.DateField(db_column='opiOverdoseDate1', blank=True, null=True)  # Field name made lowercase.
    opioverdosedate2 = models.DateField(db_column='opiOverdoseDate2', blank=True, null=True)  # Field name made lowercase.
    employment = models.CharField(max_length=200, blank=True, null=True)
    lastcounselingdate = models.DateField(db_column='lastCounselingDate', blank=True, null=True)  # Field name made lowercase.
    activeinaa = models.IntegerField(db_column='activeInAA', blank=True, null=True)  # Field name made lowercase.
    activeinna = models.IntegerField(db_column='activeInNA', blank=True, null=True)  # Field name made lowercase.
    activeinsmart = models.IntegerField(db_column='activeInSmart', blank=True, null=True)  # Field name made lowercase.
    urgetouse1 = models.IntegerField(db_column='urgeToUse1', blank=True, null=True)  # Field name made lowercase.
    urgetouse2 = models.IntegerField(db_column='urgeToUse2', blank=True, null=True)  # Field name made lowercase.
    urgetouse3 = models.IntegerField(db_column='urgeToUse3', blank=True, null=True)  # Field name made lowercase.
    urgetouse4 = models.IntegerField(db_column='urgeToUse4', blank=True, null=True)  # Field name made lowercase.
    urgetouse5 = models.IntegerField(db_column='urgeToUse5', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'jaiData'


