from __future__ import unicode_literals

from django.db import models

class Payprosstoredcards(models.Model):
    mrn = models.BigIntegerField(blank=True, null=True)
    cardnum = models.CharField(db_column='cardNum', max_length=30, blank=True, null=True)  # Field name made lowercase.
    expdate = models.DateField(db_column='expDate', blank=True, null=True)  # Field name made lowercase.
    storeddate = models.DateTimeField(db_column='storedDate', blank=True, null=True)  # Field name made lowercase.
    user = models.CharField(max_length=35, blank=True, null=True)
    cardpayerid = models.CharField(db_column='cardPayerId', max_length=50, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'payProsStoredCards'


