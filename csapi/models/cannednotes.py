from __future__ import unicode_literals

from django.db import models

class Cannednotes(models.Model):
    id = models.BigAutoField(primary_key=True)
    visittype = models.BigIntegerField(db_column='visitType')  # Field name made lowercase.
    notetext = models.TextField(db_column='noteText')  # Field name made lowercase.
    notetable = models.CharField(db_column='noteTable', max_length=35)  # Field name made lowercase.
    notefield = models.CharField(db_column='noteField', max_length=80)  # Field name made lowercase.
    isoverwrite = models.IntegerField(db_column='isOverwrite')  # Field name made lowercase.
    issup = models.IntegerField(db_column='isSup')  # Field name made lowercase.
    isactive = models.IntegerField(db_column='isActive')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'cannedNotes'


