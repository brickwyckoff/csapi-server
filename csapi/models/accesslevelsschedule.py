from __future__ import unicode_literals

from django.db import models

class Accesslevelsschedule(models.Model):
    levelcode = models.IntegerField(db_column='levelCode', blank=True, null=True)  # Field name made lowercase.
    leveldescription = models.CharField(db_column='levelDescription', max_length=20, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'accessLevelsSchedule'


