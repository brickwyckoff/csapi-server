from __future__ import unicode_literals

from django.db import models

class Patientreminders(models.Model):
    mrn = models.BigIntegerField(blank=True, null=True)
    reminderid = models.IntegerField(db_column='reminderId', blank=True, null=True)  # Field name made lowercase.
    timegenerated = models.DateTimeField(db_column='timeGenerated', blank=True, null=True)  # Field name made lowercase.
    iscritical = models.IntegerField(db_column='isCritical', blank=True, null=True)  # Field name made lowercase.
    reviewedby = models.CharField(db_column='reviewedBy', max_length=35, blank=True, null=True)  # Field name made lowercase.
    timereviewed = models.DateTimeField(db_column='timeReviewed', blank=True, null=True)  # Field name made lowercase.
    freetext = models.CharField(db_column='freeText', max_length=500)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'patientReminders'


