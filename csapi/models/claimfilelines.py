from __future__ import unicode_literals

from django.db import models

class Claimfilelines(models.Model):
    batchcontrolnumber = models.CharField(db_column='batchControlNumber', max_length=35, blank=True, null=True)  # Field name made lowercase.
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    clientdatabase = models.CharField(db_column='clientDatabase', max_length=35, blank=True, null=True)  # Field name made lowercase.
    linetype = models.CharField(db_column='lineType', max_length=5, blank=True, null=True)  # Field name made lowercase.
    claimline = models.TextField(db_column='claimLine', blank=True, null=True)  # Field name made lowercase.
    creationtime = models.DateTimeField(db_column='creationTime', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'claimFileLines'


