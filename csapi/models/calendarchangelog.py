from __future__ import unicode_literals

from django.db import models

class Calendarchangelog(models.Model):
    user = models.CharField(max_length=30, blank=True, null=True)
    changetime = models.DateTimeField(db_column='changeTime', blank=True, null=True)  # Field name made lowercase.
    eventid = models.IntegerField(db_column='eventId', blank=True, null=True)  # Field name made lowercase.
    useraction = models.CharField(db_column='userAction', max_length=10, blank=True, null=True)  # Field name made lowercase.
    table = models.CharField(max_length=50, blank=True, null=True)
    mrn = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'calendarChangeLog'


