from __future__ import unicode_literals

from django.db import models

class Mcadataold(models.Model):
    id = models.BigAutoField(primary_key=True)
    mrn = models.BigIntegerField(blank=True, null=True)
    familyhx = models.TextField(db_column='familyHx', blank=True, null=True)  # Field name made lowercase.
    familymed = models.TextField(db_column='familyMed', blank=True, null=True)  # Field name made lowercase.
    parentalob = models.TextField(db_column='parentalOb', blank=True, null=True)  # Field name made lowercase.
    devhx = models.TextField(db_column='devHx', blank=True, null=True)  # Field name made lowercase.
    supportrel = models.TextField(db_column='supportRel', blank=True, null=True)  # Field name made lowercase.
    meaningfulact = models.TextField(db_column='meaningfulAct', blank=True, null=True)  # Field name made lowercase.
    commsupport = models.TextField(db_column='commSupport', blank=True, null=True)  # Field name made lowercase.
    religion = models.TextField(blank=True, null=True)
    cultural = models.TextField(blank=True, null=True)
    sexhx = models.TextField(db_column='sexHx', blank=True, null=True)  # Field name made lowercase.
    dailylimitations = models.TextField(db_column='dailyLimitations', blank=True, null=True)  # Field name made lowercase.
    renthome = models.IntegerField(db_column='rentHome', blank=True, null=True)  # Field name made lowercase.
    ownhome = models.IntegerField(db_column='ownHome', blank=True, null=True)  # Field name made lowercase.
    hospital = models.IntegerField(blank=True, null=True)
    temphousing = models.IntegerField(db_column='tempHousing', blank=True, null=True)  # Field name made lowercase.
    residentialprogram = models.IntegerField(db_column='residentialProgram', blank=True, null=True)  # Field name made lowercase.
    nursinghome = models.IntegerField(db_column='nursingHome', blank=True, null=True)  # Field name made lowercase.
    supportivehousing = models.IntegerField(db_column='supportiveHousing', blank=True, null=True)  # Field name made lowercase.
    friendhome = models.IntegerField(db_column='friendHome', blank=True, null=True)  # Field name made lowercase.
    relativehome = models.IntegerField(db_column='relativeHome', blank=True, null=True)  # Field name made lowercase.
    fostercare = models.IntegerField(db_column='fosterCare', blank=True, null=True)  # Field name made lowercase.
    respite = models.IntegerField(blank=True, null=True)
    jail = models.IntegerField(blank=True, null=True)
    homelessfriend = models.IntegerField(db_column='homelessFriend', blank=True, null=True)  # Field name made lowercase.
    homelessshelter = models.IntegerField(db_column='homelessShelter', blank=True, null=True)  # Field name made lowercase.
    homeother = models.CharField(db_column='homeOther', max_length=150, blank=True, null=True)  # Field name made lowercase.
    atrisklosinghousing = models.IntegerField(db_column='atRiskLosingHousing', blank=True, null=True)  # Field name made lowercase.
    housingsatisfied = models.IntegerField(db_column='housingSatisfied', blank=True, null=True)  # Field name made lowercase.
    housingcomments = models.TextField(db_column='housingComments', blank=True, null=True)  # Field name made lowercase.
    needsemployment = models.IntegerField(db_column='needsEmployment', blank=True, null=True)  # Field name made lowercase.
    needsemploymentcomment = models.TextField(db_column='needsEmploymentComment', blank=True, null=True)  # Field name made lowercase.
    needsemploymentyn = models.IntegerField(db_column='needsEmploymentYN', blank=True, null=True)  # Field name made lowercase.
    needshousekeeping = models.IntegerField(db_column='needsHousekeeping', blank=True, null=True)  # Field name made lowercase.
    needshousekeepingcomment = models.TextField(db_column='needsHousekeepingComment', blank=True, null=True)  # Field name made lowercase.
    needshousekeepingyn = models.IntegerField(db_column='needsHousekeepingYN', blank=True, null=True)  # Field name made lowercase.
    needseducation = models.IntegerField(db_column='needsEducation', blank=True, null=True)  # Field name made lowercase.
    needseducationcomment = models.TextField(db_column='needsEducationComment', blank=True, null=True)  # Field name made lowercase.
    needseducationyn = models.IntegerField(db_column='needsEducationYN', blank=True, null=True)  # Field name made lowercase.
    foodprep = models.IntegerField(db_column='foodPrep', blank=True, null=True)  # Field name made lowercase.
    foodprepcomment = models.TextField(db_column='foodPrepComment', blank=True, null=True)  # Field name made lowercase.
    foodprepyn = models.IntegerField(db_column='foodPrepYN', blank=True, null=True)  # Field name made lowercase.
    medmanagement = models.IntegerField(db_column='medManagement', blank=True, null=True)  # Field name made lowercase.
    medmanagementcomment = models.TextField(db_column='medManagementComment', blank=True, null=True)  # Field name made lowercase.
    medmanagementyn = models.IntegerField(db_column='medManagementYN', blank=True, null=True)  # Field name made lowercase.
    moneymanagement = models.IntegerField(db_column='moneyManagement', blank=True, null=True)  # Field name made lowercase.
    moneymanagementcomment = models.TextField(db_column='moneyManagementComment', blank=True, null=True)  # Field name made lowercase.
    moneymanagementyn = models.IntegerField(db_column='moneyManagementYN', blank=True, null=True)  # Field name made lowercase.
    personalcare = models.IntegerField(db_column='personalCare', blank=True, null=True)  # Field name made lowercase.
    personalcarecomment = models.TextField(db_column='personalCareComment', blank=True, null=True)  # Field name made lowercase.
    personalcareyn = models.IntegerField(db_column='personalCareYN', blank=True, null=True)  # Field name made lowercase.
    exercise = models.IntegerField(blank=True, null=True)
    exercisecomment = models.TextField(db_column='exerciseComment', blank=True, null=True)  # Field name made lowercase.
    exerciseyn = models.IntegerField(db_column='exerciseYN', blank=True, null=True)  # Field name made lowercase.
    transportation = models.IntegerField(blank=True, null=True)
    transportationcomment = models.TextField(db_column='transportationComment', blank=True, null=True)  # Field name made lowercase.
    transportationyn = models.IntegerField(db_column='transportationYN', blank=True, null=True)  # Field name made lowercase.
    problemsolving = models.IntegerField(db_column='problemSolving', blank=True, null=True)  # Field name made lowercase.
    problemsolvingcomment = models.TextField(db_column='problemSolvingComment', blank=True, null=True)  # Field name made lowercase.
    problemsolvingyn = models.IntegerField(db_column='problemSolvingYN', blank=True, null=True)  # Field name made lowercase.
    timemanagement = models.IntegerField(db_column='timeManagement', blank=True, null=True)  # Field name made lowercase.
    timemanagementcomment = models.TextField(db_column='timeManagementComment', blank=True, null=True)  # Field name made lowercase.
    timemanagementyn = models.IntegerField(db_column='timeManagementYN', blank=True, null=True)  # Field name made lowercase.
    substanceuse = models.IntegerField(db_column='substanceUse', blank=True, null=True)  # Field name made lowercase.
    substanceusecomment = models.TextField(db_column='substanceUseComment', blank=True, null=True)  # Field name made lowercase.
    substanceuseyn = models.IntegerField(db_column='substanceUseYN', blank=True, null=True)  # Field name made lowercase.
    otheraddiction = models.IntegerField(db_column='otherAddiction', blank=True, null=True)  # Field name made lowercase.
    otheraddictioncomment = models.TextField(db_column='otherAddictionComment', blank=True, null=True)  # Field name made lowercase.
    otheraddictionyn = models.IntegerField(db_column='otherAddictionYN', blank=True, null=True)  # Field name made lowercase.
    anger = models.IntegerField(blank=True, null=True)
    angercomment = models.TextField(db_column='angerComment', blank=True, null=True)  # Field name made lowercase.
    angeryn = models.IntegerField(db_column='angerYN', blank=True, null=True)  # Field name made lowercase.
    antisocial = models.IntegerField(blank=True, null=True)
    antisocialcomment = models.TextField(db_column='antisocialComment', blank=True, null=True)  # Field name made lowercase.
    antisocialyn = models.IntegerField(db_column='antisocialYN', blank=True, null=True)  # Field name made lowercase.
    impulsivity = models.IntegerField(blank=True, null=True)
    impulsivitycomment = models.TextField(db_column='impulsivityComment', blank=True, null=True)  # Field name made lowercase.
    impulsivityyn = models.IntegerField(db_column='impulsivityYN', blank=True, null=True)  # Field name made lowercase.
    lackassertiveness = models.IntegerField(db_column='lackAssertiveness', blank=True, null=True)  # Field name made lowercase.
    lackassertivenesscomment = models.TextField(db_column='lackAssertivenessComment', blank=True, null=True)  # Field name made lowercase.
    lackassertivenessyn = models.IntegerField(db_column='lackAssertivenessYN', blank=True, null=True)  # Field name made lowercase.
    legalproblems = models.IntegerField(db_column='legalProblems', blank=True, null=True)  # Field name made lowercase.
    legalproblemscomment = models.TextField(db_column='legalProblemsComment', blank=True, null=True)  # Field name made lowercase.
    legalproblemsyn = models.IntegerField(db_column='legalProblemsYN', blank=True, null=True)  # Field name made lowercase.
    oppositional = models.IntegerField(blank=True, null=True)
    oppositionalcomment = models.TextField(db_column='oppositionalComment', blank=True, null=True)  # Field name made lowercase.
    oppositionalyn = models.IntegerField(db_column='oppositionalYN', blank=True, null=True)  # Field name made lowercase.
    communicationskills = models.IntegerField(db_column='communicationSkills', blank=True, null=True)  # Field name made lowercase.
    communicationskillscomment = models.TextField(db_column='communicationSkillsComment', blank=True, null=True)  # Field name made lowercase.
    communicationskillsyn = models.IntegerField(db_column='communicationSkillsYN', blank=True, null=True)  # Field name made lowercase.
    communityintegration = models.IntegerField(db_column='communityIntegration', blank=True, null=True)  # Field name made lowercase.
    communityintegrationcomment = models.TextField(db_column='communityIntegrationComment', blank=True, null=True)  # Field name made lowercase.
    communityintegrationyn = models.IntegerField(db_column='communityIntegrationYN', blank=True, null=True)  # Field name made lowercase.
    dependencyissues = models.IntegerField(db_column='dependencyIssues', blank=True, null=True)  # Field name made lowercase.
    dependencyissuescomment = models.TextField(db_column='dependencyIssuesComment', blank=True, null=True)  # Field name made lowercase.
    dependencyissuesyn = models.IntegerField(db_column='dependencyIssuesYN', blank=True, null=True)  # Field name made lowercase.
    familyeducation = models.IntegerField(db_column='familyEducation', blank=True, null=True)  # Field name made lowercase.
    familyeducationcomment = models.TextField(db_column='familyEducationComment', blank=True, null=True)  # Field name made lowercase.
    familyeducationyn = models.IntegerField(db_column='familyEducationYN', blank=True, null=True)  # Field name made lowercase.
    familyrelationships = models.IntegerField(db_column='familyRelationships', blank=True, null=True)  # Field name made lowercase.
    familyrelationshipscomment = models.TextField(db_column='familyRelationshipsComment', blank=True, null=True)  # Field name made lowercase.
    familyrelationshipsyn = models.IntegerField(db_column='familyRelationshipsYN', blank=True, null=True)  # Field name made lowercase.
    peersupport = models.IntegerField(db_column='peerSupport', blank=True, null=True)  # Field name made lowercase.
    peersupportcomment = models.TextField(db_column='peerSupportComment', blank=True, null=True)  # Field name made lowercase.
    peersupportyn = models.IntegerField(db_column='peerSupportYN', blank=True, null=True)  # Field name made lowercase.
    recreationskills = models.IntegerField(db_column='recreationSkills', blank=True, null=True)  # Field name made lowercase.
    recreationskillscomment = models.TextField(db_column='recreationSkillsComment', blank=True, null=True)  # Field name made lowercase.
    recreationskillsyn = models.IntegerField(db_column='recreationSkillsYN', blank=True, null=True)  # Field name made lowercase.
    socialskills = models.IntegerField(db_column='socialSkills', blank=True, null=True)  # Field name made lowercase.
    socialskillscomment = models.TextField(db_column='socialSkillsComment', blank=True, null=True)  # Field name made lowercase.
    socialskillsyn = models.IntegerField(db_column='socialSkillsYN', blank=True, null=True)  # Field name made lowercase.
    anxiety = models.IntegerField(blank=True, null=True)
    anxietycomment = models.TextField(db_column='anxietyComment', blank=True, null=True)  # Field name made lowercase.
    anxietyyn = models.IntegerField(db_column='anxietyYN', blank=True, null=True)  # Field name made lowercase.
    copingskills = models.IntegerField(db_column='copingSkills', blank=True, null=True)  # Field name made lowercase.
    copingskillscomment = models.TextField(db_column='copingSkillsComment', blank=True, null=True)  # Field name made lowercase.
    copingskillsyn = models.IntegerField(db_column='copingSkillsYN', blank=True, null=True)  # Field name made lowercase.
    cognitiveproblems = models.IntegerField(db_column='cognitiveProblems', blank=True, null=True)  # Field name made lowercase.
    cognitiveproblemscomment = models.TextField(db_column='cognitiveProblemsComment', blank=True, null=True)  # Field name made lowercase.
    cognitiveproblemsyn = models.IntegerField(db_column='cognitiveProblemsYN', blank=True, null=True)  # Field name made lowercase.
    compulsivebehavior = models.IntegerField(db_column='compulsiveBehavior', blank=True, null=True)  # Field name made lowercase.
    compulsivebehaviorcomment = models.TextField(db_column='compulsiveBehaviorComment', blank=True, null=True)  # Field name made lowercase.
    compulsivebehavioryn = models.IntegerField(db_column='compulsiveBehaviorYN', blank=True, null=True)  # Field name made lowercase.
    depressionsadness = models.IntegerField(db_column='depressionSadness', blank=True, null=True)  # Field name made lowercase.
    depressionsadnesscomment = models.TextField(db_column='depressionSadnessComment', blank=True, null=True)  # Field name made lowercase.
    depressionsadnessyn = models.IntegerField(db_column='depressionSadnessYN', blank=True, null=True)  # Field name made lowercase.
    dissociation = models.IntegerField(blank=True, null=True)
    dissociationcomment = models.TextField(db_column='dissociationComment', blank=True, null=True)  # Field name made lowercase.
    dissociationyn = models.IntegerField(db_column='dissociationYN', blank=True, null=True)  # Field name made lowercase.
    disturbedreality = models.IntegerField(db_column='disturbedReality', blank=True, null=True)  # Field name made lowercase.
    disturbedrealitycomment = models.TextField(db_column='disturbedRealityComment', blank=True, null=True)  # Field name made lowercase.
    disturbedrealityyn = models.IntegerField(db_column='disturbedRealityYN', blank=True, null=True)  # Field name made lowercase.
    genderidentity = models.IntegerField(db_column='genderIdentity', blank=True, null=True)  # Field name made lowercase.
    genderidentitycomment = models.TextField(db_column='genderIdentityComment', blank=True, null=True)  # Field name made lowercase.
    genderidentityyn = models.IntegerField(db_column='genderIdentityYN', blank=True, null=True)  # Field name made lowercase.
    bereavement = models.IntegerField(blank=True, null=True)
    bereavementcomment = models.TextField(db_column='bereavementComment', blank=True, null=True)  # Field name made lowercase.
    bereavementyn = models.IntegerField(db_column='bereavementYN', blank=True, null=True)  # Field name made lowercase.
    hyperactivity = models.IntegerField(blank=True, null=True)
    hyperactivitycomment = models.TextField(db_column='hyperactivityComment', blank=True, null=True)  # Field name made lowercase.
    hyperactivityyn = models.IntegerField(db_column='hyperactivityYN', blank=True, null=True)  # Field name made lowercase.
    moodswings = models.IntegerField(db_column='moodSwings', blank=True, null=True)  # Field name made lowercase.
    moodswingscomment = models.TextField(db_column='moodSwingsComment', blank=True, null=True)  # Field name made lowercase.
    moodswingsyn = models.IntegerField(db_column='moodSwingsYN', blank=True, null=True)  # Field name made lowercase.
    obsessions = models.IntegerField(blank=True, null=True)
    obsessionscomment = models.TextField(db_column='obsessionsComment', blank=True, null=True)  # Field name made lowercase.
    obsessionsyn = models.IntegerField(db_column='obsessionsYN', blank=True, null=True)  # Field name made lowercase.
    somaticproblems = models.IntegerField(db_column='somaticProblems', blank=True, null=True)  # Field name made lowercase.
    somaticproblemscomment = models.TextField(db_column='somaticProblemsComment', blank=True, null=True)  # Field name made lowercase.
    somaticproblemsyn = models.IntegerField(db_column='somaticProblemsYN', blank=True, null=True)  # Field name made lowercase.
    stressmanagement = models.IntegerField(db_column='stressManagement', blank=True, null=True)  # Field name made lowercase.
    stressmanagementcomment = models.TextField(db_column='stressManagementComment', blank=True, null=True)  # Field name made lowercase.
    stressmanagementyn = models.IntegerField(db_column='stressManagementYN', blank=True, null=True)  # Field name made lowercase.
    trauma = models.IntegerField(blank=True, null=True)
    traumacomment = models.TextField(db_column='traumaComment', blank=True, null=True)  # Field name made lowercase.
    traumayn = models.IntegerField(db_column='traumaYN', blank=True, null=True)  # Field name made lowercase.
    healthpractices = models.IntegerField(db_column='healthPractices', blank=True, null=True)  # Field name made lowercase.
    healthpracticescomment = models.TextField(db_column='healthPracticesComment', blank=True, null=True)  # Field name made lowercase.
    healthpracticesyn = models.IntegerField(db_column='healthPracticesYN', blank=True, null=True)  # Field name made lowercase.
    dietnutrition = models.IntegerField(db_column='dietNutrition', blank=True, null=True)  # Field name made lowercase.
    dietnutritioncomment = models.TextField(db_column='dietNutritionComment', blank=True, null=True)  # Field name made lowercase.
    dietnutritionyn = models.IntegerField(db_column='dietNutritionYN', blank=True, null=True)  # Field name made lowercase.
    painmanagement = models.IntegerField(db_column='painManagement', blank=True, null=True)  # Field name made lowercase.
    painmanagementcomment = models.TextField(db_column='painManagementComment', blank=True, null=True)  # Field name made lowercase.
    painmanagementyn = models.IntegerField(db_column='painManagementYN', blank=True, null=True)  # Field name made lowercase.
    sexualproblems = models.IntegerField(db_column='sexualProblems', blank=True, null=True)  # Field name made lowercase.
    sexualproblemscomment = models.TextField(db_column='sexualProblemsComment', blank=True, null=True)  # Field name made lowercase.
    sexualproblemsyn = models.IntegerField(db_column='sexualProblemsYN', blank=True, null=True)  # Field name made lowercase.
    sleepproblems = models.IntegerField(db_column='sleepProblems', blank=True, null=True)  # Field name made lowercase.
    sleepproblemscomment = models.TextField(db_column='sleepProblemsComment', blank=True, null=True)  # Field name made lowercase.
    sleepproblemsyn = models.IntegerField(db_column='sleepProblemsYN', blank=True, null=True)  # Field name made lowercase.
    highriskbehaviors = models.IntegerField(db_column='highRiskBehaviors', blank=True, null=True)  # Field name made lowercase.
    highriskbehaviorscomment = models.TextField(db_column='highRiskBehaviorsComment', blank=True, null=True)  # Field name made lowercase.
    highriskbehaviorsyn = models.IntegerField(db_column='highRiskBehaviorsYN', blank=True, null=True)  # Field name made lowercase.
    suicidalideation = models.IntegerField(db_column='suicidalIdeation', blank=True, null=True)  # Field name made lowercase.
    suicidalideationcomment = models.TextField(db_column='suicidalIdeationComment', blank=True, null=True)  # Field name made lowercase.
    suicidalideationyn = models.IntegerField(db_column='suicidalIdeationYN', blank=True, null=True)  # Field name made lowercase.
    homicidalideation = models.IntegerField(db_column='homicidalIdeation', blank=True, null=True)  # Field name made lowercase.
    homicidalideationcomment = models.TextField(db_column='homicidalIdeationComment', blank=True, null=True)  # Field name made lowercase.
    homicidalideationyn = models.IntegerField(db_column='homicidalIdeationYN', blank=True, null=True)  # Field name made lowercase.
    safetyskills = models.IntegerField(db_column='safetySkills', blank=True, null=True)  # Field name made lowercase.
    safetyskillscomment = models.TextField(db_column='safetySkillsComment', blank=True, null=True)  # Field name made lowercase.
    safetyskillsyn = models.IntegerField(db_column='safetySkillsYN', blank=True, null=True)  # Field name made lowercase.
    personalqualities = models.IntegerField(db_column='personalQualities', blank=True, null=True)  # Field name made lowercase.
    personalqualitiescomment = models.TextField(db_column='personalQualitiesComment', blank=True, null=True)  # Field name made lowercase.
    personalqualitiesyn = models.IntegerField(db_column='personalQualitiesYN', blank=True, null=True)  # Field name made lowercase.
    dailylivingsit = models.IntegerField(db_column='dailyLivingSit', blank=True, null=True)  # Field name made lowercase.
    dailylivingsitcomment = models.TextField(db_column='dailyLivingSitComment', blank=True, null=True)  # Field name made lowercase.
    dailylivingsityn = models.IntegerField(db_column='dailyLivingSitYN', blank=True, null=True)  # Field name made lowercase.
    financial = models.IntegerField(blank=True, null=True)
    financialcomment = models.TextField(db_column='financialComment', blank=True, null=True)  # Field name made lowercase.
    financialyn = models.IntegerField(db_column='financialYN', blank=True, null=True)  # Field name made lowercase.
    employmented = models.IntegerField(db_column='employmentEd', blank=True, null=True)  # Field name made lowercase.
    employmentedcomment = models.TextField(db_column='employmentEdComment', blank=True, null=True)  # Field name made lowercase.
    employmentedyn = models.IntegerField(db_column='employmentEdYN', blank=True, null=True)  # Field name made lowercase.
    health = models.IntegerField(blank=True, null=True)
    healthcomment = models.TextField(db_column='healthComment', blank=True, null=True)  # Field name made lowercase.
    healthyn = models.IntegerField(db_column='healthYN', blank=True, null=True)  # Field name made lowercase.
    leisure = models.IntegerField(blank=True, null=True)
    leisurecomment = models.TextField(db_column='leisureComment', blank=True, null=True)  # Field name made lowercase.
    leisureyn = models.IntegerField(db_column='leisureYN', blank=True, null=True)  # Field name made lowercase.
    spirituality = models.IntegerField(blank=True, null=True)
    spiritualitycomment = models.TextField(db_column='spiritualityComment', blank=True, null=True)  # Field name made lowercase.
    spiritualityyn = models.IntegerField(db_column='spiritualityYN', blank=True, null=True)  # Field name made lowercase.
    otherassessedneeds = models.IntegerField(db_column='otherAssessedNeeds', blank=True, null=True)  # Field name made lowercase.
    otherassessedneedscomment = models.TextField(db_column='otherAssessedNeedsComment', blank=True, null=True)  # Field name made lowercase.
    otherassessedneedsyn = models.IntegerField(db_column='otherAssessedNeedsYN', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'mcaDataOld'


