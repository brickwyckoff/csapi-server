from __future__ import unicode_literals

from django.db import models

class Autprintlog(models.Model):
    id = models.BigAutoField(primary_key=True)
    autid = models.BigIntegerField(db_column='autId', blank=True, null=True)  # Field name made lowercase.
    user = models.CharField(max_length=30, blank=True, null=True)
    printtime = models.DateTimeField(db_column='printTime', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'autPrintLog'


