from __future__ import unicode_literals

from django.db import models

class Pharmacyadditionalcoverage(models.Model):
    controlnumber = models.CharField(db_column='controlNumber', max_length=19, blank=True, null=True)  # Field name made lowercase.
    payerincr = models.IntegerField(db_column='payerIncr', blank=True, null=True)  # Field name made lowercase.
    payerorder = models.CharField(db_column='payerOrder', max_length=3, blank=True, null=True)  # Field name made lowercase.
    name = models.CharField(max_length=60, blank=True, null=True)
    policynumberqual = models.CharField(db_column='policyNumberQual', max_length=2, blank=True, null=True)  # Field name made lowercase.
    policynumber = models.CharField(db_column='policyNumber', max_length=80, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'pharmacyAdditionalCoverage'


