from __future__ import unicode_literals

from django.db import models

class Imgresults(models.Model):
    id = models.BigAutoField(primary_key=True)
    mrn = models.BigIntegerField()
    imgtitle = models.CharField(db_column='imgTitle', max_length=80)  # Field name made lowercase.
    imgfile = models.CharField(db_column='imgFile', max_length=150)  # Field name made lowercase.
    imgnotes = models.CharField(db_column='imgNotes', max_length=1000)  # Field name made lowercase.
    importtime = models.DateTimeField(db_column='importTime')  # Field name made lowercase.
    imgreviewed = models.IntegerField(db_column='imgReviewed')  # Field name made lowercase.
    reviewedby = models.CharField(db_column='reviewedBy', max_length=35)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'imgResults'


