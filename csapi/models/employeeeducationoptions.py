from __future__ import unicode_literals

from django.db import models

class Employeeeducationoptions(models.Model):
    optiondesc = models.CharField(db_column='optionDesc', max_length=50, blank=True, null=True)  # Field name made lowercase.
    optionfunction = models.CharField(db_column='optionFunction', max_length=50, blank=True, null=True)  # Field name made lowercase.
    isactive = models.IntegerField(db_column='isActive', blank=True, null=True)  # Field name made lowercase.
    displayorder = models.IntegerField(db_column='displayOrder', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'employeeEducationOptions'


