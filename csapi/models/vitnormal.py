from __future__ import unicode_literals

from django.db import models

class Vitnormal(models.Model):
    vital = models.CharField(max_length=50, blank=True, null=True)
    low = models.IntegerField(blank=True, null=True)
    high = models.IntegerField(blank=True, null=True)
    agelow = models.IntegerField(db_column='ageLow', blank=True, null=True)  # Field name made lowercase.
    agehigh = models.IntegerField(db_column='ageHigh', blank=True, null=True)  # Field name made lowercase.
    weightlow = models.IntegerField(db_column='weightLow', blank=True, null=True)  # Field name made lowercase.
    weighthigh = models.IntegerField(db_column='weightHigh', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'vitNormal'


