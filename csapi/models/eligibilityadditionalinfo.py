from __future__ import unicode_literals

from django.db import models

class Eligibilityadditionalinfo(models.Model):
    verifiedid = models.BigIntegerField(db_column='verifiedId')  # Field name made lowercase.
    controlnumber = models.CharField(db_column='controlNumber', max_length=19, blank=True, null=True)  # Field name made lowercase.
    relatedto = models.CharField(db_column='relatedTo', max_length=5, blank=True, null=True)  # Field name made lowercase.
    refqual = models.CharField(db_column='refQual', max_length=3, blank=True, null=True)  # Field name made lowercase.
    refident = models.CharField(db_column='refIdent', max_length=50, blank=True, null=True)  # Field name made lowercase.
    refdescription = models.CharField(db_column='refDescription', max_length=80, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'eligibilityAdditionalInfo'


