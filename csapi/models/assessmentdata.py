from __future__ import unicode_literals

from django.db import models

class Assessmentdata(models.Model):
    id = models.BigAutoField(primary_key=True)
    mrn = models.BigIntegerField()
    assesscode = models.CharField(db_column='assessCode', max_length=15)  # Field name made lowercase.
    assessdate = models.DateTimeField(db_column='assessDate')  # Field name made lowercase.
    assessvaluenum = models.BigIntegerField(db_column='assessValueNum')  # Field name made lowercase.
    assesstype = models.CharField(db_column='assessType', max_length=10)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'assessmentData'


