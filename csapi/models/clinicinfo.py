from __future__ import unicode_literals

from django.db import models

class Clinicinfo(models.Model):
    name = models.CharField(max_length=100, blank=True, null=True)
    claimname = models.CharField(db_column='claimName', max_length=100, blank=True, null=True)  # Field name made lowercase.
    shortname = models.CharField(db_column='shortName', max_length=15, blank=True, null=True)  # Field name made lowercase.
    address1 = models.CharField(max_length=60, blank=True, null=True)
    address2 = models.CharField(max_length=60, blank=True, null=True)
    city = models.CharField(max_length=100, blank=True, null=True)
    state = models.CharField(max_length=2, blank=True, null=True)
    zip = models.CharField(max_length=10, blank=True, null=True)
    country = models.CharField(max_length=35)
    phone = models.BigIntegerField(blank=True, null=True)
    fax = models.BigIntegerField(blank=True, null=True)
    email = models.CharField(max_length=100, blank=True, null=True)
    manager = models.CharField(max_length=100, blank=True, null=True)
    medicaldirector = models.CharField(db_column='medicalDirector', max_length=100, blank=True, null=True)  # Field name made lowercase.
    npi = models.BigIntegerField(blank=True, null=True)
    taxid = models.BigIntegerField(db_column='taxId', blank=True, null=True)  # Field name made lowercase.
    clianumber = models.CharField(db_column='cliaNumber', max_length=15, blank=True, null=True)  # Field name made lowercase.
    billingdbid = models.BigIntegerField(db_column='billingDbId', blank=True, null=True)  # Field name made lowercase.
    billinglocationcode = models.CharField(db_column='billingLocationCode', max_length=3, blank=True, null=True)  # Field name made lowercase.
    logo = models.CharField(max_length=100, blank=True, null=True)
    emrurl = models.CharField(db_column='emrUrl', max_length=200, blank=True, null=True)  # Field name made lowercase.
    rxsignatureexpiration = models.IntegerField(db_column='rxSignatureExpiration', blank=True, null=True)  # Field name made lowercase.
    lateappointmentcheck = models.IntegerField(db_column='lateAppointmentCheck', blank=True, null=True)  # Field name made lowercase.
    ip = models.CharField(max_length=15, blank=True, null=True)
    defaultinfo = models.IntegerField(db_column='defaultInfo', blank=True, null=True)  # Field name made lowercase.
    defaultpttype = models.CharField(db_column='defaultPtType', max_length=10, blank=True, null=True)  # Field name made lowercase.
    hours = models.CharField(max_length=100, blank=True, null=True)
    autoschedule = models.IntegerField(db_column='autoSchedule', blank=True, null=True)  # Field name made lowercase.
    screenlocktime = models.BigIntegerField(db_column='screenLockTime', blank=True, null=True)  # Field name made lowercase.
    usestrikes = models.IntegerField(db_column='useStrikes', blank=True, null=True)  # Field name made lowercase.
    usedme = models.IntegerField(db_column='useDme', blank=True, null=True)  # Field name made lowercase.
    useprocedurenote = models.IntegerField(db_column='useProcedureNote', blank=True, null=True)  # Field name made lowercase.
    popupdefaultread = models.IntegerField(db_column='popupDefaultRead', blank=True, null=True)  # Field name made lowercase.
    clinicgroup = models.IntegerField(db_column='clinicGroup', blank=True, null=True)  # Field name made lowercase.
    usepharmacycopay = models.IntegerField(db_column='usePharmacyCopay', blank=True, null=True)  # Field name made lowercase.
    useextrapayment = models.IntegerField(db_column='useExtraPayment', blank=True, null=True)  # Field name made lowercase.
    userecurringalerts = models.IntegerField(db_column='useRecurringAlerts', blank=True, null=True)  # Field name made lowercase.
    defaultsupervisor = models.CharField(db_column='defaultSupervisor', max_length=2, blank=True, null=True)  # Field name made lowercase.
    useim = models.IntegerField(db_column='useIm', blank=True, null=True)  # Field name made lowercase.
    statrxim = models.IntegerField(db_column='statRxIm', blank=True, null=True)  # Field name made lowercase.
    autocheckpreauth = models.IntegerField(db_column='autoCheckPreAuth', blank=True, null=True)  # Field name made lowercase.
    uselastpreauthreq = models.IntegerField(db_column='useLastPreauthReq', blank=True, null=True)  # Field name made lowercase.
    useadditionalreferrals = models.IntegerField(db_column='useAdditionalReferrals', blank=True, null=True)  # Field name made lowercase.
    usecosigner = models.IntegerField(db_column='useCosigner', blank=True, null=True)  # Field name made lowercase.
    usereleaselog = models.IntegerField(db_column='useReleaseLog', blank=True, null=True)  # Field name made lowercase.
    useprintableforms = models.IntegerField(db_column='usePrintableForms', blank=True, null=True)  # Field name made lowercase.
    labdirector = models.CharField(db_column='labDirector', max_length=100, blank=True, null=True)  # Field name made lowercase.
    requiredx = models.IntegerField(db_column='requireDx', blank=True, null=True)  # Field name made lowercase.
    usedxonencounternote = models.IntegerField(db_column='useDxOnEncounterNote', blank=True, null=True)  # Field name made lowercase.
    showanalyzerlabgraph = models.IntegerField(db_column='showAnalyzerLabGraph', blank=True, null=True)  # Field name made lowercase.
    usepttypereport = models.IntegerField(db_column='usePtTypeReport', blank=True, null=True)  # Field name made lowercase.
    autoentercopaycharge = models.IntegerField(db_column='autoEnterCopayCharge', blank=True, null=True)  # Field name made lowercase.
    autoentercopaypmt = models.IntegerField(db_column='autoEnterCopayPmt', blank=True, null=True)  # Field name made lowercase.
    referralalertnumremaining = models.IntegerField(db_column='referralAlertNumRemaining', blank=True, null=True)  # Field name made lowercase.
    referralalertdaysremaining = models.IntegerField(db_column='referralAlertDaysRemaining', blank=True, null=True)  # Field name made lowercase.
    showhcghistory = models.IntegerField(db_column='showHcgHistory', blank=True, null=True)  # Field name made lowercase.
    mainbilling = models.IntegerField(db_column='mainBilling', blank=True, null=True)  # Field name made lowercase.
    feetable = models.CharField(db_column='feeTable', max_length=15, blank=True, null=True)  # Field name made lowercase.
    usecopayalert = models.IntegerField(db_column='useCopayAlert', blank=True, null=True)  # Field name made lowercase.
    usedispensabledrugs = models.IntegerField(db_column='useDispensableDrugs', blank=True, null=True)  # Field name made lowercase.
    showgrowthchart = models.IntegerField(db_column='showGrowthChart', blank=True, null=True)  # Field name made lowercase.
    reqhistorystartdate = models.IntegerField(db_column='reqHistoryStartDate', blank=True, null=True)  # Field name made lowercase.
    patientportalurl = models.CharField(db_column='patientPortalUrl', max_length=100, blank=True, null=True)  # Field name made lowercase.
    managercellphone = models.CharField(db_column='managerCellPhone', max_length=10, blank=True, null=True)  # Field name made lowercase.
    manageremail = models.CharField(db_column='managerEmail', max_length=100, blank=True, null=True)  # Field name made lowercase.
    useeprescribe = models.IntegerField(db_column='useEPrescribe', blank=True, null=True)  # Field name made lowercase.
    publichealthreg = models.IntegerField(db_column='publicHealthReg', blank=True, null=True)  # Field name made lowercase.
    pqriwaiversigned = models.IntegerField(db_column='pqriWaiverSigned', blank=True, null=True)  # Field name made lowercase.
    useimagingorders = models.IntegerField(db_column='useImagingOrders')  # Field name made lowercase.
    rxsignprovideronly = models.IntegerField(db_column='rxSignProviderOnly')  # Field name made lowercase.
    mainsupervisor = models.CharField(db_column='mainSupervisor', max_length=2)  # Field name made lowercase.
    showreqappt = models.IntegerField(db_column='showReqAppt')  # Field name made lowercase.
    firstname270 = models.CharField(db_column='firstName270', max_length=35)  # Field name made lowercase.
    lastname270 = models.CharField(db_column='lastName270', max_length=35)  # Field name made lowercase.
    senderentitycode270 = models.CharField(db_column='senderEntityCode270', max_length=35)  # Field name made lowercase.
    senderidqual270 = models.CharField(db_column='senderIdQual270', max_length=35)  # Field name made lowercase.
    billingloccode = models.CharField(db_column='billingLocCode', max_length=3)  # Field name made lowercase.
    clinicinforesults = models.IntegerField(db_column='clinicInfoResults')  # Field name made lowercase.
    useptreferredfrom = models.IntegerField(db_column='usePtReferredFrom')  # Field name made lowercase.
    userxprinthxtimeline = models.IntegerField(db_column='useRxPrintHxTimeline')  # Field name made lowercase.
    allowrxfax = models.IntegerField(db_column='allowRxFax')  # Field name made lowercase.
    allowrxpreauth = models.IntegerField(db_column='allowRxPreAuth')  # Field name made lowercase.
    suboxoneprintmdonly = models.IntegerField(db_column='suboxonePrintMdOnly')  # Field name made lowercase.
    lateappointmencheck = models.IntegerField(db_column='lateAppointmenCheck')  # Field name made lowercase.
    nosplitquestreq = models.IntegerField(db_column='noSplitQuestReq')  # Field name made lowercase.
    usesuboxonerefillcountdown = models.IntegerField(db_column='useSuboxoneRefillCountdown')  # Field name made lowercase.
    autoassignsup = models.IntegerField(db_column='autoAssignSup', blank=True, null=True)  # Field name made lowercase.
    apptreminderextra = models.CharField(db_column='apptReminderExtra', max_length=100, blank=True, null=True)  # Field name made lowercase.
    usenextapptfieldhp = models.IntegerField(db_column='useNextApptFieldHP', blank=True, null=True)  # Field name made lowercase.
    usereferralfor = models.IntegerField(db_column='useReferralFor', blank=True, null=True)  # Field name made lowercase.
    usebupgraph = models.IntegerField(db_column='useBupGraph', blank=True, null=True)  # Field name made lowercase.
    usecopselfpay = models.IntegerField(db_column='useCopSelfPay', blank=True, null=True)  # Field name made lowercase.
    initialassessmentform = models.CharField(db_column='initialAssessmentForm', max_length=50, blank=True, null=True)  # Field name made lowercase.
    uselabsupervisor = models.IntegerField(db_column='useLabSupervisor', blank=True, null=True)  # Field name made lowercase.
    usescheduleasst = models.IntegerField(db_column='useScheduleAsst', blank=True, null=True)  # Field name made lowercase.
    defaultapptspan = models.IntegerField(db_column='defaultApptSpan', blank=True, null=True)  # Field name made lowercase.
    calendarcellheight = models.IntegerField(db_column='calendarCellHeight', blank=True, null=True)  # Field name made lowercase.
    calendarstylesheet = models.CharField(db_column='calendarStyleSheet', max_length=100, blank=True, null=True)  # Field name made lowercase.
    useintakepacket = models.IntegerField(db_column='useIntakePacket', blank=True, null=True)  # Field name made lowercase.
    shownextapptdem = models.IntegerField(db_column='showNextApptDem', blank=True, null=True)  # Field name made lowercase.
    usetreatmentlevel = models.IntegerField(db_column='useTreatmentLevel', blank=True, null=True)  # Field name made lowercase.
    summarytype = models.CharField(db_column='summaryType', max_length=3, blank=True, null=True)  # Field name made lowercase.
    checkmaxsuboxone = models.IntegerField(db_column='checkMaxSuboxone', blank=True, null=True)  # Field name made lowercase.
    usenextapptdischarge = models.IntegerField(db_column='useNextApptDischarge', blank=True, null=True)  # Field name made lowercase.
    usebalancedischarge = models.IntegerField(db_column='useBalanceDischarge', blank=True, null=True)  # Field name made lowercase.
    userelationshipstatus = models.IntegerField(db_column='useRelationshipStatus', blank=True, null=True)  # Field name made lowercase.
    usepharmacyondem = models.IntegerField(db_column='usePharmacyOnDem', blank=True, null=True)  # Field name made lowercase.
    usecounselingonmedsum = models.IntegerField(db_column='useCounselingOnMedSum', blank=True, null=True)  # Field name made lowercase.
    usetemplateimportlast = models.IntegerField(db_column='useTemplateImportLast', blank=True, null=True)  # Field name made lowercase.
    showallpostings = models.IntegerField(db_column='showAllPostings', blank=True, null=True)  # Field name made lowercase.
    rxprintlayout = models.CharField(db_column='rxPrintLayout', max_length=15, blank=True, null=True)  # Field name made lowercase.
    printlabelfunction = models.CharField(db_column='printLabelFunction', max_length=35, blank=True, null=True)  # Field name made lowercase.
    usedxondischarge = models.IntegerField(db_column='useDxOnDischarge', blank=True, null=True)  # Field name made lowercase.
    usepmtondischarge = models.IntegerField(db_column='usePmtOnDischarge', blank=True, null=True)  # Field name made lowercase.
    allowrxmdonly = models.IntegerField(db_column='allowRxMdOnly', blank=True, null=True)  # Field name made lowercase.
    useerx = models.IntegerField(db_column='useERx', blank=True, null=True)  # Field name made lowercase.
    useoldrxdisplay = models.IntegerField(db_column='useOldRxDisplay', blank=True, null=True)  # Field name made lowercase.
    billingstatementoption = models.CharField(db_column='billingStatementOption', max_length=50, blank=True, null=True)  # Field name made lowercase.
    userxid = models.IntegerField(db_column='useRxId', blank=True, null=True)  # Field name made lowercase.
    dischargeheader = models.CharField(db_column='dischargeHeader', max_length=70, blank=True, null=True)  # Field name made lowercase.
    dischargeuseprovfirstname = models.IntegerField(db_column='dischargeUseProvFirstName', blank=True, null=True)  # Field name made lowercase.
    usecounselingmanager = models.IntegerField(db_column='useCounselingManager', blank=True, null=True)  # Field name made lowercase.
    usetriagefield = models.IntegerField(db_column='useTriageField', blank=True, null=True)  # Field name made lowercase.
    numdx = models.IntegerField(db_column='numDx', blank=True, null=True)  # Field name made lowercase.
    sharedclinicfullview = models.IntegerField(db_column='sharedClinicFullView', blank=True, null=True)  # Field name made lowercase.
    showcounselortopbar = models.IntegerField(db_column='showCounselorTopBar', blank=True, null=True)  # Field name made lowercase.
    showallcharges = models.IntegerField(db_column='showAllCharges', blank=True, null=True)  # Field name made lowercase.
    showotherloc = models.IntegerField(db_column='showOtherLoc', blank=True, null=True)  # Field name made lowercase.
    practiceadmin = models.CharField(db_column='practiceAdmin', max_length=35, blank=True, null=True)  # Field name made lowercase.
    useattendingnote = models.IntegerField(db_column='useAttendingNote', blank=True, null=True)  # Field name made lowercase.
    userxhold = models.IntegerField(db_column='useRxHold', blank=True, null=True)  # Field name made lowercase.
    usemonthlycheckbox = models.IntegerField(db_column='useMonthlyCheckbox', blank=True, null=True)  # Field name made lowercase.
    usemacros = models.IntegerField(db_column='useMacros', blank=True, null=True)  # Field name made lowercase.
    showpaneltestonreq = models.IntegerField(db_column='showPanelTestOnReq', blank=True, null=True)  # Field name made lowercase.
    useinitialtxplan = models.IntegerField(db_column='useInitialTxPlan', blank=True, null=True)  # Field name made lowercase.
    usequarterlytxplan = models.IntegerField(db_column='useQuarterlyTxPlan', blank=True, null=True)  # Field name made lowercase.
    usedischargesummarycsl = models.IntegerField(db_column='useDischargeSummaryCsl', blank=True, null=True)  # Field name made lowercase.
    useasi = models.IntegerField(db_column='useAsi', blank=True, null=True)  # Field name made lowercase.
    usediffdx = models.IntegerField(db_column='useDiffDx', blank=True, null=True)  # Field name made lowercase.
    mainlabsup = models.CharField(db_column='mainLabSup', max_length=5, blank=True, null=True)  # Field name made lowercase.
    chargeonsign = models.IntegerField(db_column='chargeOnSign', blank=True, null=True)  # Field name made lowercase.
    grouplabtablebydate = models.IntegerField(db_column='groupLabTableByDate', blank=True, null=True)  # Field name made lowercase.
    cashonly = models.IntegerField(db_column='cashOnly', blank=True, null=True)  # Field name made lowercase.
    abnormallabcolor = models.CharField(db_column='abnormalLabColor', max_length=6, blank=True, null=True)  # Field name made lowercase.
    hcfapaypatient = models.IntegerField(db_column='hcfaPayPatient', blank=True, null=True)  # Field name made lowercase.
    useutoxreviewedcheck = models.IntegerField(db_column='useUtoxReviewedCheck', blank=True, null=True)  # Field name made lowercase.
    usecliniconsummary = models.IntegerField(db_column='useClinicOnSummary', blank=True, null=True)  # Field name made lowercase.
    dischargeintrocustom = models.CharField(db_column='dischargeIntroCustom', max_length=800, blank=True, null=True)  # Field name made lowercase.
    calendarversion = models.IntegerField(db_column='calendarVersion', blank=True, null=True)  # Field name made lowercase.
    showbiannualexam = models.IntegerField(db_column='showBiannualExam', blank=True, null=True)  # Field name made lowercase.
    supportaccessusers = models.CharField(db_column='supportAccessUsers', max_length=200, blank=True, null=True)  # Field name made lowercase.
    usesupondischarge = models.IntegerField(db_column='useSupOnDischarge', blank=True, null=True)  # Field name made lowercase.
    dischargeinfofile = models.CharField(db_column='dischargeInfoFile', max_length=50, blank=True, null=True)  # Field name made lowercase.
    useoptvisit = models.IntegerField(db_column='useOptVisit', blank=True, null=True)  # Field name made lowercase.
    useoptinitial = models.IntegerField(db_column='useOptInitial', blank=True, null=True)  # Field name made lowercase.
    useoptvbh = models.IntegerField(db_column='useOptVBH', blank=True, null=True)  # Field name made lowercase.
    optvisit = models.CharField(db_column='optVisit', max_length=50, blank=True, null=True)  # Field name made lowercase.
    optinitial = models.CharField(db_column='optInitial', max_length=50, blank=True, null=True)  # Field name made lowercase.
    optvbh = models.CharField(db_column='optVBH', max_length=50, blank=True, null=True)  # Field name made lowercase.
    zip4 = models.CharField(max_length=4, blank=True, null=True)
    useptmessaging = models.IntegerField(db_column='usePtMessaging')  # Field name made lowercase.
    chargecardresource = models.CharField(db_column='chargeCardResource', max_length=35, blank=True, null=True)  # Field name made lowercase.
    printcallinespace = models.IntegerField(db_column='printCalLineSpace', blank=True, null=True)  # Field name made lowercase.
    decisionsupportusers = models.CharField(db_column='decisionSupportUsers', max_length=200)  # Field name made lowercase.
    useoptionaldeminfo = models.IntegerField(db_column='useOptionalDemInfo')  # Field name made lowercase.
    useoptionalvaccineinfo = models.IntegerField(db_column='useOptionalVaccineInfo')  # Field name made lowercase.
    ccn = models.CharField(max_length=15)
    usenursedropdown = models.IntegerField(db_column='useNurseDropdown')  # Field name made lowercase.
    usesnomedprocedures = models.IntegerField(db_column='useSnomedProcedures')  # Field name made lowercase.
    directmessageip = models.CharField(db_column='directMessageIp', max_length=35)  # Field name made lowercase.
    directmessageport = models.CharField(db_column='directMessagePort', max_length=35)  # Field name made lowercase.
    directmessageuser = models.CharField(db_column='directMessageUser', max_length=50)  # Field name made lowercase.
    directmessagepassword = models.CharField(db_column='directMessagePassword', max_length=50)  # Field name made lowercase.
    usemultdea = models.IntegerField(db_column='useMultDea', blank=True, null=True)  # Field name made lowercase.
    autoloadpted = models.IntegerField(db_column='autoLoadPtEd')  # Field name made lowercase.
    methadoneadminusers = models.IntegerField(db_column='methadoneAdminUsers', blank=True, null=True)  # Field name made lowercase.
    userecurringcharge = models.CharField(db_column='useRecurringCharge', max_length=35, blank=True, null=True)  # Field name made lowercase.
    usesplitaccounting = models.IntegerField(db_column='useSplitAccounting', blank=True, null=True)  # Field name made lowercase.
    showtreatmentlevelontimeline = models.IntegerField(db_column='showTreatmentLevelOnTimeline', blank=True, null=True)  # Field name made lowercase.
    userandomlaborder = models.IntegerField(db_column='useRandomLabOrder', blank=True, null=True)  # Field name made lowercase.
    usesecondcounselingcpt = models.IntegerField(db_column='useSecondCounselingCpt', blank=True, null=True)  # Field name made lowercase.
    emergcontactdisplay = models.CharField(db_column='emergContactDisplay', max_length=50)  # Field name made lowercase.
    claimacceptassignment = models.IntegerField(db_column='claimAcceptAssignment')  # Field name made lowercase.
    fulltimedoc = models.CharField(db_column='fullTimedoc', max_length=20, blank=True, null=True)  # Field name made lowercase.
    showextrarx = models.IntegerField(db_column='showExtraRx')  # Field name made lowercase.
    usebillingcodetemplate = models.IntegerField(db_column='useBillingCodeTemplate')  # Field name made lowercase.
    useoldrxprint = models.IntegerField(db_column='useOldRxPrint', blank=True, null=True)  # Field name made lowercase.
    deanum = models.CharField(db_column='deaNum', max_length=15)  # Field name made lowercase.
    showallfieldssummary = models.IntegerField(db_column='showAllFieldsSummary')  # Field name made lowercase.
    usepastdxbillingcodepopup = models.IntegerField(db_column='usePastDxBillingCodePopup')  # Field name made lowercase.
    usemedordernoterx = models.IntegerField(db_column='useMedOrderNotERx')  # Field name made lowercase.
    usedonotbill = models.IntegerField(db_column='useDoNotBill')  # Field name made lowercase.
    closerxprintwindow = models.IntegerField(db_column='closeRxPrintWindow')  # Field name made lowercase.
    showerxonorders = models.IntegerField(db_column='showErxOnOrders')  # Field name made lowercase.
    dispensingadminusers = models.CharField(db_column='dispensingAdminUsers', max_length=15)  # Field name made lowercase.
    usegtn = models.IntegerField(db_column='useGtn', blank=True, null=True)  # Field name made lowercase.
    useeligibilitydow = models.IntegerField(db_column='useEligibilityDoW', blank=True, null=True)  # Field name made lowercase.
    eligibilitydow = models.IntegerField(db_column='eligibilityDoW', blank=True, null=True)  # Field name made lowercase.
    useoldmrnfield = models.IntegerField(db_column='useOldMrnField', blank=True, null=True)  # Field name made lowercase.
    usedispensingonreviewpt = models.IntegerField(db_column='useDispensingOnReviewPt', blank=True, null=True)  # Field name made lowercase.
    useprintableletters = models.TextField(db_column='usePrintableLetters')  # Field name made lowercase.
    calprintdob = models.IntegerField(db_column='calPrintDoB', blank=True, null=True)  # Field name made lowercase.
    calprintphone = models.IntegerField(db_column='calPrintPhone', blank=True, null=True)  # Field name made lowercase.
    usephoneinterviewdem = models.IntegerField(db_column='usePhoneInterviewDem')  # Field name made lowercase.
    useptmiddledem = models.IntegerField(db_column='usePtMiddleDem')  # Field name made lowercase.
    showlftgraph = models.IntegerField(db_column='showLftGraph')  # Field name made lowercase.
    showpastvisitgraphsallpt = models.IntegerField(db_column='showPastVisitGraphsAllPt')  # Field name made lowercase.
    usenewdispensingvisit = models.IntegerField(db_column='useNewDispensingVisit')  # Field name made lowercase.
    triagefieldlabel = models.CharField(db_column='triageFieldLabel', max_length=50, blank=True, null=True)  # Field name made lowercase.
    useextralabordererclaim = models.IntegerField(db_column='useExtraLabOrdererClaim', blank=True, null=True)  # Field name made lowercase.
    usevoiceapptreminder = models.IntegerField(db_column='useVoiceApptReminder', blank=True, null=True)  # Field name made lowercase.
    usesuponlaborders = models.IntegerField(db_column='useSupOnLabOrders', blank=True, null=True)  # Field name made lowercase.
    useadditionademsubreport = models.IntegerField(db_column='useAdditionaDemSubReport', blank=True, null=True)  # Field name made lowercase.
    useoldmrnsubreport = models.IntegerField(db_column='useOldMrnSubReport', blank=True, null=True)  # Field name made lowercase.
    autodischargecode = models.IntegerField(db_column='autoDischargeCode', blank=True, null=True)  # Field name made lowercase.
    clinicinfo = models.IntegerField(db_column='clinicInfo', blank=True, null=True)  # Field name made lowercase.
    disablecalendarsettings = models.IntegerField(db_column='disableCalendarSettings', blank=True, null=True)  # Field name made lowercase.
    utoxhxmovetoend = models.CharField(db_column='utoxHxMoveToEnd', max_length=300, blank=True, null=True)  # Field name made lowercase.
    biannualexamsshowmaindoc = models.IntegerField(db_column='biannualExamsShowMainDoc', blank=True, null=True)  # Field name made lowercase.
    showunbilledinscoonpostingspreadsheet = models.IntegerField(db_column='showUnbilledInsCoOnPostingSpreadsheet', blank=True, null=True)  # Field name made lowercase.
    useprintstatementmult = models.IntegerField(db_column='usePrintStatementMult', blank=True, null=True)  # Field name made lowercase.
    useencounterbilling = models.IntegerField(db_column='useEncounterBilling', blank=True, null=True)  # Field name made lowercase.
    signonhandp = models.IntegerField(db_column='signOnHandP', blank=True, null=True)  # Field name made lowercase.
    showallpayments = models.IntegerField(db_column='showAllPayments', blank=True, null=True)  # Field name made lowercase.
    showscansondemoonly = models.IntegerField(db_column='showScansOnDemoOnly', blank=True, null=True)  # Field name made lowercase.
    showallselfpay = models.IntegerField(db_column='showAllSelfPay', blank=True, null=True)  # Field name made lowercase.
    showallinstypecharges = models.IntegerField(db_column='showAllInsTypeCharges', blank=True, null=True)  # Field name made lowercase.
    showpthxondemoonly = models.IntegerField(db_column='showPtHxOnDemoOnly', blank=True, null=True)  # Field name made lowercase.
    enterchargeonsign = models.IntegerField(db_column='enterChargeOnSign', blank=True, null=True)  # Field name made lowercase.
    accountingreadonly = models.IntegerField(db_column='accountingReadOnly', blank=True, null=True)  # Field name made lowercase.
    pronlyifnosecondary = models.IntegerField(db_column='prOnlyIfNoSecondary', blank=True, null=True)  # Field name made lowercase.
    usecodeentryonsign = models.IntegerField(db_column='useCodeEntryOnSign', blank=True, null=True)  # Field name made lowercase.
    usecodeentryondischarge = models.IntegerField(db_column='useCodeEntryOnDischarge', blank=True, null=True)  # Field name made lowercase.
    dischargeexcludeencounters = models.IntegerField(db_column='dischargeExcludeEncounters', blank=True, null=True)  # Field name made lowercase.
    useaccordiontemplate = models.IntegerField(db_column='useAccordionTemplate', blank=True, null=True)  # Field name made lowercase.
    uselinebreaktemplatephysical = models.IntegerField(db_column='useLineBreakTemplatePhysical', blank=True, null=True)  # Field name made lowercase.
    usemaindoctoronvisit = models.IntegerField(db_column='useMainDoctorOnVisit', blank=True, null=True)  # Field name made lowercase.
    usemaincliniconvisit = models.IntegerField(db_column='useMainClinicOnVisit', blank=True, null=True)  # Field name made lowercase.
    usemedchiefcomoncounseling = models.IntegerField(db_column='useMedChiefComOnCounseling', blank=True, null=True)  # Field name made lowercase.
    cslsummaryextra = models.CharField(db_column='cslSummaryExtra', max_length=100, blank=True, null=True)  # Field name made lowercase.
    useutoxcountonorders = models.IntegerField(db_column='useUtoxCountOnOrders', blank=True, null=True)  # Field name made lowercase.
    marknoentrylabnml = models.IntegerField(db_column='markNoEntryLabNml', blank=True, null=True)  # Field name made lowercase.
    editpostingusers = models.IntegerField(db_column='editPostingUsers', blank=True, null=True)  # Field name made lowercase.
    usedxonstatement = models.IntegerField(db_column='useDxOnStatement', blank=True, null=True)  # Field name made lowercase.
    sendccdaondischarge = models.CharField(db_column='sendCCDAOnDischarge', max_length=35, blank=True, null=True)  # Field name made lowercase.
    usepreauthfield = models.IntegerField(db_column='usePreAuthField', blank=True, null=True)  # Field name made lowercase.
    showcodesonclaimsreport = models.IntegerField(db_column='showCodesOnClaimsReport', blank=True, null=True)  # Field name made lowercase.
    usestdcslinitialassessment = models.IntegerField(db_column='useStdCslInitialAssessment', blank=True, null=True)  # Field name made lowercase.
    billingloconly = models.IntegerField(db_column='billingLocOnly', blank=True, null=True)  # Field name made lowercase.
    calprintbalance = models.IntegerField(db_column='calPrintBalance', blank=True, null=True)  # Field name made lowercase.
    calprintcopay = models.IntegerField(db_column='calPrintCopay', blank=True, null=True)  # Field name made lowercase.
    showcnslog = models.IntegerField(db_column='showCnsLog', blank=True, null=True)  # Field name made lowercase.
    calprintappttype = models.IntegerField(db_column='calPrintApptType', blank=True, null=True)  # Field name made lowercase.
    calprintnoendtime = models.IntegerField(db_column='calPrintNoEndTime', blank=True, null=True)  # Field name made lowercase.
    taxonomycode = models.CharField(db_column='taxonomyCode', max_length=15, blank=True, null=True)  # Field name made lowercase.
    numproc = models.IntegerField(db_column='numProc', blank=True, null=True)  # Field name made lowercase.
    printtaxononhcfa = models.IntegerField(db_column='printTaxonOnHcfa', blank=True, null=True)  # Field name made lowercase.
    primaryphonedisplay = models.CharField(db_column='primaryPhoneDisplay', max_length=35, blank=True, null=True)  # Field name made lowercase.
    hpifieldlabel = models.CharField(db_column='hpiFieldLabel', max_length=35, blank=True, null=True)  # Field name made lowercase.
    showclinicnameandaddress = models.IntegerField(db_column='showClinicNameAndAddress', blank=True, null=True)  # Field name made lowercase.
    changetypetocslonsign = models.IntegerField(db_column='changeTypeToCslOnSign', blank=True, null=True)  # Field name made lowercase.
    printdxonrx = models.IntegerField(db_column='printDxOnRx', blank=True, null=True)  # Field name made lowercase.
    locationtype = models.CharField(db_column='locationType', max_length=50, blank=True, null=True)  # Field name made lowercase.
    billingstatementmultoption = models.CharField(db_column='billingStatementMultOption', max_length=50, blank=True, null=True)  # Field name made lowercase.
    forcebhinsonclaim = models.IntegerField(db_column='forceBhInsOnClaim', blank=True, null=True)  # Field name made lowercase.
    formprintaddressasline = models.IntegerField(db_column='formPrintAddressAsLine', blank=True, null=True)  # Field name made lowercase.
    formprintfontsize = models.IntegerField(db_column='formPrintFontSize', blank=True, null=True)  # Field name made lowercase.
    usecheckintimeonsummary = models.IntegerField(db_column='useCheckInTimeOnSummary')  # Field name made lowercase.
    useptpassword = models.IntegerField(db_column='usePtPassword', blank=True, null=True)  # Field name made lowercase.
    useentityasrendering = models.IntegerField(db_column='useEntityAsRendering', blank=True, null=True)  # Field name made lowercase.
    usesecondaryasrespparty = models.IntegerField(db_column='useSecondaryAsRespParty', blank=True, null=True)  # Field name made lowercase.
    calprintoutinmilitarytime = models.IntegerField(db_column='calPrintoutInMilitaryTime')  # Field name made lowercase.
    showutoxlabgraph = models.IntegerField(db_column='showUtoxLabGraph')  # Field name made lowercase.
    alertbackgroundcolor = models.IntegerField(db_column='alertBackgroundColor', blank=True, null=True)  # Field name made lowercase.
    vitalsalertminutes = models.IntegerField(db_column='vitalsAlertMinutes', blank=True, null=True)  # Field name made lowercase.
    active = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'clinicInfo'


