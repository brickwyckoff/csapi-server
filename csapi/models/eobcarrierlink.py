from __future__ import unicode_literals

from django.db import models

class Eobcarrierlink(models.Model):
    id = models.BigAutoField(primary_key=True)
    eobcarrier = models.CharField(db_column='eobCarrier', max_length=100, blank=True, null=True)  # Field name made lowercase.
    insid = models.BigIntegerField(db_column='insId', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'eobCarrierLink'


