from __future__ import unicode_literals

from django.db import models

class Treatmentcontracttext(models.Model):
    itemid = models.CharField(db_column='itemId', max_length=30, blank=True, null=True)  # Field name made lowercase.
    itemtext = models.CharField(db_column='itemText', max_length=500, blank=True, null=True)  # Field name made lowercase.
    itemdisplay = models.CharField(db_column='itemDisplay', max_length=300, blank=True, null=True)  # Field name made lowercase.
    itemtype = models.CharField(db_column='itemType', max_length=10, blank=True, null=True)  # Field name made lowercase.
    hierlevel = models.IntegerField(db_column='hierLevel', blank=True, null=True)  # Field name made lowercase.
    parentid = models.CharField(db_column='parentId', max_length=30, blank=True, null=True)  # Field name made lowercase.
    releaseto = models.CharField(db_column='releaseTo', max_length=30, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'treatmentContractText'


