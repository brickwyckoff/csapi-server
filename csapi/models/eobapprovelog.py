from __future__ import unicode_literals

from django.db import models

class Eobapprovelog(models.Model):
    controlnumber = models.CharField(db_column='controlNumber', max_length=80, blank=True, null=True)  # Field name made lowercase.
    user = models.CharField(max_length=35, blank=True, null=True)
    approvetime = models.DateTimeField(db_column='approveTime', blank=True, null=True)  # Field name made lowercase.
    batchnumber = models.CharField(db_column='batchNumber', max_length=30, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'eobApproveLog'


