from __future__ import unicode_literals

from django.db import models

class Icsdata(models.Model):
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    presenting = models.CharField(max_length=1000, blank=True, null=True)
    psychhx = models.CharField(db_column='psychHx', max_length=1000, blank=True, null=True)  # Field name made lowercase.
    psychsuicide = models.CharField(db_column='psychSuicide', max_length=1000, blank=True, null=True)  # Field name made lowercase.
    psychviolence = models.CharField(db_column='psychViolence', max_length=1000, blank=True, null=True)  # Field name made lowercase.
    psychtrauma = models.CharField(db_column='psychTrauma', max_length=1000, blank=True, null=True)  # Field name made lowercase.
    psychlegal = models.CharField(db_column='psychLegal', max_length=1000, blank=True, null=True)  # Field name made lowercase.
    psychfamily = models.CharField(db_column='psychFamily', max_length=1000, blank=True, null=True)  # Field name made lowercase.
    psychfamilyrelationship = models.CharField(db_column='psychFamilyRelationship', max_length=1000, blank=True, null=True)  # Field name made lowercase.
    psychfamilymental = models.CharField(db_column='psychFamilyMental', max_length=1000, blank=True, null=True)  # Field name made lowercase.
    psychliving = models.CharField(db_column='psychLiving', max_length=1000, blank=True, null=True)  # Field name made lowercase.
    psychmarital = models.CharField(db_column='psychMarital', max_length=100, blank=True, null=True)  # Field name made lowercase.
    psychchildren = models.CharField(db_column='psychChildren', max_length=1000, blank=True, null=True)  # Field name made lowercase.
    psycheducation = models.CharField(db_column='psychEducation', max_length=100, blank=True, null=True)  # Field name made lowercase.
    psychemployment = models.CharField(db_column='psychEmployment', max_length=1000, blank=True, null=True)  # Field name made lowercase.
    medpcp = models.CharField(db_column='medPcp', max_length=100, blank=True, null=True)  # Field name made lowercase.
    medotherphysician = models.CharField(db_column='medOtherPhysician', max_length=100, blank=True, null=True)  # Field name made lowercase.
    medrx = models.CharField(db_column='medRx', max_length=1000, blank=True, null=True)  # Field name made lowercase.
    medcompliant = models.IntegerField(db_column='medCompliant', blank=True, null=True)  # Field name made lowercase.
    medallergies = models.CharField(db_column='medAllergies', max_length=1000, blank=True, null=True)  # Field name made lowercase.
    medloc = models.CharField(db_column='medLoc', max_length=1000, blank=True, null=True)  # Field name made lowercase.
    medinfo = models.CharField(db_column='medInfo', max_length=1000, blank=True, null=True)  # Field name made lowercase.
    subabusehx = models.CharField(db_column='subAbuseHx', max_length=1000, blank=True, null=True)  # Field name made lowercase.
    subabusetobacco = models.IntegerField(db_column='subAbuseTobacco', blank=True, null=True)  # Field name made lowercase.
    subabusetobaccoage = models.CharField(db_column='subAbuseTobaccoAge', max_length=100, blank=True, null=True)  # Field name made lowercase.
    subabusetobaccocurrent = models.CharField(db_column='subAbuseTobaccoCurrent', max_length=100, blank=True, null=True)  # Field name made lowercase.
    subabusetxhx = models.CharField(db_column='subAbuseTxHx', max_length=1000, blank=True, null=True)  # Field name made lowercase.
    subabuseoverdoses = models.CharField(db_column='subAbuseOverdoses', max_length=1000, blank=True, null=True)  # Field name made lowercase.
    subabuseiv = models.CharField(db_column='subAbuseIv', max_length=1000, blank=True, null=True)  # Field name made lowercase.
    subabusesobriety = models.CharField(db_column='subAbuseSobriety', max_length=1000, blank=True, null=True)  # Field name made lowercase.
    subabusemeans = models.CharField(db_column='subAbuseMeans', max_length=1000, blank=True, null=True)  # Field name made lowercase.
    subabusefamilyhx = models.CharField(db_column='subAbuseFamilyHx', max_length=1000, blank=True, null=True)  # Field name made lowercase.
    subabusetxmotivation = models.CharField(db_column='subAbuseTxMotivation', max_length=100, blank=True, null=True)  # Field name made lowercase.
    addlikeself = models.CharField(db_column='addLikeSelf', max_length=1000, blank=True, null=True)  # Field name made lowercase.
    adddislikeself = models.CharField(db_column='addDislikeSelf', max_length=1000, blank=True, null=True)  # Field name made lowercase.
    addassets = models.CharField(db_column='addAssets', max_length=1000, blank=True, null=True)  # Field name made lowercase.
    addweakness = models.CharField(db_column='addWeakness', max_length=1000, blank=True, null=True)  # Field name made lowercase.
    addfun = models.CharField(db_column='addFun', max_length=1000, blank=True, null=True)  # Field name made lowercase.
    addwishes = models.CharField(db_column='addWishes', max_length=1000, blank=True, null=True)  # Field name made lowercase.
    addgoals = models.CharField(db_column='addGoals', max_length=1000, blank=True, null=True)  # Field name made lowercase.
    addother = models.CharField(db_column='addOther', max_length=1000, blank=True, null=True)  # Field name made lowercase.
    assess = models.CharField(max_length=1000, blank=True, null=True)
    assessaxis1 = models.CharField(db_column='assessAxis1', max_length=100, blank=True, null=True)  # Field name made lowercase.
    assessaxis2 = models.CharField(db_column='assessAxis2', max_length=100, blank=True, null=True)  # Field name made lowercase.
    assessaxis3 = models.CharField(db_column='assessAxis3', max_length=100, blank=True, null=True)  # Field name made lowercase.
    assessaxis4 = models.CharField(db_column='assessAxis4', max_length=100, blank=True, null=True)  # Field name made lowercase.
    assessaxis5 = models.CharField(db_column='assessAxis5', max_length=100, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'icsData'


