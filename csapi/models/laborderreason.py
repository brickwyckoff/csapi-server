from __future__ import unicode_literals

from django.db import models

class Laborderreason(models.Model):
    id = models.BigAutoField(primary_key=True)
    laborderid = models.BigIntegerField(db_column='labOrderId')  # Field name made lowercase.
    laborderpendingid = models.BigIntegerField(db_column='labOrderPendingId')  # Field name made lowercase.
    reason = models.CharField(max_length=150)
    entrytime = models.DateTimeField(db_column='entryTime')  # Field name made lowercase.
    isordered = models.IntegerField(db_column='isOrdered')  # Field name made lowercase.
    isreviewed = models.IntegerField(db_column='isReviewed')  # Field name made lowercase.
    username = models.CharField(max_length=30, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'labOrderReason'


