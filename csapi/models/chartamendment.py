from __future__ import unicode_literals

from django.db import models

class Chartamendment(models.Model):
    id = models.BigAutoField(primary_key=True)
    visitnum = models.BigIntegerField(db_column='visitNum')  # Field name made lowercase.
    amendmentrequest = models.CharField(db_column='amendmentRequest', max_length=500)  # Field name made lowercase.
    amendment = models.CharField(max_length=10000)
    amendmenttime = models.DateTimeField(db_column='amendmentTime', blank=True, null=True)  # Field name made lowercase.
    amendmentsource = models.CharField(db_column='amendmentSource', max_length=150)  # Field name made lowercase.
    isaccepted = models.IntegerField(db_column='isAccepted')  # Field name made lowercase.
    statustime = models.DateTimeField(db_column='statusTime')  # Field name made lowercase.
    requesttime = models.DateTimeField(db_column='requestTime')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'chartAmendment'


