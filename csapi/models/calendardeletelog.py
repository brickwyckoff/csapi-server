from __future__ import unicode_literals

from django.db import models

class Calendardeletelog(models.Model):
    id = models.IntegerField(primary_key=True)
    calendar_id = models.IntegerField(blank=True, null=True)
    title = models.CharField(max_length=250, blank=True, null=True)
    mrn = models.CharField(max_length=11, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    date = models.DateField(blank=True, null=True)
    start_time = models.CharField(max_length=5)
    end_time = models.CharField(max_length=5)
    recurring_id = models.CharField(max_length=32, blank=True, null=True)
    repeat_type = models.CharField(max_length=10, blank=True, null=True)
    repeat_end_date = models.CharField(max_length=10, blank=True, null=True)
    reminder1_type = models.CharField(max_length=10, blank=True, null=True)
    reminder1_time = models.SmallIntegerField(blank=True, null=True)
    reminder1_time_type = models.CharField(max_length=10, blank=True, null=True)
    reminder1_ts = models.FloatField(blank=True, null=True)
    reminder2_type = models.CharField(max_length=10, blank=True, null=True)
    reminder2_time = models.SmallIntegerField(blank=True, null=True)
    reminder2_time_type = models.CharField(max_length=10, blank=True, null=True)
    reminder2_ts = models.FloatField(blank=True, null=True)
    cal_id = models.IntegerField(blank=True, null=True)
    type = models.CharField(max_length=9, blank=True, null=True)
    start_date = models.DateField(blank=True, null=True)
    end_date = models.DateField(blank=True, null=True)
    repeat_type2 = models.CharField(max_length=7, blank=True, null=True)
    repeat_interval = models.IntegerField(blank=True, null=True)
    repeat_count = models.IntegerField(blank=True, null=True)
    repeat_data = models.CharField(max_length=30, blank=True, null=True)
    location = models.CharField(max_length=255, blank=True, null=True)
    available = models.CharField(max_length=1, blank=True, null=True)
    public = models.CharField(max_length=1, blank=True, null=True)
    image = models.CharField(max_length=100, blank=True, null=True)
    invitation = models.CharField(max_length=1, blank=True, null=True)
    invitation_event_id = models.IntegerField(blank=True, null=True)
    invitation_creator_id = models.IntegerField(blank=True, null=True)
    invitation_response = models.CharField(max_length=7, blank=True, null=True)
    created_by = models.CharField(max_length=30, blank=True, null=True)
    modified_by = models.CharField(max_length=30, blank=True, null=True)
    created_on = models.CharField(max_length=19, blank=True, null=True)
    updated_on = models.CharField(max_length=19, blank=True, null=True)
    end_timestamp = models.IntegerField()
    repeat_deleted_indexes = models.CharField(max_length=255)
    color = models.CharField(max_length=15)
    start_timestamp = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'calendarDeleteLog'


