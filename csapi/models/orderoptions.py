from __future__ import unicode_literals

from django.db import models

class Orderoptions(models.Model):
    ordername = models.CharField(db_column='orderName', max_length=60, blank=True, null=True)  # Field name made lowercase.
    active = models.IntegerField(blank=True, null=True)
    gender = models.CharField(max_length=1, blank=True, null=True)
    ordercode = models.CharField(db_column='orderCode', max_length=15)  # Field name made lowercase.
    sendto = models.CharField(db_column='sendTo', max_length=15)  # Field name made lowercase.
    autoorder = models.IntegerField(db_column='autoOrder')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'orderOptions'


