from __future__ import unicode_literals

from django.db import models

class Scadatamerged(models.Model):
    mrn = models.BigIntegerField(blank=True, null=True)
    scantype = models.CharField(db_column='scanType', max_length=50, blank=True, null=True)  # Field name made lowercase.
    scanfile = models.CharField(db_column='scanFile', max_length=100, blank=True, null=True)  # Field name made lowercase.
    scandate = models.DateTimeField(db_column='scanDate', blank=True, null=True)  # Field name made lowercase.
    user = models.CharField(max_length=80, blank=True, null=True)
    scandeleted = models.IntegerField(db_column='scanDeleted', blank=True, null=True)  # Field name made lowercase.
    pdfconvert = models.IntegerField(db_column='pdfConvert', blank=True, null=True)  # Field name made lowercase.
    cfupload = models.IntegerField(db_column='cfUpload')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'scaDataMerged'


