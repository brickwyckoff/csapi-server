from __future__ import unicode_literals

from django.db import models

class Standarddischargeend(models.Model):
    pttype = models.CharField(db_column='ptType', max_length=3, blank=True, null=True)  # Field name made lowercase.
    discharge = models.CharField(max_length=1000, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'standardDischargeEnd'


