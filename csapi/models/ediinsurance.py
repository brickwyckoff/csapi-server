from __future__ import unicode_literals

from django.db import models

class Ediinsurance(models.Model):
    company = models.CharField(max_length=30, blank=True, null=True)
    interchangereceiveridqual = models.CharField(db_column='interchangeReceiverIdQual', max_length=2, blank=True, null=True)  # Field name made lowercase.
    interchangereceiverid = models.CharField(db_column='interchangeReceiverId', max_length=30, blank=True, null=True)  # Field name made lowercase.
    number_835senderidqual = models.CharField(db_column='835senderIdQual', max_length=2)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    number_835senderid = models.CharField(db_column='835senderId', max_length=30)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    receivercode = models.CharField(db_column='receiverCode', max_length=30, blank=True, null=True)  # Field name made lowercase.
    submittercode = models.CharField(db_column='submitterCode', max_length=10, blank=True, null=True)  # Field name made lowercase.
    submitterid = models.CharField(db_column='submitterId', max_length=30, blank=True, null=True)  # Field name made lowercase.
    receivername = models.CharField(db_column='receiverName', max_length=30, blank=True, null=True)  # Field name made lowercase.
    receiverprimaryid = models.CharField(db_column='receiverPrimaryId', max_length=30, blank=True, null=True)  # Field name made lowercase.
    interchangesubmitteridqual = models.CharField(db_column='interchangeSubmitterIdQual', max_length=2, blank=True, null=True)  # Field name made lowercase.
    interchangesubmitterid = models.CharField(db_column='interchangeSubmitterId', max_length=30, blank=True, null=True)  # Field name made lowercase.
    transactionnum = models.IntegerField(db_column='transactionNum', blank=True, null=True)  # Field name made lowercase.
    lastupdate = models.CharField(db_column='lastUpdate', max_length=12, blank=True, null=True)  # Field name made lowercase.
    filingidentcode = models.CharField(db_column='filingIdentCode', max_length=5, blank=True, null=True)  # Field name made lowercase.
    receiver = models.CharField(max_length=30, blank=True, null=True)
    tablename = models.CharField(db_column='tableName', max_length=50, blank=True, null=True)  # Field name made lowercase.
    billcount = models.IntegerField(db_column='billCount', blank=True, null=True)  # Field name made lowercase.
    internalcode = models.CharField(db_column='internalCode', max_length=5, blank=True, null=True)  # Field name made lowercase.
    oldbilllimit = models.IntegerField(db_column='oldBillLimit')  # Field name made lowercase.
    autosend = models.IntegerField(db_column='autoSend')  # Field name made lowercase.
    autosendfilename = models.CharField(db_column='autoSendFileName', max_length=20)  # Field name made lowercase.
    edi = models.IntegerField()
    senderid = models.CharField(db_column='senderId', max_length=100)  # Field name made lowercase.
    payerid = models.CharField(db_column='payerId', max_length=50)  # Field name made lowercase.
    nodependent270 = models.IntegerField(db_column='noDependent270')  # Field name made lowercase.
    send270 = models.SmallIntegerField()
    eobcompany = models.CharField(db_column='eobCompany', max_length=50)  # Field name made lowercase.
    eobaddress1 = models.CharField(db_column='eobAddress1', max_length=60)  # Field name made lowercase.
    eobcity = models.CharField(db_column='eobCity', max_length=60)  # Field name made lowercase.
    eobstate = models.CharField(db_column='eobState', max_length=2)  # Field name made lowercase.
    eobzip = models.CharField(db_column='eobZip', max_length=10)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ediInsurance'


