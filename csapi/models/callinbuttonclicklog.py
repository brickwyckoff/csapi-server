from __future__ import unicode_literals

from django.db import models

class Callinbuttonclicklog(models.Model):
    id = models.BigAutoField(primary_key=True)
    mrn = models.BigIntegerField(blank=True, null=True)
    clicktime = models.DateTimeField(db_column='clickTime', blank=True, null=True)  # Field name made lowercase.
    username = models.CharField(max_length=30, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'callInButtonClickLog'


