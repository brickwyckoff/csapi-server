from __future__ import unicode_literals

from django.db import models

class Macrosrightclick(models.Model):
    macrosection = models.CharField(db_column='macroSection', max_length=100, blank=True, null=True)  # Field name made lowercase.
    macroid = models.CharField(db_column='macroId', max_length=100, blank=True, null=True)  # Field name made lowercase.
    macrodisplay = models.CharField(db_column='macroDisplay', max_length=100, blank=True, null=True)  # Field name made lowercase.
    macrotext = models.TextField(db_column='macroText', blank=True, null=True)  # Field name made lowercase.
    isactive = models.IntegerField(db_column='isActive', blank=True, null=True)  # Field name made lowercase.
    displayorder = models.IntegerField(db_column='displayOrder', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'macrosRightClick'


