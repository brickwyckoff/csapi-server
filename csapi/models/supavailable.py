from __future__ import unicode_literals

from django.db import models

class Supavailable(models.Model):
    supervisor = models.CharField(max_length=5, blank=True, null=True)
    clinicid = models.BigIntegerField(db_column='clinicId', blank=True, null=True)  # Field name made lowercase.
    availableslots = models.IntegerField(db_column='availableSlots', blank=True, null=True)  # Field name made lowercase.
    useslot = models.IntegerField(db_column='useSlot')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'supAvailable'


