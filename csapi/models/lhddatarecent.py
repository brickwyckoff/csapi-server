from __future__ import unicode_literals

from django.db import models

class Lhddatarecent(models.Model):
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    mrn = models.BigIntegerField(blank=True, null=True)
    placerid = models.BigIntegerField(db_column='placerId', blank=True, null=True)  # Field name made lowercase.
    fillerid = models.CharField(db_column='fillerId', max_length=22, blank=True, null=True)  # Field name made lowercase.
    messagecontrolid = models.CharField(db_column='messageControlId', max_length=22, blank=True, null=True)  # Field name made lowercase.
    testid = models.CharField(db_column='testId', max_length=50, blank=True, null=True)  # Field name made lowercase.
    testname = models.CharField(db_column='testName', max_length=200, blank=True, null=True)  # Field name made lowercase.
    subheader = models.CharField(db_column='subHeader', max_length=100, blank=True, null=True)  # Field name made lowercase.
    subheaderid = models.CharField(db_column='subHeaderId', max_length=15, blank=True, null=True)  # Field name made lowercase.
    observationdate = models.DateTimeField(db_column='observationDate', blank=True, null=True)  # Field name made lowercase.
    resultstatus = models.CharField(db_column='resultStatus', max_length=1, blank=True, null=True)  # Field name made lowercase.
    replaced = models.IntegerField(blank=True, null=True)
    analysedby = models.CharField(db_column='analysedBy', max_length=15, blank=True, null=True)  # Field name made lowercase.
    ptnamereported = models.CharField(db_column='ptNameReported', max_length=100)  # Field name made lowercase.
    loinccode = models.CharField(db_column='loincCode', max_length=15)  # Field name made lowercase.
    resultsreporteddate = models.DateTimeField(db_column='resultsReportedDate', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'lhdDataRecent'


