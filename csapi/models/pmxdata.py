from __future__ import unicode_literals

from django.db import models

class Pmxdata(models.Model):
    mrn = models.BigIntegerField(blank=True, null=True)
    socialother = models.CharField(db_column='socialOther', max_length=5000, blank=True, null=True)  # Field name made lowercase.
    familyother = models.CharField(db_column='familyOther', max_length=5000, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'pmxData'


