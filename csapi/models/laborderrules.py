from __future__ import unicode_literals

from django.db import models

class Laborderrules(models.Model):
    rulename = models.CharField(db_column='ruleName', max_length=30, blank=True, null=True)  # Field name made lowercase.
    payorid = models.CharField(db_column='payorId', max_length=30, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'labOrderRules'


