from __future__ import unicode_literals

from django.db import models

class Referrallog(models.Model):
    id = models.BigAutoField(primary_key=True)
    mrn = models.BigIntegerField(blank=True, null=True)
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    username = models.CharField(max_length=30, blank=True, null=True)
    provider = models.CharField(max_length=50, blank=True, null=True)
    requesteddate = models.DateField(db_column='requestedDate', blank=True, null=True)  # Field name made lowercase.
    reasonforreferral = models.CharField(db_column='reasonForReferral', max_length=100, blank=True, null=True)  # Field name made lowercase.
    directdirectoryid = models.BigIntegerField(db_column='directDirectoryId', blank=True, null=True)  # Field name made lowercase.
    directaddress = models.CharField(db_column='directAddress', max_length=150, blank=True, null=True)  # Field name made lowercase.
    sendtime = models.DateTimeField(db_column='sendTime', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'referralLog'


