from __future__ import unicode_literals

from django.db import models

class Rxcancelfaxqueue(models.Model):
    id = models.BigAutoField(primary_key=True)
    mrn = models.BigIntegerField()
    cancelprescriptionlogid = models.BigIntegerField(db_column='cancelPrescriptionLogId')  # Field name made lowercase.
    newprescriptionlogid = models.BigIntegerField(db_column='newPrescriptionLogId')  # Field name made lowercase.
    ncpdpid = models.CharField(max_length=7)
    canceldate = models.DateTimeField(db_column='cancelDate')  # Field name made lowercase.
    cancelfaxtype = models.IntegerField(db_column='cancelFaxType')  # Field name made lowercase.
    faxtransmitted = models.IntegerField(db_column='faxTransmitted')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'rxCancelFaxQueue'


