from __future__ import unicode_literals

from django.db import models

class Inscoarchive(models.Model):
    mrn = models.BigIntegerField(blank=True, null=True)
    name = models.CharField(max_length=100, blank=True, null=True)
    policynum = models.CharField(db_column='policyNum', max_length=60, blank=True, null=True)  # Field name made lowercase.
    groupnum = models.CharField(db_column='groupNum', max_length=60, blank=True, null=True)  # Field name made lowercase.
    startdate = models.DateField(db_column='startDate', blank=True, null=True)  # Field name made lowercase.
    enddate = models.DateField(db_column='endDate', blank=True, null=True)  # Field name made lowercase.
    insuredfirstname = models.CharField(db_column='insuredFirstName', max_length=60, blank=True, null=True)  # Field name made lowercase.
    insuredlastname = models.CharField(db_column='insuredLastName', max_length=60, blank=True, null=True)  # Field name made lowercase.
    insuredaddress1 = models.CharField(db_column='insuredAddress1', max_length=60, blank=True, null=True)  # Field name made lowercase.
    insuredaddress2 = models.CharField(db_column='insuredAddress2', max_length=60, blank=True, null=True)  # Field name made lowercase.
    insuredcity = models.CharField(db_column='insuredCity', max_length=100, blank=True, null=True)  # Field name made lowercase.
    insuredstate = models.CharField(db_column='insuredState', max_length=2, blank=True, null=True)  # Field name made lowercase.
    insuredzip = models.CharField(db_column='insuredZip', max_length=10, blank=True, null=True)  # Field name made lowercase.
    insureddob = models.DateField(db_column='insuredDob', blank=True, null=True)  # Field name made lowercase.
    insuredgender = models.CharField(db_column='insuredGender', max_length=1, blank=True, null=True)  # Field name made lowercase.
    insuredrelation = models.CharField(db_column='insuredRelation', max_length=5, blank=True, null=True)  # Field name made lowercase.
    insurancetype = models.IntegerField(db_column='insuranceType')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'insCoArchive'


