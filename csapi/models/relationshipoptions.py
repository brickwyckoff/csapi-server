from __future__ import unicode_literals

from django.db import models

class Relationshipoptions(models.Model):
    optionname = models.CharField(db_column='optionName', max_length=35, blank=True, null=True)  # Field name made lowercase.
    isactive = models.IntegerField(db_column='isActive', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'relationshipOptions'


