from __future__ import unicode_literals

from django.db import models

class Pdmplog(models.Model):
    mrn = models.BigIntegerField(blank=True, null=True)
    visitnum = models.IntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    entrydate = models.DateTimeField(db_column='entryDate', blank=True, null=True)  # Field name made lowercase.
    checkeddate = models.DateField(db_column='checkedDate', blank=True, null=True)  # Field name made lowercase.
    user = models.CharField(max_length=20, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pdmpLog'


