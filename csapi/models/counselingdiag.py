from __future__ import unicode_literals

from django.db import models

class Counselingdiag(models.Model):
    id = models.BigAutoField(primary_key=True)
    code = models.CharField(max_length=10)
    description = models.CharField(max_length=80)
    isactive = models.IntegerField(db_column='isActive')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'counselingDiag'


