from __future__ import unicode_literals

from django.db import models

class Locktable(models.Model):
    user = models.CharField(max_length=50, blank=True, null=True)
    section = models.CharField(max_length=3, blank=True, null=True)
    mrn = models.BigIntegerField(blank=True, null=True)
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    entertime = models.DateTimeField(db_column='enterTime', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'lockTable'


