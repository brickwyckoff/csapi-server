from __future__ import unicode_literals

from django.db import models

class Customformsmeta(models.Model):
    formname = models.CharField(db_column='formName', max_length=30, blank=True, null=True)  # Field name made lowercase.
    formsection = models.CharField(db_column='formSection', max_length=30, blank=True, null=True)  # Field name made lowercase.
    indextype = models.IntegerField(db_column='indexType', blank=True, null=True)  # Field name made lowercase.
    displayname = models.CharField(db_column='displayName', max_length=100, blank=True, null=True)  # Field name made lowercase.
    tablewidth = models.IntegerField(db_column='tableWidth', blank=True, null=True)  # Field name made lowercase.
    tableheader = models.CharField(db_column='tableHeader', max_length=400, blank=True, null=True)  # Field name made lowercase.
    colwidths = models.CharField(db_column='colWidths', max_length=100, blank=True, null=True)  # Field name made lowercase.
    displayorder = models.IntegerField(db_column='displayOrder')  # Field name made lowercase.
    isactive = models.IntegerField(db_column='isActive')  # Field name made lowercase.
    formdisplayname = models.CharField(db_column='formDisplayName', max_length=50)  # Field name made lowercase.
    siglines = models.CharField(db_column='sigLines', max_length=50, blank=True, null=True)  # Field name made lowercase.
    printonsummary = models.IntegerField(db_column='printOnSummary', blank=True, null=True)  # Field name made lowercase.
    printcheckfield = models.CharField(db_column='printCheckField', max_length=35, blank=True, null=True)  # Field name made lowercase.
    useimportbutton = models.IntegerField(db_column='useImportButton', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'customFormsMeta'


