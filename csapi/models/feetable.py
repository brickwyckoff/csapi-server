from __future__ import unicode_literals

from django.db import models

class Feetable(models.Model):
    code = models.CharField(max_length=9, blank=True, null=True)
    fee = models.IntegerField(blank=True, null=True)
    locationcode = models.CharField(db_column='locationCode', max_length=5, blank=True, null=True)  # Field name made lowercase.
    npi = models.CharField(max_length=15, blank=True, null=True)
    selfpayonly = models.IntegerField(db_column='selfPayOnly', blank=True, null=True)  # Field name made lowercase.
    inclreferral = models.IntegerField(db_column='inclReferral')  # Field name made lowercase.
    islabcode = models.IntegerField(db_column='isLabCode', blank=True, null=True)  # Field name made lowercase.
    noprovider = models.IntegerField(db_column='noProvider')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'feeTable'


