from __future__ import unicode_literals

from django.db import models

class Orddata(models.Model):
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    orderid = models.IntegerField(db_column='orderId', blank=True, null=True)  # Field name made lowercase.
    orderby = models.CharField(db_column='orderBy', max_length=30, blank=True, null=True)  # Field name made lowercase.
    ordertime = models.DateTimeField(db_column='orderTime', blank=True, null=True)  # Field name made lowercase.
    fillby = models.CharField(db_column='fillBy', max_length=30, blank=True, null=True)  # Field name made lowercase.
    filltime = models.DateTimeField(db_column='fillTime', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ordData'


