from __future__ import unicode_literals

from django.db import models

class Scantypes(models.Model):
    scantype = models.CharField(db_column='scanType', max_length=60, blank=True, null=True)  # Field name made lowercase.
    listordinal = models.IntegerField(db_column='listOrdinal', blank=True, null=True)  # Field name made lowercase.
    barcodestart = models.CharField(db_column='barcodeStart', max_length=4, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'scanTypes'


