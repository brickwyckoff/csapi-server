from __future__ import unicode_literals

from django.db import models

class Dandata(models.Model):
    id = models.IntegerField()
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    notes = models.CharField(max_length=500, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'danData'


