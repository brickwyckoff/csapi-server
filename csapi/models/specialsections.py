from __future__ import unicode_literals

from django.db import models

class Specialsections(models.Model):
    code = models.CharField(max_length=3, blank=True, null=True)
    name = models.CharField(max_length=50, blank=True, null=True)
    filename = models.CharField(db_column='fileName', max_length=50, blank=True, null=True)  # Field name made lowercase.
    active = models.IntegerField(blank=True, null=True)
    ispttype = models.IntegerField(db_column='isPtType', blank=True, null=True)  # Field name made lowercase.
    sectionorder = models.IntegerField(db_column='sectionOrder', blank=True, null=True)  # Field name made lowercase.
    useonencounternote = models.IntegerField(db_column='useOnEncounterNote', blank=True, null=True)  # Field name made lowercase.
    description = models.CharField(max_length=1000, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'specialSections'


