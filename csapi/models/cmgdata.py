from __future__ import unicode_literals

from django.db import models

class Cmgdata(models.Model):
    id = models.BigAutoField(primary_key=True)
    mrn = models.BigIntegerField(blank=True, null=True)
    clinician = models.CharField(max_length=5, blank=True, null=True)
    casemgr = models.CharField(db_column='caseMgr', max_length=5, blank=True, null=True)  # Field name made lowercase.
    biopsycosocialdate = models.CharField(db_column='bioPsycoSocialDate', max_length=15, blank=True, null=True)  # Field name made lowercase.
    initialtxplandate = models.CharField(db_column='initialTxPlanDate', max_length=15, blank=True, null=True)  # Field name made lowercase.
    indtxplandate = models.CharField(db_column='indTxPlanDate', max_length=15, blank=True, null=True)  # Field name made lowercase.
    txplanreviewdate = models.CharField(db_column='txPlanReviewDate', max_length=15, blank=True, null=True)  # Field name made lowercase.
    projdxdate = models.CharField(db_column='projDxDate', max_length=15, blank=True, null=True)  # Field name made lowercase.
    dxreason = models.CharField(db_column='dxReason', max_length=500, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'cmgData'


