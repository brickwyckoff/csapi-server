from __future__ import unicode_literals

from django.db import models

class Billingpendingclaims(models.Model):
    id = models.BigAutoField(primary_key=True)
    insco = models.CharField(db_column='insCo', max_length=50)  # Field name made lowercase.
    filename = models.CharField(db_column='fileName', max_length=150)  # Field name made lowercase.
    timecreated = models.DateTimeField(db_column='timeCreated')  # Field name made lowercase.
    claimamt = models.IntegerField(db_column='claimAmt')  # Field name made lowercase.
    controlnumber = models.BigIntegerField(db_column='controlNumber')  # Field name made lowercase.
    fileext = models.CharField(db_column='fileExt', max_length=3)  # Field name made lowercase.
    sent = models.IntegerField()
    status = models.CharField(max_length=15)

    class Meta:
        managed = False
        db_table = 'billingPendingClaims'


