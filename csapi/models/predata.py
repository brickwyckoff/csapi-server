from __future__ import unicode_literals

from django.db import models

class Predata(models.Model):
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    prescription1 = models.CharField(max_length=150, blank=True, null=True)
    freq1 = models.CharField(max_length=30, blank=True, null=True)
    qty1 = models.CharField(max_length=15, blank=True, null=True)
    route1 = models.CharField(max_length=15, blank=True, null=True)
    refills1 = models.CharField(max_length=15, blank=True, null=True)
    prescription2 = models.CharField(max_length=150, blank=True, null=True)
    freq2 = models.CharField(max_length=30, blank=True, null=True)
    qty2 = models.CharField(max_length=15, blank=True, null=True)
    route2 = models.CharField(max_length=15, blank=True, null=True)
    refills2 = models.CharField(max_length=15, blank=True, null=True)
    prescription3 = models.CharField(max_length=150, blank=True, null=True)
    freq3 = models.CharField(max_length=30, blank=True, null=True)
    qty3 = models.CharField(max_length=15, blank=True, null=True)
    route3 = models.CharField(max_length=15, blank=True, null=True)
    refills3 = models.CharField(max_length=15, blank=True, null=True)
    prescription4 = models.CharField(max_length=150, blank=True, null=True)
    freq4 = models.CharField(max_length=30, blank=True, null=True)
    qty4 = models.CharField(max_length=15, blank=True, null=True)
    route4 = models.CharField(max_length=15, blank=True, null=True)
    refills4 = models.CharField(max_length=15, blank=True, null=True)
    prescription5 = models.CharField(max_length=150, blank=True, null=True)
    freq5 = models.CharField(max_length=30, blank=True, null=True)
    qty5 = models.CharField(max_length=15, blank=True, null=True)
    route5 = models.CharField(max_length=15, blank=True, null=True)
    refills5 = models.CharField(max_length=15, blank=True, null=True)
    prescription6 = models.CharField(max_length=150, blank=True, null=True)
    freq6 = models.CharField(max_length=30, blank=True, null=True)
    qty6 = models.CharField(max_length=15, blank=True, null=True)
    route6 = models.CharField(max_length=15, blank=True, null=True)
    refills6 = models.CharField(max_length=15, blank=True, null=True)
    prescription7 = models.CharField(max_length=150, blank=True, null=True)
    freq7 = models.CharField(max_length=30, blank=True, null=True)
    qty7 = models.CharField(max_length=15, blank=True, null=True)
    route7 = models.CharField(max_length=15, blank=True, null=True)
    refills7 = models.CharField(max_length=15, blank=True, null=True)
    prescription8 = models.CharField(max_length=150, blank=True, null=True)
    freq8 = models.CharField(max_length=30, blank=True, null=True)
    qty8 = models.CharField(max_length=15, blank=True, null=True)
    route8 = models.CharField(max_length=15, blank=True, null=True)
    refills = models.CharField(max_length=15, blank=True, null=True)
    refills8 = models.CharField(max_length=15, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'preData'


