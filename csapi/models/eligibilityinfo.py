from __future__ import unicode_literals

from django.db import models

class Eligibilityinfo(models.Model):
    code = models.CharField(max_length=2, blank=True, null=True)
    reason = models.CharField(max_length=200, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'eligibilityInfo'


