from __future__ import unicode_literals

from django.db import models

class Stadatamerged(models.Model):
    mrn = models.BigIntegerField(blank=True, null=True)
    smokingstatus = models.IntegerField(db_column='smokingStatus', blank=True, null=True)  # Field name made lowercase.
    smokingstartyear = models.TextField(db_column='smokingStartYear', blank=True, null=True)  # Field name made lowercase. This field type is a guess.
    pttransferedin = models.IntegerField(db_column='ptTransferedIn')  # Field name made lowercase.
    medrecin = models.IntegerField(db_column='medRecIn')  # Field name made lowercase.
    txstatus = models.IntegerField(db_column='txStatus', blank=True, null=True)  # Field name made lowercase.
    room = models.CharField(max_length=35, blank=True, null=True)
    changetime = models.DateTimeField(db_column='changeTime')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'staDataMerged'


