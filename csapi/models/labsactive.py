from __future__ import unicode_literals

from django.db import models

class Labsactive(models.Model):
    code = models.CharField(max_length=30)
    loccode = models.CharField(db_column='locCode', max_length=10, blank=True, null=True)  # Field name made lowercase.
    cptcode = models.CharField(db_column='cptCode', max_length=30, blank=True, null=True)  # Field name made lowercase.
    standardfreq = models.IntegerField(db_column='standardFreq')  # Field name made lowercase.
    sendto = models.CharField(db_column='sendTo', max_length=15)  # Field name made lowercase.
    ordercode = models.CharField(db_column='orderCode', max_length=15)  # Field name made lowercase.
    isactive = models.IntegerField(db_column='isActive', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'labsActive'


