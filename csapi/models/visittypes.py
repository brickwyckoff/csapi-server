from __future__ import unicode_literals

from django.db import models

class Visittypes(models.Model):
    visitname = models.CharField(db_column='visitName', max_length=30, blank=True, null=True)  # Field name made lowercase.
    active = models.IntegerField(blank=True, null=True)
    emrtype = models.CharField(db_column='emrType', max_length=30, blank=True, null=True)  # Field name made lowercase.
    assignsup = models.IntegerField(db_column='assignSup', blank=True, null=True)  # Field name made lowercase.
    ismedical = models.IntegerField(db_column='isMedical', blank=True, null=True)  # Field name made lowercase.
    chiefcomplaint = models.CharField(db_column='chiefComplaint', max_length=50, blank=True, null=True)  # Field name made lowercase.
    showasmdvisit = models.IntegerField(db_column='showAsMdVisit')  # Field name made lowercase.
    subtype = models.CharField(db_column='subType', max_length=30, blank=True, null=True)  # Field name made lowercase.
    iscounseling = models.IntegerField(db_column='isCounseling')  # Field name made lowercase.
    notetitle = models.CharField(db_column='noteTitle', max_length=30, blank=True, null=True)  # Field name made lowercase.
    isbillable = models.IntegerField(db_column='isBillable')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'visitTypes'


