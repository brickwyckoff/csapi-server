from __future__ import unicode_literals

from django.db import models

class Ccdfiles(models.Model):
    mrn = models.BigIntegerField(blank=True, null=True)
    filename = models.CharField(db_column='fileName', max_length=200, blank=True, null=True)  # Field name made lowercase.
    creationtime = models.DateTimeField(db_column='creationTime', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ccdFiles'


