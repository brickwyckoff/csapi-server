from __future__ import unicode_literals

from django.db import models

class Prescriptionfaxlog(models.Model):
    mrn = models.BigIntegerField(blank=True, null=True)
    prescription = models.CharField(max_length=60, blank=True, null=True)
    freq = models.CharField(max_length=15, blank=True, null=True)
    qty = models.CharField(max_length=15, blank=True, null=True)
    route = models.CharField(max_length=15, blank=True, null=True)
    refills = models.CharField(max_length=15, blank=True, null=True)
    provider = models.CharField(max_length=30, blank=True, null=True)
    faxtime = models.DateTimeField(db_column='faxTime', blank=True, null=True)  # Field name made lowercase.
    usedvisitnum = models.BigIntegerField(db_column='usedVisitNum', blank=True, null=True)  # Field name made lowercase.
    faxto = models.CharField(db_column='faxTo', max_length=20)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'prescriptionFaxLog'


