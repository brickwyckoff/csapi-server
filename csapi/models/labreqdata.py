from __future__ import unicode_literals

from django.db import models

class Labreqdata(models.Model):
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    code = models.CharField(max_length=20, blank=True, null=True)
    loccode = models.CharField(db_column='locCode', max_length=15, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'labReqData'


