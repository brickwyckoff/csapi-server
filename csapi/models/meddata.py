from __future__ import unicode_literals

from django.db import models

class Meddata(models.Model):
    mrn = models.BigIntegerField(blank=True, null=True)
    medication = models.CharField(max_length=200, blank=True, null=True)
    dose = models.CharField(max_length=20, blank=True, null=True)
    freq = models.CharField(max_length=20, blank=True, null=True)
    since = models.CharField(max_length=20, blank=True, null=True)
    creationtime = models.DateTimeField(db_column='creationTime', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'medData'


