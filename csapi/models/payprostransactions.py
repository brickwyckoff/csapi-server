from __future__ import unicode_literals

from django.db import models

class Payprostransactions(models.Model):
    clientcode = models.CharField(db_column='clientCode', max_length=15, blank=True, null=True)  # Field name made lowercase.
    txtime = models.DateTimeField(db_column='txTime', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'payProsTransactions'


