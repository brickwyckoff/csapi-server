from __future__ import unicode_literals

from django.db import models

class Lablocactive(models.Model):
    loccode = models.CharField(db_column='locCode', max_length=15, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'labLocActive'


