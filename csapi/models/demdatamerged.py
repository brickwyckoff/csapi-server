from __future__ import unicode_literals

from django.db import models

class Demdatamerged(models.Model):
    ptfirstname = models.CharField(db_column='ptFirstName', max_length=50)  # Field name made lowercase.
    ptlastname = models.CharField(db_column='ptLastName', max_length=50)  # Field name made lowercase.
    ptaddress1 = models.CharField(db_column='ptAddress1', max_length=100)  # Field name made lowercase.
    ptaddress2 = models.CharField(db_column='ptAddress2', max_length=100, blank=True, null=True)  # Field name made lowercase.
    ptcity = models.CharField(db_column='ptCity', max_length=100)  # Field name made lowercase.
    ptstate = models.CharField(db_column='ptState', max_length=2)  # Field name made lowercase.
    ptzip = models.CharField(db_column='ptZip', max_length=5)  # Field name made lowercase.
    ptdob = models.DateField(db_column='ptDob')  # Field name made lowercase.
    ptgender = models.CharField(db_column='ptGender', max_length=1)  # Field name made lowercase.
    mrn = models.CharField(max_length=15)
    ptssnum = models.BigIntegerField(db_column='ptSSNum')  # Field name made lowercase.
    pthomephone = models.BigIntegerField(db_column='ptHomePhone', blank=True, null=True)  # Field name made lowercase.
    ptcellphone = models.BigIntegerField(db_column='ptCellPhone', blank=True, null=True)  # Field name made lowercase.
    ptworkphone = models.BigIntegerField(db_column='ptWorkPhone', blank=True, null=True)  # Field name made lowercase.
    ptworkphoneext = models.CharField(db_column='ptWorkPhoneExt', max_length=7, blank=True, null=True)  # Field name made lowercase.
    ptemail = models.CharField(db_column='ptEmail', max_length=100, blank=True, null=True)  # Field name made lowercase.
    primaryinsuredfirstname = models.CharField(db_column='primaryInsuredFirstName', max_length=60, blank=True, null=True)  # Field name made lowercase.
    primaryinsuredlastname = models.CharField(db_column='primaryInsuredLastName', max_length=60, blank=True, null=True)  # Field name made lowercase.
    primaryinsuredaddress1 = models.CharField(db_column='primaryInsuredAddress1', max_length=100, blank=True, null=True)  # Field name made lowercase.
    primaryinsuredaddress2 = models.CharField(db_column='primaryInsuredAddress2', max_length=100, blank=True, null=True)  # Field name made lowercase.
    primaryinsuredcity = models.CharField(db_column='primaryInsuredCity', max_length=100, blank=True, null=True)  # Field name made lowercase.
    primaryinsuredstate = models.CharField(db_column='primaryInsuredState', max_length=2, blank=True, null=True)  # Field name made lowercase.
    primaryinsuredzip = models.CharField(db_column='primaryInsuredZip', max_length=10, blank=True, null=True)  # Field name made lowercase.
    primaryinsconame = models.CharField(db_column='primaryInsCoName', max_length=100, blank=True, null=True)  # Field name made lowercase.
    primaryinscopolicynum = models.CharField(db_column='primaryInsCoPolicyNum', max_length=60, blank=True, null=True)  # Field name made lowercase.
    primaryinsureddob = models.DateField(db_column='primaryInsuredDob')  # Field name made lowercase.
    primaryinsuredgender = models.CharField(db_column='primaryInsuredGender', max_length=1)  # Field name made lowercase.
    primaryinscogroup = models.CharField(db_column='primaryInsCoGroup', max_length=60, blank=True, null=True)  # Field name made lowercase.
    primaryinscogroupnum = models.CharField(db_column='primaryInsCoGroupNum', max_length=60, blank=True, null=True)  # Field name made lowercase.
    primaryinsuredrelation = models.CharField(db_column='primaryInsuredRelation', max_length=5, blank=True, null=True)  # Field name made lowercase.
    secondaryinsuredfirstname = models.CharField(db_column='secondaryInsuredFirstName', max_length=60, blank=True, null=True)  # Field name made lowercase.
    secondaryinsuredlastname = models.CharField(db_column='secondaryInsuredLastName', max_length=60, blank=True, null=True)  # Field name made lowercase.
    secondaryinsuredaddress1 = models.CharField(db_column='secondaryInsuredAddress1', max_length=100, blank=True, null=True)  # Field name made lowercase.
    secondaryinsuredaddress2 = models.CharField(db_column='secondaryInsuredAddress2', max_length=100, blank=True, null=True)  # Field name made lowercase.
    secondaryinsuredcity = models.CharField(db_column='secondaryInsuredCity', max_length=100, blank=True, null=True)  # Field name made lowercase.
    secondaryinsuredstate = models.CharField(db_column='secondaryInsuredState', max_length=2, blank=True, null=True)  # Field name made lowercase.
    secondaryinsuredzip = models.CharField(db_column='secondaryInsuredZip', max_length=10, blank=True, null=True)  # Field name made lowercase.
    secondaryinsconame = models.CharField(db_column='secondaryInsCoName', max_length=100, blank=True, null=True)  # Field name made lowercase.
    secondaryinscopolicynum = models.CharField(db_column='secondaryInsCoPolicyNum', max_length=60, blank=True, null=True)  # Field name made lowercase.
    secondaryinsureddob = models.DateField(db_column='secondaryInsuredDob')  # Field name made lowercase.
    secondaryinsuredgender = models.CharField(db_column='secondaryInsuredGender', max_length=1)  # Field name made lowercase.
    secondaryinscogroup = models.CharField(db_column='secondaryInsCoGroup', max_length=60, blank=True, null=True)  # Field name made lowercase.
    secondaryinscogroupnum = models.CharField(db_column='secondaryInsCoGroupNum', max_length=60, blank=True, null=True)  # Field name made lowercase.
    secondaryinsuredrelation = models.CharField(db_column='secondaryInsuredRelation', max_length=5, blank=True, null=True)  # Field name made lowercase.
    pcpid = models.BigIntegerField(db_column='pcpId', blank=True, null=True)  # Field name made lowercase.
    pttype = models.CharField(db_column='ptType', max_length=3, blank=True, null=True)  # Field name made lowercase.
    referral = models.CharField(max_length=20, blank=True, null=True)
    numreferrals = models.IntegerField(db_column='numReferrals', blank=True, null=True)  # Field name made lowercase.
    referralfromdate = models.DateField(db_column='referralFromDate', blank=True, null=True)  # Field name made lowercase.
    referraluntildate = models.DateField(db_column='referralUntilDate', blank=True, null=True)  # Field name made lowercase.
    priorauthuntildate = models.DateField(db_column='priorAuthUntilDate', blank=True, null=True)  # Field name made lowercase.
    maindoctor = models.CharField(db_column='mainDoctor', max_length=20, blank=True, null=True)  # Field name made lowercase.
    nonarcotics = models.IntegerField(db_column='noNarcotics', blank=True, null=True)  # Field name made lowercase.
    nosee = models.IntegerField(db_column='noSee', blank=True, null=True)  # Field name made lowercase.
    unassignedstrikes = models.IntegerField(db_column='unassignedStrikes', blank=True, null=True)  # Field name made lowercase.
    nofaxpcp = models.IntegerField(db_column='noFaxPcp', blank=True, null=True)  # Field name made lowercase.
    textappt = models.IntegerField(db_column='textAppt', blank=True, null=True)  # Field name made lowercase.
    drugtestauthorize = models.IntegerField(db_column='drugTestAuthorize', blank=True, null=True)  # Field name made lowercase.
    dischargedate = models.DateField(db_column='dischargeDate', blank=True, null=True)  # Field name made lowercase.
    outsidecounselor = models.CharField(db_column='outsideCounselor', max_length=60, blank=True, null=True)  # Field name made lowercase.
    defaultpharmacy = models.BigIntegerField(db_column='defaultPharmacy', blank=True, null=True)  # Field name made lowercase.
    ptrace = models.CharField(db_column='ptRace', max_length=15, blank=True, null=True)  # Field name made lowercase.
    ptrace2 = models.CharField(db_column='ptRace2', max_length=15)  # Field name made lowercase.
    ptrace3 = models.CharField(db_column='ptRace3', max_length=15)  # Field name made lowercase.
    ptrace4 = models.CharField(db_column='ptRace4', max_length=15)  # Field name made lowercase.
    emergencycontact = models.CharField(db_column='emergencyContact', max_length=100, blank=True, null=True)  # Field name made lowercase.
    emergencyphone = models.BigIntegerField(db_column='emergencyPhone', blank=True, null=True)  # Field name made lowercase.
    mainclinic = models.IntegerField(db_column='mainClinic', blank=True, null=True)  # Field name made lowercase.
    emergencyphoneext = models.CharField(db_column='emergencyPhoneExt', max_length=10, blank=True, null=True)  # Field name made lowercase.
    ptlicensenum = models.CharField(db_column='ptLicenseNum', max_length=30, blank=True, null=True)  # Field name made lowercase.
    psychpriorauth = models.CharField(db_column='psychPriorAuth', max_length=35, blank=True, null=True)  # Field name made lowercase.
    psychpriorauthuntildate = models.DateField(db_column='psychPriorAuthUntilDate', blank=True, null=True)  # Field name made lowercase.
    psychpriorauthremaining = models.IntegerField(db_column='psychPriorAuthRemaining', blank=True, null=True)  # Field name made lowercase.
    reqcopay = models.FloatField(db_column='reqCopay', blank=True, null=True)  # Field name made lowercase.
    compreference = models.IntegerField(db_column='comPreference', blank=True, null=True)  # Field name made lowercase.
    restricted = models.IntegerField()
    picturefile = models.CharField(db_column='pictureFile', max_length=50)  # Field name made lowercase.
    guarantorfirstname = models.CharField(db_column='guarantorFirstName', max_length=60)  # Field name made lowercase.
    guarantorlastname = models.CharField(db_column='guarantorLastName', max_length=60)  # Field name made lowercase.
    guarantordob = models.DateField(db_column='guarantorDob')  # Field name made lowercase.
    guarantorgender = models.CharField(db_column='guarantorGender', max_length=1)  # Field name made lowercase.
    guarantoraddress1 = models.CharField(db_column='guarantorAddress1', max_length=60)  # Field name made lowercase.
    guarantoraddress2 = models.CharField(db_column='guarantorAddress2', max_length=60)  # Field name made lowercase.
    guarantorcity = models.CharField(db_column='guarantorCity', max_length=60)  # Field name made lowercase.
    guarantorstate = models.CharField(db_column='guarantorState', max_length=2)  # Field name made lowercase.
    guarantorzip = models.CharField(db_column='guarantorZip', max_length=10)  # Field name made lowercase.
    guarantorphone = models.CharField(db_column='guarantorPhone', max_length=10)  # Field name made lowercase.
    guarantorrelation = models.CharField(db_column='guarantorRelation', max_length=2)  # Field name made lowercase.
    ptcountry = models.CharField(db_column='ptCountry', max_length=50)  # Field name made lowercase.
    ptlanguage = models.CharField(db_column='ptLanguage', max_length=10)  # Field name made lowercase.
    ptethnicity = models.CharField(db_column='ptEthnicity', max_length=15)  # Field name made lowercase.
    ptreferredfrom = models.CharField(db_column='ptReferredFrom', max_length=100)  # Field name made lowercase.
    bhinsuredfirstname = models.CharField(db_column='bhInsuredFirstName', max_length=60)  # Field name made lowercase.
    bhinsuredlastname = models.CharField(db_column='bhInsuredLastName', max_length=60)  # Field name made lowercase.
    bhinsuredaddress1 = models.CharField(db_column='bhInsuredAddress1', max_length=100)  # Field name made lowercase.
    bhinsuredaddress2 = models.CharField(db_column='bhInsuredAddress2', max_length=100)  # Field name made lowercase.
    bhinsuredcity = models.CharField(db_column='bhInsuredCity', max_length=100)  # Field name made lowercase.
    bhinsuredstate = models.CharField(db_column='bhInsuredState', max_length=2)  # Field name made lowercase.
    bhinsuredzip = models.CharField(db_column='bhInsuredZip', max_length=10)  # Field name made lowercase.
    bhinsureddob = models.DateField(db_column='bhInsuredDob')  # Field name made lowercase.
    bhinsuredgender = models.CharField(db_column='bhInsuredGender', max_length=1)  # Field name made lowercase.
    bhinsuredrelation = models.CharField(db_column='bhInsuredRelation', max_length=5, blank=True, null=True)  # Field name made lowercase.
    bhinsconame = models.CharField(db_column='bhInsCoName', max_length=100)  # Field name made lowercase.
    bhinscopolicynum = models.CharField(db_column='bhInsCoPolicyNum', max_length=60)  # Field name made lowercase.
    bhinscogroup = models.CharField(db_column='bhInsCoGroup', max_length=60)  # Field name made lowercase.
    bhinscogroupnum = models.CharField(db_column='bhInsCoGroupNum', max_length=60)  # Field name made lowercase.
    voiceapptreminders = models.IntegerField(db_column='voiceApptReminders')  # Field name made lowercase.
    treatmentlevel = models.IntegerField(db_column='treatmentLevel', blank=True, null=True)  # Field name made lowercase.
    maincounselor = models.CharField(db_column='mainCounselor', max_length=20, blank=True, null=True)  # Field name made lowercase.
    referralfor = models.CharField(db_column='referralFor', max_length=20, blank=True, null=True)  # Field name made lowercase.
    ptrelationshipstatus = models.IntegerField(db_column='ptRelationshipStatus', blank=True, null=True)  # Field name made lowercase.
    defaultpharmacytext = models.CharField(db_column='defaultPharmacyText', max_length=100, blank=True, null=True)  # Field name made lowercase.
    defaultpharmacytextphone = models.BigIntegerField(db_column='defaultPharmacyTextPhone', blank=True, null=True)  # Field name made lowercase.
    monthlypt = models.IntegerField(db_column='monthlyPt', blank=True, null=True)  # Field name made lowercase.
    rxhold = models.IntegerField(db_column='rxHold', blank=True, null=True)  # Field name made lowercase.
    patientstudy = models.IntegerField(db_column='patientStudy', blank=True, null=True)  # Field name made lowercase.
    ismap = models.IntegerField(db_column='isMap', blank=True, null=True)  # Field name made lowercase.
    optvisit = models.CharField(db_column='optVisit', max_length=50, blank=True, null=True)  # Field name made lowercase.
    optinitial = models.CharField(db_column='optInitial', max_length=50, blank=True, null=True)  # Field name made lowercase.
    optvbh = models.CharField(db_column='optVBH', max_length=50, blank=True, null=True)  # Field name made lowercase.
    priorauthnum = models.CharField(db_column='priorAuthNum', max_length=50, blank=True, null=True)  # Field name made lowercase.
    mothermaidenfirstname = models.CharField(db_column='motherMaidenFirstName', max_length=35)  # Field name made lowercase.
    mothermaidenlastname = models.CharField(db_column='motherMaidenLastName', max_length=35)  # Field name made lowercase.
    ptmiddlename = models.CharField(db_column='ptMiddleName', max_length=35)  # Field name made lowercase.
    relfirstname = models.CharField(db_column='relFirstName', max_length=35)  # Field name made lowercase.
    rellastname = models.CharField(db_column='relLastName', max_length=35)  # Field name made lowercase.
    relmiddlename = models.CharField(db_column='relMiddleName', max_length=35)  # Field name made lowercase.
    reladdress1 = models.CharField(db_column='relAddress1', max_length=80)  # Field name made lowercase.
    reladdress2 = models.CharField(db_column='relAddress2', max_length=80)  # Field name made lowercase.
    relcity = models.CharField(db_column='relCity', max_length=80)  # Field name made lowercase.
    relstate = models.CharField(db_column='relState', max_length=2)  # Field name made lowercase.
    relzip = models.CharField(db_column='relZip', max_length=10)  # Field name made lowercase.
    relcountry = models.CharField(db_column='relCountry', max_length=35)  # Field name made lowercase.
    relphone = models.CharField(db_column='relPhone', max_length=10)  # Field name made lowercase.
    relcode = models.CharField(db_column='relCode', max_length=10)  # Field name made lowercase.
    primaryinscoimportcode = models.CharField(db_column='primaryInsCoImportCode', max_length=35)  # Field name made lowercase.
    medhxconsent = models.IntegerField(db_column='medHxConsent')  # Field name made lowercase.
    donotbill = models.IntegerField(db_column='doNotBill')  # Field name made lowercase.
    eligibilitydow = models.IntegerField(db_column='eligibilityDoW', blank=True, null=True)  # Field name made lowercase.
    ptoldmrn = models.CharField(db_column='ptOldMrn', max_length=20, blank=True, null=True)  # Field name made lowercase.
    labsupervisor = models.CharField(db_column='labSupervisor', max_length=20, blank=True, null=True)  # Field name made lowercase.
    voiceappt = models.IntegerField(db_column='voiceAppt', blank=True, null=True)  # Field name made lowercase.
    emergencyrelation = models.CharField(db_column='emergencyRelation', max_length=15, blank=True, null=True)  # Field name made lowercase.
    numudspriorauths = models.SmallIntegerField(db_column='numUDSPriorAuths', blank=True, null=True)  # Field name made lowercase.
    pbmgroup = models.CharField(db_column='pbmGroup', max_length=35, blank=True, null=True)  # Field name made lowercase.
    pbmbin = models.CharField(db_column='pbmBin', max_length=35, blank=True, null=True)  # Field name made lowercase.
    pbmpcn = models.CharField(db_column='pbmPcn', max_length=35, blank=True, null=True)  # Field name made lowercase.
    priorauthnumreason = models.CharField(db_column='priorAuthNumReason', max_length=200, blank=True, null=True)  # Field name made lowercase.
    priorauthnum2 = models.CharField(db_column='priorAuthNum2', max_length=35, blank=True, null=True)  # Field name made lowercase.
    priorauthnum2reason = models.CharField(db_column='priorAuthNum2Reason', max_length=200, blank=True, null=True)  # Field name made lowercase.
    priorauthnum3 = models.CharField(db_column='priorAuthNum3', max_length=35, blank=True, null=True)  # Field name made lowercase.
    priorauthnum3reason = models.CharField(db_column='priorAuthNum3Reason', max_length=200, blank=True, null=True)  # Field name made lowercase.
    priorauthnum4 = models.CharField(db_column='priorAuthNum4', max_length=35, blank=True, null=True)  # Field name made lowercase.
    priorauthnum4reason = models.CharField(db_column='priorAuthNum4Reason', max_length=200, blank=True, null=True)  # Field name made lowercase.
    ptsecondaryphone = models.BigIntegerField(db_column='ptSecondaryPhone', blank=True, null=True)  # Field name made lowercase.
    ptpassword = models.CharField(db_column='ptPassword', max_length=20, blank=True, null=True)  # Field name made lowercase.
    messageconsent = models.IntegerField(db_column='messageConsent')  # Field name made lowercase.
    dischargereason = models.IntegerField(db_column='dischargeReason', blank=True, null=True)  # Field name made lowercase.
    utoxalertcount = models.IntegerField(db_column='utoxAlertCount')  # Field name made lowercase.
    referralpcp = models.BigIntegerField(db_column='referralPcp', blank=True, null=True)  # Field name made lowercase.
    udspastartdate = models.DateField(db_column='udsPaStartDate', blank=True, null=True)  # Field name made lowercase.
    udspaenddate = models.DateField(db_column='udsPaEndDate', blank=True, null=True)  # Field name made lowercase.
    tertiaryinsuredfirstname = models.CharField(db_column='tertiaryInsuredFirstName', max_length=60)  # Field name made lowercase.
    tertiaryinsuredlastname = models.CharField(db_column='tertiaryInsuredLastName', max_length=60)  # Field name made lowercase.
    tertiaryinsuredaddress1 = models.CharField(db_column='tertiaryInsuredAddress1', max_length=100)  # Field name made lowercase.
    tertiaryinsuredaddress2 = models.CharField(db_column='tertiaryInsuredAddress2', max_length=100)  # Field name made lowercase.
    tertiaryinsuredcity = models.CharField(db_column='tertiaryInsuredCity', max_length=100)  # Field name made lowercase.
    tertiaryinsuredstate = models.CharField(db_column='tertiaryInsuredState', max_length=2)  # Field name made lowercase.
    tertiaryinsuredzip = models.CharField(db_column='tertiaryInsuredZip', max_length=10)  # Field name made lowercase.
    tertiaryinsureddob = models.DateField(db_column='tertiaryInsuredDob')  # Field name made lowercase.
    tertiaryinsuredgender = models.CharField(db_column='tertiaryInsuredGender', max_length=1)  # Field name made lowercase.
    tertiaryinsuredrelation = models.CharField(db_column='tertiaryInsuredRelation', max_length=5, blank=True, null=True)  # Field name made lowercase.
    tertiaryinsconame = models.CharField(db_column='tertiaryInsCoName', max_length=100)  # Field name made lowercase.
    tertiaryinscopolicynum = models.CharField(db_column='tertiaryInsCoPolicyNum', max_length=60)  # Field name made lowercase.
    tertiaryinscogroup = models.CharField(db_column='tertiaryInsCoGroup', max_length=60)  # Field name made lowercase.
    tertiaryinscogroupnum = models.CharField(db_column='tertiaryInsCoGroupNum', max_length=60)  # Field name made lowercase.
    primaryinscostartdate = models.DateField(db_column='primaryInsCoStartDate', blank=True, null=True)  # Field name made lowercase.
    secondaryinscostartdate = models.DateField(db_column='secondaryInsCoStartDate', blank=True, null=True)  # Field name made lowercase.
    bhinscostartdate = models.DateField(db_column='bhInsCoStartDate', blank=True, null=True)  # Field name made lowercase.
    tertiaryinscostartdate = models.DateField(db_column='tertiaryInsCoStartDate', blank=True, null=True)  # Field name made lowercase.
    nonbillableinsuredfirstname = models.CharField(db_column='nonbillableInsuredFirstName', max_length=60)  # Field name made lowercase.
    nonbillableinsuredlastname = models.CharField(db_column='nonbillableInsuredLastName', max_length=60)  # Field name made lowercase.
    nonbillableinsuredaddress1 = models.CharField(db_column='nonbillableInsuredAddress1', max_length=100)  # Field name made lowercase.
    nonbillableinsuredaddress2 = models.CharField(db_column='nonbillableInsuredAddress2', max_length=100)  # Field name made lowercase.
    nonbillableinsuredcity = models.CharField(db_column='nonbillableInsuredCity', max_length=100)  # Field name made lowercase.
    nonbillableinsuredstate = models.CharField(db_column='nonbillableInsuredState', max_length=2)  # Field name made lowercase.
    nonbillableinsuredzip = models.CharField(db_column='nonbillableInsuredZip', max_length=10)  # Field name made lowercase.
    nonbillableinsureddob = models.DateField(db_column='nonbillableInsuredDob')  # Field name made lowercase.
    nonbillableinsuredgender = models.CharField(db_column='nonbillableInsuredGender', max_length=1)  # Field name made lowercase.
    nonbillableinsuredrelation = models.CharField(db_column='nonbillableInsuredRelation', max_length=5, blank=True, null=True)  # Field name made lowercase.
    nonbillableinsconame = models.CharField(db_column='nonbillableInsCoName', max_length=100)  # Field name made lowercase.
    nonbillableinscopolicynum = models.CharField(db_column='nonbillableInsCoPolicyNum', max_length=60)  # Field name made lowercase.
    nonbillableinscogroup = models.CharField(db_column='nonbillableInsCoGroup', max_length=60)  # Field name made lowercase.
    nonbillableinscogroupnum = models.CharField(db_column='nonbillableInsCoGroupNum', max_length=60)  # Field name made lowercase.
    nonbillableinscostart = models.DateField(db_column='nonbillableInsCoStart', blank=True, null=True)  # Field name made lowercase.
    nonbillableinscostartdate = models.DateField(db_column='nonbillableInsCoStartDate', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'demDataMerged'


