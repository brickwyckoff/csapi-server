from __future__ import unicode_literals

from django.db import models

class Ptfaxlog(models.Model):
    id = models.BigAutoField(primary_key=True)
    mrn = models.BigIntegerField()
    faxto = models.CharField(db_column='faxTo', max_length=50)  # Field name made lowercase.
    faxtonumber = models.CharField(db_column='faxToNumber', max_length=10)  # Field name made lowercase.
    faxtype = models.CharField(db_column='faxType', max_length=50)  # Field name made lowercase.
    faxdate = models.DateTimeField(db_column='faxDate')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ptFaxLog'


