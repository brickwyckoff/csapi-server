from __future__ import unicode_literals

from django.db import models

class Mspdata(models.Model):
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    appearancewnl = models.IntegerField(db_column='appearanceWNL', blank=True, null=True)  # Field name made lowercase.
    appearanceneat = models.IntegerField(db_column='appearanceNeat', blank=True, null=True)  # Field name made lowercase.
    appearanceunkempt = models.IntegerField(db_column='appearanceUnkempt', blank=True, null=True)  # Field name made lowercase.
    clothingwnl = models.IntegerField(db_column='clothingWNL', blank=True, null=True)  # Field name made lowercase.
    clothingdisheveled = models.IntegerField(db_column='clothingDisheveled', blank=True, null=True)  # Field name made lowercase.
    clothingout = models.IntegerField(db_column='clothingOut', blank=True, null=True)  # Field name made lowercase.
    eyewnl = models.IntegerField(db_column='eyeWNL', blank=True, null=True)  # Field name made lowercase.
    eyeavoidant = models.IntegerField(db_column='eyeAvoidant', blank=True, null=True)  # Field name made lowercase.
    eyeintense = models.IntegerField(db_column='eyeIntense', blank=True, null=True)  # Field name made lowercase.
    eyeintermittent = models.IntegerField(db_column='eyeIntermittent', blank=True, null=True)  # Field name made lowercase.
    buildwnl = models.IntegerField(db_column='buildWNL', blank=True, null=True)  # Field name made lowercase.
    buildthin = models.IntegerField(db_column='buildThin', blank=True, null=True)  # Field name made lowercase.
    buildoverweight = models.IntegerField(db_column='buildOverweight', blank=True, null=True)  # Field name made lowercase.
    buildshort = models.IntegerField(db_column='buildShort', blank=True, null=True)  # Field name made lowercase.
    buildtall = models.IntegerField(db_column='buildTall', blank=True, null=True)  # Field name made lowercase.
    posturewnl = models.IntegerField(db_column='postureWNL', blank=True, null=True)  # Field name made lowercase.
    postureslumped = models.IntegerField(db_column='postureSlumped', blank=True, null=True)  # Field name made lowercase.
    posturerigid = models.IntegerField(db_column='postureRigid', blank=True, null=True)  # Field name made lowercase.
    postureatypical = models.IntegerField(db_column='postureAtypical', blank=True, null=True)  # Field name made lowercase.
    bodywnl = models.IntegerField(db_column='bodyWNL', blank=True, null=True)  # Field name made lowercase.
    bodyaccelerated = models.IntegerField(db_column='bodyAccelerated', blank=True, null=True)  # Field name made lowercase.
    bodyslowed = models.IntegerField(db_column='bodySlowed', blank=True, null=True)  # Field name made lowercase.
    bodypeculiar = models.IntegerField(db_column='bodyPeculiar', blank=True, null=True)  # Field name made lowercase.
    bodyrestless = models.IntegerField(db_column='bodyRestless', blank=True, null=True)  # Field name made lowercase.
    bodyagitated = models.IntegerField(db_column='bodyAgitated', blank=True, null=True)  # Field name made lowercase.
    behaviorrelaxed = models.IntegerField(db_column='behaviorRelaxed', blank=True, null=True)  # Field name made lowercase.
    behaviorcooperative = models.IntegerField(db_column='behaviorCooperative', blank=True, null=True)  # Field name made lowercase.
    behavioruncooperative = models.IntegerField(db_column='behaviorUncooperative', blank=True, null=True)  # Field name made lowercase.
    behavioroverlycompliant = models.IntegerField(db_column='behaviorOverlyCompliant', blank=True, null=True)  # Field name made lowercase.
    behaviorwithdrawn = models.IntegerField(db_column='behaviorWithdrawn', blank=True, null=True)  # Field name made lowercase.
    behaviorsleepy = models.IntegerField(db_column='behaviorSleepy', blank=True, null=True)  # Field name made lowercase.
    behaviornervous = models.IntegerField(db_column='behaviorNervous', blank=True, null=True)  # Field name made lowercase.
    behaviorrestless = models.IntegerField(db_column='behaviorRestless', blank=True, null=True)  # Field name made lowercase.
    behaviorsilly = models.IntegerField(db_column='behaviorSilly', blank=True, null=True)  # Field name made lowercase.
    behavioravoidant = models.IntegerField(db_column='behaviorAvoidant', blank=True, null=True)  # Field name made lowercase.
    behaviorpreoccupied = models.IntegerField(db_column='behaviorPreoccupied', blank=True, null=True)  # Field name made lowercase.
    behaviordemanding = models.IntegerField(db_column='behaviorDemanding', blank=True, null=True)  # Field name made lowercase.
    behaviorcontrolling = models.IntegerField(db_column='behaviorControlling', blank=True, null=True)  # Field name made lowercase.
    behaviornopleasure = models.IntegerField(db_column='behaviorNoPleasure', blank=True, null=True)  # Field name made lowercase.
    behaviorprovocative = models.IntegerField(db_column='behaviorProvocative', blank=True, null=True)  # Field name made lowercase.
    behaviorhyperactive = models.IntegerField(db_column='behaviorHyperactive', blank=True, null=True)  # Field name made lowercase.
    behaviorimpulsive = models.IntegerField(db_column='behaviorImpulsive', blank=True, null=True)  # Field name made lowercase.
    behavioragitated = models.IntegerField(db_column='behaviorAgitated', blank=True, null=True)  # Field name made lowercase.
    behaviorangry = models.IntegerField(db_column='behaviorAngry', blank=True, null=True)  # Field name made lowercase.
    behaviorassaultive = models.IntegerField(db_column='behaviorAssaultive')  # Field name made lowercase.
    behavioraggressive = models.IntegerField(db_column='behaviorAggressive', blank=True, null=True)  # Field name made lowercase.
    behaviorcompulsive = models.IntegerField(db_column='behaviorCompulsive', blank=True, null=True)  # Field name made lowercase.
    speechwnl = models.IntegerField(db_column='speechWNL', blank=True, null=True)  # Field name made lowercase.
    speechmute = models.IntegerField(db_column='speechMute', blank=True, null=True)  # Field name made lowercase.
    speechovertalkative = models.IntegerField(db_column='speechOverTalkative', blank=True, null=True)  # Field name made lowercase.
    speechslowed = models.IntegerField(db_column='speechSlowed', blank=True, null=True)  # Field name made lowercase.
    speechslurred = models.IntegerField(db_column='speechSlurred', blank=True, null=True)  # Field name made lowercase.
    speechstammer = models.IntegerField(db_column='speechStammer', blank=True, null=True)  # Field name made lowercase.
    speechrapid = models.IntegerField(db_column='speechRapid', blank=True, null=True)  # Field name made lowercase.
    speechpressured = models.IntegerField(db_column='speechPressured', blank=True, null=True)  # Field name made lowercase.
    speechloud = models.IntegerField(db_column='speechLoud', blank=True, null=True)  # Field name made lowercase.
    speechsoft = models.IntegerField(db_column='speechSoft', blank=True, null=True)  # Field name made lowercase.
    speechclear = models.IntegerField(db_column='speechClear', blank=True, null=True)  # Field name made lowercase.
    speechrepetitive = models.IntegerField(db_column='speechRepetitive', blank=True, null=True)  # Field name made lowercase.
    moodwnl = models.IntegerField(db_column='moodWNL', blank=True, null=True)  # Field name made lowercase.
    moodlack = models.IntegerField(db_column='moodLack', blank=True, null=True)  # Field name made lowercase.
    moodblunted = models.IntegerField(db_column='moodBlunted', blank=True, null=True)  # Field name made lowercase.
    moodelated = models.IntegerField(db_column='moodElated', blank=True, null=True)  # Field name made lowercase.
    moodtranquil = models.IntegerField(db_column='moodTranquil', blank=True, null=True)  # Field name made lowercase.
    moodanger = models.IntegerField(db_column='moodAnger', blank=True, null=True)  # Field name made lowercase.
    moodhostility = models.IntegerField(db_column='moodHostility', blank=True, null=True)  # Field name made lowercase.
    moodirritable = models.IntegerField(db_column='moodIrritable', blank=True, null=True)  # Field name made lowercase.
    moodfear = models.IntegerField(db_column='moodFear', blank=True, null=True)  # Field name made lowercase.
    mooddepressed = models.IntegerField(db_column='moodDepressed', blank=True, null=True)  # Field name made lowercase.
    moodanxious = models.IntegerField(db_column='moodAnxious', blank=True, null=True)  # Field name made lowercase.
    affectwnl = models.IntegerField(db_column='affectWNL', blank=True, null=True)  # Field name made lowercase.
    affectconstricted = models.IntegerField(db_column='affectConstricted', blank=True, null=True)  # Field name made lowercase.
    affectflat = models.IntegerField(db_column='affectFlat', blank=True, null=True)  # Field name made lowercase.
    affectinappropriate = models.IntegerField(db_column='affectInappropriate', blank=True, null=True)  # Field name made lowercase.
    affectchangeable = models.IntegerField(db_column='affectChangeable', blank=True, null=True)  # Field name made lowercase.
    affectfull = models.IntegerField(db_column='affectFull', blank=True, null=True)  # Field name made lowercase.
    affectpanic = models.IntegerField(db_column='affectPanic', blank=True, null=True)  # Field name made lowercase.
    affectsleep = models.IntegerField(db_column='affectSleep', blank=True, null=True)  # Field name made lowercase.
    affectappetite = models.IntegerField(db_column='affectAppetite', blank=True, null=True)  # Field name made lowercase.
    facialwnl = models.IntegerField(db_column='facialWNL', blank=True, null=True)  # Field name made lowercase.
    facialanxiety = models.IntegerField(db_column='facialAnxiety', blank=True, null=True)  # Field name made lowercase.
    facialsadness = models.IntegerField(db_column='facialSadness', blank=True, null=True)  # Field name made lowercase.
    facialanger = models.IntegerField(db_column='facialAnger', blank=True, null=True)  # Field name made lowercase.
    facialexpressionless = models.IntegerField(db_column='facialExpressionless', blank=True, null=True)  # Field name made lowercase.
    facialunvarying = models.IntegerField(db_column='facialUnvarying', blank=True, null=True)  # Field name made lowercase.
    facialinappropriate = models.IntegerField(db_column='facialInappropriate', blank=True, null=True)  # Field name made lowercase.
    facialelated = models.IntegerField(db_column='facialElated', blank=True, null=True)  # Field name made lowercase.
    perceptionwnl = models.IntegerField(db_column='perceptionWNL', blank=True, null=True)  # Field name made lowercase.
    perceptionillusions = models.IntegerField(db_column='perceptionIllusions', blank=True, null=True)  # Field name made lowercase.
    perceptiondepersonalization = models.IntegerField(db_column='perceptionDepersonalization', blank=True, null=True)  # Field name made lowercase.
    perceptionderealization = models.IntegerField(db_column='perceptionDerealization', blank=True, null=True)  # Field name made lowercase.
    perceptionreexperiencing = models.IntegerField(db_column='perceptionReexperiencing', blank=True, null=True)  # Field name made lowercase.
    hallucinationsauditory = models.IntegerField(db_column='hallucinationsAuditory', blank=True, null=True)  # Field name made lowercase.
    hallucinationsvisual = models.IntegerField(db_column='hallucinationsVisual', blank=True, null=True)  # Field name made lowercase.
    hallucinationsolfactory = models.IntegerField(db_column='hallucinationsOlfactory', blank=True, null=True)  # Field name made lowercase.
    hallucinationsgustatory = models.IntegerField(db_column='hallucinationsGustatory', blank=True, null=True)  # Field name made lowercase.
    hallucinationstactile = models.IntegerField(db_column='hallucinationsTactile', blank=True, null=True)  # Field name made lowercase.
    hallucinationscommand = models.IntegerField(db_column='hallucinationsCommand', blank=True, null=True)  # Field name made lowercase.
    thoughtwnl = models.IntegerField(db_column='thoughtWNL', blank=True, null=True)  # Field name made lowercase.
    delusionsnone = models.IntegerField(db_column='delusionsNone', blank=True, null=True)  # Field name made lowercase.
    delusionsgrandiose = models.IntegerField(db_column='delusionsGrandiose', blank=True, null=True)  # Field name made lowercase.
    delusionspersecutory = models.IntegerField(db_column='delusionsPersecutory', blank=True, null=True)  # Field name made lowercase.
    delusionssomatic = models.IntegerField(db_column='delusionsSomatic', blank=True, null=True)  # Field name made lowercase.
    delusionsillogical = models.IntegerField(db_column='delusionsIllogical', blank=True, null=True)  # Field name made lowercase.
    delusionschaotic = models.IntegerField(db_column='delusionsChaotic', blank=True, null=True)  # Field name made lowercase.
    delusionsreligious = models.IntegerField(db_column='delusionsReligious', blank=True, null=True)  # Field name made lowercase.
    otherthoughtpreoccupied = models.IntegerField(db_column='otherThoughtPreoccupied', blank=True, null=True)  # Field name made lowercase.
    otherthoughtobsessional = models.IntegerField(db_column='otherThoughtObsessional', blank=True, null=True)  # Field name made lowercase.
    otherthoughtguarded = models.IntegerField(db_column='otherThoughtGuarded', blank=True, null=True)  # Field name made lowercase.
    otherthoughtphobic = models.IntegerField(db_column='otherThoughtPhobic', blank=True, null=True)  # Field name made lowercase.
    otherthoughtsuspicious = models.IntegerField(db_column='otherThoughtSuspicious', blank=True, null=True)  # Field name made lowercase.
    otherthoughtguilty = models.IntegerField(db_column='otherThoughtGuilty', blank=True, null=True)  # Field name made lowercase.
    otherthoughtbroadcasting = models.IntegerField(db_column='otherThoughtBroadcasting', blank=True, null=True)  # Field name made lowercase.
    otherthoughtinsertion = models.IntegerField(db_column='otherThoughtInsertion', blank=True, null=True)  # Field name made lowercase.
    otherthoughtreference = models.IntegerField(db_column='otherThoughtReference', blank=True, null=True)  # Field name made lowercase.
    selfabusenone = models.IntegerField(db_column='selfAbuseNone', blank=True, null=True)  # Field name made lowercase.
    selfabusecutting = models.IntegerField(db_column='selfAbuseCutting', blank=True, null=True)  # Field name made lowercase.
    selfabuseburning = models.IntegerField(db_column='selfAbuseBurning', blank=True, null=True)  # Field name made lowercase.
    selfabusemutilation = models.IntegerField(db_column='selfAbuseMutilation', blank=True, null=True)  # Field name made lowercase.
    suicidalnone = models.IntegerField(db_column='suicidalNone', blank=True, null=True)  # Field name made lowercase.
    suicidalpassive = models.IntegerField(db_column='suicidalPassive', blank=True, null=True)  # Field name made lowercase.
    suicidalintent = models.IntegerField(db_column='suicidalIntent', blank=True, null=True)  # Field name made lowercase.
    suicidalplan = models.IntegerField(db_column='suicidalPlan', blank=True, null=True)  # Field name made lowercase.
    suicidalmeans = models.IntegerField(db_column='suicidalMeans', blank=True, null=True)  # Field name made lowercase.
    aggressivenone = models.IntegerField(db_column='aggressiveNone', blank=True, null=True)  # Field name made lowercase.
    aggressiveintent = models.IntegerField(db_column='aggressiveIntent', blank=True, null=True)  # Field name made lowercase.
    aggressiveplan = models.IntegerField(db_column='aggressivePlan', blank=True, null=True)  # Field name made lowercase.
    aggressivemeans = models.IntegerField(db_column='aggressiveMeans', blank=True, null=True)  # Field name made lowercase.
    processwnl = models.IntegerField(db_column='processWNL', blank=True, null=True)  # Field name made lowercase.
    processincoherent = models.IntegerField(db_column='processIncoherent', blank=True, null=True)  # Field name made lowercase.
    processcircumstantial = models.IntegerField(db_column='processCircumstantial', blank=True, null=True)  # Field name made lowercase.
    processdecreased = models.IntegerField(db_column='processDecreased', blank=True, null=True)  # Field name made lowercase.
    processblocked = models.IntegerField(db_column='processBlocked', blank=True, null=True)  # Field name made lowercase.
    processflight = models.IntegerField(db_column='processFlight', blank=True, null=True)  # Field name made lowercase.
    processloose = models.IntegerField(db_column='processLoose', blank=True, null=True)  # Field name made lowercase.
    processracing = models.IntegerField(db_column='processRacing', blank=True, null=True)  # Field name made lowercase.
    processincreased = models.IntegerField(db_column='processIncreased', blank=True, null=True)  # Field name made lowercase.
    processconcrete = models.IntegerField(db_column='processConcrete', blank=True, null=True)  # Field name made lowercase.
    processtangential = models.IntegerField(db_column='processTangential', blank=True, null=True)  # Field name made lowercase.
    intellectualwnl = models.IntegerField(db_column='intellectualWNL', blank=True, null=True)  # Field name made lowercase.
    intellectuallessened = models.IntegerField(db_column='intellectualLessened', blank=True, null=True)  # Field name made lowercase.
    intellectualshort = models.IntegerField(db_column='intellectualShort', blank=True, null=True)  # Field name made lowercase.
    intellectualconcentration = models.IntegerField(db_column='intellectualConcentration', blank=True, null=True)  # Field name made lowercase.
    intellectualcalculation = models.IntegerField(db_column='intellectualCalculation', blank=True, null=True)  # Field name made lowercase.
    intelligencemr = models.IntegerField(db_column='intelligenceMR', blank=True, null=True)  # Field name made lowercase.
    intelligenceborderline = models.IntegerField(db_column='intelligenceBorderline', blank=True, null=True)  # Field name made lowercase.
    intelligenceaverage = models.IntegerField(db_column='intelligenceAverage', blank=True, null=True)  # Field name made lowercase.
    intelligenceabove = models.IntegerField(db_column='intelligenceAbove', blank=True, null=True)  # Field name made lowercase.
    intelligencenotesting = models.IntegerField(db_column='intelligenceNoTesting', blank=True, null=True)  # Field name made lowercase.
    orientationwnl = models.IntegerField(db_column='orientationWNL', blank=True, null=True)  # Field name made lowercase.
    orientationperson = models.IntegerField(db_column='orientationPerson', blank=True, null=True)  # Field name made lowercase.
    orientationtime = models.IntegerField(db_column='orientationTime', blank=True, null=True)  # Field name made lowercase.
    orientationplace = models.IntegerField(db_column='orientationPlace', blank=True, null=True)  # Field name made lowercase.
    memorywnl = models.IntegerField(db_column='memoryWNL', blank=True, null=True)  # Field name made lowercase.
    memoryrecall = models.IntegerField(db_column='memoryRecall', blank=True, null=True)  # Field name made lowercase.
    memoryrecent = models.IntegerField(db_column='memoryRecent', blank=True, null=True)  # Field name made lowercase.
    memoryremote = models.IntegerField(db_column='memoryRemote', blank=True, null=True)  # Field name made lowercase.
    insightwnl = models.IntegerField(db_column='insightWNL', blank=True, null=True)  # Field name made lowercase.
    insightdifficulty = models.IntegerField(db_column='insightDifficulty', blank=True, null=True)  # Field name made lowercase.
    insightblames = models.IntegerField(db_column='insightBlames', blank=True, null=True)  # Field name made lowercase.
    insightnoproblem = models.IntegerField(db_column='insightNoProblem', blank=True, null=True)  # Field name made lowercase.
    judgementwnl = models.IntegerField(db_column='judgementWNL', blank=True, null=True)  # Field name made lowercase.
    judgementsome = models.IntegerField(db_column='judgementSome', blank=True, null=True)  # Field name made lowercase.
    judgementsevere = models.IntegerField(db_column='judgementSevere', blank=True, null=True)  # Field name made lowercase.
    harmnone = models.IntegerField(db_column='harmNone', blank=True, null=True)  # Field name made lowercase.
    harmself = models.IntegerField(db_column='harmSelf', blank=True, null=True)  # Field name made lowercase.
    harmothers = models.IntegerField(db_column='harmOthers', blank=True, null=True)  # Field name made lowercase.
    comment = models.CharField(max_length=700, blank=True, null=True)
    cptcode = models.CharField(db_column='cptCode', max_length=10, blank=True, null=True)  # Field name made lowercase.
    cptqty = models.CharField(db_column='cptQty', max_length=5, blank=True, null=True)  # Field name made lowercase.
    axisi = models.CharField(db_column='axisI', max_length=200, blank=True, null=True)  # Field name made lowercase.
    axisii = models.CharField(db_column='axisII', max_length=200, blank=True, null=True)  # Field name made lowercase.
    axisiii = models.CharField(db_column='axisIII', max_length=200, blank=True, null=True)  # Field name made lowercase.
    axisiv = models.CharField(db_column='axisIV', max_length=200, blank=True, null=True)  # Field name made lowercase.
    axisvgaf = models.CharField(db_column='axisVGaf', max_length=200, blank=True, null=True)  # Field name made lowercase.
    axisvgafhighest = models.CharField(db_column='axisVGafHighest', max_length=200, blank=True, null=True)  # Field name made lowercase.
    axisidx2 = models.CharField(db_column='axisIDx2', max_length=20, blank=True, null=True)  # Field name made lowercase.
    cptmod1 = models.CharField(db_column='cptMod1', max_length=3)  # Field name made lowercase.
    cptmod2 = models.CharField(db_column='cptMod2', max_length=3)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'mspData'


