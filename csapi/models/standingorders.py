from __future__ import unicode_literals

from django.db import models

class Standingorders(models.Model):
    id = models.BigAutoField(primary_key=True)
    mrn = models.BigIntegerField()
    labcode = models.CharField(db_column='labCode', max_length=10)  # Field name made lowercase.
    loccode = models.CharField(db_column='locCode', max_length=10)  # Field name made lowercase.
    provider = models.CharField(max_length=20, blank=True, null=True)
    signtime = models.DateTimeField(db_column='signTime')  # Field name made lowercase.
    exptime = models.DateTimeField(db_column='expTime')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'standingOrders'


