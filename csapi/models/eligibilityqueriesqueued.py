from __future__ import unicode_literals

from django.db import models

class Eligibilityqueriesqueued(models.Model):
    id = models.BigAutoField(primary_key=True)
    mrn = models.BigIntegerField()
    queuetime = models.DateTimeField(db_column='queueTime')  # Field name made lowercase.
    queueby = models.CharField(db_column='queueBy', max_length=35)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'eligibilityQueriesQueued'


