from __future__ import unicode_literals

from django.db import models

class Rxhistorydenial(models.Model):
    mrn = models.BigIntegerField(blank=True, null=True)
    denialcode = models.CharField(db_column='denialCode', max_length=10, blank=True, null=True)  # Field name made lowercase.
    denialtext = models.CharField(db_column='denialText', max_length=100, blank=True, null=True)  # Field name made lowercase.
    creationtime = models.DateTimeField(db_column='creationTime', blank=True, null=True)  # Field name made lowercase.
    payorname = models.CharField(db_column='payorName', max_length=100, blank=True, null=True)  # Field name made lowercase.
    memberid = models.CharField(db_column='memberId', max_length=100, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'rxHistoryDenial'


