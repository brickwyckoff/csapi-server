from __future__ import unicode_literals

from django.db import models

class Releaselog(models.Model):
    mrn = models.BigIntegerField(blank=True, null=True)
    releasedto = models.CharField(db_column='releasedTo', max_length=100, blank=True, null=True)  # Field name made lowercase.
    releasedinfo = models.CharField(db_column='releasedInfo', max_length=300, blank=True, null=True)  # Field name made lowercase.
    releaseddate = models.DateTimeField(db_column='releasedDate', blank=True, null=True)  # Field name made lowercase.
    user = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'releaseLog'


