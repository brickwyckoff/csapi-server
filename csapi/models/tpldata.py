from __future__ import unicode_literals

from django.db import models

class Tpldata(models.Model):
    id = models.BigAutoField(primary_key=True)
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    itemid = models.BigIntegerField(db_column='itemId', blank=True, null=True)  # Field name made lowercase.
    optionid = models.CharField(db_column='optionId', max_length=200, blank=True, null=True)  # Field name made lowercase.
    creationtime = models.DateTimeField(db_column='creationTime', blank=True, null=True)  # Field name made lowercase.
    user = models.CharField(max_length=60, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tplData'


