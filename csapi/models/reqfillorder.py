from __future__ import unicode_literals

from django.db import models

class Reqfillorder(models.Model):
    orderid = models.IntegerField(db_column='orderId', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'reqFillOrder'


