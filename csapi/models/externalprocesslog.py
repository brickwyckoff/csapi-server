from __future__ import unicode_literals

from django.db import models

class Externalprocesslog(models.Model):
    id = models.BigAutoField(primary_key=True)
    processname = models.CharField(db_column='processName', max_length=50)  # Field name made lowercase.
    visitdate = models.DateField(db_column='visitDate')  # Field name made lowercase.
    processtime = models.DateTimeField(db_column='processTime')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'externalProcessLog'


