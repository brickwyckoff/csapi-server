from __future__ import unicode_literals

from django.db import models

class Deniedclaimsdeleted(models.Model):
    payerid = models.CharField(db_column='payerId', max_length=15, blank=True, null=True)  # Field name made lowercase.
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    company = models.CharField(max_length=50, blank=True, null=True)
    returndate = models.DateField(db_column='returnDate', blank=True, null=True)  # Field name made lowercase.
    user = models.CharField(max_length=30, blank=True, null=True)
    creationtime = models.DateTimeField(db_column='creationTime', blank=True, null=True)  # Field name made lowercase.
    denyroute = models.IntegerField(db_column='denyRoute', blank=True, null=True)  # Field name made lowercase.
    denyreason = models.CharField(db_column='denyReason', max_length=100)  # Field name made lowercase.
    proccode = models.CharField(db_column='procCode', max_length=10, blank=True, null=True)  # Field name made lowercase.
    deletedby = models.CharField(db_column='deletedBy', max_length=35)  # Field name made lowercase.
    deletetime = models.DateTimeField(db_column='deleteTime')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'deniedClaimsDeleted'


