from __future__ import unicode_literals

from django.db import models

class Mainscreenmessages(models.Model):
    user = models.CharField(max_length=60, blank=True, null=True)
    enteredby = models.CharField(db_column='enteredBy', max_length=60, blank=True, null=True)  # Field name made lowercase.
    creationtime = models.DateTimeField(db_column='creationTime', blank=True, null=True)  # Field name made lowercase.
    dismissedtime = models.DateTimeField(db_column='dismissedTime', blank=True, null=True)  # Field name made lowercase.
    isviewed = models.IntegerField(db_column='isViewed', blank=True, null=True)  # Field name made lowercase.
    message = models.CharField(max_length=500, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'mainScreenMessages'


