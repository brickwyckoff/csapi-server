from __future__ import unicode_literals

from django.db import models

class Recdatamerged(models.Model):
    mrn = models.BigIntegerField(blank=True, null=True)
    hcg = models.IntegerField(db_column='hCG', blank=True, null=True)  # Field name made lowercase.
    lfts = models.IntegerField(db_column='LFTs', blank=True, null=True)  # Field name made lowercase.
    randomutoxfreq = models.IntegerField(db_column='randomUtoxFreq', blank=True, null=True)  # Field name made lowercase.
    hiv = models.IntegerField(db_column='HIV', blank=True, null=True)  # Field name made lowercase.
    hcv = models.IntegerField(db_column='HCV', blank=True, null=True)  # Field name made lowercase.
    hbv = models.IntegerField(db_column='HBV')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'recDataMerged'


