from __future__ import unicode_literals

from django.db import models

class Customforms(models.Model):
    formname = models.CharField(db_column='formName', max_length=30, blank=True, null=True)  # Field name made lowercase.
    formsection = models.CharField(db_column='formSection', max_length=30, blank=True, null=True)  # Field name made lowercase.
    itemtype = models.CharField(db_column='itemType', max_length=15, blank=True, null=True)  # Field name made lowercase.
    display = models.CharField(max_length=400, blank=True, null=True)
    field = models.CharField(max_length=30, blank=True, null=True)
    sizerows = models.IntegerField(db_column='sizeRows', blank=True, null=True)  # Field name made lowercase.
    sizecols = models.IntegerField(db_column='sizeCols', blank=True, null=True)  # Field name made lowercase.
    active = models.IntegerField(blank=True, null=True)
    displayorder = models.IntegerField(db_column='displayOrder', blank=True, null=True)  # Field name made lowercase.
    cptadd = models.CharField(db_column='cptAdd', max_length=5, blank=True, null=True)  # Field name made lowercase.
    asrow = models.IntegerField(db_column='asRow', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'customForms'


