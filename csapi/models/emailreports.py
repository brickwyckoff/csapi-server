from __future__ import unicode_literals

from django.db import models

class Emailreports(models.Model):
    username = models.CharField(max_length=60, blank=True, null=True)
    active = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'emailReports'


