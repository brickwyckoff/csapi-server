from __future__ import unicode_literals

from django.db import models

class Mardata(models.Model):
    mrn = models.BigIntegerField(blank=True, null=True)
    visitnum = models.IntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    entrydate = models.DateTimeField(db_column='entryDate')  # Field name made lowercase.
    user = models.CharField(max_length=80, blank=True, null=True)
    section = models.CharField(max_length=10, blank=True, null=True)
    origfilldate = models.DateField(db_column='origFillDate', blank=True, null=True)  # Field name made lowercase.
    prescription = models.CharField(max_length=200, blank=True, null=True)
    pillsperday = models.IntegerField(db_column='pillsPerDay', blank=True, null=True)  # Field name made lowercase.
    medsidentified = models.IntegerField(db_column='medsIdentified', blank=True, null=True)  # Field name made lowercase.
    actualcount = models.IntegerField(db_column='actualCount', blank=True, null=True)  # Field name made lowercase.
    dayssincefill = models.IntegerField(db_column='daysSinceFill', blank=True, null=True)  # Field name made lowercase.
    medication = models.CharField(max_length=200, blank=True, null=True)
    dosage = models.CharField(max_length=50, blank=True, null=True)
    observation = models.CharField(max_length=1500, blank=True, null=True)
    condition = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'marData'


