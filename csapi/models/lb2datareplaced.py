from __future__ import unicode_literals

from django.db import models

class Lb2Datareplaced(models.Model):
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    mrn = models.BigIntegerField(blank=True, null=True)
    messagecontrolid = models.CharField(db_column='messageControlId', max_length=22, blank=True, null=True)  # Field name made lowercase.
    parentorderid = models.CharField(db_column='parentOrderId', max_length=22, blank=True, null=True)  # Field name made lowercase.
    subheaderid = models.CharField(db_column='subHeaderId', max_length=15, blank=True, null=True)  # Field name made lowercase.
    labid = models.CharField(db_column='labId', max_length=50, blank=True, null=True)  # Field name made lowercase.
    labname = models.CharField(db_column='labName', max_length=100, blank=True, null=True)  # Field name made lowercase.
    labdate = models.DateTimeField(db_column='labDate', blank=True, null=True)  # Field name made lowercase.
    labresult = models.CharField(db_column='labResult', max_length=72, blank=True, null=True)  # Field name made lowercase.
    labresultnum = models.FloatField(db_column='labResultNum')  # Field name made lowercase.
    labunit = models.CharField(db_column='labUnit', max_length=50, blank=True, null=True)  # Field name made lowercase.
    labreferencerange = models.CharField(db_column='labReferenceRange', max_length=50, blank=True, null=True)  # Field name made lowercase.
    abnormalflag = models.CharField(db_column='abnormalFlag', max_length=2, blank=True, null=True)  # Field name made lowercase.
    orderedbyid = models.CharField(db_column='orderedById', max_length=100, blank=True, null=True)  # Field name made lowercase.
    orderedbyname = models.CharField(db_column='orderedByName', max_length=100, blank=True, null=True)  # Field name made lowercase.
    analysedat = models.CharField(db_column='analysedAt', max_length=100, blank=True, null=True)  # Field name made lowercase.
    analysedby = models.CharField(db_column='analysedBy', max_length=15, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'lb2DataReplaced'


