from __future__ import unicode_literals

from django.db import models

class ProcessedScans(models.Model):
    scandate = models.DateField(db_column='scanDate', blank=True, null=True)  # Field name made lowercase.
    scanfilename = models.CharField(db_column='scanFileName', max_length=60, blank=True, null=True)  # Field name made lowercase.
    scanfilesize = models.BigIntegerField(db_column='scanFileSize', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'processed_scans'


