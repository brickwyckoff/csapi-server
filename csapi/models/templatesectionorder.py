from __future__ import unicode_literals

from django.db import models

class Templatesectionorder(models.Model):
    id = models.BigAutoField(primary_key=True)
    sectionname = models.CharField(db_column='sectionName', max_length=150)  # Field name made lowercase.
    sectionorder = models.IntegerField(db_column='sectionOrder')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'templateSectionOrder'


