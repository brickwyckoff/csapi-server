from __future__ import unicode_literals

from django.db import models

class Statrxdoctors(models.Model):
    providercode = models.CharField(db_column='providerCode', max_length=3, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'statRxDoctors'


