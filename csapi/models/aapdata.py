from __future__ import unicode_literals

from django.db import models

class Aapdata(models.Model):
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    assessandplan = models.CharField(db_column='assessAndPlan', max_length=9000, blank=True, null=True)  # Field name made lowercase.
    futurecaregoals1 = models.CharField(db_column='futureCareGoals1', max_length=2000, blank=True, null=True)  # Field name made lowercase.
    futurecareinstructions1 = models.CharField(db_column='futureCareInstructions1', max_length=2000, blank=True, null=True)  # Field name made lowercase.
    futurecaregoals2 = models.CharField(db_column='futureCareGoals2', max_length=2000, blank=True, null=True)  # Field name made lowercase.
    futurecareinstructions2 = models.CharField(db_column='futureCareInstructions2', max_length=2000, blank=True, null=True)  # Field name made lowercase.
    preventativehealth = models.CharField(db_column='preventativeHealth', max_length=2000, blank=True, null=True)  # Field name made lowercase.
    concerndiversionrelapse = models.IntegerField(db_column='concernDiversionRelapse')  # Field name made lowercase.
    utoxnoteentered = models.IntegerField(db_column='utoxNoteEntered')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'aapData'


