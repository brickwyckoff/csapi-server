from __future__ import unicode_literals

from django.db import models

class Medorderlogmerged(models.Model):
    visitnum = models.IntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    drugid = models.CharField(db_column='drugId', max_length=10, blank=True, null=True)  # Field name made lowercase.
    tradeid = models.IntegerField(db_column='tradeId', blank=True, null=True)  # Field name made lowercase.
    doseform = models.IntegerField(db_column='doseForm', blank=True, null=True)  # Field name made lowercase.
    route = models.IntegerField(blank=True, null=True)
    strength = models.IntegerField(blank=True, null=True)
    freetext = models.CharField(db_column='freeText', max_length=80, blank=True, null=True)  # Field name made lowercase.
    genericbrand = models.CharField(db_column='genericBrand', max_length=1, blank=True, null=True)  # Field name made lowercase.
    rxotc = models.CharField(db_column='rxOtc', max_length=6, blank=True, null=True)  # Field name made lowercase.
    orderedby = models.CharField(db_column='orderedBy', max_length=35, blank=True, null=True)  # Field name made lowercase.
    creationtime = models.DateTimeField(db_column='creationTime', blank=True, null=True)  # Field name made lowercase.
    filled = models.IntegerField(blank=True, null=True)
    filledby = models.CharField(db_column='filledBy', max_length=35, blank=True, null=True)  # Field name made lowercase.
    filledtime = models.DateTimeField(db_column='filledTime', blank=True, null=True)  # Field name made lowercase.
    adminamount = models.CharField(db_column='adminAmount', max_length=25, blank=True, null=True)  # Field name made lowercase.
    adminunits = models.CharField(db_column='adminUnits', max_length=25, blank=True, null=True)  # Field name made lowercase.
    lotnum = models.CharField(db_column='lotNum', max_length=35, blank=True, null=True)  # Field name made lowercase.
    expirationdate = models.DateField(db_column='expirationDate', blank=True, null=True)  # Field name made lowercase.
    cvxcode = models.CharField(db_column='cvxCode', max_length=5, blank=True, null=True)  # Field name made lowercase.
    mvxcode = models.CharField(db_column='mvxCode', max_length=5, blank=True, null=True)  # Field name made lowercase.
    freq = models.IntegerField(blank=True, null=True)
    vacvfcqual = models.IntegerField(db_column='vacVfcQual')  # Field name made lowercase.
    adminroute = models.CharField(db_column='adminRoute', max_length=10)  # Field name made lowercase.
    adminsite = models.CharField(db_column='adminSite', max_length=10)  # Field name made lowercase.
    vacrecallpref = models.CharField(db_column='vacRecallPref', max_length=10)  # Field name made lowercase.
    adminfirstname = models.CharField(db_column='adminFirstName', max_length=35)  # Field name made lowercase.
    adminlastname = models.CharField(db_column='adminLastName', max_length=35)  # Field name made lowercase.
    adminmiddlename = models.CharField(db_column='adminMiddleName', max_length=35)  # Field name made lowercase.
    vactype = models.CharField(db_column='vacType', max_length=35)  # Field name made lowercase.
    vacpublishdate = models.CharField(db_column='vacPublishDate', max_length=50)  # Field name made lowercase.
    vacpresentdate = models.CharField(db_column='vacPresentDate', max_length=50)  # Field name made lowercase.
    vacinfosource = models.CharField(db_column='vacInfoSource', max_length=2)  # Field name made lowercase.
    vacrefusal = models.CharField(db_column='vacRefusal', max_length=3)  # Field name made lowercase.
    vacimmunity = models.CharField(db_column='vacImmunity', max_length=15)  # Field name made lowercase.
    vacconsenttoshare = models.CharField(db_column='vacConsentToShare', max_length=1)  # Field name made lowercase.
    vacadminnote = models.CharField(db_column='vacAdminNote', max_length=300)  # Field name made lowercase.
    mrn = models.BigIntegerField()

    class Meta:
        managed = False
        db_table = 'medOrderLogMerged'


