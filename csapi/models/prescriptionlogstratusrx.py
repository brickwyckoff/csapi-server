from __future__ import unicode_literals

from django.db import models

class Prescriptionlogstratusrx(models.Model):
    messageid = models.CharField(db_column='messageId', max_length=35, blank=True, null=True)  # Field name made lowercase.
    sendtime = models.DateTimeField(db_column='sendTime', blank=True, null=True)  # Field name made lowercase.
    senddate = models.DateField(db_column='sendDate', blank=True, null=True)  # Field name made lowercase.
    sendroute = models.CharField(db_column='sendRoute', max_length=1, blank=True, null=True)  # Field name made lowercase.
    surescriptsspi = models.CharField(db_column='sureScriptsSpi', max_length=50, blank=True, null=True)  # Field name made lowercase.
    user = models.CharField(max_length=50, blank=True, null=True)
    ndc = models.CharField(max_length=30, blank=True, null=True)
    status = models.CharField(max_length=3, blank=True, null=True)
    benefitscontrolnumber = models.CharField(db_column='benefitsControlNumber', max_length=19, blank=True, null=True)  # Field name made lowercase.
    qty = models.CharField(max_length=35, blank=True, null=True)
    refills = models.IntegerField(blank=True, null=True)
    refillsqual = models.CharField(db_column='refillsQual', max_length=3, blank=True, null=True)  # Field name made lowercase.
    signatureid = models.BigIntegerField(db_column='signatureId', blank=True, null=True)  # Field name made lowercase.
    mrn = models.BigIntegerField(blank=True, null=True)
    clientcode = models.CharField(db_column='clientCode', max_length=30, blank=True, null=True)  # Field name made lowercase.
    drugid = models.CharField(db_column='drugId', max_length=30, blank=True, null=True)  # Field name made lowercase.
    tradeid = models.CharField(db_column='tradeId', max_length=30, blank=True, null=True)  # Field name made lowercase.
    doseform = models.CharField(db_column='doseForm', max_length=30, blank=True, null=True)  # Field name made lowercase.
    route = models.CharField(max_length=30, blank=True, null=True)
    strength = models.CharField(max_length=30, blank=True, null=True)
    genericbrand = models.CharField(db_column='genericBrand', max_length=1, blank=True, null=True)  # Field name made lowercase.
    rxotc = models.CharField(db_column='rxOtc', max_length=6, blank=True, null=True)  # Field name made lowercase.
    freq = models.CharField(max_length=30, blank=True, null=True)
    numperdose = models.CharField(db_column='numPerDose', max_length=30, blank=True, null=True)  # Field name made lowercase.
    numperdoseunit = models.CharField(db_column='numPerDoseUnit', max_length=30, blank=True, null=True)  # Field name made lowercase.
    clinicalroute = models.CharField(db_column='clinicalRoute', max_length=30, blank=True, null=True)  # Field name made lowercase.
    sigtext = models.CharField(db_column='sigText', max_length=400, blank=True, null=True)  # Field name made lowercase.
    daw = models.CharField(max_length=1, blank=True, null=True)
    pharmacynote = models.CharField(db_column='pharmacyNote', max_length=210, blank=True, null=True)  # Field name made lowercase.
    writtendate = models.DateTimeField(db_column='writtenDate', blank=True, null=True)  # Field name made lowercase.
    selectedpbm = models.CharField(db_column='selectedPbm', max_length=100, blank=True, null=True)  # Field name made lowercase.
    relatestomessageid = models.CharField(db_column='relatesToMessageId', max_length=35, blank=True, null=True)  # Field name made lowercase.
    responsestatus = models.CharField(db_column='responseStatus', max_length=4, blank=True, null=True)  # Field name made lowercase.
    csacode = models.CharField(db_column='csaCode', max_length=3, blank=True, null=True)  # Field name made lowercase.
    pendingnoted = models.IntegerField(db_column='pendingNoted', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'prescriptionLogStratusRx'


