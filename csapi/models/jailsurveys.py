from __future__ import unicode_literals

from django.db import models

class Jailsurveys(models.Model):
    evalinterval = models.CharField(db_column='evalInterval', max_length=20, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'jailSurveys'


