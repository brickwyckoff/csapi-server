from __future__ import unicode_literals

from django.db import models

class Smddata(models.Model):
    id = models.BigAutoField(primary_key=True)
    visitnum = models.BigIntegerField(db_column='visitNum')  # Field name made lowercase.
    code = models.CharField(max_length=20)
    codetype = models.CharField(db_column='codeType', max_length=10)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'smdData'


