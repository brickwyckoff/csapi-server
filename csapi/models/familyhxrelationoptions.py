from __future__ import unicode_literals

from django.db import models

class Familyhxrelationoptions(models.Model):
    id = models.BigAutoField(primary_key=True)
    snomedcode = models.CharField(db_column='snomedCode', max_length=30)  # Field name made lowercase.
    displayorder = models.IntegerField(db_column='displayOrder')  # Field name made lowercase.
    isfirstdegree = models.IntegerField(db_column='isFirstDegree')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'familyHxRelationOptions'


