from __future__ import unicode_literals

from django.db import models

class Tredata(models.Model):
    id = models.BigAutoField(primary_key=True)
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    mrn = models.BigIntegerField(blank=True, null=True)
    username = models.CharField(max_length=30, blank=True, null=True)
    completedate = models.DateTimeField(db_column='completeDate', blank=True, null=True)  # Field name made lowercase.
    diag1 = models.IntegerField()
    diag2 = models.IntegerField()
    diag3 = models.IntegerField()
    diag4 = models.IntegerField()
    diag5 = models.IntegerField()
    diagphase1 = models.CharField(db_column='diagPhase1', max_length=30, blank=True, null=True)  # Field name made lowercase.
    diagphase2 = models.CharField(db_column='diagPhase2', max_length=30, blank=True, null=True)  # Field name made lowercase.
    diagphase3 = models.CharField(db_column='diagPhase3', max_length=30, blank=True, null=True)  # Field name made lowercase.
    diagphase4 = models.CharField(db_column='diagPhase4', max_length=30, blank=True, null=True)  # Field name made lowercase.
    diagphase5 = models.CharField(db_column='diagPhase5', max_length=30, blank=True, null=True)  # Field name made lowercase.
    treatmentphase = models.IntegerField(db_column='treatmentPhase', blank=True, null=True)  # Field name made lowercase.
    freq = models.IntegerField(blank=True, null=True)
    strength1 = models.IntegerField()
    strength2 = models.IntegerField()
    strength3 = models.IntegerField()
    strength4 = models.IntegerField()
    strength5 = models.IntegerField()
    strength6 = models.IntegerField()
    strength7 = models.IntegerField()
    strength8 = models.IntegerField()
    strength9 = models.IntegerField()
    barrier1 = models.IntegerField()
    barrier2 = models.IntegerField()
    barrier3 = models.IntegerField()
    barrier4 = models.IntegerField()
    barrier5 = models.IntegerField()
    barrier6 = models.IntegerField()
    barrier7 = models.IntegerField()
    barrier8 = models.IntegerField()
    barrier9 = models.IntegerField()
    barrier10 = models.IntegerField()
    plan = models.IntegerField(blank=True, null=True)
    problem0 = models.IntegerField()
    problem1 = models.IntegerField()
    problem2 = models.IntegerField()
    problem3 = models.IntegerField()
    problem4 = models.IntegerField()
    problem5 = models.IntegerField()
    problem6 = models.IntegerField()
    problem7 = models.IntegerField()
    problem8 = models.IntegerField()
    outcome1 = models.IntegerField(blank=True, null=True)
    outcome2 = models.IntegerField(blank=True, null=True)
    outcome3 = models.IntegerField(blank=True, null=True)
    outcome4 = models.IntegerField(blank=True, null=True)
    outcome5 = models.IntegerField(blank=True, null=True)
    outcome6 = models.IntegerField(blank=True, null=True)
    outcome7 = models.IntegerField(blank=True, null=True)
    outcome8 = models.IntegerField(blank=True, null=True)
    outcome9 = models.IntegerField(blank=True, null=True)
    outcome10 = models.IntegerField(blank=True, null=True)
    progressnarrative = models.CharField(db_column='progressNarrative', max_length=5000, blank=True, null=True)  # Field name made lowercase.
    isprinted = models.IntegerField(db_column='isPrinted')  # Field name made lowercase.
    scanfilename = models.CharField(db_column='scanFilename', max_length=100, blank=True, null=True)  # Field name made lowercase.
    providersignature = models.DateTimeField(db_column='providerSignature', blank=True, null=True)  # Field name made lowercase.
    patientsignature = models.IntegerField(db_column='patientSignature')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'treData'


