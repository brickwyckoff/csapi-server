from __future__ import unicode_literals

from django.db import models

class Utoxpaschedule(models.Model):
    id = models.BigAutoField(primary_key=True)
    insco = models.CharField(db_column='insCo', max_length=50)  # Field name made lowercase.
    numutoxperpa = models.IntegerField(db_column='numUtoxPerPa')  # Field name made lowercase.
    numdaysperpa = models.IntegerField(db_column='numDaysPerPa')  # Field name made lowercase.
    isactive = models.IntegerField(db_column='isActive')  # Field name made lowercase.
    cptcodes = models.CharField(db_column='cptCodes', max_length=100)  # Field name made lowercase.
    countstart = models.IntegerField(db_column='countStart')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'utoxPaSchedule'


