from __future__ import unicode_literals

from django.db import models

class Infrawareusers(models.Model):
    id = models.BigAutoField(primary_key=True)
    code = models.CharField(max_length=5)
    authorid = models.CharField(db_column='authorId', max_length=35)  # Field name made lowercase.
    isdefault = models.IntegerField(db_column='isDefault')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'infrawareUsers'


