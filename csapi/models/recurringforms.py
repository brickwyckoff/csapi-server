from __future__ import unicode_literals

from django.db import models

class Recurringforms(models.Model):
    id = models.BigAutoField(primary_key=True)
    insco = models.CharField(db_column='insCo', max_length=35)  # Field name made lowercase.
    labcodes = models.CharField(db_column='labCodes', max_length=100)  # Field name made lowercase.
    goodfordays = models.IntegerField(db_column='goodForDays')  # Field name made lowercase.
    goodfortests = models.IntegerField(db_column='goodForTests')  # Field name made lowercase.
    formtype = models.CharField(db_column='formType', max_length=35)  # Field name made lowercase.
    autosend = models.IntegerField(db_column='autoSend')  # Field name made lowercase.
    annualreset = models.IntegerField(db_column='annualReset')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'recurringForms'


