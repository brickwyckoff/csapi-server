from __future__ import unicode_literals

from django.db import models

class Labsupexclusion(models.Model):
    id = models.BigAutoField(primary_key=True)
    insco = models.CharField(db_column='insCo', max_length=35)  # Field name made lowercase.
    provider = models.CharField(max_length=5)
    switchprovider = models.CharField(db_column='switchProvider', max_length=5)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'labSupExclusion'


