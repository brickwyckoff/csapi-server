from __future__ import unicode_literals

from django.db import models

class Users(models.Model):
    username = models.CharField(max_length=30, blank=True, null=True)
    password = models.CharField(max_length=80, blank=True, null=True)
    name = models.CharField(max_length=40)
    email = models.CharField(max_length=100)
    accesslevel = models.IntegerField(db_column='accessLevel')  # Field name made lowercase.
    code = models.CharField(max_length=20, blank=True, null=True)
    resetdate = models.DateField(db_column='resetDate', blank=True, null=True)  # Field name made lowercase.
    cellphone = models.CharField(db_column='cellPhone', max_length=10, blank=True, null=True)  # Field name made lowercase.
    activeuser = models.IntegerField(db_column='activeUser', blank=True, null=True)  # Field name made lowercase.
    rxaccesslevel = models.IntegerField(db_column='rxAccessLevel', blank=True, null=True)  # Field name made lowercase.
    emergencyaccess = models.IntegerField(db_column='emergencyAccess', blank=True, null=True)  # Field name made lowercase.
    reqsup = models.IntegerField(db_column='reqSup')  # Field name made lowercase.
    sendtextreminders = models.IntegerField(db_column='sendTextReminders')  # Field name made lowercase.
    firstname = models.CharField(db_column='firstName', max_length=80, blank=True, null=True)  # Field name made lowercase.
    lastname = models.CharField(db_column='lastName', max_length=80, blank=True, null=True)  # Field name made lowercase.
    rxadmin = models.IntegerField(db_column='rxAdmin', blank=True, null=True)  # Field name made lowercase.
    asamcontinuumuser = models.IntegerField(db_column='asamContinuumUser', blank=True, null=True)  # Field name made lowercase.
    accesslevelsched = models.IntegerField(db_column='accessLevelSched', blank=True, null=True)  # Field name made lowercase.
    billingaccesslevel = models.IntegerField(db_column='billingAccessLevel', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'users'


