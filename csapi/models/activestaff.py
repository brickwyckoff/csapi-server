from __future__ import unicode_literals

from django.db import models

class Activestaff(models.Model):
    username = models.CharField(max_length=50, blank=True, null=True)
    userrole = models.CharField(db_column='userRole', max_length=50, blank=True, null=True)  # Field name made lowercase.
    sendalerts = models.IntegerField(db_column='sendAlerts', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'activeStaff'


