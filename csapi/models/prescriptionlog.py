from __future__ import unicode_literals

from django.db import models

class Prescriptionlog(models.Model):
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    prescription = models.CharField(max_length=150, blank=True, null=True)
    method = models.CharField(max_length=50, blank=True, null=True)
    user = models.CharField(max_length=50, blank=True, null=True)
    trackingnum = models.CharField(db_column='trackingNum', max_length=30, blank=True, null=True)  # Field name made lowercase.
    creationtime = models.DateTimeField(db_column='creationTime', blank=True, null=True)  # Field name made lowercase.
    autid = models.BigIntegerField(db_column='autId')  # Field name made lowercase.
    pharmacyid = models.BigIntegerField(db_column='pharmacyId')  # Field name made lowercase.
    faxto = models.BigIntegerField(db_column='faxTo')  # Field name made lowercase.
    provider = models.CharField(max_length=2)
    mrn = models.BigIntegerField()
    printlogid = models.BigIntegerField(db_column='printLogId')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'prescriptionLog'


