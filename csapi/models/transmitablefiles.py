from __future__ import unicode_literals

from django.db import models

class Transmitablefiles(models.Model):
    mrn = models.BigIntegerField(blank=True, null=True)
    filename = models.CharField(db_column='fileName', max_length=150, blank=True, null=True)  # Field name made lowercase.
    encryptedfilename = models.CharField(db_column='encryptedFileName', max_length=150, blank=True, null=True)  # Field name made lowercase.
    hashvaluefilename = models.CharField(db_column='hashValueFileName', max_length=150, blank=True, null=True)  # Field name made lowercase.
    filetype = models.CharField(db_column='fileType', max_length=30, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'transmitableFiles'


