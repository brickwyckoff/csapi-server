from __future__ import unicode_literals

from django.db import models

class Pharmacypolicyinfo(models.Model):
    controlnumber = models.CharField(db_column='controlNumber', max_length=19, blank=True, null=True)  # Field name made lowercase.
    mrn = models.BigIntegerField(blank=True, null=True)
    checkdate = models.DateField(db_column='checkDate', blank=True, null=True)  # Field name made lowercase.
    policynumberqual = models.CharField(db_column='policyNumberQual', max_length=2, blank=True, null=True)  # Field name made lowercase.
    policynumber = models.CharField(db_column='policyNumber', max_length=80, blank=True, null=True)  # Field name made lowercase.
    policyname = models.CharField(db_column='policyName', max_length=100, blank=True, null=True)  # Field name made lowercase.
    policypayorid = models.CharField(db_column='policyPayorId', max_length=100, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'pharmacyPolicyInfo'


