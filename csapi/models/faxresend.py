from __future__ import unicode_literals

from django.db import models

class Faxresend(models.Model):
    id = models.BigAutoField(primary_key=True)
    faxid = models.BigIntegerField(db_column='faxId')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'faxResend'


