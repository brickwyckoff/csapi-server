from __future__ import unicode_literals

from django.db import models

class Refdata(models.Model):
    id = models.BigAutoField(primary_key=True)
    mrn = models.BigIntegerField(blank=True, null=True)
    startdate = models.DateField(db_column='startDate', blank=True, null=True)  # Field name made lowercase.
    enddate = models.DateField(db_column='endDate', blank=True, null=True)  # Field name made lowercase.
    referringprovidernpi = models.CharField(db_column='referringProviderNpi', max_length=12, blank=True, null=True)  # Field name made lowercase.
    referredtoprovider = models.CharField(db_column='referredToProvider', max_length=20, blank=True, null=True)  # Field name made lowercase.
    referralnumber = models.CharField(db_column='referralNumber', max_length=20, blank=True, null=True)  # Field name made lowercase.
    numreferrals = models.IntegerField(db_column='numReferrals', blank=True, null=True)  # Field name made lowercase.
    removed = models.IntegerField()
    clinicid = models.BigIntegerField(db_column='clinicId', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'refData'


