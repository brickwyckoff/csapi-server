from __future__ import unicode_literals

from django.db import models

class Labaoeresponse(models.Model):
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    issent = models.IntegerField(db_column='isSent', blank=True, null=True)  # Field name made lowercase.
    labcode = models.CharField(db_column='labCode', max_length=15, blank=True, null=True)  # Field name made lowercase.
    labname = models.CharField(db_column='labName', max_length=175, blank=True, null=True)  # Field name made lowercase.
    lablocationcode = models.CharField(db_column='labLocationCode', max_length=30, blank=True, null=True)  # Field name made lowercase.
    responsetype = models.CharField(db_column='responseType', max_length=2, blank=True, null=True)  # Field name made lowercase.
    response = models.CharField(max_length=500, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'labAoeResponse'


