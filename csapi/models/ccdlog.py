from __future__ import unicode_literals

from django.db import models

class Ccdlog(models.Model):
    mrn = models.BigIntegerField(blank=True, null=True)
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    sendto = models.CharField(db_column='sendTo', max_length=35, blank=True, null=True)  # Field name made lowercase.
    sendby = models.CharField(db_column='sendBy', max_length=35, blank=True, null=True)  # Field name made lowercase.
    sendtime = models.DateTimeField(db_column='sendTime', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ccdLog'


