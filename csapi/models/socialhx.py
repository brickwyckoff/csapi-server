from __future__ import unicode_literals

from django.db import models

class Socialhx(models.Model):
    mrn = models.IntegerField(blank=True, null=True)
    hxid = models.BigIntegerField(db_column='hxId', blank=True, null=True)  # Field name made lowercase.
    freetext = models.CharField(db_column='freeText', max_length=100)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'socialHx'


