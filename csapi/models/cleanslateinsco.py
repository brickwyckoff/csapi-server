from __future__ import unicode_literals

from django.db import models

class Cleanslateinsco(models.Model):
    id = models.IntegerField(blank=True, null=True)
    inscocode = models.CharField(db_column='insCoCode', max_length=20, blank=True, null=True)  # Field name made lowercase.
    insco = models.CharField(db_column='insCo', max_length=150, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'cleanSlateInsCo'


