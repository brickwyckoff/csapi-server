from __future__ import unicode_literals

from django.db import models

class Scriptstocallin(models.Model):
    id = models.BigAutoField(primary_key=True)
    mrn = models.BigIntegerField()
    prescriptionlogid = models.BigIntegerField(db_column='prescriptionLogId')  # Field name made lowercase.
    lastprescriptionlogid = models.BigIntegerField(db_column='lastPrescriptionLogId')  # Field name made lowercase.
    clinicloc = models.BigIntegerField(db_column='clinicLoc')  # Field name made lowercase.
    ncpdpid = models.CharField(max_length=7)
    iscalledin = models.IntegerField(db_column='isCalledIn')  # Field name made lowercase.
    callintime = models.DateTimeField(db_column='callInTime')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'scriptsToCallIn'


