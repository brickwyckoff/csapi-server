from __future__ import unicode_literals

from django.db import models

class Rosold(models.Model):
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    ros = models.CharField(max_length=500, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'rosOld'


