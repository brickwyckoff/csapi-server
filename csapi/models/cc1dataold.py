from __future__ import unicode_literals

from django.db import models

class Cc1Dataold(models.Model):
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    alleviatesx = models.IntegerField(db_column='alleviateSx', blank=True, null=True)  # Field name made lowercase.
    improvecoping = models.IntegerField(db_column='improveCoping', blank=True, null=True)  # Field name made lowercase.
    idstrategies = models.IntegerField(db_column='idStrategies', blank=True, null=True)  # Field name made lowercase.
    maintainstability = models.IntegerField(db_column='maintainStability', blank=True, null=True)  # Field name made lowercase.
    other = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cc1DataOld'


