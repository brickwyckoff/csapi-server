from __future__ import unicode_literals

from django.db import models

class Macrovalues(models.Model):
    id = models.BigAutoField(primary_key=True)
    macroid = models.BigIntegerField(db_column='macroId', blank=True, null=True)  # Field name made lowercase.
    comchiefcomplaint = models.CharField(db_column='comChiefComplaint', max_length=200, blank=True, null=True)  # Field name made lowercase.
    hpihistory = models.CharField(db_column='hpiHistory', max_length=1500, blank=True, null=True)  # Field name made lowercase.
    pexgeneral = models.CharField(db_column='pexGeneral', max_length=500, blank=True, null=True)  # Field name made lowercase.
    pexeye = models.CharField(db_column='pexEye', max_length=500, blank=True, null=True)  # Field name made lowercase.
    pexent = models.CharField(db_column='pexEnt', max_length=500, blank=True, null=True)  # Field name made lowercase.
    pexneck = models.CharField(db_column='pexNeck', max_length=500, blank=True, null=True)  # Field name made lowercase.
    pexpulm = models.CharField(db_column='pexPulm', max_length=500, blank=True, null=True)  # Field name made lowercase.
    pexcard = models.CharField(db_column='pexCard', max_length=500, blank=True, null=True)  # Field name made lowercase.
    pexabd = models.CharField(db_column='pexAbd', max_length=500, blank=True, null=True)  # Field name made lowercase.
    pexms = models.CharField(db_column='pexMs', max_length=500, blank=True, null=True)  # Field name made lowercase.
    pexneuro = models.CharField(db_column='pexNeuro', max_length=500, blank=True, null=True)  # Field name made lowercase.
    pexlymph = models.CharField(db_column='pexLymph', max_length=500, blank=True, null=True)  # Field name made lowercase.
    pexpsych = models.CharField(db_column='pexPsych', max_length=500, blank=True, null=True)  # Field name made lowercase.
    pexother = models.CharField(db_column='pexOther', max_length=500, blank=True, null=True)  # Field name made lowercase.
    pexskin = models.CharField(db_column='pexSkin', max_length=500, blank=True, null=True)  # Field name made lowercase.
    aapassessandplan = models.CharField(db_column='aapAssessAndPlan', max_length=800, blank=True, null=True)  # Field name made lowercase.
    diafinal1 = models.CharField(db_column='diaFinal1', max_length=100, blank=True, null=True)  # Field name made lowercase.
    diafinal2 = models.CharField(db_column='diaFinal2', max_length=100, blank=True, null=True)  # Field name made lowercase.
    diafinal3 = models.CharField(db_column='diaFinal3', max_length=100, blank=True, null=True)  # Field name made lowercase.
    diafinal4 = models.CharField(db_column='diaFinal4', max_length=100, blank=True, null=True)  # Field name made lowercase.
    preprescription1 = models.CharField(db_column='prePrescription1', max_length=100, blank=True, null=True)  # Field name made lowercase.
    prefreq1 = models.CharField(db_column='preFreq1', max_length=100, blank=True, null=True)  # Field name made lowercase.
    preroute1 = models.CharField(db_column='preRoute1', max_length=100, blank=True, null=True)  # Field name made lowercase.
    preqty1 = models.CharField(db_column='preQty1', max_length=100, blank=True, null=True)  # Field name made lowercase.
    prerefills1 = models.CharField(db_column='preRefills1', max_length=100, blank=True, null=True)  # Field name made lowercase.
    preprescription2 = models.CharField(db_column='prePrescription2', max_length=100, blank=True, null=True)  # Field name made lowercase.
    prefreq2 = models.CharField(db_column='preFreq2', max_length=100, blank=True, null=True)  # Field name made lowercase.
    preroute2 = models.CharField(db_column='preRoute2', max_length=100, blank=True, null=True)  # Field name made lowercase.
    preqty2 = models.CharField(db_column='preQty2', max_length=100, blank=True, null=True)  # Field name made lowercase.
    prerefills2 = models.CharField(db_column='preRefills2', max_length=100, blank=True, null=True)  # Field name made lowercase.
    preprescription3 = models.CharField(db_column='prePrescription3', max_length=100, blank=True, null=True)  # Field name made lowercase.
    prefreq3 = models.CharField(db_column='preFreq3', max_length=100, blank=True, null=True)  # Field name made lowercase.
    preroute3 = models.CharField(db_column='preRoute3', max_length=100, blank=True, null=True)  # Field name made lowercase.
    preqty3 = models.CharField(db_column='preQty3', max_length=100, blank=True, null=True)  # Field name made lowercase.
    prerefills3 = models.CharField(db_column='preRefills3', max_length=100, blank=True, null=True)  # Field name made lowercase.
    preprescription4 = models.CharField(db_column='prePrescription4', max_length=100, blank=True, null=True)  # Field name made lowercase.
    prefreq4 = models.CharField(db_column='preFreq4', max_length=100, blank=True, null=True)  # Field name made lowercase.
    preroute4 = models.CharField(db_column='preRoute4', max_length=100, blank=True, null=True)  # Field name made lowercase.
    preqty4 = models.CharField(db_column='preQty4', max_length=100, blank=True, null=True)  # Field name made lowercase.
    prerefills4 = models.CharField(db_column='preRefills4', max_length=100, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'macroValues'


