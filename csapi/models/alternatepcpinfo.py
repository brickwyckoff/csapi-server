from __future__ import unicode_literals

from django.db import models

class Alternatepcpinfo(models.Model):
    pcpid = models.BigIntegerField(db_column='pcpId', blank=True, null=True)  # Field name made lowercase.
    faxnum = models.BigIntegerField(db_column='faxNum', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'alternatePcpInfo'


