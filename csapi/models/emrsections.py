from __future__ import unicode_literals

from django.db import models

class Emrsections(models.Model):
    sectioncode = models.CharField(db_column='sectionCode', max_length=3, blank=True, null=True)  # Field name made lowercase.
    sectionname = models.CharField(db_column='sectionName', max_length=50, blank=True, null=True)  # Field name made lowercase.
    indextype = models.IntegerField(db_column='indexType', blank=True, null=True)  # Field name made lowercase.
    autoadd = models.IntegerField(db_column='autoAdd', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'emrSections'


