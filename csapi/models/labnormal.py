from __future__ import unicode_literals

from django.db import models

class Labnormal(models.Model):
    lab = models.CharField(max_length=50)
    lowvalue = models.FloatField(db_column='lowValue')  # Field name made lowercase.
    highvalue = models.FloatField(db_column='highValue')  # Field name made lowercase.
    gender = models.CharField(max_length=1)

    class Meta:
        managed = False
        db_table = 'labNormal'


