from __future__ import unicode_literals

from django.db import models

class Datelogtemp(models.Model):
    id = models.BigAutoField(primary_key=True)
    apptime = models.DateTimeField(db_column='appTime')  # Field name made lowercase.
    dbtime = models.DateTimeField(db_column='dbTime')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'dateLogTemp'


