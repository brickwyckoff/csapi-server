from __future__ import unicode_literals

from django.db import models

class Chiefcomplainttable(models.Model):
    chiefcomplaint = models.CharField(db_column='chiefComplaint', max_length=80, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'chiefComplaintTable'


