from __future__ import unicode_literals

from django.db import models

class Rppdata(models.Model):
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    trigger1 = models.CharField(max_length=200, blank=True, null=True)
    trigger2 = models.CharField(max_length=200, blank=True, null=True)
    trigger3 = models.CharField(max_length=200, blank=True, null=True)
    trigger4 = models.CharField(max_length=200, blank=True, null=True)
    support1 = models.CharField(max_length=200, blank=True, null=True)
    support2 = models.CharField(max_length=200, blank=True, null=True)
    support3 = models.CharField(max_length=200, blank=True, null=True)
    alternative1 = models.CharField(max_length=200, blank=True, null=True)
    alternative2 = models.CharField(max_length=200, blank=True, null=True)
    alternative3 = models.CharField(max_length=200, blank=True, null=True)
    commitmentbarriers = models.CharField(db_column='commitmentBarriers', max_length=700, blank=True, null=True)  # Field name made lowercase.
    commitmentbarrierssolutions = models.CharField(db_column='commitmentBarriersSolutions', max_length=700, blank=True, null=True)  # Field name made lowercase.
    reminder1 = models.CharField(max_length=200, blank=True, null=True)
    reminder2 = models.CharField(max_length=200, blank=True, null=True)
    reminder3 = models.CharField(max_length=200, blank=True, null=True)
    reward1 = models.CharField(max_length=200, blank=True, null=True)
    reward2 = models.CharField(max_length=200, blank=True, null=True)
    reward3 = models.CharField(max_length=200, blank=True, null=True)
    wordsofencouragement = models.CharField(db_column='wordsOfEncouragement', max_length=700, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'rppData'


