from __future__ import unicode_literals

from django.db import models

class Progressnotes(models.Model):
    mrn = models.BigIntegerField(blank=True, null=True)
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    note = models.TextField(blank=True, null=True)
    activitytype = models.CharField(db_column='activityType', max_length=35, blank=True, null=True)  # Field name made lowercase.
    activitydate = models.DateTimeField(db_column='activityDate', blank=True, null=True)  # Field name made lowercase.
    activitylength = models.BigIntegerField(db_column='activityLength', blank=True, null=True)  # Field name made lowercase.
    observedby = models.CharField(db_column='observedBy', max_length=50, blank=True, null=True)  # Field name made lowercase.
    enteredby = models.CharField(db_column='enteredBy', max_length=50, blank=True, null=True)  # Field name made lowercase.
    creationtime = models.DateTimeField(db_column='creationTime', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'progressNotes'


