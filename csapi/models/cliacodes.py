from __future__ import unicode_literals

from django.db import models

class Cliacodes(models.Model):
    id = models.BigAutoField(primary_key=True)
    code = models.CharField(max_length=15)
    ordereronly = models.IntegerField(db_column='ordererOnly', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'cliaCodes'


