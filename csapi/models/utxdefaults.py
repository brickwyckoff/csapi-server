from __future__ import unicode_literals

from django.db import models

class Utxdefaults(models.Model):
    utxfield = models.CharField(db_column='utxField', max_length=10, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'utxDefaults'


