from __future__ import unicode_literals

from django.db import models

class Deletelog(models.Model):
    deletedmrn = models.BigIntegerField(db_column='deletedMrn', blank=True, null=True)  # Field name made lowercase.
    user = models.CharField(max_length=60, blank=True, null=True)
    mergetime = models.DateTimeField(db_column='mergeTime', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'deleteLog'


