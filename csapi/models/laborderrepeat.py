from __future__ import unicode_literals

from django.db import models

class Laborderrepeat(models.Model):
    mrn = models.BigIntegerField(blank=True, null=True)
    laborderid = models.CharField(db_column='labOrderId', max_length=15, blank=True, null=True)  # Field name made lowercase.
    creationtime = models.DateTimeField(db_column='creationTime', blank=True, null=True)  # Field name made lowercase.
    enteredby = models.CharField(db_column='enteredBy', max_length=35, blank=True, null=True)  # Field name made lowercase.
    repeatdate = models.DateField(db_column='repeatDate', blank=True, null=True)  # Field name made lowercase.
    ordered = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'labOrderRepeat'


