from __future__ import unicode_literals

from django.db import models

class Rxpchangelog(models.Model):
    id = models.BigAutoField(primary_key=True)
    mrn = models.BigIntegerField()
    startrefills = models.CharField(db_column='startRefills', max_length=10)  # Field name made lowercase.
    endrefills = models.CharField(db_column='endRefills', max_length=10)  # Field name made lowercase.
    changetime = models.DateTimeField(db_column='changeTime')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'rxpChangeLog'


