from __future__ import unicode_literals

from django.db import models

class Decisionsupportviewlog(models.Model):
    id = models.BigAutoField(primary_key=True)
    mrn = models.BigIntegerField()
    supportid = models.BigIntegerField(db_column='supportId')  # Field name made lowercase.
    supporttitle = models.CharField(db_column='supportTitle', max_length=150)  # Field name made lowercase.
    user = models.CharField(max_length=35)
    viewtime = models.DateTimeField(db_column='viewTime')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'decisionSupportViewLog'


