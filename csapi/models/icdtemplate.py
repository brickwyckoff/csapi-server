from __future__ import unicode_literals

from django.db import models

class Icdtemplate(models.Model):
    id = models.BigAutoField(primary_key=True)
    code = models.CharField(max_length=15)
    templatestartdate = models.DateField(db_column='templateStartDate', blank=True, null=True)  # Field name made lowercase.
    templateenddate = models.DateField(db_column='templateEndDate', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'icdTemplate'


