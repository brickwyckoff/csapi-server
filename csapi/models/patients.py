from __future__ import unicode_literals

from django.db import models

from . import demdata


class Patients(models.Model):

    class Meta:
        managed=False
        db_table = 'patients'

    mrn = models.AutoField(primary_key=True)
    firstname = models.CharField(db_column='firstName', max_length=50, blank=True, null=True)  # Field name made lowercase.
    lastname = models.CharField(db_column='lastName', max_length=50, blank=True, null=True)  # Field name made lowercase.
    dob = models.DateField(blank=True, null=True)
    city = models.CharField(max_length=100, blank=True, null=True)
    state = models.CharField(max_length=2, blank=True, null=True)
    changetime = models.DateTimeField(db_column='changeTime', blank=True, null=True)  # Field name made lowercase.
    indatabase = models.CharField(db_column='inDatabase', max_length=20, blank=True, null=True)  # Field name made lowercase.
    foreignmrn = models.BigIntegerField(db_column='foreignMrn', blank=True, null=True)  # Field name made lowercase.
    activepatient = models.IntegerField(db_column='activePatient', blank=True, null=True)  # Field name made lowercase.
    pthxupdated = models.DateTimeField(db_column='ptHxUpdated', blank=True, null=True)  # Field name made lowercase.

    demdata = models.OneToOneField (
        demdata.Demdata,
        to_field=mrn,
        primary_key=False,
        # db_column='mrn'
        # from_fields=mrn
    )
