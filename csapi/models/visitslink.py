from __future__ import unicode_literals

from django.db import models

class Visitslink(models.Model):
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    visitnumext = models.CharField(db_column='visitNumExt', max_length=35, blank=True, null=True)  # Field name made lowercase.
    linkname = models.CharField(db_column='linkName', max_length=30, blank=True, null=True)  # Field name made lowercase.
    visitused = models.IntegerField(db_column='visitUsed', blank=True, null=True)  # Field name made lowercase.
    visitdate = models.DateField(db_column='visitDate', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'visitsLink'


