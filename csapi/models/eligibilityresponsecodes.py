from __future__ import unicode_literals

from django.db import models

class Eligibilityresponsecodes(models.Model):
    code = models.IntegerField(blank=True, null=True)
    reason = models.CharField(max_length=100, blank=True, null=True)
    action = models.CharField(max_length=1, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'eligibilityResponseCodes'


