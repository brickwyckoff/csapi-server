from __future__ import unicode_literals

from django.db import models

class Rxfaxoptions(models.Model):
    optiontype = models.CharField(db_column='optionType', max_length=15, blank=True, null=True)  # Field name made lowercase.
    optionvalue = models.CharField(db_column='optionValue', max_length=35, blank=True, null=True)  # Field name made lowercase.
    optiondisplay = models.CharField(db_column='optionDisplay', max_length=35, blank=True, null=True)  # Field name made lowercase.
    lexicode = models.CharField(db_column='lexiCode', max_length=200, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'rxFaxOptions'


