from __future__ import unicode_literals

from django.db import models

class Apptreminderlog(models.Model):
    id = models.BigAutoField(primary_key=True)
    mrn = models.BigIntegerField(blank=True, null=True)
    tonumber = models.BigIntegerField(db_column='toNumber', blank=True, null=True)  # Field name made lowercase.
    fromnumber = models.BigIntegerField(db_column='fromNumber', blank=True, null=True)  # Field name made lowercase.
    sendtime = models.DateTimeField(db_column='sendTime', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'apptReminderLog'


