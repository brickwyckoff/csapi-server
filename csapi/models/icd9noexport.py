from __future__ import unicode_literals

from django.db import models

class Icd9Noexport(models.Model):
    id = models.BigAutoField(primary_key=True)
    icd9 = models.CharField(max_length=10)

    class Meta:
        managed = False
        db_table = 'icd9NoExport'


