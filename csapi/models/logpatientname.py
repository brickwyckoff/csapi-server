from __future__ import unicode_literals

from django.db import models

class Logpatientname(models.Model):
    namesearch = models.CharField(db_column='nameSearch', max_length=60, blank=True, null=True)  # Field name made lowercase.
    creationtime = models.DateTimeField(db_column='creationTime', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'logPatientName'


