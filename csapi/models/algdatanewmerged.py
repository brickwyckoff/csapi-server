from __future__ import unicode_literals

from django.db import models

class Algdatanewmerged(models.Model):
    mrn = models.BigIntegerField(blank=True, null=True)
    drugid = models.CharField(db_column='drugId', max_length=10, blank=True, null=True)  # Field name made lowercase.
    tradeid = models.IntegerField(db_column='tradeId', blank=True, null=True)  # Field name made lowercase.
    doseform = models.IntegerField(db_column='doseForm', blank=True, null=True)  # Field name made lowercase.
    route = models.IntegerField(blank=True, null=True)
    strength = models.IntegerField(blank=True, null=True)
    freetext = models.CharField(db_column='freeText', max_length=80, blank=True, null=True)  # Field name made lowercase.
    genericbrand = models.CharField(db_column='genericBrand', max_length=1, blank=True, null=True)  # Field name made lowercase.
    rxotc = models.CharField(db_column='rxOtc', max_length=6, blank=True, null=True)  # Field name made lowercase.
    freq = models.IntegerField(blank=True, null=True)
    reactiontype = models.CharField(db_column='reactionType', max_length=15, blank=True, null=True)  # Field name made lowercase.
    isactive = models.IntegerField(db_column='isActive', blank=True, null=True)  # Field name made lowercase.
    drugclass = models.IntegerField(db_column='drugClass', blank=True, null=True)  # Field name made lowercase.
    dose = models.CharField(max_length=15, blank=True, null=True)
    doseunit = models.CharField(db_column='doseUnit', max_length=15, blank=True, null=True)  # Field name made lowercase.
    updatedate = models.DateTimeField(db_column='updateDate')  # Field name made lowercase.
    infosource = models.CharField(db_column='infoSource', max_length=100)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'algDataNewMerged'


