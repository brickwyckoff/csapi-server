from __future__ import unicode_literals

from django.db import models

class Errace(models.Model):
    aanum = models.BigIntegerField(db_column='aaNum')  # Field name made lowercase.
    race = models.CharField(max_length=10, blank=True, null=True)
    language = models.CharField(max_length=10, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'erRace'


