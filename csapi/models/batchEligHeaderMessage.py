from django.db import models
from csapi.models.batchEligHeader import BatchEligHeader

class BatchEligHeaderMessage (models.Model):
    class Meta:
        db_table = 'batchEligHeaderMessage'
        managed = True

    id = models.AutoField (auto_created=True, primary_key=True, serialize=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    message = models.CharField (max_length=256)

    batchHeader = models.ForeignKey(BatchEligHeader, to_field='id')


