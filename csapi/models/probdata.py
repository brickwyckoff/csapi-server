from __future__ import unicode_literals

from django.db import models

class Probdata(models.Model):
    id = models.BigAutoField(primary_key=True)
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    probation = models.IntegerField(blank=True, null=True)
    source = models.CharField(max_length=20, blank=True, null=True)
    enddate = models.DateField(db_column='endDate', blank=True, null=True)  # Field name made lowercase.
    mrn = models.BigIntegerField(blank=True, null=True)
    entrydate = models.DateField(db_column='entryDate', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'probData'


