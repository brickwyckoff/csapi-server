from __future__ import unicode_literals

from django.db import models
from .current_amd_visits import CurrentAmdVisits
class CurrentAmdPatientInsurances(models.Model):
    # Insurance block
    insurance_id = models.BigIntegerField(db_column='insuranceId')  # Field name made lowercase.
    carcode = models.CharField(db_column='carcode',max_length=50)
    carname = models.CharField(db_column='carName',max_length=50)
    create_date = models.DateTimeField(db_column='createDate')
    change_date = models.DateTimeField(db_column='changeDate', null=True, blank=True)

    mrn = models.BigIntegerField(db_column='mrn')
    visit_num = models.BigIntegerField(db_column='visitNnum')
    class Meta:
        db_table = 'currentAmdPatientInsurances'