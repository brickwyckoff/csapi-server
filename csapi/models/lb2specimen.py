from __future__ import unicode_literals

from django.db import models

class Lb2Specimen(models.Model):
    id = models.BigAutoField(primary_key=True)
    messagecontrolid = models.CharField(db_column='messageControlId', max_length=22)  # Field name made lowercase.
    labid = models.CharField(db_column='labId', max_length=50)  # Field name made lowercase.
    type = models.CharField(max_length=35)
    typecode = models.CharField(db_column='typeCode', max_length=35)  # Field name made lowercase.
    conditiondesc = models.CharField(db_column='conditionDesc', max_length=35)  # Field name made lowercase.
    conditioncode = models.CharField(db_column='conditionCode', max_length=35)  # Field name made lowercase.
    rejectreasondesc = models.CharField(db_column='rejectReasonDesc', max_length=80)  # Field name made lowercase.
    rejectreasoncode = models.CharField(db_column='rejectReasonCode', max_length=35)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'lb2Specimen'


