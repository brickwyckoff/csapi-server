from __future__ import unicode_literals

from django.db import models

class Utxdata(models.Model):
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    urinecollector = models.CharField(db_column='urineCollector', max_length=50, blank=True, null=True)  # Field name made lowercase.
    urineref = models.CharField(db_column='urineRef', max_length=15, blank=True, null=True)  # Field name made lowercase.
    urinecollecttime = models.CharField(db_column='urineCollectTime', max_length=15, blank=True, null=True)  # Field name made lowercase.
    urinereadtime = models.CharField(db_column='urineReadTime', max_length=15, blank=True, null=True)  # Field name made lowercase.
    urinereader = models.CharField(db_column='urineReader', max_length=50, blank=True, null=True)  # Field name made lowercase.
    urinetemp = models.FloatField(db_column='urineTemp', blank=True, null=True)  # Field name made lowercase.
    thc = models.IntegerField(blank=True, null=True)
    coc = models.IntegerField(blank=True, null=True)
    amp = models.IntegerField(blank=True, null=True)
    pcp = models.IntegerField(blank=True, null=True)
    opi = models.IntegerField(blank=True, null=True)
    met = models.IntegerField(blank=True, null=True)
    bar = models.IntegerField(blank=True, null=True)
    bzo = models.IntegerField(blank=True, null=True)
    mtd = models.IntegerField(blank=True, null=True)
    oxy = models.IntegerField(blank=True, null=True)
    bup = models.IntegerField(blank=True, null=True)
    senttolab = models.IntegerField(db_column='sentToLab', blank=True, null=True)  # Field name made lowercase.
    qcchecked = models.IntegerField(db_column='qcChecked', blank=True, null=True)  # Field name made lowercase.
    hcgqcchecked = models.IntegerField(db_column='hcgQcChecked', blank=True, null=True)  # Field name made lowercase.
    supurine = models.IntegerField(db_column='supUrine', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'utxData'


