from __future__ import unicode_literals

from django.db import models

class Counselinggroups(models.Model):
    groupname = models.CharField(db_column='groupName', max_length=100, blank=True, null=True)  # Field name made lowercase.
    dayofweek = models.IntegerField(db_column='dayOfWeek', blank=True, null=True)  # Field name made lowercase.
    starttime = models.TimeField(db_column='startTime', blank=True, null=True)  # Field name made lowercase.
    endtime = models.TimeField(db_column='endTime', blank=True, null=True)  # Field name made lowercase.
    counselor = models.CharField(max_length=20, blank=True, null=True)
    manager = models.CharField(max_length=20, blank=True, null=True)
    isactive = models.IntegerField(db_column='isActive', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'counselingGroups'


