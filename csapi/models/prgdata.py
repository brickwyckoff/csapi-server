from __future__ import unicode_literals

from django.db import models

class Prgdata(models.Model):
    id = models.BigAutoField(primary_key=True)
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    pregnant = models.IntegerField()
    source = models.CharField(max_length=20, blank=True, null=True)
    duedate = models.DateField(db_column='dueDate', blank=True, null=True)  # Field name made lowercase.
    mrn = models.BigIntegerField(blank=True, null=True)
    date = models.DateField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'prgData'


