from __future__ import unicode_literals

from django.db import models

class Smkdata(models.Model):
    id = models.BigAutoField(primary_key=True)
    visitnum = models.BigIntegerField(db_column='visitNum')  # Field name made lowercase.
    status = models.CharField(max_length=20)

    class Meta:
        managed = False
        db_table = 'smkData'


