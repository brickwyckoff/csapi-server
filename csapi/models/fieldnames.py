from __future__ import unicode_literals

from django.db import models

class Fieldnames(models.Model):
    fieldname = models.CharField(db_column='fieldName', max_length=60, blank=True, null=True)  # Field name made lowercase.
    fieldid = models.CharField(db_column='fieldId', max_length=60, blank=True, null=True)  # Field name made lowercase.
    rowname = models.CharField(db_column='rowName', max_length=60, blank=True, null=True)  # Field name made lowercase.
    indextype = models.IntegerField(db_column='indexType', blank=True, null=True)  # Field name made lowercase.
    otherinstances = models.CharField(db_column='otherInstances', max_length=150, blank=True, null=True)  # Field name made lowercase.
    lockable = models.IntegerField(blank=True, null=True)
    localtransfer = models.IntegerField(db_column='localTransfer', blank=True, null=True)  # Field name made lowercase.
    accesslevel = models.IntegerField(db_column='accessLevel', blank=True, null=True)  # Field name made lowercase.
    active = models.IntegerField(blank=True, null=True)
    valuetype = models.CharField(db_column='valueType', max_length=15, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'fieldNames'


