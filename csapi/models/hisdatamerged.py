from __future__ import unicode_literals

from django.db import models

class Hisdatamerged(models.Model):
    mrn = models.BigIntegerField(blank=True, null=True)
    nkda = models.IntegerField(blank=True, null=True)
    medhx = models.CharField(db_column='medHx', max_length=800, blank=True, null=True)  # Field name made lowercase.
    tobacco = models.IntegerField(blank=True, null=True)
    alcohol = models.IntegerField(blank=True, null=True)
    socialhx = models.CharField(db_column='socialHx', max_length=800, blank=True, null=True)  # Field name made lowercase.
    familyhx = models.CharField(db_column='familyHx', max_length=800, blank=True, null=True)  # Field name made lowercase.
    hepc = models.IntegerField(db_column='hepC')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'hisDataMerged'


