from __future__ import unicode_literals

from django.db import models

class Attestationtypes(models.Model):
    attestationtext = models.CharField(db_column='attestationText', max_length=300, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'attestationTypes'


