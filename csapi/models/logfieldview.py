from __future__ import unicode_literals

from django.db import models

class Logfieldview(models.Model):
    id = models.BigAutoField(primary_key=True)
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    user = models.CharField(max_length=30, blank=True, null=True)
    timein = models.DateTimeField(db_column='timeIn', blank=True, null=True)  # Field name made lowercase.
    timeout = models.DateTimeField(db_column='timeOut', blank=True, null=True)  # Field name made lowercase.
    field = models.CharField(max_length=50, blank=True, null=True)
    mrn = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'logFieldView'


