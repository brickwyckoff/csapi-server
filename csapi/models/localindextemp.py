from __future__ import unicode_literals

from django.db import models

class Localindextemp(models.Model):
    localmrn = models.BigIntegerField(db_column='localMrn', blank=True, null=True)  # Field name made lowercase.
    localvisitnum = models.BigIntegerField(db_column='localVisitNum', blank=True, null=True)  # Field name made lowercase.
    remotemrn = models.BigIntegerField(db_column='remoteMrn', blank=True, null=True)  # Field name made lowercase.
    remotevisitnum = models.BigIntegerField(db_column='remoteVisitNum', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'localIndexTemp'


