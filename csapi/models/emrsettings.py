from __future__ import unicode_literals

from django.db import models

class Emrsettings(models.Model):
    setting = models.CharField(max_length=30, blank=True, null=True)
    timezone = models.CharField(db_column='timeZone', max_length=100, blank=True, null=True)  # Field name made lowercase.
    maindoctor = models.CharField(db_column='mainDoctor', max_length=3, blank=True, null=True)  # Field name made lowercase.
    defaultprovider = models.CharField(db_column='defaultProvider', max_length=3, blank=True, null=True)  # Field name made lowercase.
    defaultsocialworker = models.CharField(db_column='defaultSocialWorker', max_length=3, blank=True, null=True)  # Field name made lowercase.
    siglock = models.IntegerField(db_column='sigLock', blank=True, null=True)  # Field name made lowercase.
    dragablepopups = models.IntegerField(db_column='dragablePopups', blank=True, null=True)  # Field name made lowercase.
    showbillinginfo = models.IntegerField(db_column='showBillingInfo', blank=True, null=True)  # Field name made lowercase.
    useoutsidecounselors = models.IntegerField(db_column='useOutsideCounselors', blank=True, null=True)  # Field name made lowercase.
    emrtype = models.CharField(db_column='emrType', max_length=10, blank=True, null=True)  # Field name made lowercase.
    eligibilityverify = models.IntegerField(db_column='eligibilityVerify', blank=True, null=True)  # Field name made lowercase.
    counselingtab = models.CharField(db_column='counselingTab', max_length=10, blank=True, null=True)  # Field name made lowercase.
    strikecolor = models.CharField(db_column='strikeColor', max_length=7, blank=True, null=True)  # Field name made lowercase.
    noshowcolor = models.CharField(db_column='noShowColor', max_length=7, blank=True, null=True)  # Field name made lowercase.
    usesupformd = models.IntegerField(db_column='useSupForMd', blank=True, null=True)  # Field name made lowercase.
    userxsupervisor = models.IntegerField(db_column='useRxSupervisor', blank=True, null=True)  # Field name made lowercase.
    allowcalendardelete = models.IntegerField(db_column='allowCalendarDelete', blank=True, null=True)  # Field name made lowercase.
    sendlabforcesupervisor = models.IntegerField(db_column='sendLabForceSupervisor', blank=True, null=True)  # Field name made lowercase.
    providersigndisclaimer = models.CharField(db_column='providerSignDisclaimer', max_length=500, blank=True, null=True)  # Field name made lowercase.
    usebilling = models.IntegerField(db_column='useBilling', blank=True, null=True)  # Field name made lowercase.
    uselexicomp = models.IntegerField(db_column='useLexiComp', blank=True, null=True)  # Field name made lowercase.
    usepatientportal = models.IntegerField(db_column='usePatientPortal', blank=True, null=True)  # Field name made lowercase.
    eprescribeurl = models.CharField(db_column='ePrescribeUrl', max_length=150, blank=True, null=True)  # Field name made lowercase.
    autologoutminutes = models.IntegerField(db_column='autoLogOutMinutes', blank=True, null=True)  # Field name made lowercase.
    drugalertlevel = models.IntegerField(db_column='drugAlertLevel', blank=True, null=True)  # Field name made lowercase.
    allergyalertlevel = models.IntegerField(db_column='allergyAlertLevel')  # Field name made lowercase.
    eprescribedbname = models.CharField(db_column='ePrescribeDbName', max_length=35)  # Field name made lowercase.
    eprescribedbip = models.CharField(db_column='ePrescribeDbIp', max_length=20)  # Field name made lowercase.
    eprescribedbport = models.CharField(db_column='ePrescribeDbPort', max_length=5)  # Field name made lowercase.
    eprescribedbuser = models.CharField(db_column='ePrescribeDbUser', max_length=20)  # Field name made lowercase.
    eprescribedbpassword = models.CharField(db_column='ePrescribeDbPassword', max_length=35)  # Field name made lowercase.
    allowincicd9 = models.IntegerField(db_column='allowIncIcd9')  # Field name made lowercase.
    lateappointmentcheck = models.IntegerField(db_column='lateAppointmentCheck')  # Field name made lowercase.
    billingreportnoencounter = models.IntegerField(db_column='billingReportNoEncounter')  # Field name made lowercase.
    showptlistonload = models.IntegerField(db_column='showPtListOnLoad')  # Field name made lowercase.
    useadvancedmd = models.IntegerField(db_column='useAdvancedMd', blank=True, null=True)  # Field name made lowercase.
    reqdxcptonsign = models.IntegerField(db_column='reqDxCptOnSign', blank=True, null=True)  # Field name made lowercase.
    hl7verifyoption = models.CharField(db_column='hl7verifyOption', max_length=20, blank=True, null=True)  # Field name made lowercase.
    conditionexactsearch = models.IntegerField(db_column='conditionExactSearch', blank=True, null=True)  # Field name made lowercase.
    inclinteractioncitations = models.IntegerField(db_column='inclInteractionCitations', blank=True, null=True)  # Field name made lowercase.
    cdsusersconditions = models.CharField(db_column='cdsUsersConditions', max_length=35)  # Field name made lowercase.
    cdsusersmedications = models.CharField(db_column='cdsUsersMedications', max_length=35)  # Field name made lowercase.
    cdsusersallergies = models.CharField(db_column='cdsUsersAllergies', max_length=35)  # Field name made lowercase.
    cdsusersdemographics = models.CharField(db_column='cdsUsersDemographics', max_length=35)  # Field name made lowercase.
    cdsuserslabs = models.CharField(db_column='cdsUsersLabs', max_length=35)  # Field name made lowercase.
    cdsusersvitals = models.CharField(db_column='cdsUsersVitals', max_length=35)  # Field name made lowercase.
    cdsusersdrugint = models.CharField(db_column='cdsUsersDrugInt', max_length=35)  # Field name made lowercase.
    cdsuserscondint = models.CharField(db_column='cdsUsersCondInt', max_length=35)  # Field name made lowercase.
    cdsusersalgint = models.CharField(db_column='cdsUsersAlgInt', max_length=35)  # Field name made lowercase.
    usesnomeddx = models.IntegerField(db_column='useSnomedDx')  # Field name made lowercase.
    usenursedropdown = models.IntegerField(db_column='useNurseDropdown')  # Field name made lowercase.
    usesnomedprocedures = models.IntegerField(db_column='useSnomedProcedures')  # Field name made lowercase.
    searchconditionsasdisorders = models.IntegerField(db_column='searchConditionsAsDisorders')  # Field name made lowercase.
    searchmedsgenflag = models.IntegerField(db_column='searchMedsGenFlag')  # Field name made lowercase.
    showemrclock = models.TextField(db_column='showEmrClock')  # Field name made lowercase.
    showfullclinicaddress = models.IntegerField(db_column='showFullClinicAddress')  # Field name made lowercase.
    usefunctionalstatus = models.IntegerField(db_column='useFunctionalStatus')  # Field name made lowercase.
    schedulefuturetests = models.IntegerField(db_column='scheduleFutureTests')  # Field name made lowercase.
    useprovidermiddlename = models.IntegerField(db_column='useProviderMiddleName')  # Field name made lowercase.
    newprovideralert = models.CharField(db_column='newProviderAlert', max_length=250, blank=True, null=True)  # Field name made lowercase.
    apptcalldays = models.IntegerField(db_column='apptCallDays', blank=True, null=True)  # Field name made lowercase.
    dischargeexcludeencounters = models.IntegerField(db_column='dischargeExcludeEncounters', blank=True, null=True)  # Field name made lowercase.
    useemercontactrelation = models.IntegerField(db_column='useEmerContactRelation', blank=True, null=True)  # Field name made lowercase.
    usedefaultpex = models.IntegerField(db_column='useDefaultPex', blank=True, null=True)  # Field name made lowercase.
    usepregnantflag = models.IntegerField(db_column='usePregnantFlag', blank=True, null=True)  # Field name made lowercase.
    usecounselingbilling = models.IntegerField(db_column='useCounselingBilling', blank=True, null=True)  # Field name made lowercase.
    usemedsuponcounseling = models.IntegerField(db_column='useMedSupOnCounseling', blank=True, null=True)  # Field name made lowercase.
    forceiframereload = models.IntegerField(db_column='forceIFrameReload', blank=True, null=True)  # Field name made lowercase.
    erxonly = models.IntegerField(db_column='erxOnly', blank=True, null=True)  # Field name made lowercase.
    useselfpayallpostings = models.IntegerField(db_column='useSelfPayAllPostings', blank=True, null=True)  # Field name made lowercase.
    usealtpcpinfo = models.IntegerField(db_column='useAltPcpInfo')  # Field name made lowercase.
    maindoctext = models.CharField(db_column='mainDocText', max_length=25)  # Field name made lowercase.
    useinsuitedoc = models.IntegerField(db_column='useInSuiteDoc')  # Field name made lowercase.
    entercodeonbilling = models.IntegerField(db_column='enterCodeOnBilling', blank=True, null=True)  # Field name made lowercase.
    useoptionalpriorauth = models.IntegerField(db_column='useOptionalPriorAuth', blank=True, null=True)  # Field name made lowercase.
    licenselevel = models.CharField(db_column='licenseLevel', max_length=35, blank=True, null=True)  # Field name made lowercase.
    useeligqueriesondemand = models.IntegerField(db_column='useEligQueriesOnDemand', blank=True, null=True)  # Field name made lowercase.
    useeligqueriesschedule = models.IntegerField(db_column='useEligQueriesSchedule', blank=True, null=True)  # Field name made lowercase.
    usetextapptreminder = models.IntegerField(db_column='useTextApptReminder', blank=True, null=True)  # Field name made lowercase.
    usevoiceapptreminder = models.IntegerField(db_column='useVoiceApptReminder', blank=True, null=True)  # Field name made lowercase.
    showlasttxpdate = models.IntegerField(db_column='showLastTxpDate', blank=True, null=True)  # Field name made lowercase.
    txpdaysagoalert = models.IntegerField(db_column='txpDaysAgoAlert', blank=True, null=True)  # Field name made lowercase.
    useclianumonall = models.IntegerField(db_column='useCliaNumOnAll', blank=True, null=True)  # Field name made lowercase.
    decreferralonbilling = models.IntegerField(db_column='decReferralOnBilling', blank=True, null=True)  # Field name made lowercase.
    showpasthxinentryorder = models.IntegerField(db_column='showPastHxInEntryOrder', blank=True, null=True)  # Field name made lowercase.
    entergroupasnewnote = models.IntegerField(db_column='enterGroupAsNewNote', blank=True, null=True)  # Field name made lowercase.
    useorderingprovideronallclaims = models.IntegerField(db_column='useOrderingProviderOnAllClaims', blank=True, null=True)  # Field name made lowercase.
    splitclaimsbynpi = models.IntegerField(db_column='splitClaimsByNpi', blank=True, null=True)  # Field name made lowercase.
    setnpionsubmit = models.IntegerField(db_column='setNpiOnSubmit', blank=True, null=True)  # Field name made lowercase.
    useprintfacesheet = models.IntegerField(db_column='usePrintFaceSheet', blank=True, null=True)  # Field name made lowercase.
    showbalancedem = models.IntegerField(db_column='showBalanceDem', blank=True, null=True)  # Field name made lowercase.
    commlogsources = models.CharField(db_column='commLogSources', max_length=200, blank=True, null=True)  # Field name made lowercase.
    usepostingsarchive = models.IntegerField(db_column='usePostingsArchive', blank=True, null=True)  # Field name made lowercase.
    checkinnumbertype = models.CharField(db_column='checkInNumberType', max_length=20, blank=True, null=True)  # Field name made lowercase.
    usedrugbilling = models.CharField(db_column='useDrugBilling', max_length=20, blank=True, null=True)  # Field name made lowercase.
    useposselect = models.IntegerField(db_column='usePosSelect', blank=True, null=True)  # Field name made lowercase.
    showpharmondem = models.IntegerField(db_column='showPharmOnDem', blank=True, null=True)  # Field name made lowercase.
    usepdmp = models.IntegerField(db_column='usePdmp', blank=True, null=True)  # Field name made lowercase.
    usemrnformvawc = models.IntegerField(db_column='useMrnForMvaWc', blank=True, null=True)  # Field name made lowercase.
    useprintmva = models.IntegerField(db_column='usePrintMva', blank=True, null=True)  # Field name made lowercase.
    usesetcptfee = models.IntegerField(db_column='useSetCptFee', blank=True, null=True)  # Field name made lowercase.
    useboardcertfields = models.IntegerField(db_column='useBoardCertFields', blank=True, null=True)  # Field name made lowercase.
    asamppcresource = models.IntegerField(db_column='asamPpcResource', blank=True, null=True)  # Field name made lowercase.
    superbillpdf = models.CharField(db_column='superbillPdf', max_length=50, blank=True, null=True)  # Field name made lowercase.
    superbilltpdf = models.CharField(db_column='superbillTPdf', max_length=50, blank=True, null=True)  # Field name made lowercase.
    usehospitalizationdates = models.IntegerField(db_column='useHospitalizationDates', blank=True, null=True)  # Field name made lowercase.
    useprobationflag = models.IntegerField(db_column='useProbationFlag', blank=True, null=True)  # Field name made lowercase.
    showclinicnameonly = models.IntegerField(db_column='showClinicNameOnly', blank=True, null=True)  # Field name made lowercase.
    numpostingrows = models.IntegerField(db_column='numPostingRows', blank=True, null=True)  # Field name made lowercase.
    counselingsuplabel = models.CharField(db_column='counselingSupLabel', max_length=35, blank=True, null=True)  # Field name made lowercase.
    usesecondaryphone = models.IntegerField(db_column='useSecondaryPhone', blank=True, null=True)  # Field name made lowercase.
    hpifieldlabel = models.CharField(db_column='hpiFieldLabel', max_length=35, blank=True, null=True)  # Field name made lowercase.
    useepa = models.IntegerField(db_column='useEpa', blank=True, null=True)  # Field name made lowercase.
    numfutureapptstoshow = models.IntegerField(db_column='numFutureApptsToShow', blank=True, null=True)  # Field name made lowercase.
    ptsearchbydob = models.IntegerField(db_column='ptSearchByDoB', blank=True, null=True)  # Field name made lowercase.
    pdflababnormalsinred = models.IntegerField(db_column='pdfLabAbnormalsInRed', blank=True, null=True)  # Field name made lowercase.
    eobautopost = models.IntegerField(db_column='eobAutopost', blank=True, null=True)  # Field name made lowercase.
    dischargeunderline = models.IntegerField(db_column='dischargeUnderline', blank=True, null=True)  # Field name made lowercase.
    usepronaccounting = models.IntegerField(db_column='usePrOnAccounting', blank=True, null=True)  # Field name made lowercase.
    storedclickrx = models.CharField(db_column='storedClickRx', max_length=200, blank=True, null=True)  # Field name made lowercase.
    useinsuranceonreviewpatients = models.IntegerField(db_column='useInsuranceOnReviewPatients')  # Field name made lowercase.
    useadditionalclaiminfo = models.IntegerField(db_column='useAdditionalClaimInfo', blank=True, null=True)  # Field name made lowercase.
    useerxoncsl = models.IntegerField(db_column='useErxOnCsl', blank=True, null=True)  # Field name made lowercase.
    providerdisplaytext = models.CharField(db_column='providerDisplayText', max_length=35, blank=True, null=True)  # Field name made lowercase.
    maindoctextdem = models.CharField(db_column='mainDocTextDem', max_length=35, blank=True, null=True)  # Field name made lowercase.
    chiefcomplainttext = models.CharField(db_column='chiefComplaintText', max_length=35, blank=True, null=True)  # Field name made lowercase.
    utoxtestids = models.CharField(db_column='utoxTestIds', max_length=150, blank=True, null=True)  # Field name made lowercase.
    useallusersonencounternote = models.IntegerField(db_column='useAllUsersOnEncounterNote', blank=True, null=True)  # Field name made lowercase.
    blockviewdischargedpt = models.IntegerField(db_column='blockViewDischargedPt', blank=True, null=True)  # Field name made lowercase.
    usepostingchangenotify = models.IntegerField(db_column='usePostingChangeNotify', blank=True, null=True)  # Field name made lowercase.
    showentityonstatement = models.IntegerField(db_column='showEntityOnStatement', blank=True, null=True)  # Field name made lowercase.
    use837i = models.IntegerField(db_column='use837I', blank=True, null=True)  # Field name made lowercase.
    signonceperchart = models.IntegerField(db_column='signOncePerChart')  # Field name made lowercase.
    showonlypramtonstmt = models.IntegerField(db_column='showOnlyPrAmtOnStmt', blank=True, null=True)  # Field name made lowercase.
    computeproviderpay = models.IntegerField(db_column='computeProviderPay')  # Field name made lowercase.
    showpttypehx = models.IntegerField(db_column='showPtTypeHx', blank=True, null=True)  # Field name made lowercase.
    showrxfieldsonhandp = models.IntegerField(db_column='showRxFieldsOnHandP')  # Field name made lowercase.
    usecsldxselect = models.IntegerField(db_column='useCslDxSelect', blank=True, null=True)  # Field name made lowercase.
    erxautoloadnumdays = models.IntegerField(db_column='erxAutoloadNumDays', blank=True, null=True)  # Field name made lowercase.
    excludecalendarusers = models.CharField(db_column='excludeCalendarUsers', max_length=100, blank=True, null=True)  # Field name made lowercase.
    receiptprintlink = models.CharField(db_column='receiptPrintLink', max_length=100, blank=True, null=True)  # Field name made lowercase.
    showvisitnumontracker = models.IntegerField(db_column='showVisitNumOnTracker')  # Field name made lowercase.
    usediagnotaxisoncsl = models.IntegerField(db_column='useDiagNotAxisOnCsl')  # Field name made lowercase.
    rightclickelementfx = models.CharField(db_column='rightClickElementFx', max_length=100, blank=True, null=True)  # Field name made lowercase.
    useapi = models.CharField(db_column='useApi', max_length=500, blank=True, null=True)  # Field name made lowercase.
    usecslsup = models.IntegerField(db_column='useCslSup', blank=True, null=True)  # Field name made lowercase.
    urgenttasks = models.CharField(db_column='urgentTasks', max_length=1000, blank=True, null=True)  # Field name made lowercase.
    printxdeaonsummary = models.IntegerField(db_column='printXdeaOnSummary', blank=True, null=True)  # Field name made lowercase.
    useclaimsubmitdaterange = models.IntegerField(db_column='useClaimSubmitDateRange', blank=True, null=True)  # Field name made lowercase.
    usedischargelineontimeline = models.IntegerField(db_column='useDischargeLineOnTimeline')  # Field name made lowercase.
    showdischargedateincalendar = models.IntegerField(db_column='showDischargeDateInCalendar')  # Field name made lowercase.
    calendaradminonlyviewdischargedpts = models.IntegerField(db_column='calendarAdminOnlyViewDischargedPts')  # Field name made lowercase.
    usemaincounseloronptsreport = models.IntegerField(db_column='useMainCounselorOnPtsReport')  # Field name made lowercase.
    usemse = models.IntegerField(db_column='useMse', blank=True, null=True)  # Field name made lowercase.
    remindertimeperiod = models.BigIntegerField(db_column='reminderTimePeriod', blank=True, null=True)  # Field name made lowercase.
    useportalassessments = models.IntegerField(db_column='usePortalAssessments', blank=True, null=True)  # Field name made lowercase.
    pastmedhxheader = models.CharField(db_column='pastMedHxHeader', max_length=50, blank=True, null=True)  # Field name made lowercase.
    insdropdownfornonadminusers = models.IntegerField(db_column='insDropdownForNonAdminUsers')  # Field name made lowercase.
    usesocialworkerstable = models.IntegerField(db_column='useSocialWorkersTable')  # Field name made lowercase.
    userossystems = models.IntegerField(db_column='useRosSystems')  # Field name made lowercase.
    useshowcurrenttxplanbutton = models.IntegerField(db_column='useShowCurrentTxPlanButton', blank=True, null=True)  # Field name made lowercase.
    usecslsummarybutton = models.IntegerField(db_column='useCslSummaryButton', blank=True, null=True)  # Field name made lowercase.
    usecslformsbutton = models.IntegerField(db_column='useCslFormsButton', blank=True, null=True)  # Field name made lowercase.
    userelapsepreventionform = models.IntegerField(db_column='useRelapsePreventionForm', blank=True, null=True)  # Field name made lowercase.
    claimspreviewdateoffset = models.IntegerField(db_column='claimsPreviewDateOffset', blank=True, null=True)  # Field name made lowercase.
    usefeedisplayonclaimspreview = models.IntegerField(db_column='useFeeDisplayOnClaimsPreview', blank=True, null=True)  # Field name made lowercase.
    usescrubonqueue = models.IntegerField(db_column='useScrubOnQueue', blank=True, null=True)  # Field name made lowercase.
    previewonlyunsentclaims = models.IntegerField(db_column='previewOnlyUnsentClaims', blank=True, null=True)  # Field name made lowercase.
    showlabresultsoncounselingsummary = models.IntegerField(db_column='showLabResultsOnCounselingSummary')  # Field name made lowercase.
    cslsiglock = models.IntegerField(db_column='cslSigLock')  # Field name made lowercase.
    usequicknotes = models.IntegerField(db_column='useQuickNotes')  # Field name made lowercase.
    usesignaturepad = models.IntegerField(db_column='useSignaturePad')  # Field name made lowercase.
    showinsuranceonmainscreen = models.IntegerField(db_column='showInsuranceOnMainScreen')  # Field name made lowercase.
    noeobdays = models.IntegerField(db_column='noEobDays', blank=True, null=True)  # Field name made lowercase.
    noclaimstatusdays = models.IntegerField(db_column='noClaimStatusDays', blank=True, null=True)  # Field name made lowercase.
    billingstartdate = models.DateField(db_column='billingStartDate', blank=True, null=True)  # Field name made lowercase.
    pdfabnormalsinred = models.IntegerField(db_column='pdfAbnormalsInRed')  # Field name made lowercase.
    showaccountingondemoonly = models.IntegerField(db_column='showAccountingOnDemoOnly')  # Field name made lowercase.
    showptnotesondemoonly = models.IntegerField(db_column='showPtNotesOnDemoOnly')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'emrSettings'


