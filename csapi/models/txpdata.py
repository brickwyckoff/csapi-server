from __future__ import unicode_literals

from django.db import models

class Txpdata(models.Model):
    visitnum = models.BigIntegerField(db_column='visitNum', blank=True, null=True)  # Field name made lowercase.
    axisi = models.CharField(db_column='axisI', max_length=200, blank=True, null=True)  # Field name made lowercase.
    axisii = models.CharField(db_column='axisII', max_length=200, blank=True, null=True)  # Field name made lowercase.
    axisiii = models.CharField(db_column='axisIII', max_length=200, blank=True, null=True)  # Field name made lowercase.
    axisiv = models.CharField(db_column='axisIV', max_length=200, blank=True, null=True)  # Field name made lowercase.
    axisv = models.CharField(db_column='axisV', max_length=200, blank=True, null=True)  # Field name made lowercase.
    treatmentreason = models.CharField(db_column='treatmentReason', max_length=700, blank=True, null=True)  # Field name made lowercase.
    shorttermgoal = models.CharField(db_column='shortTermGoal', max_length=700, blank=True, null=True)  # Field name made lowercase.
    longtermgoal = models.CharField(db_column='longTermGoal', max_length=700, blank=True, null=True)  # Field name made lowercase.
    shortterminterventions = models.CharField(db_column='shortTermInterventions', max_length=1000, blank=True, null=True)  # Field name made lowercase.
    shorttermsupports = models.CharField(db_column='shortTermSupports', max_length=1000, blank=True, null=True)  # Field name made lowercase.
    longterminterventions = models.CharField(db_column='longTermInterventions', max_length=1000, blank=True, null=True)  # Field name made lowercase.
    longtermsupports = models.CharField(db_column='longTermSupports', max_length=1000, blank=True, null=True)  # Field name made lowercase.
    adulted = models.IntegerField(db_column='adultEd')  # Field name made lowercase.
    mhref = models.IntegerField(db_column='mhRef')  # Field name made lowercase.
    detoxref = models.IntegerField(db_column='detoxRef')  # Field name made lowercase.
    iptref = models.IntegerField(db_column='iptRef')  # Field name made lowercase.
    medref = models.IntegerField(db_column='medRef')  # Field name made lowercase.
    familyrec = models.IntegerField(db_column='familyRec')  # Field name made lowercase.
    treatmentcomments = models.CharField(db_column='treatmentComments', max_length=1000)  # Field name made lowercase.
    progressdate1 = models.CharField(db_column='progressDate1', max_length=35)  # Field name made lowercase.
    progress1 = models.CharField(max_length=1000)
    progressdate2 = models.CharField(db_column='progressDate2', max_length=35)  # Field name made lowercase.
    progress2 = models.CharField(max_length=1000)
    grouptherapycount = models.CharField(db_column='groupTherapyCount', max_length=35)  # Field name made lowercase.
    smartgroupcount = models.CharField(db_column='smartGroupCount', max_length=35)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'txpData'


