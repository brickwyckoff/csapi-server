from __future__ import unicode_literals

from django.db import models

class Edilast271(models.Model):
    transactionnum = models.IntegerField(db_column='transactionNum', blank=True, null=True)  # Field name made lowercase.
    lastupdate = models.CharField(db_column='lastUpdate', max_length=12, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ediLast271'


