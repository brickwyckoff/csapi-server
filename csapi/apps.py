
from django.apps import AppConfig

class CsapiConfig (AppConfig):
    name = 'csapi'
    verbose_name = 'Clean Slate REST API Service'

    def __init__(self, app_name, app_module):
        super().__init__( app_name, app_module)

