
from django.db import models
import hashlib

class user (models.Model):
	class Meta:
		managed = False
		db_table = 'users'
		
	id = models.IntegerField (default=0, unique=True, primary_key=True)
	username = models.CharField (max_length=30)
	password = models.CharField (max_length=80)
	email = models.CharField (max_length=100)
	accessLevel = models.SmallIntegerField ()
	rxAccessLevel = models.SmallIntegerField ()
	emergencyAccess = models.SmallIntegerField ()
	reqSup = models.SmallIntegerField ()
	rxAdmin = models.SmallIntegerField ()
	asamContinuumUser = models.SmallIntegerField ()
	accessLevelSched = models.SmallIntegerField ()
	billingAccessLevel = models.SmallIntegerField ()

	def __unicode__ (self):
		return self.username	

	def as_dict(self):
		return {
			"id": self.id,
			"username": self.username,
			"email": self.email,
			"accessLevel": self.accessLevel,
			"rxAccessLevel": self.rxAccessLevel,
			"emergencyAccess": self.emergencyAccess,
		}
	# @staticmethod
	# def props(x):
	# 	# return dict((key, getattr(x, key)) for key in dir(x) if key not in dir(x.__class__))
	# 	return dict((key, getattr(x, key)) for key in dir(x) if not key.startswith ("_"))

	@staticmethod
	def getByUsername (uname):
		return User.objects.filter (username=uname)[0]
	
	def checkPassword (self, pw):
		sha = hashlib.sha1 (pw.encode('utf-8'))	
		return sha.hexdigest () == self.password
		
		