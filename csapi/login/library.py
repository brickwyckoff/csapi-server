
import base64

class BasicAuthorization:
    @staticmethod
    def ValidateBasicAuthorization (authData):

        if None == authData:
            return None

        if "" == authData:
            return None

        if not authData.startswith ("Basic "):
            return None

        try:
            decoded = base64.b64decode (authData [6:])  # returns a byte array
            parts = decoded.decode ("UTF-8").split (":")

            return {
                "username": parts [0],
                "password": parts[1],
            }
        except:
            return None

