
from django.views.decorators.http import require_POST, require_GET
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse #, HttpRequest, BadHeaderError
import sys

from csapi.services.loginService import LoginService
from csapi.services.jwtService import JwtService
from csapi.login.library import BasicAuthorization

@csrf_exempt
@require_GET
def checkCredentials(request):
	try:
		pwvalid = False

		if not 'HTTP_AUTHORIZATION' in request.META:
			return JsonResponse({"message": "Authentication required. No authorization header supplied."}, status=401);

		headerData = request.META ['HTTP_AUTHORIZATION']

		result = BasicAuthorization.ValidateBasicAuthorization (headerData)

		if not result:
			return JsonResponse({"message": "Invalid credentials supplied."}, status=401);

		un = result['username']
		pw = result['password']

		u = LoginService.login (un, pw)

		if not u is None:
			pwvalid = True

		if not pwvalid:
			print ("Password not matched, returning 401")
			return JsonResponse({"message": "Invalid credentials supplied."}, status=401);

		udict = u.as_dict ()
		udict["client_ip"] = request.META ['REMOTE_ADDR']

		t =  JwtService.buildToken (udict)

		# stash token result
		request.token = t.decode ("UTF-8")

		r = {
			'result': pwvalid,
		}

		return JsonResponse(r, status=200)
	except:
		print (sys.exc_info()[0])
		return JsonResponse({}, status=401)
