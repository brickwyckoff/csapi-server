
from threading import Thread, Lock, BoundedSemaphore
from django.db import connections
from csapi.core.dataSetToDict import DatasetToDictionaryConverter

class DbQueryThread (Thread):
    resultObject = []

    def __init__(self, query, dbKey = 'cleanslateCert'):
        self.query = query
        self.dbKey = dbKey
        print("dbQueryThread.py: Constructing for query: '{}'".format (self.query))
        Thread.__init__(self)

    def run(self):
        with connections[self.dbKey].cursor () as cursor:
            cursor.execute (self.query)
            # columns = [col[0] for col in cursor.description]
            #
            # self.resultObject = [
            #     dict (zip (columns, row))
            #     for row in cursor.fetchall()
            # ]

            self.resultObject = DatasetToDictionaryConverter.convertCursorData (cursor)

            print ("dbQueryThread.py: Query complete.".format (self.query))

            return
