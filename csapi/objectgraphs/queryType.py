import graphene

from csapi.models import patients
from csapi.objectgraphs.types.patientsType import patientsGraph


class QueryType (graphene.ObjectType):
    name = 'Query'
    decription = 'Required object graph root type'

    all_patients = graphene.List(
        patientsGraph, description='Returns all patients'
    )
    patients = graphene.Field (
        patientsGraph,
        mrn = graphene.Int ()
    )
    # all_visits = graphene.List( VisitsType)

    # all_charges = graphene.List(AccountingChargedPermType)

    # def resolve_all_charges (self, args, request, info):
    #     return AccountingChargedperm.objects.all()
    # def resolve_all_visits(self, args, request, info):
    #     return Visits.objects.all()
    def resolve_all_patients (self, args, request, info):
        return  patients.Patients.objects.all()
    def resolve_patients (self, args, request, info):
        id = args.get('mrn')
        return patients.Patients.objects.get(pk=id)
