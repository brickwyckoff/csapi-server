
import graphene

from csapi.objectgraphs.queryType import QueryType

schema = graphene.Schema(query=QueryType)