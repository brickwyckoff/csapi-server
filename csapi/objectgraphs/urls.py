
from django.conf.urls import url, include
from django.views.decorators.csrf import csrf_exempt
from csapi.objectgraphs.schema import schema
from graphene_django.views import GraphQLView

urlpatterns = [
    url(r'^graphiql', include('django_graphiql.urls')),
    url(r'^graphql', csrf_exempt(GraphQLView.as_view(schema=schema))),
# url(r'^graphql', GraphQLView.as_view(graphiql=True)),
]