import graphene

class PastmedhxdataimportedType(graphene.ObjectType):
    mrn  = graphene.Int()
    code  = graphene.String()
    altcode  = graphene.String()
    codetable  = graphene.String()
    freetext  = graphene.String()
    startdate  = graphene.String()
    enddate  = graphene.String()
    isactive  = graphene.Int()
    isresolved  = graphene.Int()
    batchid  = graphene.Int()
    importstatus  = graphene.Int()
    updatedate  = graphene.String()
    onsetage  = graphene.Int()
    relation  = graphene.String()
    infosource  = graphene.String()
