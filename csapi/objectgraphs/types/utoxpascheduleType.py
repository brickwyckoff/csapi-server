import graphene

class UtoxpascheduleType(graphene.ObjectType):
    id  = graphene.Int()
    insco  = graphene.String()
    numutoxperpa  = graphene.Int()
    numdaysperpa  = graphene.Int()
    isactive  = graphene.Int()
    cptcodes  = graphene.String()
    countstart  = graphene.Int()
