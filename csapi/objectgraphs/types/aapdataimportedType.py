import graphene

class AapdataimportedType(graphene.ObjectType):
    mrn  = graphene.Int()
    assessandplan  = graphene.String()
    futurecaregoals  = graphene.String()
    futurecareinstructions  = graphene.String()
    batchid  = graphene.Int()
