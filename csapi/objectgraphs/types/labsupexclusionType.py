import graphene

class LabsupexclusionType(graphene.ObjectType):
    id  = graphene.Int()
    insco  = graphene.String()
    provider  = graphene.String()
    switchprovider  = graphene.String()
