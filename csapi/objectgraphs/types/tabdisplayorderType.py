import graphene

class TabdisplayorderType(graphene.ObjectType):
    tabid  = graphene.Int()
    taborder  = graphene.Int()
    accesslevel  = graphene.Int()
