import graphene

class Changelogcleanslate090214BType(graphene.ObjectType):
    field  = graphene.String()
    fielddisplay  = graphene.String()
    oldvalue  = graphene.String()
    newvalue  = graphene.String()
    visitnum  = graphene.Int()
    mrn  = graphene.Int()
    user  = graphene.String()
    changetime  = graphene.String()
    actiontype  = graphene.String()
