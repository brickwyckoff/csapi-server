import graphene

class RxfaxoptionsType(graphene.ObjectType):
    optiontype  = graphene.String()
    optionvalue  = graphene.String()
    optiondisplay  = graphene.String()
    lexicode  = graphene.String()
