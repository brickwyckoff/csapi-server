import graphene

class OrddataType(graphene.ObjectType):
    visitnum  = graphene.Int()
    orderid  = graphene.Int()
    orderby  = graphene.String()
    ordertime  = graphene.String()
    fillby  = graphene.String()
    filltime  = graphene.String()
