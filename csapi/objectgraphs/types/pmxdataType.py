import graphene

class PmxdataType(graphene.ObjectType):
    mrn  = graphene.Int()
    socialother  = graphene.String()
    familyother  = graphene.String()
