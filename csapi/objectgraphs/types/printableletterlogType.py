import graphene

class PrintableletterlogType(graphene.ObjectType):
    id  = graphene.Int()
    mrn  = graphene.Int()
    user  = graphene.String()
    printtime  = graphene.String()
    letterid  = graphene.Int()
