import graphene

class ImmediaterxavailableType(graphene.ObjectType):
    id  = graphene.Int()
    code  = graphene.String()
    remaining  = graphene.Int()
