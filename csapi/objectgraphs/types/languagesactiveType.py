import graphene

class LanguagesactiveType(graphene.ObjectType):
    id  = graphene.Int()
    alphathreecode  = graphene.String()
