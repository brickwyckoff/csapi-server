import graphene

class RvwdataType(graphene.ObjectType):
    visitnum  = graphene.Int()
    medhx  = graphene.Int()
    socialhx  = graphene.Int()
    familyhx  = graphene.Int()
    lastvisit  = graphene.Int()
    vitals  = graphene.Int()
    ptreminders  = graphene.Int()
    utox  = graphene.Int()
    drugscreenord  = graphene.Int()
