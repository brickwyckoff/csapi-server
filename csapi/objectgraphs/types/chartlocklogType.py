import graphene

class ChartlocklogType(graphene.ObjectType):
    id  = graphene.Int()
    visitnum  = graphene.Int()
    user  = graphene.String()
    lockmessage  = graphene.String()
