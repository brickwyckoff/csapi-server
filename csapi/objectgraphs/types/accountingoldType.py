import graphene

class AccountingoldType(graphene.ObjectType):
    type  = graphene.String()
    claimamt  = graphene.Float()
    creationtime  = graphene.String()
    visitnum  = graphene.Int()
    firstname  = graphene.String()
    lastname  = graphene.String()
    mrn  = graphene.Int()
    chargecredit  = graphene.String()
    code  = graphene.String()
    codetype  = graphene.String()
    enteredby  = graphene.String()
    visitloc  = graphene.String()
    controlnumber  = graphene.String()
    payerid  = graphene.String()
    eobdate  = graphene.String()
