import graphene

class CheckupdatelogType(graphene.ObjectType):
    mrn  = graphene.Int()
    checktime  = graphene.String()
    checktype  = graphene.String()
