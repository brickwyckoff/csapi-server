import graphene

class PrescriptionfrequencyType(graphene.ObjectType):
    code  = graphene.String()
    meaning  = graphene.String()
