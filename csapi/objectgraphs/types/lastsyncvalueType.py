import graphene

class LastsyncvalueType(graphene.ObjectType):
    mrn  = graphene.Int()
    field  = graphene.String()
    value  = graphene.String()
    synctime  = graphene.String()
