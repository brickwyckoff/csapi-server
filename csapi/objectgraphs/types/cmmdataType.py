import graphene

class CmmdataType(graphene.ObjectType):
    mrn  = graphene.Int()
    commtype  = graphene.String()
    commmessage  = graphene.String()
    commdate  = graphene.String()
    user  = graphene.String()
