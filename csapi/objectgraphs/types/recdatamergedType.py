import graphene

class RecdatamergedType(graphene.ObjectType):
    mrn  = graphene.Int()
    hcg  = graphene.Int()
    lfts  = graphene.Int()
    randomutoxfreq  = graphene.Int()
    hiv  = graphene.Int()
    hcv  = graphene.Int()
    hbv  = graphene.Int()
