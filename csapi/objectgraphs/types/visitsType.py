import graphene

class VisitsType(graphene.ObjectType):
    name = 'visits'
    decription = "Represents access to the 'visits' table data"

    id = graphene.Int()
    mrn  = graphene.Int()
    date  = graphene.String()
    visittime  = graphene.String()
    provider  = graphene.String()
    supervisor  = graphene.String()
    socialworker  = graphene.String()
    cosigner  = graphene.String()
    status  = graphene.Int()
    changetime  = graphene.String()
    visitloc  = graphene.Int()
    visittype  = graphene.Int()
    eligibilityverified  = graphene.String()
    eligibilitystatus  = graphene.Int()
    labsupervisor  = graphene.String()
    visittimestamp  = graphene.String()
    attester  = graphene.String()
    attestationtype  = graphene.Int()
    nurse  = graphene.String()
    insuitedoc  = graphene.String()
    socialworkersup  = graphene.String()
    billinghold  = graphene.String()

