import graphene

class EobapprovelogType(graphene.ObjectType):
    controlnumber  = graphene.String()
    user  = graphene.String()
    approvetime  = graphene.String()
    batchnumber  = graphene.String()
