import graphene

class CosdataType(graphene.ObjectType):
    cosigner  = graphene.String()
    clinicloc  = graphene.Int()
    day  = graphene.Int()
    starttime  = graphene.String()
    endtime  = graphene.String()
    date  = graphene.String()
