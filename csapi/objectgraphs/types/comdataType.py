import graphene

class ComdataType(graphene.ObjectType):
    mrn  = graphene.Int()
    visitnum  = graphene.Int()
    chiefcomplaint  = graphene.String()
    selectedtemplate  = graphene.String()
    templateid  = graphene.Int()
    quicknotes  = graphene.String()
