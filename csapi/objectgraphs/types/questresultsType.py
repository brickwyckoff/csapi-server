import graphene

class QuestresultsType(graphene.ObjectType):
    filename  = graphene.String()
    downloadtime  = graphene.String()
    processed  = graphene.Int()
    sentreceipt  = graphene.Int()
