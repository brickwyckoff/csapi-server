import graphene

class DispensinginventoryType(graphene.ObjectType):
    id  = graphene.Int()
    bottleid  = graphene.String()
    clinicid  = graphene.Int()
    drugid  = graphene.String()
    tradeid  = graphene.Int()
    genericbrand  = graphene.String()
    rxotc  = graphene.String()
    doseform  = graphene.Int()
    route  = graphene.Int()
    strength  = graphene.Int()
    originalamount  = graphene.Float()
    currentamount  = graphene.Float()
    addedby  = graphene.String()
    addeddate  = graphene.String()
    activedate  = graphene.String()
    isactive  = graphene.Int()
    isondeck  = graphene.Int()
