import graphene

class PatientremindersType(graphene.ObjectType):
    mrn  = graphene.Int()
    reminderid  = graphene.Int()
    timegenerated  = graphene.String()
    iscritical  = graphene.Int()
    reviewedby  = graphene.String()
    timereviewed  = graphene.String()
    freetext  = graphene.String()
