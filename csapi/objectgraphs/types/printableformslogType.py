import graphene

class PrintableformslogType(graphene.ObjectType):
    id  = graphene.Int()
    mrn  = graphene.Int()
    formtype  = graphene.String()
    creationtime  = graphene.String()
    sendmethod  = graphene.String()
    user  = graphene.String()
    filename  = graphene.String()
