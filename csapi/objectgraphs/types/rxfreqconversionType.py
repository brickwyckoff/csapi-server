import graphene

class RxfreqconversionType(graphene.ObjectType):
    textfreq  = graphene.String()
    numfreq  = graphene.Float()
