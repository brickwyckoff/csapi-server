import graphene

class AccountsreceivableType(graphene.ObjectType):
    visitnum  = graphene.Int()
    insco  = graphene.String()
    amount  = graphene.Float()
    creationtime  = graphene.String()
    haspr  = graphene.Int()
    charge  = graphene.Float()
    adjustment  = graphene.Float()
    insbalance  = graphene.Float()
    inspayment  = graphene.Float()
    ptbalance  = graphene.Float()
    ptpayment  = graphene.Float()
    agingtime  = graphene.String()
    code  = graphene.String()
    pendingresponse  = graphene.Int()
