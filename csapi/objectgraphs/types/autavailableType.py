import graphene

class AutavailableType(graphene.ObjectType):
    mrn  = graphene.Int()
    prescription  = graphene.String()
    freq  = graphene.String()
    qty  = graphene.String()
    route  = graphene.String()
    refills  = graphene.String()
    signature  = graphene.String()
    signtime  = graphene.String()
    useddate  = graphene.String()
    usedvisitnum  = graphene.Int()
