import graphene

class CommonconditionsType(graphene.ObjectType):
    typecode  = graphene.String()
    code  = graphene.String()
    codetable  = graphene.String()
    freetext  = graphene.String()
