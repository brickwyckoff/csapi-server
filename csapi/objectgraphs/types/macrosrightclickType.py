import graphene

class MacrosrightclickType(graphene.ObjectType):
    macrosection  = graphene.String()
    macroid  = graphene.String()
    macrodisplay  = graphene.String()
    macrotext  = graphene.String()
    isactive  = graphene.Int()
    displayorder  = graphene.Int()
