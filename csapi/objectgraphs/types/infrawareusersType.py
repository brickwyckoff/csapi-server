import graphene

class InfrawareusersType(graphene.ObjectType):
    id  = graphene.Int()
    code  = graphene.String()
    authorid  = graphene.String()
    isdefault  = graphene.Int()
