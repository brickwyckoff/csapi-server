import graphene

class EobcheckinfoType(graphene.ObjectType):
    id  = graphene.Int()
    checkamt  = graphene.Float()
    checkdate  = graphene.String()
    checknumber  = graphene.String()
    checktype  = graphene.String()
    batchnumber  = graphene.String()
    enteredbatchamount  = graphene.Float()
