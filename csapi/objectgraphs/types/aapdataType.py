import graphene

class AapdataType(graphene.ObjectType):
    visitnum  = graphene.Int()
    assessandplan  = graphene.String()
    futurecaregoals1  = graphene.String()
    futurecareinstructions1  = graphene.String()
    futurecaregoals2  = graphene.String()
    futurecareinstructions2  = graphene.String()
    preventativehealth  = graphene.String()
    concerndiversionrelapse  = graphene.Int()
    utoxnoteentered  = graphene.Int()
