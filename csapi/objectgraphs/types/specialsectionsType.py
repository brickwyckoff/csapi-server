import graphene

class SpecialsectionsType(graphene.ObjectType):
    code  = graphene.String()
    name  = graphene.String()
    filename  = graphene.String()
    active  = graphene.Int()
    ispttype  = graphene.Int()
    sectionorder  = graphene.Int()
    useonencounternote  = graphene.Int()
    description  = graphene.String()
