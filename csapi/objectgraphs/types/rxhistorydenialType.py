import graphene

class RxhistorydenialType(graphene.ObjectType):
    mrn  = graphene.Int()
    denialcode  = graphene.String()
    denialtext  = graphene.String()
    creationtime  = graphene.String()
    payorname  = graphene.String()
    memberid  = graphene.String()
