import graphene

class ClaimrefnumbersType(graphene.ObjectType):
    payerid  = graphene.String()
    visitnum  = graphene.Int()
    refnumber  = graphene.String()
    creationtime  = graphene.String()
