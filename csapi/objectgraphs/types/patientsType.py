import graphene
from csapi.models.accounting_chargedpermtemp import AccountingChargedpermtemp
from csapi.models.visits import Visits

from csapi.models.demdata import Demdata
from csapi.objectgraphs.types.accountingChargedPermType import AccountingChargedPermType
from csapi.objectgraphs.types.demdataType import demdataType
from csapi.objectgraphs.types.visitsType import VisitsType


class patientsGraph (graphene.ObjectType):
    name = 'patients'
    decription = "Represents access to the 'patients' table data"

    mrn = graphene.Int ()
    firstname = graphene.String ()
    lastname = graphene.String ()
    demdata = graphene.Field (demdataType)
    charges = graphene.List(AccountingChargedPermType)
    visits = graphene.List(VisitsType)
    #
    def resolve_visits(self, args, request, info):
        id = self.mrn
        return Visits.objects.filter(mrn=id)
    def resolve_demdata (self, args, request, info):
        id = self.mrn
        return Demdata.objects.filter (mrn=id).first ()

    def resolve_charges (self, args, request, info):
        id = self.mrn
        return AccountingChargedpermtemp.objects.filter(mrn=id)