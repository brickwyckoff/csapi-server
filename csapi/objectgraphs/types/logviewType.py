import graphene

class LogviewType(graphene.ObjectType):
    visitnum  = graphene.Int()
    mrn  = graphene.Int()
    user  = graphene.String()
    timein  = graphene.String()
    timeout  = graphene.String()
    ip  = graphene.String()
