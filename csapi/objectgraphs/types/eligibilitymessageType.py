import graphene

class EligibilitymessageType(graphene.ObjectType):
    verifiedid  = graphene.Int()
    visitnum  = graphene.Int()
    plandescription  = graphene.String()
    message  = graphene.String()
    ebid  = graphene.Int()
