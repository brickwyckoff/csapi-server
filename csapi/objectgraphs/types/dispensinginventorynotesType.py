import graphene

class DispensinginventorynotesType(graphene.ObjectType):
    id  = graphene.Int()
    dispensinginventoryid  = graphene.Int()
    note  = graphene.String()
    newamount  = graphene.Float()
    user  = graphene.String()
    entrytime  = graphene.String()
