import graphene

class DatelogtempType(graphene.ObjectType):
    id  = graphene.Int()
    apptime  = graphene.String()
    dbtime  = graphene.String()
