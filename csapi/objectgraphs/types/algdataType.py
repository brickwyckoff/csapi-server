import graphene

class AlgdataType(graphene.ObjectType):
    mrn  = graphene.Int()
    allergy  = graphene.String()
    creationtime  = graphene.String()
