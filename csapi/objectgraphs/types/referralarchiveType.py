import graphene

class ReferralarchiveType(graphene.ObjectType):
    id  = graphene.Int()
    mrn  = graphene.Int()
    referral  = graphene.String()
    pcp  = graphene.Int()
    startdate  = graphene.String()
    enddate  = graphene.String()
