import graphene

class ReferrallogType(graphene.ObjectType):
    id  = graphene.Int()
    mrn  = graphene.Int()
    visitnum  = graphene.Int()
    username  = graphene.String()
    provider  = graphene.String()
    requesteddate  = graphene.String()
    reasonforreferral  = graphene.String()
    directdirectoryid  = graphene.Int()
    directaddress  = graphene.String()
    sendtime  = graphene.String()
