import graphene

class EligibilitybadinfoType(graphene.ObjectType):
    visitnum  = graphene.Int()
    badmessage  = graphene.String()
    creationtime  = graphene.String()
    controlnum  = graphene.String()
