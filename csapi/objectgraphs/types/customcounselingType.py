import graphene

class CustomcounselingType(graphene.ObjectType):
    topcenter  = graphene.String()
    topright  = graphene.String()
    bottomcenter  = graphene.String()
    bottomright  = graphene.String()
