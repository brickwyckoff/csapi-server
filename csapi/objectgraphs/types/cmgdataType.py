import graphene

class CmgdataType(graphene.ObjectType):
    id  = graphene.Int()
    mrn  = graphene.Int()
    clinician  = graphene.String()
    casemgr  = graphene.String()
    biopsycosocialdate  = graphene.String()
    initialtxplandate  = graphene.String()
    indtxplandate  = graphene.String()
    txplanreviewdate  = graphene.String()
    projdxdate  = graphene.String()
    dxreason  = graphene.String()
