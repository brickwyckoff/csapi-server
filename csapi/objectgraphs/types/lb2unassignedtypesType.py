import graphene

class Lb2UnassignedtypesType(graphene.ObjectType):
    isassigned  = graphene.Int()
    description  = graphene.String()
