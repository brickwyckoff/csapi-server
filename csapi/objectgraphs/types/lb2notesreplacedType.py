import graphene

class Lb2NotesreplacedType(graphene.ObjectType):
    mrn  = graphene.Int()
    visitnum  = graphene.Int()
    messagecontrolid  = graphene.String()
    labid  = graphene.String()
    note  = graphene.String()
