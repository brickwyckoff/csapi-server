import graphene

class AccesslevelsType(graphene.ObjectType):
    levelcode  = graphene.Int()
    leveldescription  = graphene.String()
    defaulttab  = graphene.Int()
    remoteaccess  = graphene.Int()
    reportsonly  = graphene.Int()
