import graphene

class CustomlabalertsType(graphene.ObjectType):
    labloc  = graphene.String()
    labcode  = graphene.String()
    checkvalue  = graphene.String()
    checktype  = graphene.String()
    alertmessage  = graphene.String()
    isactive  = graphene.Int()
