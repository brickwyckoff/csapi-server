import graphene

class DjangoMigrationsType(graphene.ObjectType):
    app  = graphene.String()
    name  = graphene.String()
    applied  = graphene.String()
