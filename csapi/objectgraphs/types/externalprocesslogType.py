import graphene

class ExternalprocesslogType(graphene.ObjectType):
    id  = graphene.Int()
    processname  = graphene.String()
    visitdate  = graphene.String()
    processtime  = graphene.String()
