import graphene

class PrintablelettersType(graphene.ObjectType):
    id  = graphene.Int()
    lettername  = graphene.String()
    lettercontent  = graphene.String()
    isactive  = graphene.Int()
    infotoset  = graphene.String()
