import graphene

class SharedpatientsType(graphene.ObjectType):
    mrn  = graphene.Int()
    childdb  = graphene.String()
