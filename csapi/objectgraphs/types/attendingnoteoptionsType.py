import graphene

class AttendingnoteoptionsType(graphene.ObjectType):
    id  = graphene.Int()
    attendingnotetext  = graphene.String()
    active  = graphene.Int()
    displayorder  = graphene.Int()
