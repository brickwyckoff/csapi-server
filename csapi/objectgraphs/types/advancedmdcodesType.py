import graphene

class AdvancedmdcodesType(graphene.ObjectType):
    codetype  = graphene.String()
    code  = graphene.String()
    advancedmdcode  = graphene.String()
    visitloc  = graphene.Int()
    isdefault  = graphene.Int()
