import graphene

class ProcessedScansType(graphene.ObjectType):
    scandate  = graphene.String()
    scanfilename  = graphene.String()
    scanfilesize  = graphene.Int()
