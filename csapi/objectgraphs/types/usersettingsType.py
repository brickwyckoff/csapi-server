import graphene

class UsersettingsType(graphene.ObjectType):
    id  = graphene.Int()
    username  = graphene.String()
    practicemanager  = graphene.Int()
