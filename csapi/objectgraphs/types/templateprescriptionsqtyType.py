import graphene

class TemplateprescriptionsqtyType(graphene.ObjectType):
    prescriptionclass  = graphene.String()
    qty  = graphene.String()
