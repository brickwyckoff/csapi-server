import graphene

class DjangoContentTypeType(graphene.ObjectType):
    app_label  = graphene.String()
    model  = graphene.String()
