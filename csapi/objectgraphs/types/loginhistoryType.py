import graphene

class LoginhistoryType(graphene.ObjectType):
    username  = graphene.String()
    time  = graphene.String()
    ip  = graphene.String()
    valid  = graphene.Int()
    serverip  = graphene.String()
