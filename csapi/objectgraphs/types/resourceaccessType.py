import graphene

class ResourceaccessType(graphene.ObjectType):
    resourcename  = graphene.String()
    resourceusername  = graphene.String()
    resourcepassword  = graphene.String()
    resourceversion  = graphene.String()
    resourceother  = graphene.String()
    resourcetoken  = graphene.String()
    clinicid  = graphene.Int()
    resourceurl  = graphene.String()
