import graphene

class DecisionsupportviewlogType(graphene.ObjectType):
    id  = graphene.Int()
    mrn  = graphene.Int()
    supportid  = graphene.Int()
    supporttitle  = graphene.String()
    user  = graphene.String()
    viewtime  = graphene.String()
