import graphene

from csapi.models.visits import Visits
from csapi.objectgraphs.types.visitsType import VisitsType


class AccountingChargedPermType(graphene.ObjectType):
    name = 'AccountingChargedPerm'
    decription = "Represents access to the 'AccountingChargedPerm' table data"

    type = graphene.String()
    claimamt = graphene.Float()
    creationtime = graphene.String()
    visitnum = graphene.Int()
    mrn = graphene.Int()
    chargecredit = graphene.String()
    code = graphene.String()
    procqty = graphene.String()
    proccodetype = graphene.String()
    codetype = graphene.String()
    enteredby = graphene.String()
    controlnumber = graphene.String()
    inscorefnum =graphene.String()
    payerid = graphene.String()
    eobdate = graphene.String()
    postdate = graphene.String()
    processid = graphene.String()
    policynum = graphene.String()
    setcontrolnumber =graphene.String()
    npi = graphene.String()
    firstname = graphene.String()
    lastname = graphene.String()
    visitdate = graphene.String()
    batchnumber = graphene.String()
    billfrom = graphene.String()
    pos = graphene.String()
    claimentitytype = graphene.String()
    billingprovider = graphene.String()
    replaced = graphene.Int()
    insuranceorder = graphene.Int()
    claimsuffix = graphene.String()

    visit = graphene.Field(VisitsType)

    def resolve_visit(self, args, request, info):
        id = self.visitnum
        return Visits.objects.get(id=id)

