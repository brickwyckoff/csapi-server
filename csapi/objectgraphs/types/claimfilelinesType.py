import graphene

class ClaimfilelinesType(graphene.ObjectType):
    batchcontrolnumber  = graphene.String()
    visitnum  = graphene.Int()
    clientdatabase  = graphene.String()
    linetype  = graphene.String()
    claimline  = graphene.String()
    creationtime  = graphene.String()
