import graphene

class MspformmetaType(graphene.ObjectType):
    categoryname  = graphene.String()
    datafield  = graphene.String()
    displaytext  = graphene.String()
    pageordinal  = graphene.Int()
    listordinal  = graphene.Int()
