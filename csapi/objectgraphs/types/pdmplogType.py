import graphene

class PdmplogType(graphene.ObjectType):
    mrn  = graphene.Int()
    visitnum  = graphene.Int()
    entrydate  = graphene.String()
    checkeddate  = graphene.String()
    user  = graphene.String()
