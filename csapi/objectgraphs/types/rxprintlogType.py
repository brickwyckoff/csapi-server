import graphene

class RxprintlogType(graphene.ObjectType):
    mrn  = graphene.Int()
    prescription  = graphene.String()
    freq  = graphene.String()
    qty  = graphene.String()
    route  = graphene.String()
    refills  = graphene.String()
    prescriber  = graphene.String()
    printtime  = graphene.String()
    pharmacy  = graphene.Int()
    visitnum  = graphene.Int()
    isfax  = graphene.Int()
    filldate  = graphene.String()
    faxdate  = graphene.String()
    filldateoverride  = graphene.String()
