import graphene

class FeetableType(graphene.ObjectType):
    code  = graphene.String()
    fee  = graphene.Int()
    locationcode  = graphene.String()
    npi  = graphene.String()
    selfpayonly  = graphene.Int()
    inclreferral  = graphene.Int()
    islabcode  = graphene.Int()
    noprovider  = graphene.Int()
