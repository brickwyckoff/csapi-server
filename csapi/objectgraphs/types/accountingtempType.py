import graphene

class AccountingtempType(graphene.ObjectType):
    id  = graphene.Int()
    visitnum  = graphene.Int()
    sessionid  = graphene.String()
    postdate  = graphene.String()
    proccode  = graphene.String()
    insco  = graphene.String()
    charge  = graphene.String()
    payment  = graphene.String()
    credit  = graphene.String()
    deductible  = graphene.String()
    adjustment  = graphene.String()
    coins  = graphene.String()
    copay  = graphene.String()
    pending  = graphene.String()
    denial  = graphene.String()
    enteredby  = graphene.String()
