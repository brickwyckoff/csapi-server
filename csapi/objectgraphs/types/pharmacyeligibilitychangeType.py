import graphene

class PharmacyeligibilitychangeType(graphene.ObjectType):
    id  = graphene.Int()
    controlnumber  = graphene.String()
    mrn  = graphene.Int()
    firstname  = graphene.String()
    lastname  = graphene.String()
    middlename  = graphene.String()
    dob  = graphene.String()
    address1  = graphene.String()
    address2  = graphene.String()
    city  = graphene.String()
    state  = graphene.String()
    zip  = graphene.String()
    gender  = graphene.String()
