import graphene

class SignatureexpType(graphene.ObjectType):
    id  = graphene.Int()
    sigtype  = graphene.String()
    pttype  = graphene.String()
    visittype  = graphene.String()
    isnewpt  = graphene.Int()
    sigtext  = graphene.String()
    isonsite  = graphene.Int()
