import graphene

class RxbatchlogType(graphene.ObjectType):
    id  = graphene.Int()
    prescriptionlogid  = graphene.Int()
    batchtype  = graphene.String()
    batchid  = graphene.Int()
