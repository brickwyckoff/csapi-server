import graphene

class AssessmentdataType(graphene.ObjectType):
    id  = graphene.Int()
    mrn  = graphene.Int()
    assesscode  = graphene.String()
    assessdate  = graphene.String()
    assessvaluenum  = graphene.Int()
    assesstype  = graphene.String()
