import graphene

class ProviderstateinfochangelogType(graphene.ObjectType):
    id  = graphene.Int()
    code  = graphene.String()
    providerstateinfoid  = graphene.Int()
    field  = graphene.String()
    oldvalue  = graphene.String()
    newvalue  = graphene.String()
    user  = graphene.String()
    changetime  = graphene.String()
