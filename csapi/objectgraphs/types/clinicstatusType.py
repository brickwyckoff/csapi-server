import graphene

class ClinicstatusType(graphene.ObjectType):
    status  = graphene.String()
    code  = graphene.Int()
    displayorder  = graphene.Int()
    clinicgroup  = graphene.Int()
