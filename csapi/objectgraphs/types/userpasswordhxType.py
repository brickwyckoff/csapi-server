import graphene

class UserpasswordhxType(graphene.ObjectType):
    id  = graphene.Int()
    username  = graphene.String()
    password  = graphene.String()
    createtime  = graphene.String()
