import graphene

class PatientsdeletedType(graphene.ObjectType):
    mrn  = graphene.String()
    firstname  = graphene.String()
    lastname  = graphene.String()
    dob  = graphene.String()
    city  = graphene.String()
    state  = graphene.String()
    changetime  = graphene.String()
    indatabase  = graphene.String()
    foreignmrn  = graphene.Int()
    activepatient  = graphene.Int()
