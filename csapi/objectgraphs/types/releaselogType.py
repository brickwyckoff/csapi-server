import graphene

class ReleaselogType(graphene.ObjectType):
    mrn  = graphene.Int()
    releasedto  = graphene.String()
    releasedinfo  = graphene.String()
    releaseddate  = graphene.String()
    user  = graphene.String()
