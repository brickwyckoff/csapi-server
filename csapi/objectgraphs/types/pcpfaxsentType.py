import graphene

class PcpfaxsentType(graphene.ObjectType):
    visitnum  = graphene.Int()
    user  = graphene.String()
    sentto  = graphene.Int()
    creationtime  = graphene.String()
