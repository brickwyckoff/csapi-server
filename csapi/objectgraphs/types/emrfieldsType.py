import graphene

class EmrfieldsType(graphene.ObjectType):
    field  = graphene.String()
    section  = graphene.String()
