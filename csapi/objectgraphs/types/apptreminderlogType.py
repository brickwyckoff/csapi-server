import graphene

class ApptreminderlogType(graphene.ObjectType):
    id  = graphene.Int()
    mrn  = graphene.Int()
    tonumber  = graphene.Int()
    fromnumber  = graphene.Int()
    sendtime  = graphene.String()
