import graphene

class CliacodesType(graphene.ObjectType):
    id  = graphene.Int()
    code  = graphene.String()
    ordereronly  = graphene.Int()
