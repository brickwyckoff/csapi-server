import graphene

class AccountingundistributedType(graphene.ObjectType):
    mrn  = graphene.Int()
    undistamt  = graphene.Float()
