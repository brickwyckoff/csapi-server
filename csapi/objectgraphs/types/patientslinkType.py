import graphene

class PatientslinkType(graphene.ObjectType):
    mrn  = graphene.Int()
    mrnext  = graphene.String()
    linkname  = graphene.String()
    lastquerytime  = graphene.String()
