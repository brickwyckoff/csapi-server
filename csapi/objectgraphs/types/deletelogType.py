import graphene

class DeletelogType(graphene.ObjectType):
    deletedmrn  = graphene.Int()
    user  = graphene.String()
    mergetime  = graphene.String()
