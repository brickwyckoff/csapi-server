import graphene

class LabelsheetType(graphene.ObjectType):
    id  = graphene.Int()
    labelcode  = graphene.String()
    labeldesc  = graphene.String()
    isactive  = graphene.Int()
