import graphene

class PharmacyeliginfoType(graphene.ObjectType):
    controlnumber  = graphene.String()
    relatedto  = graphene.String()
    benefitstatus  = graphene.String()
    servicetype  = graphene.String()
    insurancetype  = graphene.String()
    plandescription  = graphene.String()
    monetaryamount  = graphene.String()
