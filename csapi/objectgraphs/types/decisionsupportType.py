import graphene

class DecisionsupportType(graphene.ObjectType):
    id  = graphene.Int()
    decisionrunlab  = graphene.String()
    decisionrunfreq  = graphene.Int()
    decisioncitation  = graphene.String()
    decisiondeveloper  = graphene.String()
    decisionfunding  = graphene.String()
    decisionreleasedate  = graphene.String()
    decisionrevisondate  = graphene.String()
    crittype1  = graphene.String()
    critvalue1  = graphene.String()
    crittype2  = graphene.String()
    critvalue2  = graphene.String()
    crittype3  = graphene.String()
    critvalue3  = graphene.String()
    crittype4  = graphene.String()
    critvalue4  = graphene.String()
    crittype5  = graphene.String()
    critvalue5  = graphene.String()
    crittype6  = graphene.String()
    critvalue6  = graphene.String()
    crittype7  = graphene.String()
    critvalue7  = graphene.String()
    crittype8  = graphene.String()
    critvalue8  = graphene.String()
    crittype9  = graphene.String()
    critvalue9  = graphene.String()
    crittype10  = graphene.String()
    critvalue10  = graphene.String()
    user  = graphene.String()
    addtime  = graphene.String()
    isactive  = graphene.Int()
    userlevel  = graphene.String()
