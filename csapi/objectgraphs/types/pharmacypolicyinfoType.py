import graphene

class PharmacypolicyinfoType(graphene.ObjectType):
    controlnumber  = graphene.String()
    mrn  = graphene.Int()
    checkdate  = graphene.String()
    policynumberqual  = graphene.String()
    policynumber  = graphene.String()
    policyname  = graphene.String()
    policypayorid  = graphene.String()
