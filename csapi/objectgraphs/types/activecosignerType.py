import graphene

class ActivecosignerType(graphene.ObjectType):
    clinicid  = graphene.Int()
    providercode  = graphene.String()
    validendtime  = graphene.String()
    validstarttime  = graphene.String()
