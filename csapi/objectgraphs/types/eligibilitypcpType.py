import graphene

class EligibilitypcpType(graphene.ObjectType):
    verifiedid  = graphene.Int()
    visitnum  = graphene.Int()
    firstname  = graphene.String()
    lastname  = graphene.String()
    address1  = graphene.String()
    address2  = graphene.String()
    city  = graphene.String()
    state  = graphene.String()
    zip  = graphene.String()
    phone  = graphene.String()
