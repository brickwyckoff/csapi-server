import graphene

class CalendarchangelogType(graphene.ObjectType):
    user  = graphene.String()
    changetime  = graphene.String()
    eventid  = graphene.Int()
    useraction  = graphene.String()
    table  = graphene.String()
    mrn  = graphene.Int()
