import graphene

class TreatmentcontracttextType(graphene.ObjectType):
    itemid  = graphene.String()
    itemtext  = graphene.String()
    itemdisplay  = graphene.String()
    itemtype  = graphene.String()
    hierlevel  = graphene.Int()
    parentid  = graphene.String()
    releaseto  = graphene.String()
