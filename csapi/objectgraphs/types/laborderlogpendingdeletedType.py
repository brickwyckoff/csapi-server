import graphene

class LaborderlogpendingdeletedType(graphene.ObjectType):
    mrn  = graphene.Int()
    visitnum  = graphene.Int()
    labcontrolid  = graphene.String()
    labcode  = graphene.String()
    sentto  = graphene.String()
    sentby  = graphene.String()
    creationtime  = graphene.String()
    datereviewed  = graphene.String()
    orderentered  = graphene.Int()
    enteredby  = graphene.String()
    entertime  = graphene.String()
    deleteby  = graphene.String()
    deletetime  = graphene.String()
