import graphene

class PtfaxlogType(graphene.ObjectType):
    id  = graphene.Int()
    mrn  = graphene.Int()
    faxto  = graphene.String()
    faxtonumber  = graphene.String()
    faxtype  = graphene.String()
    faxdate  = graphene.String()
