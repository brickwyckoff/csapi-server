import graphene

class PatientportalccdfilesType(graphene.ObjectType):
    id  = graphene.Int()
    mrn  = graphene.Int()
    ccdfilename  = graphene.String()
    generatedtime  = graphene.String()
