import graphene

class Cc1DataoldType(graphene.ObjectType):
    visitnum  = graphene.Int()
    alleviatesx  = graphene.Int()
    improvecoping  = graphene.Int()
    idstrategies  = graphene.Int()
    maintainstability  = graphene.Int()
    other  = graphene.Int()
