import graphene

class LevdataType(graphene.ObjectType):
    id  = graphene.Int()
    visitnum  = graphene.Int()
    treatmentlevel  = graphene.Int()
