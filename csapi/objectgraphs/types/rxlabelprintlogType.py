import graphene

class RxlabelprintlogType(graphene.ObjectType):
    id  = graphene.Int()
    mrn  = graphene.Int()
    rxid  = graphene.Int()
    user  = graphene.String()
    printtime  = graphene.String()
    visitloc  = graphene.Int()
