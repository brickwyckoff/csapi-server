import graphene

class NotificationusersType(graphene.ObjectType):
    username  = graphene.String()
    cellphone  = graphene.String()
    email  = graphene.String()
    notificationtype  = graphene.String()
