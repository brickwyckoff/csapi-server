import graphene

class RxdashboardtempType(graphene.ObjectType):
    id  = graphene.Int()
    insertquery  = graphene.String()
    prescription  = graphene.String()
    mrn  = graphene.String()
    rxid  = graphene.String()
    numperdose  = graphene.String()
    freq  = graphene.String()
    route  = graphene.String()
    qty  = graphene.String()
    refills  = graphene.String()
    prescriber  = graphene.String()
