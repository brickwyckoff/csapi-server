import graphene

class AccountingnotesType(graphene.ObjectType):
    notes  = graphene.String()
    creationtime  = graphene.String()
    visitnum  = graphene.Int()
    user  = graphene.String()
    mrn  = graphene.Int()
