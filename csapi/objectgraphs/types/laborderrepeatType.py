import graphene

class LaborderrepeatType(graphene.ObjectType):
    mrn  = graphene.Int()
    laborderid  = graphene.String()
    creationtime  = graphene.String()
    enteredby  = graphene.String()
    repeatdate  = graphene.String()
    ordered  = graphene.Int()
