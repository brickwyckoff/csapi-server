import graphene

class DmelistType(graphene.ObjectType):
    dmename  = graphene.String()
    cptcode  = graphene.String()
    qtyinstock  = graphene.Int()
