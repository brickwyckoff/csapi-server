import graphene

class LabresultlogrecentType(graphene.ObjectType):
    mrn  = graphene.Int()
    labcontrolid  = graphene.String()
    creationtime  = graphene.String()
    datereviewed  = graphene.String()
    reviewedby  = graphene.String()
