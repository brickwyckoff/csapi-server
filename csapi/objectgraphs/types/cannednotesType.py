import graphene

class CannednotesType(graphene.ObjectType):
    id  = graphene.Int()
    visittype  = graphene.Int()
    notetext  = graphene.String()
    notetable  = graphene.String()
    notefield  = graphene.String()
    isoverwrite  = graphene.Int()
    issup  = graphene.Int()
    isactive  = graphene.Int()
