import graphene

class RxpchangelogType(graphene.ObjectType):
    id  = graphene.Int()
    mrn  = graphene.Int()
    startrefills  = graphene.String()
    endrefills  = graphene.String()
    changetime  = graphene.String()
