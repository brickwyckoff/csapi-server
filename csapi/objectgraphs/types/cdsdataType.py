import graphene

class CdsdataType(graphene.ObjectType):
    visitnum  = graphene.Int()
    intakedate  = graphene.String()
    dischargedate  = graphene.String()
    intakegaf  = graphene.String()
    dischargegaf  = graphene.String()
    dischargereason  = graphene.String()
    initialdx  = graphene.String()
    txgoals  = graphene.String()
    interventions  = graphene.String()
    progress  = graphene.String()
    recommendations  = graphene.String()
