import graphene

class CounselingformlogType(graphene.ObjectType):
    id  = graphene.Int()
    visitnum  = graphene.Int()
    formname  = graphene.String()
    user  = graphene.String()
    creationtime  = graphene.String()
