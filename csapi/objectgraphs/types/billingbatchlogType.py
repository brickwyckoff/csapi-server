import graphene

class BillingbatchlogType(graphene.ObjectType):
    id  = graphene.Int()
    type  = graphene.String()
    username  = graphene.String()
    entrytime  = graphene.String()
    externalbatchid  = graphene.Int()
    batchstatus  = graphene.String()
