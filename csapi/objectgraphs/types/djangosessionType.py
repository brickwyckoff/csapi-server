import graphene

class DjangoSessionType(graphene.ObjectType):
    session_key  = graphene.String()
    session_data  = graphene.String()
    expire_date  = graphene.String()
