import graphene

class DirectmessagelogType(graphene.ObjectType):
    id  = graphene.Int()
    messageto  = graphene.String()
    messagefrom  = graphene.String()
    messagesubject  = graphene.String()
    messagebody  = graphene.String()
    filename  = graphene.String()
    attachmentfilename  = graphene.String()
    isread  = graphene.Int()
    creationtime  = graphene.String()
