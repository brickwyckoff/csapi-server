import graphene

class VisitslinkType(graphene.ObjectType):
    visitnum  = graphene.Int()
    visitnumext  = graphene.String()
    linkname  = graphene.String()
    visitused  = graphene.Int()
    visitdate  = graphene.String()
