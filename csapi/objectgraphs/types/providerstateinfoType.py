import graphene

class ProviderstateinfoType(graphene.ObjectType):
    id  = graphene.Int()
    code  = graphene.String()
    state  = graphene.String()
    medicallicense  = graphene.String()
    deanumber  = graphene.String()
    xdeanumber  = graphene.String()
    pmpusername  = graphene.String()
    pmppassword  = graphene.String()
    updatetime  = graphene.String()
    specialty  = graphene.String()
