import graphene

class PrescriptiontofaxType(graphene.ObjectType):
    id  = graphene.Int()
    prescriptionlogid  = graphene.Int()
    faxdate  = graphene.String()
