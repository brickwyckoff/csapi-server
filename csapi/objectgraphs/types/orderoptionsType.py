import graphene

class OrderoptionsType(graphene.ObjectType):
    ordername  = graphene.String()
    active  = graphene.Int()
    gender  = graphene.String()
    ordercode  = graphene.String()
    sendto  = graphene.String()
    autoorder  = graphene.Int()
