import graphene

class FamilyhxnewType(graphene.ObjectType):
    mrn  = graphene.Int()
    code  = graphene.String()
    altcode  = graphene.String()
    codetable  = graphene.String()
    freetext  = graphene.String()
    startdate  = graphene.String()
    enddate  = graphene.String()
    isactive  = graphene.Int()
    isresolved  = graphene.Int()
    updatedate  = graphene.String()
    relation  = graphene.String()
    onsetage  = graphene.Int()
    infosource  = graphene.String()
