import graphene

class TreatmentplanoptionsType(graphene.ObjectType):
    optionid  = graphene.String()
    optiontext  = graphene.String()
    expandedtext  = graphene.String()
    pdfheight  = graphene.Float()
    additionaloption  = graphene.String()
    isactive  = graphene.Int()
    activedate  = graphene.String()
    inactivedate  = graphene.String()
    pttype  = graphene.String()
