import graphene

class CounselinginitiallogType(graphene.ObjectType):
    id  = graphene.Int()
    visitnum  = graphene.Int()
    user  = graphene.String()
    creationtime  = graphene.String()
