import graphene

class Lb2NotesType(graphene.ObjectType):
    mrn  = graphene.Int()
    visitnum  = graphene.Int()
    messagecontrolid  = graphene.String()
    labid  = graphene.String()
    note  = graphene.String()
