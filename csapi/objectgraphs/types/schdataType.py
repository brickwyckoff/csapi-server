import graphene

class SchdataType(graphene.ObjectType):
    id  = graphene.Int()
    mrn  = graphene.Int()
    visitnum  = graphene.Int()
    test  = graphene.String()
    type  = graphene.String()
    code  = graphene.String()
    date  = graphene.String()
