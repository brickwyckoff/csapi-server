import graphene

class EligibilityresponsecodesType(graphene.ObjectType):
    code  = graphene.Int()
    reason  = graphene.String()
    action  = graphene.String()
