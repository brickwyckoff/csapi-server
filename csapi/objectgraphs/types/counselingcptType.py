import graphene

class CounselingcptType(graphene.ObjectType):
    id  = graphene.Int()
    code  = graphene.String()
    description  = graphene.String()
    isactive  = graphene.Int()
