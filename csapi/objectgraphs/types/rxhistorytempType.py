import graphene

class RxhistorytempType(graphene.ObjectType):
    mrn  = graphene.Int()
    productname  = graphene.String()
    productid  = graphene.String()
    productidqual  = graphene.String()
    dosage  = graphene.String()
    strength  = graphene.String()
    strengthunits  = graphene.String()
    productdbcode  = graphene.String()
    productdbcodequal  = graphene.String()
    qtyqual  = graphene.String()
    qtycodelistqual  = graphene.String()
    qty  = graphene.String()
    directions  = graphene.String()
    daysupply  = graphene.String()
    lastfilldate  = graphene.String()
    writtendate  = graphene.String()
    expirationdate  = graphene.String()
    effectivedate  = graphene.String()
    enddate  = graphene.String()
    substitution  = graphene.String()
    refillqual  = graphene.String()
    refillqty  = graphene.String()
    diagqual  = graphene.String()
    diag  = graphene.String()
    note  = graphene.String()
    pharmacyid  = graphene.String()
    prescriberdea  = graphene.String()
    prescribernpi  = graphene.String()
    prescriberclinic  = graphene.String()
    prescriberfirstname  = graphene.String()
    prescriberlastname  = graphene.String()
    prescriberaddress1  = graphene.String()
    prescriberaddress2  = graphene.String()
    prescribercity  = graphene.String()
    prescriberstate  = graphene.String()
    prescriberzip  = graphene.String()
    prescriberphone  = graphene.String()
    prescriberphonequal  = graphene.String()
    creationtime  = graphene.String()
    payorname  = graphene.String()
    memberid  = graphene.String()
