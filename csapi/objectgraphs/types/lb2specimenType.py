import graphene

class Lb2SpecimenType(graphene.ObjectType):
    id  = graphene.Int()
    messagecontrolid  = graphene.String()
    labid  = graphene.String()
    type  = graphene.String()
    typecode  = graphene.String()
    conditiondesc  = graphene.String()
    conditioncode  = graphene.String()
    rejectreasondesc  = graphene.String()
    rejectreasoncode  = graphene.String()
