import graphene

class LhddataarchivedType(graphene.ObjectType):
    visitnum  = graphene.Int()
    mrn  = graphene.Int()
    placerid  = graphene.Int()
    fillerid  = graphene.String()
    messagecontrolid  = graphene.String()
    testid  = graphene.String()
    testname  = graphene.String()
    subheader  = graphene.String()
    subheaderid  = graphene.String()
    observationdate  = graphene.String()
    resultstatus  = graphene.String()
    replaced  = graphene.Int()
    analysedby  = graphene.String()
    ptnamereported  = graphene.String()
    loinccode  = graphene.String()
    resultsreporteddate  = graphene.String()
