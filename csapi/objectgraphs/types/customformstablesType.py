import graphene

class CustomformstablesType(graphene.ObjectType):
    id  = graphene.Int()
    formname  = graphene.String()
    tablename  = graphene.String()
    indextype  = graphene.Int()
    createdby  = graphene.String()
    creationtime  = graphene.String()
    isactive  = graphene.Int()
    formlocation  = graphene.String()
    syncfields  = graphene.String()
    formnamecode  = graphene.String()
