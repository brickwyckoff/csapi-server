import graphene

class ProtocolorderexpType(graphene.ObjectType):
    id  = graphene.Int()
    orderid  = graphene.Int()
    orderexp  = graphene.String()
    expstartdate  = graphene.String()
    expenddate  = graphene.String()
