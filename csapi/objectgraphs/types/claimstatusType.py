import graphene

class ClaimstatusType(graphene.ObjectType):
    id  = graphene.Int()
    visitnum  = graphene.Int()
    claimdate  = graphene.String()
    batchcontrolnum  = graphene.String()
    batchstatus  = graphene.String()
    batchstatusdate  = graphene.String()
    setcontrolnum  = graphene.String()
    claimstatus  = graphene.String()
    claimstatusamt  = graphene.String()
    claimstatuscodes  = graphene.String()
    claimstatusdate  = graphene.String()
    eobdate  = graphene.String()
    claimsuffix  = graphene.String()
