import graphene

class ScantypesType(graphene.ObjectType):
    scantype  = graphene.String()
    listordinal  = graphene.Int()
    barcodestart  = graphene.String()
