import graphene

class EligibilitycoveragelevelType(graphene.ObjectType):
    code  = graphene.String()
    reason  = graphene.String()
