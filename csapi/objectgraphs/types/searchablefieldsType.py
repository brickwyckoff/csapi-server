import graphene

class SearchablefieldsType(graphene.ObjectType):
    searchtype  = graphene.String()
    searchtable  = graphene.String()
    searchfields  = graphene.String()
    isactive  = graphene.Int()
