import graphene

class CcdfilesunlinkedType(graphene.ObjectType):
    fileid  = graphene.String()
    firstname  = graphene.String()
    lastname  = graphene.String()
    dob  = graphene.String()
    gender  = graphene.String()
