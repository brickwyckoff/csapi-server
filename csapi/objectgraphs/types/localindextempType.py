import graphene

class LocalindextempType(graphene.ObjectType):
    localmrn  = graphene.Int()
    localvisitnum  = graphene.Int()
    remotemrn  = graphene.Int()
    remotevisitnum  = graphene.Int()
