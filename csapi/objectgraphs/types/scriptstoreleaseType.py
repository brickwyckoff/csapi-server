import graphene

class ScriptstoreleaseType(graphene.ObjectType):
    id  = graphene.Int()
    mrn  = graphene.Int()
    prescriptionlogid  = graphene.Int()
    clinicloc  = graphene.Int()
    ncpdpid  = graphene.String()
    donotfilldate  = graphene.String()
    isreleased  = graphene.Int()
    releasetime  = graphene.String()
    releaseuser  = graphene.String()
    activetime  = graphene.String()
    visitnum  = graphene.Int()
