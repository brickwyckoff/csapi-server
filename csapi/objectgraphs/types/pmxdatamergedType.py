import graphene

class PmxdatamergedType(graphene.ObjectType):
    mrn  = graphene.Int()
    socialother  = graphene.String()
    familyother  = graphene.String()
