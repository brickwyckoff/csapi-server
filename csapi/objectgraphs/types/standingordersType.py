import graphene

class StandingordersType(graphene.ObjectType):
    id  = graphene.Int()
    mrn  = graphene.Int()
    labcode  = graphene.String()
    loccode  = graphene.String()
    provider  = graphene.String()
    signtime  = graphene.String()
    exptime  = graphene.String()
