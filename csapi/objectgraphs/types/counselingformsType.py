import graphene

class CounselingformsType(graphene.ObjectType):
    formname  = graphene.String()
    formdescription  = graphene.String()
    formurl  = graphene.String()
    active  = graphene.Int()
