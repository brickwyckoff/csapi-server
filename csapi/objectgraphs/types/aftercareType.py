import graphene

class AftercareType(graphene.ObjectType):
    filename  = graphene.String()
    sheetname  = graphene.String()
    keyterms  = graphene.String()
