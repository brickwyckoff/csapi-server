import graphene

class ChartamendmentType(graphene.ObjectType):
    id  = graphene.Int()
    visitnum  = graphene.Int()
    amendmentrequest  = graphene.String()
    amendment  = graphene.String()
    amendmenttime  = graphene.String()
    amendmentsource  = graphene.String()
    isaccepted  = graphene.Int()
    statustime  = graphene.String()
    requesttime  = graphene.String()
