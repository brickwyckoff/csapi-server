import graphene

class PostingcodesType(graphene.ObjectType):
    code  = graphene.String()
    description  = graphene.String()
    codegroup  = graphene.String()
    bucket  = graphene.String()
