import graphene

class SupavailableType(graphene.ObjectType):
    supervisor  = graphene.String()
    clinicid  = graphene.Int()
    availableslots  = graphene.Int()
    useslot  = graphene.Int()
