import graphene

class EligibilityverifiedinfoType(graphene.ObjectType):
    visitnum  = graphene.Int()
    firstname  = graphene.String()
    lastname  = graphene.String()
    address1  = graphene.String()
    address2  = graphene.String()
    city  = graphene.String()
    state  = graphene.String()
    zip  = graphene.String()
    dob  = graphene.String()
    gender  = graphene.String()
    insco  = graphene.String()
    policynum  = graphene.String()
    creationtime  = graphene.String()
    depfirstname  = graphene.String()
    deplastname  = graphene.String()
    depaddress1  = graphene.String()
    depaddress2  = graphene.String()
    depcity  = graphene.String()
    depstate  = graphene.String()
    depzip  = graphene.String()
    depdob  = graphene.String()
    depgender  = graphene.String()
    otherpayer  = graphene.String()
    controlnumber  = graphene.String()
    verifiedid  = graphene.Int()
    otherpayerpolicynum  = graphene.String()
