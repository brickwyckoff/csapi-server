import graphene

class AlertmessagesType(graphene.ObjectType):
    mrn  = graphene.Int()
    visitnum  = graphene.Int()
    message  = graphene.String()
    isviewed  = graphene.Int()
    accesslevel  = graphene.Int()
    creationtime  = graphene.String()
    dismissedby  = graphene.String()
    dismissedtime  = graphene.String()
