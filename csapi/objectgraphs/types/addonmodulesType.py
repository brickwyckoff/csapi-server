import graphene

class AddonmodulesType(graphene.ObjectType):
    name  = graphene.String()
    file  = graphene.String()
    active  = graphene.Int()
    ordinal  = graphene.Int()
