import graphene

class LabnotessendType(graphene.ObjectType):
    visitnum  = graphene.Int()
    lablocationcode  = graphene.String()
    notesource  = graphene.String()
    note  = graphene.String()
    issent  = graphene.Int()
