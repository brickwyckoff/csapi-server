import graphene

class StatrxauthType(graphene.ObjectType):
    visitnum  = graphene.Int()
    prescription  = graphene.String()
    freq  = graphene.String()
    qty  = graphene.String()
    route  = graphene.String()
    refills  = graphene.String()
    pharmacyid  = graphene.Int()
    requestedby  = graphene.String()
    requestedtime  = graphene.String()
    authorizedby  = graphene.String()
    authorizedtime  = graphene.String()
    imid  = graphene.String()
