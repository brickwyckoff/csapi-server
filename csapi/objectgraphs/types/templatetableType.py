import graphene

class TemplatetableType(graphene.ObjectType):
    keyword  = graphene.String()
    templatedisplayname  = graphene.String()
    templatename  = graphene.String()
    uselastdxbutton  = graphene.Int()
    uselastrxbutton  = graphene.Int()
