import graphene

class TemplateprescriptionsType(graphene.ObjectType):
    prescriptionclass  = graphene.String()
    prescription  = graphene.String()
