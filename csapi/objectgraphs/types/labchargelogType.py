import graphene

class LabchargelogType(graphene.ObjectType):
    id  = graphene.Int()
    messagecontrolid  = graphene.String()
    cptcode  = graphene.String()
    creationtime  = graphene.String()
