import graphene

class BillingeditedpostingsType(graphene.ObjectType):
    originalid  = graphene.Int()
    originalamt  = graphene.Float()
    originalenteredby  = graphene.String()
    originaltime  = graphene.String()
    creationtime  = graphene.String()
    editreason  = graphene.String()
