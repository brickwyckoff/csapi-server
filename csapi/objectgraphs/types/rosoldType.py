import graphene

class RosoldType(graphene.ObjectType):
    visitnum  = graphene.Int()
    ros  = graphene.String()
