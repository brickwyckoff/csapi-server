import graphene

class EmrsettingsType(graphene.ObjectType):
    setting  = graphene.String()
    timezone  = graphene.String()
    maindoctor  = graphene.String()
    defaultprovider  = graphene.String()
    defaultsocialworker  = graphene.String()
    siglock  = graphene.Int()
    dragablepopups  = graphene.Int()
    showbillinginfo  = graphene.Int()
    useoutsidecounselors  = graphene.Int()
    emrtype  = graphene.String()
    eligibilityverify  = graphene.Int()
    counselingtab  = graphene.String()
    strikecolor  = graphene.String()
    noshowcolor  = graphene.String()
    usesupformd  = graphene.Int()
    userxsupervisor  = graphene.Int()
    allowcalendardelete  = graphene.Int()
    sendlabforcesupervisor  = graphene.Int()
    providersigndisclaimer  = graphene.String()
    usebilling  = graphene.Int()
    uselexicomp  = graphene.Int()
    usepatientportal  = graphene.Int()
    eprescribeurl  = graphene.String()
    autologoutminutes  = graphene.Int()
    drugalertlevel  = graphene.Int()
    allergyalertlevel  = graphene.Int()
    eprescribedbname  = graphene.String()
    eprescribedbip  = graphene.String()
    eprescribedbport  = graphene.String()
    eprescribedbuser  = graphene.String()
    eprescribedbpassword  = graphene.String()
    allowincicd9  = graphene.Int()
    lateappointmentcheck  = graphene.Int()
    billingreportnoencounter  = graphene.Int()
    showptlistonload  = graphene.Int()
    useadvancedmd  = graphene.Int()
    reqdxcptonsign  = graphene.Int()
    hl7verifyoption  = graphene.String()
    conditionexactsearch  = graphene.Int()
    inclinteractioncitations  = graphene.Int()
    cdsusersconditions  = graphene.String()
    cdsusersmedications  = graphene.String()
    cdsusersallergies  = graphene.String()
    cdsusersdemographics  = graphene.String()
    cdsuserslabs  = graphene.String()
    cdsusersvitals  = graphene.String()
    cdsusersdrugint  = graphene.String()
    cdsuserscondint  = graphene.String()
    cdsusersalgint  = graphene.String()
    usesnomeddx  = graphene.Int()
    usenursedropdown  = graphene.Int()
    usesnomedprocedures  = graphene.Int()
    searchconditionsasdisorders  = graphene.Int()
    searchmedsgenflag  = graphene.Int()
    showemrclock  = graphene.String()
    showfullclinicaddress  = graphene.Int()
    usefunctionalstatus  = graphene.Int()
    schedulefuturetests  = graphene.Int()
    useprovidermiddlename  = graphene.Int()
    newprovideralert  = graphene.String()
    apptcalldays  = graphene.Int()
    dischargeexcludeencounters  = graphene.Int()
    useemercontactrelation  = graphene.Int()
    usedefaultpex  = graphene.Int()
    usepregnantflag  = graphene.Int()
    usecounselingbilling  = graphene.Int()
    usemedsuponcounseling  = graphene.Int()
    forceiframereload  = graphene.Int()
    erxonly  = graphene.Int()
    useselfpayallpostings  = graphene.Int()
    usealtpcpinfo  = graphene.Int()
    maindoctext  = graphene.String()
    useinsuitedoc  = graphene.Int()
    entercodeonbilling  = graphene.Int()
    useoptionalpriorauth  = graphene.Int()
    licenselevel  = graphene.String()
    useeligqueriesondemand  = graphene.Int()
    useeligqueriesschedule  = graphene.Int()
    usetextapptreminder  = graphene.Int()
    usevoiceapptreminder  = graphene.Int()
    showlasttxpdate  = graphene.Int()
    txpdaysagoalert  = graphene.Int()
    useclianumonall  = graphene.Int()
    decreferralonbilling  = graphene.Int()
    showpasthxinentryorder  = graphene.Int()
    entergroupasnewnote  = graphene.Int()
    useorderingprovideronallclaims  = graphene.Int()
    splitclaimsbynpi  = graphene.Int()
    setnpionsubmit  = graphene.Int()
    useprintfacesheet  = graphene.Int()
    showbalancedem  = graphene.Int()
    commlogsources  = graphene.String()
    usepostingsarchive  = graphene.Int()
    checkinnumbertype  = graphene.String()
    usedrugbilling  = graphene.String()
    useposselect  = graphene.Int()
    showpharmondem  = graphene.Int()
    usepdmp  = graphene.Int()
    usemrnformvawc  = graphene.Int()
    useprintmva  = graphene.Int()
    usesetcptfee  = graphene.Int()
    useboardcertfields  = graphene.Int()
    asamppcresource  = graphene.Int()
    superbillpdf  = graphene.String()
    superbilltpdf  = graphene.String()
    usehospitalizationdates  = graphene.Int()
    useprobationflag  = graphene.Int()
    showclinicnameonly  = graphene.Int()
    numpostingrows  = graphene.Int()
    counselingsuplabel  = graphene.String()
    usesecondaryphone  = graphene.Int()
    hpifieldlabel  = graphene.String()
    useepa  = graphene.Int()
    numfutureapptstoshow  = graphene.Int()
    ptsearchbydob  = graphene.Int()
    pdflababnormalsinred  = graphene.Int()
    eobautopost  = graphene.Int()
    dischargeunderline  = graphene.Int()
    usepronaccounting  = graphene.Int()
    storedclickrx  = graphene.String()
    useinsuranceonreviewpatients  = graphene.Int()
    useadditionalclaiminfo  = graphene.Int()
    useerxoncsl  = graphene.Int()
    providerdisplaytext  = graphene.String()
    maindoctextdem  = graphene.String()
    chiefcomplainttext  = graphene.String()
    utoxtestids  = graphene.String()
    useallusersonencounternote  = graphene.Int()
    blockviewdischargedpt  = graphene.Int()
    usepostingchangenotify  = graphene.Int()
    showentityonstatement  = graphene.Int()
    use837i  = graphene.Int()
    signonceperchart  = graphene.Int()
    showonlypramtonstmt  = graphene.Int()
    computeproviderpay  = graphene.Int()
    showpttypehx  = graphene.Int()
    showrxfieldsonhandp  = graphene.Int()
    usecsldxselect  = graphene.Int()
    erxautoloadnumdays  = graphene.Int()
    excludecalendarusers  = graphene.String()
    receiptprintlink  = graphene.String()
    showvisitnumontracker  = graphene.Int()
    usediagnotaxisoncsl  = graphene.Int()
    rightclickelementfx  = graphene.String()
    useapi  = graphene.String()
    usecslsup  = graphene.Int()
    urgenttasks  = graphene.String()
    printxdeaonsummary  = graphene.Int()
    useclaimsubmitdaterange  = graphene.Int()
    usedischargelineontimeline  = graphene.Int()
    showdischargedateincalendar  = graphene.Int()
    calendaradminonlyviewdischargedpts  = graphene.Int()
    usemaincounseloronptsreport  = graphene.Int()
    usemse  = graphene.Int()
    remindertimeperiod  = graphene.Int()
    useportalassessments  = graphene.Int()
    pastmedhxheader  = graphene.String()
    insdropdownfornonadminusers  = graphene.Int()
    usesocialworkerstable  = graphene.Int()
    userossystems  = graphene.Int()
    useshowcurrenttxplanbutton  = graphene.Int()
    usecslsummarybutton  = graphene.Int()
    usecslformsbutton  = graphene.Int()
    userelapsepreventionform  = graphene.Int()
    claimspreviewdateoffset  = graphene.Int()
    usefeedisplayonclaimspreview  = graphene.Int()
    usescrubonqueue  = graphene.Int()
    previewonlyunsentclaims  = graphene.Int()
    showlabresultsoncounselingsummary  = graphene.Int()
    cslsiglock  = graphene.Int()
    usequicknotes  = graphene.Int()
    usesignaturepad  = graphene.Int()
    showinsuranceonmainscreen  = graphene.Int()
    noeobdays  = graphene.Int()
    noclaimstatusdays  = graphene.Int()
    billingstartdate  = graphene.String()
    pdfabnormalsinred  = graphene.Int()
    showaccountingondemoonly  = graphene.Int()
    showptnotesondemoonly  = graphene.Int()
