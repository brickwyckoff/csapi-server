import graphene

class MsvdataType(graphene.ObjectType):
    id  = graphene.Int()
    visitnum  = graphene.Int()
    callresult  = graphene.String()
    calldone  = graphene.Int()
    rxcancelled  = graphene.Int()
    rxearlypickup  = graphene.Int()
