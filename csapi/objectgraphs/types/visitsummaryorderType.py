import graphene

class VisitsummaryorderType(graphene.ObjectType):
    id  = graphene.Int()
    arrayindex  = graphene.String()
    displayorder  = graphene.Int()
