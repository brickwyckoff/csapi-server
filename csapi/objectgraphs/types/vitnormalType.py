import graphene

class VitnormalType(graphene.ObjectType):
    vital  = graphene.String()
    low  = graphene.Int()
    high  = graphene.Int()
    agelow  = graphene.Int()
    agehigh  = graphene.Int()
    weightlow  = graphene.Int()
    weighthigh  = graphene.Int()
