import graphene

class PrintableformsType(graphene.ObjectType):
    isactive  = graphene.Int()
    formclass  = graphene.Int()
    haschildren  = graphene.Int()
    parentid  = graphene.Int()
    pageorder  = graphene.Int()
    formtype  = graphene.String()
    formname  = graphene.String()
    notificationemails  = graphene.String()
    recipientname  = graphene.String()
    recipientnamexy  = graphene.String()
    recipientfax  = graphene.String()
    recipientfaxxy  = graphene.String()
    formduration  = graphene.Int()
    formfunction  = graphene.String()
    backgroundform  = graphene.String()
    formwidth  = graphene.Float()
    formheight  = graphene.Float()
    font  = graphene.String()
    fontsize  = graphene.Int()
    fontcolor  = graphene.String()
    providernamexy  = graphene.String()
    providerfirstnamexy  = graphene.String()
    providerlastnamexy  = graphene.String()
    providernpixy  = graphene.String()
    providercontactxy  = graphene.String()
    providerphonexy  = graphene.String()
    providerphoneareaxy  = graphene.String()
    providerphonecityxy  = graphene.String()
    providerphonenumxy  = graphene.String()
    providerfaxxy  = graphene.String()
    providerfax  = graphene.String()
    providerfaxareaxy  = graphene.String()
    providerfaxcityxy  = graphene.String()
    providerfaxnumxy  = graphene.String()
    providersignaturexy  = graphene.String()
    provideraddressxy  = graphene.String()
    providercityxy  = graphene.String()
    providerstatexy  = graphene.String()
    providerzipxy  = graphene.String()
    ptnamexy  = graphene.String()
    ptfirstnamexy  = graphene.String()
    ptlastnamexy  = graphene.String()
    ptdobxy  = graphene.String()
    ptgenderxy  = graphene.String()
    malebox  = graphene.String()
    femalebox  = graphene.String()
    malecheckxy  = graphene.String()
    femalecheckxy  = graphene.String()
    ptpolicynumxy  = graphene.String()
    priminsurednamexy  = graphene.String()
    priminsuredphonexy  = graphene.String()
    ptinscoxy  = graphene.String()
    drugxy  = graphene.String()
    sigxy  = graphene.String()
    dxxy  = graphene.String()
    dxtextxy  = graphene.String()
    dx  = graphene.String()
    durationxy  = graphene.String()
    duration  = graphene.String()
    printdatexy  = graphene.String()
    defaultchecked  = graphene.String()
    newtxcheckxy  = graphene.String()
    conttxcheckxy  = graphene.String()
    txstartdatexy  = graphene.String()
    utoxycheckxy  = graphene.String()
    utoxncheckxy  = graphene.String()
    utoxdatexy  = graphene.String()
    defaultboxes  = graphene.String()
    specialtyxy  = graphene.String()
    cptxy  = graphene.String()
    cpt  = graphene.String()
    senderxy  = graphene.String()
    sendercellxy  = graphene.String()
    senderfaxxy  = graphene.String()
    secondproviderxy  = graphene.String()
    secondprovidernpixy  = graphene.String()
    secondproviderphonexy  = graphene.String()
    clinicnamexy  = graphene.String()
    clinicnpixy  = graphene.String()
    visitnumreq  = graphene.String()
    visitnumreqxy  = graphene.String()
    fulladdressxy  = graphene.String()
    providerlocalityxy  = graphene.String()
    deaxy  = graphene.String()
    licensexy  = graphene.String()
    clinictaxidxy  = graphene.String()
    ptphonexy  = graphene.String()
    ptadd1xy  = graphene.String()
    ptadd2xy  = graphene.String()
    ptcityxy  = graphene.String()
    ptstatexy  = graphene.String()
    ptzipxy  = graphene.String()
    ptfulladdressxy  = graphene.String()
    ptlocalityxy  = graphene.String()
    qtyxy  = graphene.String()
    refillsxy  = graphene.String()
    visitnumsxy  = graphene.String()
    groupnamexy  = graphene.String()
    pharmacyxy  = graphene.String()
    pharmacyphonexy  = graphene.String()
    visitnumxy  = graphene.String()
    barcodexy  = graphene.String()
    barcodeend  = graphene.String()
    txenddatexy  = graphene.String()
    mrnxy  = graphene.String()
    checkboxxy  = graphene.String()
