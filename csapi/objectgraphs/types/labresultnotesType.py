import graphene

class LabresultnotesType(graphene.ObjectType):
    id  = graphene.Int()
    labcode  = graphene.String()
    note  = graphene.String()
