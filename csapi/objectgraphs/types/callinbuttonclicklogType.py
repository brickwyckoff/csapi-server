import graphene

class CallinbuttonclicklogType(graphene.ObjectType):
    id  = graphene.Int()
    mrn  = graphene.Int()
    clicktime  = graphene.String()
    username  = graphene.String()
