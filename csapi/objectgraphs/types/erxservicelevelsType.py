import graphene

class ErxservicelevelsType(graphene.ObjectType):
    bitvalue  = graphene.Int()
    specialtydescription  = graphene.String()
    isactive  = graphene.Int()
