import graphene

class BillingholdType(graphene.ObjectType):
    id  = graphene.Int()
    mrn  = graphene.Int()
    visitdate  = graphene.String()
    holdtime  = graphene.String()
    status  = graphene.Int()
    releasedby  = graphene.String()
    holdreason  = graphene.String()
    visitnum  = graphene.Int()
    releasedtime  = graphene.String()
