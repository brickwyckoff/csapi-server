import graphene

class DjangoAdminLogType(graphene.ObjectType):
    action_time  = graphene.String()
    object_id  = graphene.String()
    object_repr  = graphene.String()
    action_flag  = graphene.Int()
    change_message  = graphene.String()
    content_type  = graphene.String()
    user  = graphene.String()
