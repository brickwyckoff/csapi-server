import graphene

class LabordernotesType(graphene.ObjectType):
    id  = graphene.Int()
    labcontrolid  = graphene.Int()
    note  = graphene.String()
    notesource  = graphene.String()
