import graphene

class CustomformslinksType(graphene.ObjectType):
    fromform  = graphene.String()
    fromfield  = graphene.String()
    toform  = graphene.String()
    tofield  = graphene.String()
