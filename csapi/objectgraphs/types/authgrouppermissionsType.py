import graphene

class AuthGroupPermissionsType(graphene.ObjectType):
    group  = graphene.String()
    permission  = graphene.String()
