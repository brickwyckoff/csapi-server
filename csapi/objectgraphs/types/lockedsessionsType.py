import graphene

class LockedsessionsType(graphene.ObjectType):
    sessionid  = graphene.String()
    user  = graphene.String()
    creationtime  = graphene.String()
