import graphene

class CustomformsmetaType(graphene.ObjectType):
    formname  = graphene.String()
    formsection  = graphene.String()
    indextype  = graphene.Int()
    displayname  = graphene.String()
    tablewidth  = graphene.Int()
    tableheader  = graphene.String()
    colwidths  = graphene.String()
    displayorder  = graphene.Int()
    isactive  = graphene.Int()
    formdisplayname  = graphene.String()
    siglines  = graphene.String()
    printonsummary  = graphene.Int()
    printcheckfield  = graphene.String()
    useimportbutton  = graphene.Int()
