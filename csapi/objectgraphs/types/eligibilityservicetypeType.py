import graphene

class EligibilityservicetypeType(graphene.ObjectType):
    code  = graphene.String()
    reason  = graphene.String()
