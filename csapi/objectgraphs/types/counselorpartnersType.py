import graphene

class CounselorpartnersType(graphene.ObjectType):
    counselorname  = graphene.String()
    databasename  = graphene.String()
    shareinsurancedemographics  = graphene.Int()
