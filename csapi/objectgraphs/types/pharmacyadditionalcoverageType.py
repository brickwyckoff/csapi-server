import graphene

class PharmacyadditionalcoverageType(graphene.ObjectType):
    controlnumber  = graphene.String()
    payerincr  = graphene.Int()
    payerorder  = graphene.String()
    name  = graphene.String()
    policynumberqual  = graphene.String()
    policynumber  = graphene.String()
