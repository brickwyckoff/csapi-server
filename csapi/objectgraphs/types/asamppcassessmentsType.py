import graphene

class AsamppcassessmentsType(graphene.ObjectType):
    mrn  = graphene.Int()
    assessmentid  = graphene.String()
    creationtime  = graphene.String()
    asamcreationtime  = graphene.String()
    updatetime  = graphene.String()
    completiontime  = graphene.String()
    allvisited  = graphene.Int()
    iscomplete  = graphene.Int()
