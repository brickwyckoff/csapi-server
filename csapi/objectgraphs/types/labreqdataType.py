import graphene

class LabreqdataType(graphene.ObjectType):
    visitnum  = graphene.Int()
    code  = graphene.String()
    loccode  = graphene.String()
