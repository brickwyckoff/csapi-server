import graphene

class AuthUserUserPermissionsType(graphene.ObjectType):
    user  = graphene.String()
    permission  = graphene.String()
