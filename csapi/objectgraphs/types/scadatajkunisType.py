import graphene

class ScadatajkunisType(graphene.ObjectType):
    mrn  = graphene.Int()
    scantype  = graphene.String()
    scanfile  = graphene.String()
    scandate  = graphene.String()
    user  = graphene.String()
    scandeleted  = graphene.Int()
    pdfconvert  = graphene.Int()
