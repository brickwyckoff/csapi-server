import graphene

class PatientslinkmergedType(graphene.ObjectType):
    mrn  = graphene.Int()
    mrnext  = graphene.String()
    linkname  = graphene.String()
    lastquerytime  = graphene.String()
