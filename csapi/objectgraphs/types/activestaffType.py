import graphene

class ActivestaffType(graphene.ObjectType):
    username  = graphene.String()
    userrole  = graphene.String()
    sendalerts  = graphene.Int()
