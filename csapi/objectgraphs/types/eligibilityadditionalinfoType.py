import graphene

class EligibilityadditionalinfoType(graphene.ObjectType):
    verifiedid  = graphene.Int()
    controlnumber  = graphene.String()
    relatedto  = graphene.String()
    refqual  = graphene.String()
    refident  = graphene.String()
    refdescription  = graphene.String()
