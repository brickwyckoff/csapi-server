import graphene

class AlerttypesType(graphene.ObjectType):
    alertname  = graphene.String()
    active  = graphene.Int()
