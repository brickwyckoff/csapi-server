import graphene

class EobcarrierlinkType(graphene.ObjectType):
    id  = graphene.Int()
    eobcarrier  = graphene.String()
    insid  = graphene.Int()
