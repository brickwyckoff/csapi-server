import graphene

class NoteschangelogType(graphene.ObjectType):
    noteid  = graphene.Int()
    oldnote  = graphene.String()
    newnote  = graphene.String()
    notetype  = graphene.String()
    user  = graphene.String()
    changetime  = graphene.String()
