import graphene

class RnodataType(graphene.ObjectType):
    visitnum  = graphene.Int()
    roomnumber  = graphene.String()
    creationtime  = graphene.String()
