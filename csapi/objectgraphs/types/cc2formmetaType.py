import graphene

class Cc2FormmetaType(graphene.ObjectType):
    categoryname  = graphene.String()
    datafield  = graphene.String()
    displaytext  = graphene.String()
    pageordinal  = graphene.Int()
    listordinal  = graphene.Int()
