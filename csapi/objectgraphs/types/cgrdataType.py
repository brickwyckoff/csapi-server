import graphene

class CgrdataType(graphene.ObjectType):
    id  = graphene.Int()
    mrn  = graphene.Int()
    groupnum  = graphene.String()
