import graphene

class TemplatesectionorderType(graphene.ObjectType):
    id  = graphene.Int()
    sectionname  = graphene.String()
    sectionorder  = graphene.Int()
