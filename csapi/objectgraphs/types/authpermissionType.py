import graphene

class AuthPermissionType(graphene.ObjectType):
    name  = graphene.String()
    content_type  = graphene.String()
    codename  = graphene.String()
