import graphene

class PatienteducationType(graphene.ObjectType):
    visitnum  = graphene.Int()
    docid  = graphene.String()
    doctype  = graphene.String()
    doctitle  = graphene.String()
    doclink  = graphene.String()
