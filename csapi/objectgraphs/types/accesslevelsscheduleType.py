import graphene

class AccesslevelsscheduleType(graphene.ObjectType):
    levelcode  = graphene.Int()
    leveldescription  = graphene.String()
