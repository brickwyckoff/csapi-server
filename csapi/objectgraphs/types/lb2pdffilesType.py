import graphene

class Lb2PdffilesType(graphene.ObjectType):
    labid  = graphene.String()
    filename  = graphene.String()
