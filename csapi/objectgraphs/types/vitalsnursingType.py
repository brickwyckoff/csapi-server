import graphene

class VitalsnursingType(graphene.ObjectType):
    temp  = graphene.Float()
    bp  = graphene.String()
    bps  = graphene.Int()
    bpd  = graphene.Int()
    pulse  = graphene.Int()
    rr  = graphene.Int()
    oxsat  = graphene.Int()
    visitnum  = graphene.Int()
    heightft  = graphene.Int()
    heightin  = graphene.Float()
    weight  = graphene.Float()
    mrn  = graphene.Int()
    enteredtime  = graphene.String()
    enteredby  = graphene.String()
