import graphene

class DisdataType(graphene.ObjectType):
    visitnum  = graphene.Int()
    discharge  = graphene.String()
    instructionpacket1  = graphene.String()
    instructionpacket2  = graphene.String()
    instructionpacket3  = graphene.String()
    summarytopt  = graphene.Int()
    summarytopt1  = graphene.Int()
    ptrequestedehealthinfo  = graphene.Int()
    ehealthinfosent  = graphene.Int()
    pttransferedout  = graphene.Int()
    esummaryout  = graphene.Int()
