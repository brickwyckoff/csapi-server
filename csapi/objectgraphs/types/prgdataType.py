import graphene

class PrgdataType(graphene.ObjectType):
    id  = graphene.Int()
    visitnum  = graphene.Int()
    pregnant  = graphene.Int()
    source  = graphene.String()
    duedate  = graphene.String()
    mrn  = graphene.Int()
    date  = graphene.String()
