import graphene

class CcdlogType(graphene.ObjectType):
    mrn  = graphene.Int()
    visitnum  = graphene.Int()
    sendto  = graphene.String()
    sendby  = graphene.String()
    sendtime  = graphene.String()
