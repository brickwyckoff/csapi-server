import graphene

class CustomformsType(graphene.ObjectType):
    formname  = graphene.String()
    formsection  = graphene.String()
    itemtype  = graphene.String()
    display  = graphene.String()
    field  = graphene.String()
    sizerows  = graphene.Int()
    sizecols  = graphene.Int()
    active  = graphene.Int()
    displayorder  = graphene.Int()
    cptadd  = graphene.String()
    asrow  = graphene.Int()
