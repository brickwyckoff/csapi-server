import graphene

class Icd9NoexportType(graphene.ObjectType):
    id  = graphene.Int()
    icd9  = graphene.String()
