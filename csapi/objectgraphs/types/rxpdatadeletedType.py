import graphene

class RxpdatadeletedType(graphene.ObjectType):
    mrn  = graphene.Int()
    refillsleft  = graphene.Int()
    prescription  = graphene.String()
    freq  = graphene.String()
    qty  = graphene.String()
    route  = graphene.String()
    refills  = graphene.String()
    lastprescriber  = graphene.String()
    lastprescriptiontime  = graphene.String()
    lastprinttime  = graphene.String()
    lastfilldate  = graphene.String()
    nextfilldate  = graphene.String()
