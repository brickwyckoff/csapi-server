import graphene

class FieldnamesType(graphene.ObjectType):
    fieldname  = graphene.String()
    fieldid  = graphene.String()
    rowname  = graphene.String()
    indextype  = graphene.Int()
    otherinstances  = graphene.String()
    lockable  = graphene.Int()
    localtransfer  = graphene.Int()
    accesslevel  = graphene.Int()
    active  = graphene.Int()
    valuetype  = graphene.String()
