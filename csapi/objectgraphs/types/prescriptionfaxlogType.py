import graphene

class PrescriptionfaxlogType(graphene.ObjectType):
    mrn  = graphene.Int()
    prescription  = graphene.String()
    freq  = graphene.String()
    qty  = graphene.String()
    route  = graphene.String()
    refills  = graphene.String()
    provider  = graphene.String()
    faxtime  = graphene.String()
    usedvisitnum  = graphene.Int()
    faxto  = graphene.String()
