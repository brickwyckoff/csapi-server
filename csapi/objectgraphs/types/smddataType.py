import graphene

class SmddataType(graphene.ObjectType):
    id  = graphene.Int()
    visitnum  = graphene.Int()
    code  = graphene.String()
    codetype  = graphene.String()
