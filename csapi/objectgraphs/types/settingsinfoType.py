import graphene

class SettingsinfoType(graphene.ObjectType):
    id  = graphene.Int()
    fieldname  = graphene.String()
    shortdescription  = graphene.String()
    fielddescription  = graphene.String()
    accesslevel  = graphene.Int()
