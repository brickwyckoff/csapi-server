import graphene

class HisdataType(graphene.ObjectType):
    mrn  = graphene.Int()
    nkda  = graphene.Int()
    medhx  = graphene.String()
    tobacco  = graphene.Int()
    alcohol  = graphene.Int()
    socialhx  = graphene.String()
    familyhx  = graphene.String()
    hepc  = graphene.Int()
