import graphene

class TemplatesoncallType(graphene.ObjectType):
    templatename  = graphene.String()
    field  = graphene.String()
    showtext  = graphene.String()
    printvalue  = graphene.String()
    type  = graphene.String()
    sectionheader  = graphene.String()
    subheader  = graphene.String()
    radioshow  = graphene.Int()
    itemname  = graphene.String()
    itemid  = graphene.String()
    sectionordinal  = graphene.Int()
    listordinal  = graphene.Int()
    hpisection  = graphene.String()
