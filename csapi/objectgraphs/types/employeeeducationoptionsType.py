import graphene

class EmployeeeducationoptionsType(graphene.ObjectType):
    optiondesc  = graphene.String()
    optionfunction  = graphene.String()
    isactive  = graphene.Int()
    displayorder  = graphene.Int()
