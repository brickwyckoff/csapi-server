import graphene

class LabdataType(graphene.ObjectType):
    visitnum  = graphene.Int()
    code  = graphene.String()
    loccode  = graphene.String()
