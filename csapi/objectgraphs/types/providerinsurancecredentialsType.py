import graphene

class ProviderinsurancecredentialsType(graphene.ObjectType):
    id  = graphene.Int()
    provider  = graphene.String()
    insurance  = graphene.String()
