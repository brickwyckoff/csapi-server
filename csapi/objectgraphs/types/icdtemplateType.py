import graphene

class IcdtemplateType(graphene.ObjectType):
    id  = graphene.Int()
    code  = graphene.String()
    templatestartdate  = graphene.String()
    templateenddate  = graphene.String()
