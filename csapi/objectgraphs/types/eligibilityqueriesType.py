import graphene

class EligibilityqueriesType(graphene.ObjectType):
    user  = graphene.String()
    visitnum  = graphene.Int()
    checktime  = graphene.String()
    insco  = graphene.String()
    mrn  = graphene.Int()
