import graphene

class ProtocolordersType(graphene.ObjectType):
    pttype  = graphene.String()
    orderid  = graphene.Int()
    timefreq  = graphene.Int()
