import graphene

class PexdataType(graphene.ObjectType):
    visitnum  = graphene.Int()
    general  = graphene.String()
    eye  = graphene.String()
    ent  = graphene.String()
    neck  = graphene.String()
    pulm  = graphene.String()
    card  = graphene.String()
    abd  = graphene.String()
    ms  = graphene.String()
    neuro  = graphene.String()
    lymph  = graphene.String()
    psych  = graphene.String()
    other  = graphene.String()
    skin  = graphene.String()
    intoxication  = graphene.String()
    withdrawal  = graphene.String()
    mse  = graphene.String()
