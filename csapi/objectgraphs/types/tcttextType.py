import graphene

class TcttextType(graphene.ObjectType):
    id  = graphene.Int()
    visitnum  = graphene.Int()
    itemtext  = graphene.String()
    itemtype  = graphene.String()
    groupid  = graphene.String()
    creationtime  = graphene.String()
