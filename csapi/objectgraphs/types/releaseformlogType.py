import graphene

class ReleaseformlogType(graphene.ObjectType):
    id  = graphene.Int()
    visitnum  = graphene.Int()
    user  = graphene.String()
    inforeleased  = graphene.String()
    releaseexpiration  = graphene.String()
    releasepurpose  = graphene.String()
    releasedate  = graphene.String()
