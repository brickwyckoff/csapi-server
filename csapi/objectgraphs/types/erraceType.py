import graphene

class ErraceType(graphene.ObjectType):
    aanum  = graphene.Int()
    race  = graphene.String()
    language  = graphene.String()
