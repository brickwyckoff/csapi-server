import graphene

class SocialworkersType(graphene.ObjectType):
    firstname  = graphene.String()
    lastname  = graphene.String()
    title  = graphene.String()
    code  = graphene.String()
    issup  = graphene.Int()
    signature  = graphene.String()
    gender  = graphene.String()
    npi  = graphene.String()
    taxonomycode  = graphene.String()
