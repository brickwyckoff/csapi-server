import graphene

class PrescriptionrouteType(graphene.ObjectType):
    code  = graphene.String()
    meaning  = graphene.String()
