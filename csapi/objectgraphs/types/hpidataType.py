import graphene

class HpidataType(graphene.ObjectType):
    visitnum  = graphene.Int()
    history  = graphene.String()
    mvawc  = graphene.Int()
    triage  = graphene.String()
    attendingnote  = graphene.String()
    currenttreatmentlevel  = graphene.Int()
