import graphene

class TemplateprescriptionsdosageType(graphene.ObjectType):
    prescriptionclass  = graphene.String()
    dosage  = graphene.String()
