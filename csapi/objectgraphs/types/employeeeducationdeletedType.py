import graphene

class EmployeeeducationdeletedType(graphene.ObjectType):
    edtitle  = graphene.String()
    edmessage  = graphene.String()
    edvideo  = graphene.String()
    edfile  = graphene.String()
    edactivedate  = graphene.String()
    edminviews  = graphene.Int()
    edactiveread  = graphene.Int()
    edactiveuntilremoved  = graphene.Int()
    enteredby  = graphene.String()
    enteredtime  = graphene.String()
    assignedusers  = graphene.String()
    assignedusertypes  = graphene.String()
    isactive  = graphene.Int()
