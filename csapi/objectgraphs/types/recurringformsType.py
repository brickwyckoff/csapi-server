import graphene

class RecurringformsType(graphene.ObjectType):
    id  = graphene.Int()
    insco  = graphene.String()
    labcodes  = graphene.String()
    goodfordays  = graphene.Int()
    goodfortests  = graphene.Int()
    formtype  = graphene.String()
    autosend  = graphene.Int()
    annualreset  = graphene.Int()
