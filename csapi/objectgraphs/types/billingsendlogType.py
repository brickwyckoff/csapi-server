import graphene

class BillingsendlogType(graphene.ObjectType):
    visitnum  = graphene.Int()
    resend  = graphene.Int()
    user  = graphene.String()
    sendtime  = graphene.String()
