import graphene

class SocialhxType(graphene.ObjectType):
    mrn  = graphene.Int()
    hxid  = graphene.Int()
    freetext  = graphene.String()
