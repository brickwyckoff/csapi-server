import graphene

class TfxdataType(graphene.ObjectType):
    id  = graphene.Int()
    visitnum  = graphene.Int()
    medrecin  = graphene.Int()
    pttransferedin  = graphene.Int()
    sumcare  = graphene.Int()
