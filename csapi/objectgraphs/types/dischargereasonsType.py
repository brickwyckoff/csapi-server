import graphene

class DischargereasonsType(graphene.ObjectType):
    reason  = graphene.String()
    displayorder  = graphene.Int()
