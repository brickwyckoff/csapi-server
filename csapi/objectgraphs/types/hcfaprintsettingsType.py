import graphene

class HcfaprintsettingsType(graphene.ObjectType):
    id  = graphene.Int()
    username  = graphene.String()
    horizshift  = graphene.Float()
    vertshift  = graphene.Float()
    vertoffset  = graphene.Float()
    changetime  = graphene.String()
