import graphene

class PatientstudyoptionsType(graphene.ObjectType):
    studyname  = graphene.String()
    isactive  = graphene.Int()
    barcolor  = graphene.String()
    displayonreport  = graphene.Int()
    performeval  = graphene.Int()
