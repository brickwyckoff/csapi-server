import graphene

class RxfilllogType(graphene.ObjectType):
    mrn  = graphene.Int()
    prescription  = graphene.String()
    freq  = graphene.String()
    qty  = graphene.String()
    route  = graphene.String()
    refills  = graphene.String()
    prescriber  = graphene.String()
    filltime  = graphene.String()
    pharmacy  = graphene.Int()
    visitnum  = graphene.Int()
    filename  = graphene.String()
    iscancelled  = graphene.Int()
    faxdate  = graphene.String()
