import graphene

class CnslogdataType(graphene.ObjectType):
    date  = graphene.String()
    mrn  = graphene.Int()
    visitnum  = graphene.Int()
    counselor  = graphene.String()
    note  = graphene.String()
