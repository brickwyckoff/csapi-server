import graphene

class PatnotesType(graphene.ObjectType):
    mrn  = graphene.Int()
    note  = graphene.String()
    user  = graphene.String()
    creationtime  = graphene.String()
