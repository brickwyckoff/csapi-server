import graphene

class FeetableCopyType(graphene.ObjectType):
    code  = graphene.String()
    fee  = graphene.Int()
