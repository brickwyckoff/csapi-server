import graphene

class PortalinfoType(graphene.ObjectType):
    id  = graphene.Int()
    accesslevel  = graphene.String()
    databaseaccess  = graphene.String()
    codedowloadfile  = graphene.String()
    mostrecent  = graphene.String()
    downloadalldem  = graphene.Int()
    labcodedowloadfile  = graphene.String()
    demdowloadfile  = graphene.String()
