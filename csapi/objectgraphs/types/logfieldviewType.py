import graphene

class LogfieldviewType(graphene.ObjectType):
    id  = graphene.Int()
    visitnum  = graphene.Int()
    user  = graphene.String()
    timein  = graphene.String()
    timeout  = graphene.String()
    field  = graphene.String()
    mrn  = graphene.Int()
