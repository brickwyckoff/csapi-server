import graphene

class EmployeeeducationlogType(graphene.ObjectType):
    id  = graphene.Int()
    edid  = graphene.Int()
    user  = graphene.String()
    viewtime  = graphene.String()
