import graphene

class MacrosmetaType(graphene.ObjectType):
    user  = graphene.String()
    macroname  = graphene.String()
    isactive  = graphene.Int()
    displayorder  = graphene.Int()
