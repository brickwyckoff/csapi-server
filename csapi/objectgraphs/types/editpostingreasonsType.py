import graphene

class EditpostingreasonsType(graphene.ObjectType):
    id  = graphene.Int()
    reason  = graphene.String()
    active  = graphene.Int()
