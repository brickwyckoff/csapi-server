import graphene

class CommonmedicationsType(graphene.ObjectType):
    typecode  = graphene.String()
    drugid  = graphene.String()
    tradeid  = graphene.Int()
    doseform  = graphene.Int()
    route  = graphene.Int()
    strength  = graphene.Int()
    freetext  = graphene.String()
    genericbrand  = graphene.String()
    rxotc  = graphene.String()
    cvxcode  = graphene.String()
    freq  = graphene.Int()
