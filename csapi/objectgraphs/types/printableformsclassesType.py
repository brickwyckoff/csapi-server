import graphene

class PrintableformsclassesType(graphene.ObjectType):
    id  = graphene.Int()
    classname  = graphene.String()
