import graphene

class PharmacyeligibilityerrorType(graphene.ObjectType):
    mrn  = graphene.Int()
    errorcode  = graphene.String()
    errortime  = graphene.String()
    policyname  = graphene.String()
    validtransaction  = graphene.Int()
    followup  = graphene.String()
