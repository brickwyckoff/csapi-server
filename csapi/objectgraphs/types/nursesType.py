import graphene

class NursesType(graphene.ObjectType):
    id  = graphene.Int()
    firstname  = graphene.String()
    lastname  = graphene.String()
    title  = graphene.String()
    gender  = graphene.String()
    code  = graphene.String()
    mainclinic  = graphene.Int()
