import graphene

class ImmunizationssendlogType(graphene.ObjectType):
    id  = graphene.Int()
    medorderlogid  = graphene.Int()
    sentdate  = graphene.String()
    sentby  = graphene.String()
    senttoid  = graphene.Int()
