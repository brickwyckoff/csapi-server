import graphene

class RosdataType(graphene.ObjectType):
    visitnum  = graphene.Int()
    fever  = graphene.Int()
    chills  = graphene.Int()
    vision  = graphene.Int()
    discharge  = graphene.Int()
    sorethroat  = graphene.Int()
    epistaxis  = graphene.Int()
    cp  = graphene.Int()
    angina  = graphene.Int()
    edema  = graphene.Int()
    cough  = graphene.Int()
    sputum  = graphene.Int()
    dyspnea  = graphene.Int()
    myalgias  = graphene.Int()
    backpain  = graphene.Int()
    neckpain  = graphene.Int()
    ap  = graphene.Int()
    nausea  = graphene.Int()
    vomiting  = graphene.Int()
    diarrhea  = graphene.Int()
    blackstool  = graphene.Int()
    brbpr  = graphene.Int()
    constipation  = graphene.Int()
    rash  = graphene.Int()
    pruritis  = graphene.Int()
    bleeding  = graphene.Int()
    bruising  = graphene.Int()
    dysuria  = graphene.Int()
    urgency  = graphene.Int()
    frequency  = graphene.Int()
    vagdc  = graphene.Int()
    testpain  = graphene.Int()
    depression  = graphene.Int()
    sihi  = graphene.Int()
    ha  = graphene.Int()
    loc  = graphene.Int()
    paresthesias  = graphene.Int()
    hives  = graphene.Int()
    immunocompromised  = graphene.Int()
    reviewedandneg  = graphene.Int()
    fatigue  = graphene.Int()
    insomnia  = graphene.Int()
    sweats  = graphene.Int()
    anxiety  = graphene.Int()
    bonepain  = graphene.Int()
    restlessness  = graphene.Int()
    craving  = graphene.Int()
    sob  = graphene.Int()
    diaphoresis  = graphene.Int()
    hotcold  = graphene.Int()
    genpain  = graphene.Int()
    nightsweats  = graphene.Int()
    cankersores  = graphene.Int()
    diffvoiding  = graphene.Int()
    sexdys  = graphene.Int()
    legcramps  = graphene.Int()
    restlesslegs  = graphene.Int()
    tiredness  = graphene.Int()
    toothache  = graphene.Int()
    dizziness  = graphene.Int()
    decreasedapp  = graphene.Int()
    musclecramps  = graphene.Int()
    painfuljoints  = graphene.Int()
    coldsx  = graphene.Int()
    troublesleeping  = graphene.Int()
    hotflashes  = graphene.Int()
    bodyaches  = graphene.Int()
    diffurination  = graphene.Int()
    concentration  = graphene.Int()
    memory  = graphene.Int()
    irritability  = graphene.Int()
    hopelessness  = graphene.Int()
    legal  = graphene.Int()
    feeling  = graphene.Int()
    other  = graphene.String()
    exerintol  = graphene.Int()
    weightchange  = graphene.Int()
    blurredvision  = graphene.Int()
    dryeyes  = graphene.Int()
    wateryeyes  = graphene.Int()
    irritationeyes  = graphene.Int()
    earpain  = graphene.Int()
    diffhearing  = graphene.Int()
    freqnosebleeds  = graphene.Int()
    stuffynose  = graphene.Int()
    runnynose  = graphene.Int()
    drymouth  = graphene.Int()
    toothabn  = graphene.Int()
    oralabn  = graphene.Int()
    bleedinggums  = graphene.Int()
    snoring  = graphene.Int()
    mouthulcer  = graphene.Int()
    palpatations  = graphene.Int()
    heartmurmur  = graphene.Int()
    armpain  = graphene.Int()
    sobwalking  = graphene.Int()
    soblying  = graphene.Int()
    lightheadedstanding  = graphene.Int()
    drycough  = graphene.Int()
    prodcough  = graphene.Int()
    wheezing  = graphene.Int()
    coughingblood  = graphene.Int()
    diffbreathing  = graphene.Int()
    appchange  = graphene.Int()
    urinarylosscontrol  = graphene.Int()
    diffurinating  = graphene.Int()
    bloodurine  = graphene.Int()
    incbladderempty  = graphene.Int()
    muscleaches  = graphene.Int()
    muscleweakness  = graphene.Int()
    jointpain  = graphene.Int()
    jaundice  = graphene.Int()
    dryskin  = graphene.Int()
    skinlesions  = graphene.Int()
    skinitching  = graphene.Int()
    seizures  = graphene.Int()
    freqha  = graphene.Int()
    migraines  = graphene.Int()
    numbness  = graphene.Int()
    prickling  = graphene.Int()
    difffallasleep  = graphene.Int()
    diffstayasleep  = graphene.Int()
    narcaddiction  = graphene.Int()
    increasedthirst  = graphene.Int()
    hairloss  = graphene.Int()
    hairgrowth  = graphene.Int()
    coldint  = graphene.Int()
    swollenglands  = graphene.Int()
    easybruising  = graphene.Int()
    excessbleeding  = graphene.Int()
    freqsneezing  = graphene.Int()
    sinuspressure  = graphene.Int()
    itching  = graphene.Int()
    eyepain  = graphene.Int()
    dicharge  = graphene.Int()
    descreasedvision  = graphene.Int()
    tinnitus  = graphene.Int()
    bloodynose  = graphene.Int()
    hearingloss  = graphene.Int()
    sinusitus  = graphene.Int()
    hemoptysis  = graphene.Int()
    pleurisy  = graphene.Int()
    pnd  = graphene.Int()
    orthopnea  = graphene.Int()
    syncope  = graphene.Int()
    hematemesis  = graphene.Int()
    melena  = graphene.Int()
    hematuria  = graphene.Int()
    hesistancy  = graphene.Int()
    incontinence  = graphene.Int()
    uti  = graphene.Int()
    arthralgia  = graphene.Int()
    jointswelling  = graphene.Int()
    nsaid  = graphene.Int()
    sores  = graphene.Int()
    nailchanges  = graphene.Int()
    skinthickening  = graphene.Int()
    ataxia  = graphene.Int()
    tremors  = graphene.Int()
    vertigo  = graphene.Int()
    excessthirst  = graphene.Int()
    polyuria  = graphene.Int()
    hotint  = graphene.Int()
    goiter  = graphene.Int()
    antidepressants  = graphene.Int()
    alcabuse  = graphene.Int()
    drugabuse  = graphene.Int()
    bleedingdiathesis  = graphene.Int()
    bloodclots  = graphene.Int()
    lymphedema  = graphene.Int()
    allergicrhinitis  = graphene.Int()
    hayfever  = graphene.Int()
    asthma  = graphene.Int()
    positiveppd  = graphene.Int()
    mentalillness  = graphene.Int()
    weightloss  = graphene.Int()
    mouthlesions  = graphene.Int()
    rinorrhea  = graphene.Int()
    abcess  = graphene.Int()
    trackmarks  = graphene.Int()
    mouth_lesions  = graphene.Int()
    track_marks  = graphene.Int()
    acidref  = graphene.Int()
    bladdercontrol  = graphene.Int()
    bladderinf  = graphene.Int()
    bowelcont  = graphene.Int()
    darkstool  = graphene.Int()
    dentalprobs  = graphene.Int()
    depressedmood  = graphene.Int()
    difficultysleeping  = graphene.Int()
    diffswallowing  = graphene.Int()
    dry  = graphene.Int()
    earaches  = graphene.Int()
    excesssweats  = graphene.Int()
    faint  = graphene.Int()
    fainting  = graphene.Int()
    feelinganx  = graphene.Int()
    fractures  = graphene.Int()
    glassescontacts  = graphene.Int()
    hearingprobs  = graphene.Int()
    heatcold  = graphene.Int()
    hep  = graphene.Int()
    highbp  = graphene.Int()
    hiv  = graphene.Int()
    instability  = graphene.Int()
    irrheartbeat  = graphene.Int()
    itchiness  = graphene.Int()
    jointstiff  = graphene.Int()
    jointswell  = graphene.Int()
    lightheaded  = graphene.Int()
    limb  = graphene.Int()
    memoryloss  = graphene.Int()
    moles  = graphene.Int()
    musclespasms  = graphene.Int()
    nosebleeds  = graphene.Int()
    numbting  = graphene.Int()
    painurine  = graphene.Int()
    recsorethroat  = graphene.Int()
    ringingears  = graphene.Int()
    senstolight  = graphene.Int()
    sexdrive  = graphene.Int()
    shortbreathexert  = graphene.Int()
    shortbreathrest  = graphene.Int()
    sinusprobs  = graphene.Int()
    stressprobs  = graphene.Int()
    swellingfeet  = graphene.Int()
    swollenlymph  = graphene.Int()
    tb  = graphene.Int()
    vomappearance  = graphene.Int()
    weakness  = graphene.Int()
    weightchg  = graphene.Int()
