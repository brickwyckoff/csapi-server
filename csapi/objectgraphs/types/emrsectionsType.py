import graphene

class EmrsectionsType(graphene.ObjectType):
    sectioncode  = graphene.String()
    sectionname  = graphene.String()
    indextype  = graphene.Int()
    autoadd  = graphene.Int()
