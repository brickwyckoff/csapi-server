import graphene

class ReqfieldsType(graphene.ObjectType):
    fieldid  = graphene.String()
    fieldtype  = graphene.String()
    valuetype  = graphene.String()
    ismedical  = graphene.Int()
