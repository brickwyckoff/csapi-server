import graphene

class ImportedmedrecType(graphene.ObjectType):
    id  = graphene.Int()
    mrn  = graphene.Int()
    sourcename  = graphene.String()
    importtime  = graphene.String()
    isack  = graphene.Int()
