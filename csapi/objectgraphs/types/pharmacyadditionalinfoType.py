import graphene

class PharmacyadditionalinfoType(graphene.ObjectType):
    controlnumber  = graphene.String()
    relatedto  = graphene.String()
    refqual  = graphene.String()
    refident  = graphene.String()
    refdescription  = graphene.String()
