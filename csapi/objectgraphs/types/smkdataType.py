import graphene

class SmkdataType(graphene.ObjectType):
    id  = graphene.Int()
    visitnum  = graphene.Int()
    status  = graphene.String()
