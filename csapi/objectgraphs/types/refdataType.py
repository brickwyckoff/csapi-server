import graphene

class RefdataType(graphene.ObjectType):
    id  = graphene.Int()
    mrn  = graphene.Int()
    startdate  = graphene.String()
    enddate  = graphene.String()
    referringprovidernpi  = graphene.String()
    referredtoprovider  = graphene.String()
    referralnumber  = graphene.String()
    numreferrals  = graphene.Int()
    removed  = graphene.Int()
    clinicid  = graphene.Int()
