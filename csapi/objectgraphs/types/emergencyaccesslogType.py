import graphene

class EmergencyaccesslogType(graphene.ObjectType):
    mrn  = graphene.Int()
    user  = graphene.String()
    invoketime  = graphene.String()
    invokeip  = graphene.String()
    validdate  = graphene.String()
