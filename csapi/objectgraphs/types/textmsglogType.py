import graphene

class TextmsglogType(graphene.ObjectType):
    id  = graphene.Int()
    comnumber  = graphene.String()
    mrn  = graphene.Int()
    textmessage  = graphene.String()
    receivetime  = graphene.String()
    isread  = graphene.Int()
    tofrom  = graphene.Int()
    user  = graphene.String()
