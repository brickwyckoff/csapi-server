import graphene

class CaldataType(graphene.ObjectType):
    visitnum  = graphene.Int()
    unitqty  = graphene.Int()
    timeunit  = graphene.String()
    scheduled  = graphene.Int()
    changetime  = graphene.String()
    caldbid  = graphene.Int()
