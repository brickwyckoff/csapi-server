import graphene

class ProbdataType(graphene.ObjectType):
    id  = graphene.Int()
    visitnum  = graphene.Int()
    probation  = graphene.Int()
    source  = graphene.String()
    enddate  = graphene.String()
    mrn  = graphene.Int()
    entrydate  = graphene.String()
