import graphene

class LoginfailureType(graphene.ObjectType):
    username  = graphene.String()
    time  = graphene.String()
    ip  = graphene.String()
