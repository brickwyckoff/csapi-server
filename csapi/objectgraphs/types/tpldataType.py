import graphene

class TpldataType(graphene.ObjectType):
    id  = graphene.Int()
    visitnum  = graphene.Int()
    itemid  = graphene.Int()
    optionid  = graphene.String()
    creationtime  = graphene.String()
    user  = graphene.String()
