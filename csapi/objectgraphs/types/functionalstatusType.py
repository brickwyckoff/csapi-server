import graphene

class FunctionalstatusType(graphene.ObjectType):
    id  = graphene.Int()
    mrn  = graphene.Int()
    hxid  = graphene.Int()
