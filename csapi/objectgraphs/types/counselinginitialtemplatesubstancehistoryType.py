import graphene

class CounselinginitialtemplatesubstancehistoryType(graphene.ObjectType):
    datafield  = graphene.String()
    displaytext  = graphene.String()
    active  = graphene.Int()
    listorder  = graphene.Int()
