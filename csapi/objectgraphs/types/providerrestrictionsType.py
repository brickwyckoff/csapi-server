import graphene

class ProviderrestrictionsType(graphene.ObjectType):
    providersfield  = graphene.String()
    providersfieldvalue  = graphene.String()
    blockeddemfield  = graphene.String()
    blockeddemfieldterms  = graphene.String()
    blockedroles  = graphene.String()
    blockedreason  = graphene.String()
