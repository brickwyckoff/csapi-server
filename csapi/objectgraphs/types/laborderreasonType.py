import graphene

class LaborderreasonType(graphene.ObjectType):
    id  = graphene.Int()
    laborderid  = graphene.Int()
    laborderpendingid  = graphene.Int()
    reason  = graphene.String()
    entrytime  = graphene.String()
    isordered  = graphene.Int()
    isreviewed  = graphene.Int()
    username  = graphene.String()
