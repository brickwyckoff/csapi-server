import graphene

class PayprosstoredcardsType(graphene.ObjectType):
    mrn  = graphene.Int()
    cardnum  = graphene.String()
    expdate  = graphene.String()
    storeddate  = graphene.String()
    user  = graphene.String()
    cardpayerid  = graphene.String()
