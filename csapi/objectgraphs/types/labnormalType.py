import graphene

class LabnormalType(graphene.ObjectType):
    lab  = graphene.String()
    lowvalue  = graphene.Float()
    highvalue  = graphene.Float()
    gender  = graphene.String()
