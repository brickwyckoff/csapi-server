import graphene

class CopdataType(graphene.ObjectType):
    visitnum  = graphene.Int()
    copay  = graphene.Float()
    paymenttype  = graphene.Int()
    pharmacycopay  = graphene.Float()
    pharmacycopayconf  = graphene.String()
    extrapayment  = graphene.Float()
    requiredamount  = graphene.Float()
    selfpay  = graphene.Float()
