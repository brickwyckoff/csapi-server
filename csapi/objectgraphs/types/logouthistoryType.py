import graphene

class LogouthistoryType(graphene.ObjectType):
    username  = graphene.String()
    time  = graphene.String()
    ip  = graphene.String()
    serverip  = graphene.String()
