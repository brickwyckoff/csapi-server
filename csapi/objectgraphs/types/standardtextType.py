import graphene

class StandardtextType(graphene.ObjectType):
    texttype  = graphene.String()
    pttype  = graphene.String()
    visittype  = graphene.Int()
    textvalue  = graphene.String()
