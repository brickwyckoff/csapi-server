import graphene

class PharmacymessageType(graphene.ObjectType):
    controlnumber  = graphene.String()
    message  = graphene.String()
