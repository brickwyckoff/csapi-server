import graphene

class EmailreportsType(graphene.ObjectType):
    username  = graphene.String()
    active  = graphene.Int()
