import graphene

class TranscriptionpostType(graphene.ObjectType):
    id  = graphene.Int()
    transcription  = graphene.String()
    visitdate  = graphene.String()
    dictationdate  = graphene.String()
    transcriptiondate  = graphene.String()
