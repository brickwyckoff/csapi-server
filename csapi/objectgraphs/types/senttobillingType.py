import graphene

class SenttobillingType(graphene.ObjectType):
    dos  = graphene.String()
    user  = graphene.String()
    sendtime  = graphene.String()
