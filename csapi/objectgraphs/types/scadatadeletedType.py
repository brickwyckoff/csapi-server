import graphene

class ScadatadeletedType(graphene.ObjectType):
    mrn  = graphene.Int()
    scantype  = graphene.String()
    scanfile  = graphene.String()
    scandate  = graphene.String()
    user  = graphene.String()
