import graphene

class LocktableType(graphene.ObjectType):
    user  = graphene.String()
    section  = graphene.String()
    mrn  = graphene.Int()
    visitnum  = graphene.Int()
    entertime  = graphene.String()
