import graphene

class LogreportviewType(graphene.ObjectType):
    id  = graphene.Int()
    username  = graphene.String()
    report  = graphene.String()
    viewtime  = graphene.String()
