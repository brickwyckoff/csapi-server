import graphene

class EligibilitystatusinfoType(graphene.ObjectType):
    verifiedid  = graphene.Int()
    visitnum  = graphene.Int()
    insco  = graphene.String()
    status  = graphene.String()
    coveragelevel  = graphene.String()
    servicetype  = graphene.String()
    insurancetype  = graphene.String()
    plandescription  = graphene.String()
    creationtime  = graphene.String()
    monetaryamount  = graphene.String()
    percent  = graphene.String()
    quantityqual  = graphene.String()
    quantity  = graphene.String()
    authorization  = graphene.String()
    inplan  = graphene.String()
    timeperiodqual  = graphene.String()
    ebid  = graphene.Int()
