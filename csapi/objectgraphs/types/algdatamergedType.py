import graphene

class AlgdatamergedType(graphene.ObjectType):
    mrn  = graphene.Int()
    allergy  = graphene.String()
    creationtime  = graphene.String()
