import graphene

class PatientreminderssentType(graphene.ObjectType):
    mrn  = graphene.Int()
    reviewedby  = graphene.String()
    timereviewed  = graphene.String()
    labloc  = graphene.String()
    labcode  = graphene.String()
