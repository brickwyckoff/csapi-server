import graphene

class MeddatamergedType(graphene.ObjectType):
    mrn  = graphene.Int()
    medication  = graphene.String()
    dose  = graphene.String()
    freq  = graphene.String()
    since  = graphene.String()
    creationtime  = graphene.String()
