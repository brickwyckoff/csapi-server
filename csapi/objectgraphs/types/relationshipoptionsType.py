import graphene

class RelationshipoptionsType(graphene.ObjectType):
    optionname  = graphene.String()
    isactive  = graphene.Int()
