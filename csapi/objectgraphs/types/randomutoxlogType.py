import graphene

class RandomutoxlogType(graphene.ObjectType):
    id  = graphene.Int()
    mrn  = graphene.Int()
    clinicid  = graphene.Int()
    randomdate  = graphene.String()
    calledin  = graphene.Int()
