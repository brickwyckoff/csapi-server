import graphene

class PrintableformserrorlogType(graphene.ObjectType):
    id  = graphene.Int()
    mrn  = graphene.Int()
    formtype  = graphene.String()
    creationtime  = graphene.String()
    sendmethod  = graphene.String()
    user  = graphene.String()
    errortype  = graphene.String()
