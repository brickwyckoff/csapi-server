import graphene

class ImgorderlogType(graphene.ObjectType):
    mrn  = graphene.Int()
    visitnum  = graphene.Int()
    orderedby  = graphene.String()
    ordertime  = graphene.String()
    filledby  = graphene.String()
    filledtime  = graphene.String()
    imagingordertest  = graphene.String()
    imagingorderlocation  = graphene.String()
    imagingorderreads  = graphene.Int()
    imagingorderindication  = graphene.String()
    otherinfo  = graphene.String()
    loinc  = graphene.String()
