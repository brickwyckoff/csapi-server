import graphene

class AuthGroupType(graphene.ObjectType):
    name  = graphene.String()
