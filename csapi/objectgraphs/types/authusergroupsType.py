import graphene

class AuthUserGroupsType(graphene.ObjectType):
    user  = graphene.String()
    group  = graphene.String()
