import graphene

class MrcdataType(graphene.ObjectType):
    id  = graphene.Int()
    visitnum  = graphene.Int()
    refillsremaining  = graphene.Int()
    qtyremaining  = graphene.Int()
    user  = graphene.String()
    creationtime  = graphene.String()
