import graphene

class MeddatasurescriptsType(graphene.ObjectType):
    mrn  = graphene.Int()
    ndc  = graphene.String()
    drugid  = graphene.String()
    tradeid  = graphene.Int()
    doseform  = graphene.Int()
    route  = graphene.Int()
    strength  = graphene.Int()
    startdate  = graphene.String()
    enddate  = graphene.String()
    freetext  = graphene.String()
    genericbrand  = graphene.String()
    rxotc  = graphene.String()
    reviewed  = graphene.Int()
