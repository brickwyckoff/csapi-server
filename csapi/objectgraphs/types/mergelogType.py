import graphene

class MergelogType(graphene.ObjectType):
    keepmrn  = graphene.Int()
    mergemrn  = graphene.Int()
    user  = graphene.String()
    mergetime  = graphene.String()
