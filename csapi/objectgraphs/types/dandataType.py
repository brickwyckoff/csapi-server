import graphene

class DandataType(graphene.ObjectType):
    id  = graphene.Int()
    visitnum  = graphene.Int()
    notes  = graphene.String()
