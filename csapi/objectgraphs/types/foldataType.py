import graphene

class FoldataType(graphene.ObjectType):
    visitnum  = graphene.Int()
    notes  = graphene.String()
