import graphene

class PrescriptionlogType(graphene.ObjectType):
    visitnum  = graphene.Int()
    prescription  = graphene.String()
    method  = graphene.String()
    user  = graphene.String()
    trackingnum  = graphene.String()
    creationtime  = graphene.String()
    autid  = graphene.Int()
    pharmacyid  = graphene.Int()
    faxto  = graphene.Int()
    provider  = graphene.String()
    mrn  = graphene.Int()
    printlogid  = graphene.Int()
