import graphene

class LabnotesType(graphene.ObjectType):
    mrn  = graphene.Int()
    note  = graphene.String()
    user  = graphene.String()
    creationtime  = graphene.String()
