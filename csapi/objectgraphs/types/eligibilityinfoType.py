import graphene

class EligibilityinfoType(graphene.ObjectType):
    code  = graphene.String()
    reason  = graphene.String()
