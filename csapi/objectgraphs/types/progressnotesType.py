import graphene

class ProgressnotesType(graphene.ObjectType):
    mrn  = graphene.Int()
    visitnum  = graphene.Int()
    note  = graphene.String()
    activitytype  = graphene.String()
    activitydate  = graphene.String()
    activitylength  = graphene.Int()
    observedby  = graphene.String()
    enteredby  = graphene.String()
    creationtime  = graphene.String()
