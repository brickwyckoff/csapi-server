import graphene

class AdminoptionsarchiveType(graphene.ObjectType):
    display  = graphene.String()
    description  = graphene.String()
    filename  = graphene.String()
    active  = graphene.Int()
    accesslevel  = graphene.Int()
    windowdimensions  = graphene.String()
