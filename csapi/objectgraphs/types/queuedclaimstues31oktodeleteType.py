import graphene

class QueuedclaimsTues31OktodeleteType(graphene.ObjectType):
    id  = graphene.Int()
    visitnum  = graphene.Int()
    sendtype  = graphene.Int()
    claimtype  = graphene.String()
    creationtime  = graphene.String()
    usepriorauth1  = graphene.Int()
    npi  = graphene.String()
    usepriorauth2  = graphene.Int()
    usepriorauth3  = graphene.Int()
    usepriorauth4  = graphene.Int()
    pos  = graphene.String()
    billfrom  = graphene.String()
    claimfiletype  = graphene.String()
    claimentitytype  = graphene.String()
    batchnumber  = graphene.String()
    billingprovider  = graphene.String()
    labbillingprovider  = graphene.String()
    inscorefnum  = graphene.String()
    approvedbatch  = graphene.String()
    islabclaim  = graphene.Int()
