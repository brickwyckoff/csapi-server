import graphene

class ReportsType(graphene.ObjectType):
    display  = graphene.String()
    description  = graphene.String()
    filename  = graphene.String()
    active  = graphene.Int()
    accesslevel  = graphene.String()
    listorder  = graphene.Int()
    assignmentfields  = graphene.String()
