import graphene

class LabcptType(graphene.ObjectType):
    labname  = graphene.String()
    procedurecode  = graphene.String()
    procedureqty  = graphene.Int()
    insco  = graphene.String()
    labsource  = graphene.String()
    labcode  = graphene.String()
    enteronorder  = graphene.Int()
    testtype  = graphene.String()
    active  = graphene.Int()
