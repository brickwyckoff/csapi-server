import graphene

class TreatmentlevelsType(graphene.ObjectType):
    levelname  = graphene.String()
    barcolor  = graphene.String()
    displayorder  = graphene.Int()
