import graphene

class StadataType(graphene.ObjectType):
    mrn  = graphene.Int()
    smokingstatus  = graphene.Int()
    smokingstartyear  = graphene.String()
    pttransferedin  = graphene.Int()
    medrecin  = graphene.Int()
    txstatus  = graphene.Int()
    room  = graphene.String()
    changetime  = graphene.String()
