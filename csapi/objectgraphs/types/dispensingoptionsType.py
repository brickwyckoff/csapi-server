import graphene

class DispensingoptionsType(graphene.ObjectType):
    id  = graphene.Int()
    drugid  = graphene.String()
    tradeid  = graphene.Int()
    strength  = graphene.Int()
    genericbrand  = graphene.String()
    rxotc  = graphene.String()
    doseform  = graphene.Int()
    route  = graphene.Int()
    isactive  = graphene.Int()
