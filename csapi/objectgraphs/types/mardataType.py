import graphene

class MardataType(graphene.ObjectType):
    mrn  = graphene.Int()
    visitnum  = graphene.Int()
    entrydate  = graphene.String()
    user  = graphene.String()
    section  = graphene.String()
    origfilldate  = graphene.String()
    prescription  = graphene.String()
    pillsperday  = graphene.Int()
    medsidentified  = graphene.Int()
    actualcount  = graphene.Int()
    dayssincefill  = graphene.Int()
    medication  = graphene.String()
    dosage  = graphene.String()
    observation  = graphene.String()
    condition  = graphene.String()
