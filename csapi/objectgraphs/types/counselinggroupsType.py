import graphene

class CounselinggroupsType(graphene.ObjectType):
    groupname  = graphene.String()
    dayofweek  = graphene.Int()
    starttime  = graphene.String()
    endtime  = graphene.String()
    counselor  = graphene.String()
    manager  = graphene.String()
    isactive  = graphene.Int()
