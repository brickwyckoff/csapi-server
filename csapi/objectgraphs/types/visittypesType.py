import graphene

class VisittypesType(graphene.ObjectType):
    visitname  = graphene.String()
    active  = graphene.Int()
    emrtype  = graphene.String()
    assignsup  = graphene.Int()
    ismedical  = graphene.Int()
    chiefcomplaint  = graphene.String()
    showasmdvisit  = graphene.Int()
    subtype  = graphene.String()
    iscounseling  = graphene.Int()
    notetitle  = graphene.String()
    isbillable  = graphene.Int()
