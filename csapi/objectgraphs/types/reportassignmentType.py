import graphene

class ReportassignmentType(graphene.ObjectType):
    username  = graphene.String()
    insurance  = graphene.String()
    clinicid  = graphene.String()
    visitstartdate  = graphene.String()
    visitenddate  = graphene.String()
    denialstartdate  = graphene.String()
    denialenddate  = graphene.String()
    cptcode  = graphene.String()
    lastnamestart  = graphene.String()
    lastnameend  = graphene.String()
    reporttype  = graphene.String()
    reportid  = graphene.Int()
