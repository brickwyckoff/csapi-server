import graphene

class TransmitfilelogType(graphene.ObjectType):
    fileid  = graphene.Int()
    transmitto  = graphene.String()
    transmittime  = graphene.String()
    transmitby  = graphene.String()
