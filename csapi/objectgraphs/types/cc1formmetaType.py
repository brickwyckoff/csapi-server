import graphene

class Cc1FormmetaType(graphene.ObjectType):
    categoryname  = graphene.String()
    datafield  = graphene.String()
    displaytext  = graphene.String()
    pageordinal  = graphene.Int()
    listordinal  = graphene.Int()
