import graphene

class FamilyhxrelationoptionsType(graphene.ObjectType):
    id  = graphene.Int()
    snomedcode  = graphene.String()
    displayorder  = graphene.Int()
    isfirstdegree  = graphene.Int()
