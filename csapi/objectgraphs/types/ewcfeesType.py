import graphene

class EwcFeesType(graphene.ObjectType):
    code  = graphene.String()
    fee  = graphene.Int()
