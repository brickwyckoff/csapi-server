import graphene

class PrescriptionpatientsType(graphene.ObjectType):
    mrn  = graphene.Int()
    activerx  = graphene.Int()
    pendingrx  = graphene.Int()
    activeoutdate  = graphene.String()
    pendingoutdate  = graphene.String()
