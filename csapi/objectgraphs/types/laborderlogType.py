import graphene

class LaborderlogType(graphene.ObjectType):
    mrn  = graphene.Int()
    visitnum  = graphene.Int()
    labcontrolid  = graphene.String()
    labcode  = graphene.String()
    sentto  = graphene.String()
    sentby  = graphene.String()
    creationtime  = graphene.String()
    datereviewed  = graphene.String()
    resultsentered  = graphene.Int()
    enteredby  = graphene.String()
    entertime  = graphene.String()
    loinc  = graphene.String()
    observed  = graphene.Int()
