import graphene

class EmployeeeducationassignedType(graphene.ObjectType):
    id  = graphene.Int()
    edid  = graphene.Int()
    user  = graphene.String()
    iscompleted  = graphene.Int()
