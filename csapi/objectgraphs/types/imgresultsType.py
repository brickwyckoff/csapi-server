import graphene

class ImgresultsType(graphene.ObjectType):
    id  = graphene.Int()
    mrn  = graphene.Int()
    imgtitle  = graphene.String()
    imgfile  = graphene.String()
    imgnotes  = graphene.String()
    importtime  = graphene.String()
    imgreviewed  = graphene.Int()
    reviewedby  = graphene.String()
