import graphene

class HpcdataType(graphene.ObjectType):
    id  = graphene.Int()
    visitnum  = graphene.Int()
    hepcpositive  = graphene.Int()
