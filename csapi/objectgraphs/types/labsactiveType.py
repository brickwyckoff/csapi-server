import graphene

class LabsactiveType(graphene.ObjectType):
    code  = graphene.String()
    loccode  = graphene.String()
    cptcode  = graphene.String()
    standardfreq  = graphene.Int()
    sendto  = graphene.String()
    ordercode  = graphene.String()
    isactive  = graphene.Int()
