import graphene

class Hl7LogType(graphene.ObjectType):
    sendingapplication  = graphene.String()
    sendingfacility  = graphene.String()
    receivingapplication  = graphene.String()
    receivingfacility  = graphene.String()
    messagetimestamp  = graphene.String()
    messagetype  = graphene.String()
    messagecontrolid  = graphene.String()
    processtimestamp  = graphene.String()
