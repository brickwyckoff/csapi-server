import graphene

class ScriptstocallinType(graphene.ObjectType):
    id  = graphene.Int()
    mrn  = graphene.Int()
    prescriptionlogid  = graphene.Int()
    lastprescriptionlogid  = graphene.Int()
    clinicloc  = graphene.Int()
    ncpdpid  = graphene.String()
    iscalledin  = graphene.Int()
    callintime  = graphene.String()
