import graphene

class VtkdataType(graphene.ObjectType):
    id  = graphene.Int()
    visitnum  = graphene.Int()
    redstatus  = graphene.Int()
    legalreporting  = graphene.Int()
