import graphene

class TemplateprescriptionsfreqType(graphene.ObjectType):
    prescriptionclass  = graphene.String()
    freq  = graphene.String()
