import graphene

class CcdfilesType(graphene.ObjectType):
    mrn  = graphene.Int()
    filename  = graphene.String()
    creationtime  = graphene.String()
