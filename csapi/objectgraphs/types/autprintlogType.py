import graphene

class AutprintlogType(graphene.ObjectType):
    id  = graphene.Int()
    autid  = graphene.Int()
    user  = graphene.String()
    printtime  = graphene.String()
