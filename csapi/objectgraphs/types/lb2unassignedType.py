import graphene

class Lb2UnassignedType(graphene.ObjectType):
    fillerid  = graphene.String()
    messagecontrolid  = graphene.String()
    firstname  = graphene.String()
    lastname  = graphene.String()
    dob  = graphene.String()
    isassigned  = graphene.Int()
    creationtime  = graphene.String()
