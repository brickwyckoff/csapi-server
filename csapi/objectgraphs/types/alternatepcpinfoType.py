import graphene

class AlternatepcpinfoType(graphene.ObjectType):
    pcpid  = graphene.Int()
    faxnum  = graphene.Int()
