import graphene

class DeniedclaimsCopy5OktodeleteType(graphene.ObjectType):
    payerid  = graphene.String()
    visitnum  = graphene.Int()
    company  = graphene.String()
    returndate  = graphene.String()
    user  = graphene.String()
    creationtime  = graphene.String()
    denyroute  = graphene.Int()
    denyreason  = graphene.String()
    proccode  = graphene.String()
    payortrackingnum  = graphene.String()
    claimstatuscodes  = graphene.String()
    claimsuffix  = graphene.String()
