import graphene

class BillingpendingclaimsType(graphene.ObjectType):
    id  = graphene.Int()
    insco  = graphene.String()
    filename  = graphene.String()
    timecreated  = graphene.String()
    claimamt  = graphene.Int()
    controlnumber  = graphene.Int()
    fileext  = graphene.String()
    sent  = graphene.Int()
    status  = graphene.String()
