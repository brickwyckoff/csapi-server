import graphene

class RxcancelfaxqueueType(graphene.ObjectType):
    id  = graphene.Int()
    mrn  = graphene.Int()
    cancelprescriptionlogid  = graphene.Int()
    newprescriptionlogid  = graphene.Int()
    ncpdpid  = graphene.String()
    canceldate  = graphene.String()
    cancelfaxtype  = graphene.Int()
    faxtransmitted  = graphene.Int()
