import graphene

class TctdataType(graphene.ObjectType):
    id  = graphene.Int()
    visitnum  = graphene.Int()
    itemid  = graphene.Int()
    groupid  = graphene.String()
    creationtime  = graphene.String()
