import graphene

class RxhistoryerrorType(graphene.ObjectType):
    mrn  = graphene.Int()
    errorcode  = graphene.String()
    descriptioncode  = graphene.String()
    descriptiontext  = graphene.String()
    creationtime  = graphene.String()
    payorname  = graphene.String()
    memberid  = graphene.String()
