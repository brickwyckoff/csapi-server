import graphene

class MaodataType(graphene.ObjectType):
    mrn  = graphene.Int()
    visitnum  = graphene.Int()
    user  = graphene.String()
    entrydate  = graphene.String()
    section  = graphene.String()
    medication  = graphene.String()
    dosage  = graphene.String()
    observation  = graphene.String()
    condition  = graphene.String()
