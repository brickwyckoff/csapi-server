import graphene

class PharmaciesType(graphene.ObjectType):
    name  = graphene.String()
    address1  = graphene.String()
    address2  = graphene.String()
    city  = graphene.String()
    state  = graphene.String()
    zip  = graphene.String()
    fax  = graphene.Int()
    phone  = graphene.Int()
    closestclinic  = graphene.Int()
    pharmsortorder  = graphene.Int()
