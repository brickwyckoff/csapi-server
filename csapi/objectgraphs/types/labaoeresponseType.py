import graphene

class LabaoeresponseType(graphene.ObjectType):
    visitnum  = graphene.Int()
    issent  = graphene.Int()
    labcode  = graphene.String()
    labname  = graphene.String()
    lablocationcode  = graphene.String()
    responsetype  = graphene.String()
    response  = graphene.String()
