import graphene

class EligibilityqueriesqueuedType(graphene.ObjectType):
    id  = graphene.Int()
    mrn  = graphene.Int()
    queuetime  = graphene.String()
    queueby  = graphene.String()
