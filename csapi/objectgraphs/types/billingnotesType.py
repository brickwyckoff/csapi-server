import graphene

class BillingnotesType(graphene.ObjectType):
    notes  = graphene.String()
    creationtime  = graphene.String()
    aanum  = graphene.Int()
    visitloc  = graphene.String()
    user  = graphene.String()
    mrn  = graphene.Int()
