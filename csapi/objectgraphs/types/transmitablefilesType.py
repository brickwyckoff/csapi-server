import graphene

class TransmitablefilesType(graphene.ObjectType):
    mrn  = graphene.Int()
    filename  = graphene.String()
    encryptedfilename  = graphene.String()
    hashvaluefilename  = graphene.String()
    filetype  = graphene.String()
