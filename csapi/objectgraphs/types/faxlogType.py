import graphene

class FaxlogType(graphene.ObjectType):
    id  = graphene.Int()
    faxid  = graphene.String()
    faxstatus  = graphene.Int()
    faxstatuscode  = graphene.String()
    faxstatusdesc  = graphene.String()
    faxerrorcode  = graphene.String()
    faxto  = graphene.String()
    faxtonumber  = graphene.String()
    faxtime  = graphene.String()
    faxfile  = graphene.String()
    logtable  = graphene.String()
    logid  = graphene.Int()
    originalfaxid  = graphene.Int()
