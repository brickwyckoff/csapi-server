import graphene

class MainscreenmessagesType(graphene.ObjectType):
    user  = graphene.String()
    enteredby  = graphene.String()
    creationtime  = graphene.String()
    dismissedtime  = graphene.String()
    isviewed  = graphene.Int()
    message  = graphene.String()
