import graphene

class ScheduleType(graphene.ObjectType):
    mrn  = graphene.Int()
    visitdate  = graphene.String()
    reason  = graphene.String()
    user  = graphene.String()
    changetime  = graphene.String()
