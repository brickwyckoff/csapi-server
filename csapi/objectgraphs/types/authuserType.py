import graphene

class AuthUserType(graphene.ObjectType):
    password  = graphene.String()
    last_login  = graphene.String()
    is_superuser  = graphene.Int()
    username  = graphene.String()
    first_name  = graphene.String()
    last_name  = graphene.String()
    email  = graphene.String()
    is_staff  = graphene.Int()
    is_active  = graphene.Int()
    date_joined  = graphene.String()
