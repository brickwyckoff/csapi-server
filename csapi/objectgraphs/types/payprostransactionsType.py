import graphene

class PayprostransactionsType(graphene.ObjectType):
    clientcode  = graphene.String()
    txtime  = graphene.String()
