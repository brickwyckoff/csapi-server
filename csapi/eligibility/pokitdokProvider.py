from base64 import urlsafe_b64encode
from multiprocessing.pool import ThreadPool
import requests
import json
import datetime
import collections
from csapi.core.apiCredentials import ApiCredentials
from csapi.core.cacheAccess import CacheAccess

class Provider:
    def __init__ (self):
        self.apiCredentials = ApiCredentials ()
        self.cache = CacheAccess ()
        self.cacheScope = "pokitdokProvider"
        self.pdConfig = self.apiCredentials.getPokitdokConfig ()
        self.coverageCodes = {
            'coinsurance': 'A',
            'copay': 'B',
            'deductibles': 'C',
            'limitations': 'F',
            'out_of_pocket': 'G',
            'primaryCarePhysician': 'L'
        }
        self.amountKeys = {
            # 'coinsurance': 'A',  # coinsurances do not seem to include an amount...
            'copay': 'copayment',
            'deductibles': 'benefit_amount',
            'limitations': 'benefit_amount',
            'out_of_pocket': 'benefit_amount',
            'primaryCarePhysician': 'L'
        }

    def checkEligibility (self, dbData):
        data = self.transformDbData (dbData)
        data = json.dumps (data, default=self.datetime_handler)
        url = self.getUrl ()
        headerObj = self.getHeader ()
        apiResponse = requests.post (url, data, headers = headerObj)

        return self.transformResult (apiResponse.json ()['data'])

    def getUrl (self):
        return '{0}/{1}'.format (self.pdConfig['baseUri'], 'api/v4/eligibility/')

    def getApiToken (self):
        key = 'pokitdokToken'
        token = self.cache.getScopedByKey (self.cacheScope, key)

        if token:  # token here will be a structure (includes expy time), not just the actual string
            if self.isTokenValid (token):
                return token['token']

        id = self.pdConfig['id']
        secret = self.pdConfig['password']

        pool = ThreadPool(processes=1)
        async_result = pool.apply_async(self.refreshAccessToken, (id, secret))
        async_result.wait (3)
        cacheObject = async_result.get ()

        # if we're here it wasn't in the cache or it was expired, update cache:
        self.cache.upsertScopedWithKey (self.cacheScope, key, cacheObject)

        return cacheObject ['token']

    def isTokenValid (self, tokenStruct):
        if not tokenStruct:
            return False

        if not 'expires' in tokenStruct:
            return False

        dtExpy = datetime.datetime.utcfromtimestamp (tokenStruct['expires'] - 30)  # return false if very close to expiring
        return dtExpy > datetime.datetime.utcnow ()

    def getHeader (self):
        token = self.getApiToken ()
        return {
            'Authorization': 'Bearer {}'.format (token),
            'Content-Type': 'application/json'
        }

    def datetime_handler(self, x):
        if isinstance(x, datetime.datetime) or isinstance(x, datetime.date):
            return x.isoformat()

        return json._default_encoder.encode (x)

    def refreshAccessToken (self, id, secret):
        byteData = urlsafe_b64encode((id + ':' + secret).encode ())
        authContent = 'Basic {}'.format (byteData.decode ())
        headerObj = {
            'Authorization': authContent
        }
        tokenResponse = requests.post ('https://platform.pokitdok.com/oauth2/token',
                                       headers=headerObj,
                                       data = {'grant_type':'client_credentials'})
        return {
            "token": tokenResponse.json ()['access_token'],
            "expires": tokenResponse.json ()['expires']
        }

    def transformDbData (self, dbData):
        return {
            "member": {
                "birth_date": dbData['patient']['dob'],
                "last_name":  dbData['patient']['lastName'],
                "id":  dbData['patient']['policyNum']
            },
            "provider": {
                "first_name": dbData['provider']['firstName'],
                "last_name": dbData['provider']['lastName'],
                "npi": dbData['provider']['npi']
            },
            "trading_partner_id": dbData['trading_partner_id']
        }

    # intput data format :: {
    #     'eligibilitySourceCode': eligibilitySourceCode,
    #     'patient': patientData,
    #     'provider': provData,
    #     'trading_partner_id': eligId,
    # }

    # required output format:  {
    #   patientData = {
    #     'first_name': patientFirst,
    #     'last_name': patientLast,
    #     'id': patientId,
    #     'birth_date': patientDob
    #   }
    #
    #   provData = {
    #     'first_name': provFirst,
    #     'last_name': provLast,
    #     'npi': provNpi
    #   }
    #
    #   allData = {
    #     'member': patientData,
    #     'provider': provData,
    #     'trading_partner_id': eligId,
    #   }
    # }

    def transformResult (self, jsonResponse):
        # need a batch ID to be generated here?? or should the other code do that?

        # top level return object:
        # - coverage
        #       - [] coinsurance
        #       - [] limitations,
        #       - [] copay
        #       - [] deductibles
        #       - [] out_of_pocket
        #       ? - primary_care_provider
        #       - active ("isActive")(should be 1, all else map to 6)
        #       - plan_description ("planCoverage")
        #       - [] service_type_codes: flattened into a string
        #       ? - level (I don't see wherre this gets set, or rather where  $this->coverageLevelDescArray is set, which it relies on
        #       - [] messages: flattened into a string ("message")
        #       ! - [] contacts
        #       ! - [] other_payers
        # - subscriber
        #       - first_name ("subscriberFirstName"), last_name, birth_date, gender, id ("subscriberPolicyNum")
        # - payer
        #       - name ("infoSourceName")
        #  - reject_reason: this needs to be mapped to thei lookup table

        # no coverageLevel, serviceType, errorCode in db at all. code doesn't even look like it would work
        # no eligMessage, but i think that's just because there's nothing coming back?

        # actually, put another way, here is what they do populate currently:
        #   mrn,batchId,insCo,subFirstName,subLastName,subPolicyNum,eligInfo, ...,

        # results in 2 rows inserted to "eligibilityCheckInfo":
        #   - first represents the patient
        #   - second - which never seems to be called - is very similar but winds up with an 'L' in the eligInfo column, and is only insrted if there's a value for the 'primary_care_provider' node ???

        patientData = self.extractPatientData (jsonResponse)
        covSummary = self.extractCoverageSummary (jsonResponse)
        covDetails =  self.extractCoverages (jsonResponse)

        result = {
            'patientData': patientData,
            'coverageSummary': covSummary,
            'rejectionReason': jsonResponse['coverage']['reject_reason'] if 'reject_reason' in jsonResponse['coverage'] else '',
            'coverageDetails': covDetails
        }

        if 'errorCode' in jsonResponse:
            result.update ({"errorCode": jsonResponse["errorCode"]})

        return result

    def extractPatientData (self, jsonResponse):
        return {
            "firstName": jsonResponse['subscriber']['first_name'],
            "lastName": jsonResponse['subscriber']['last_name'],
            "dob": jsonResponse['subscriber']['birth_date'],
            "gender": jsonResponse['subscriber']['gender'],
            "policyNum": jsonResponse['subscriber']['id']
        }

    def extractCoverageSummary (self, jsonResponse):
        msgs = None
        if 'messages' in jsonResponse['coverage'] and isinstance (jsonResponse['coverage']['messages'], collections.Sequence):
            msgNodes = (m['message'] for m in jsonResponse['coverage']['messages'])
            msgs = self.concatListAsString (msgNodes, '; ')

        return {
            'insCo': jsonResponse['payer']['name'],
            'messages': msgs,
            'active': True if jsonResponse['coverage']['active'] == 1 else False,
            'coverageLevel': '', # todo: needs to be looked up in another table - they use coverage.level to get to their version of it
            'activeServiceTypes': jsonResponse['coverage']['service_type_codes'],
            'planCoverage': jsonResponse['coverage']['plan_description'],
            'pcp': True if 'primary_care_provider' in jsonResponse['coverage'] else False,
        }

    def extractCoverages (self, jsonResponse):
        if not (jsonResponse and jsonResponse['coverage']):
            return None

        result = {}
        cov = jsonResponse['coverage']

        covNodes = ['coinsurance', 'copay', 'deductibles', 'out_of_pocket', 'limitations']

        for n in covNodes:
            result.update ({n: self.extractCoverageByType (cov, n)})

        return result

    def extractCoverageByType (self, jsonCoverage, key):
        if not jsonCoverage:
            return None

        if not key in jsonCoverage:
            return None

        covRoot = jsonCoverage[key]
        if not isinstance (covRoot, collections.Sequence) or 1 > len (covRoot):
            return None

        result = []

        for currCov in covRoot:
            elem = {
                'coverageCode': self.coverageCodes [key],
                'coverageLevel': 'todo',  # needs to refer to a lookup table, i think
                'serviceType': self.concatListAsString (currCov['service_type_codes']),
                'insuranceType': 'Never set',
                'planCoverage': 'Never set',
                'eligTime': 'todo',
                'benefitQtyType': 'Never set',
                'benefitQty': 'Never set',
            }
            amt = None
            if key in self.amountKeys:
                if self.amountKeys [key] in currCov:
                    amtNode = currCov[self.amountKeys [key]]
                    amt = amtNode ['amount']
            elem.update ({'benefitAmount': amt})

            pct = None
            if 'benefit_percent' in currCov:
                pct = currCov['benefit_percent']
            elem.update ({'benefitPercent': pct})

            messages = ''
            if 'messages' in currCov:
                msgNodes = (m['message'] for m in currCov['messages'])
                messages = self.concatListAsString(msgNodes, '; ')

            elem.update ({'messages': messages})

            result.append (elem)
        return  result

    def concatListAsString (self, enumerable, separator = '^'):
        return separator.join([str(x) for x in enumerable])







