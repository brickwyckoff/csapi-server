
from csapi.core.apiCredentials import ApiCredentials
from csapi.core.cacheAccess import CacheAccess

class Provider:
    def __init__ (self):
        self.apiCredentials = ApiCredentials ()
        self.cache = CacheAccess ()
        self.cacheScope = "pokitdokProvider"
        self.pdConfig = self.apiCredentials.getPokitdokConfig ()

    def checkEligibility (self, dbData):
        return None
