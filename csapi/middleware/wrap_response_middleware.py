
import json
from django.apps import apps
from datetime import datetime
from datetime import tzinfo
import pytz

from csapi.services.jwtService import JwtService

class WrapResponseMiddleware (object):
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response (request)

        if not 200 == response.status_code:
            return response

        # we're going to do 2 standard processing tasks for all outbound requests:
        #   - wrap the json payload in a standard wrapper
        #   - "refresh" the token if it's in danger of expiring

        # take json payload and move it into a node called "data"
        jsonBody = json.loads (response.content.decode ("UTF-8"))

        wrappedBody = {
            "data": jsonBody
        }

        # issue a refreshed token if necessary, otherwise send the orginal back

        if hasattr (request, 'token'):
            tokenNode = {
                "token": self.slideToken (request)
            }
            wrappedBody.update (tokenNode)

        response.content = json.dumps (wrappedBody, sort_keys=False).encode ("UTF-8")

        return response

    # returns a string
    def slideToken (self, request):

        if not JwtService.getEnforceExpiration ():
            return request.token

        # the goal here is to implement a sliding expiration; as they use the service
        #   we make sure they keep getting valid tokens. if they're idle for loonger
        #   than the expiration period and send a request we'll deny them, though

        # decode the token sent with the request (this was stashed by previous middleware)
        jwtData = JwtService.decodeToken (request.token)

        resetExpy = False

        if not "exp" in jwtData:
            resetExpy = True

        if not resetExpy:
            # if we're within (token validity period - slide period) minutes, refresh
            dtExpy = jwtData["exp"]                                  # expiration of original token
            dtNow = datetime.utcnow ().timestamp ()                  # now (put in a var for debugging purposes)
            dtToRefresh = dtExpy - (60 * JwtService.getTokenRefreshPeriod ())  # start of refresh period
            gmtNow = JwtService.gmAdjustDate (dtNow)

            # if current time is inside refresh window (now > refresh start && now < expy time)
            #   replace the token they sent with one that has additional time, to allow active users
            #   to avoid logging in again

            resetExpy = gmtNow > dtToRefresh

            print ("Token expiration as timestamp: {0}; UTC time: {1}".format (dtExpy, datetime.fromtimestamp (dtExpy)))
            print ("Token refresh timestamp: {0}; UTC time: {1}".format (dtToRefresh, datetime.fromtimestamp (dtToRefresh)))
            print ("Current timestamp: {0}; UTC time: {1}".format (gmtNow, datetime.fromtimestamp (gmtNow)))
            print ("Refresh?: {0}".format (resetExpy))

        if not resetExpy:
            return request.token

        jwtData["exp"] = JwtService.getNewExpirationTime ()

        return JwtService.encodeToken (jwtData).decode ("UTF-8")

