
import sys
from django.http import HttpResponseForbidden
from csapi.services.jwtService import JwtService

class ValidateJwtMiddleware (object):
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):

        allowRequest = False

        try:
            allowRequest = self.pre_request (request)

        except:
            print (sys.exc_info ()[0])

        if not allowRequest:
            return HttpResponseForbidden ()

        response = self.get_response (request)

        return response

    def pre_request (self, request):

        # allow basic auth (un-tokenized) requests that are attempting obtaining an auth token:

        if "/login/" == request.path.lower ():
            return True

        # allow anon pre-flight requests
        if "OPTIONS" == request.method:
            return True


        # if "/graph/" in request.path.lower ():
        #     return True

        if not 'HTTP_AUTHORIZATION' in request.META:        # missing auth altogether
            return False

        authData = request.META['HTTP_AUTHORIZATION']

        if not authData.lower ().startswith ("bearer "):    # unsupported fromat
            return False

        tokenData = authData.split ()[1]

        userValid = False

        try:
            authUser = JwtService.decodeToken (tokenData, request.META['REMOTE_ADDR'])

            if authUser:
                    userValid = True
                    request.apiUser = authUser
                    request.token = tokenData

        except:
            print (sys.exc_info())


        return userValid