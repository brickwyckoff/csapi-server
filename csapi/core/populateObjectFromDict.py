
class PopulateObjectFromDict ():
    def CopyFromDict (self, targetObj, sourceDict):
        for att in sourceDict:
            if (hasattr (targetObj, att)):
                try:
                    setattr (targetObj, att, sourceDict[att])
                except:
                    pass
