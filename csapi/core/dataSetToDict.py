
class DatasetToDictionaryConverter:
    @staticmethod
    def convertCursorData (cursor):
        columns = [col[0] for col in cursor.description]

        resultObject = [
            dict(zip(columns, row))
            for row in cursor.fetchall()
        ]

        return resultObject
