
from csapi.core.cacheAccess import CacheAccess

class ApiCredentials:
    def __init__ (self):
        print("apiCredentials.py: Constructing.")
        self.sourceData = CacheAccess ().getGlobalByKey ('ApiCreds')
        self.pokitdokCreds = self.getPokitdokConfig ()

    def getPokitdokConfig (self):
        key = "pokitdok"

        if not self.sourceData:
            return None

        for index, item in enumerate (self.sourceData):
            if item['resourceName'] == key:
                result ={
                    "baseUri": item['resourceAddress'],
                    "id": item['resourceUsername'],
                    "password": item['resourcePassword']
                }
        return result




