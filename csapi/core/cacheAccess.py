
from django.core.cache import caches

class CacheAccess:
    def __init__(self):
        print("cacheAccess.py: Constructing.")
        self.effectiveCache = caches['default']

    def getGlobalByKey (self, key):
        result = {}
        print("cacheAccess.py: Request for global with key '{}'.".format (key))

        try:
            result = self.effectiveCache.get ('Global.{}'.format (key))

        except:
            pass

        return  result

    def getScopedByKey(self, scope, key):
        result = {}
        print("cacheAccess.py: Request for scope '{0}' with key '{1}'.".format (scope, key))

        try:
            result = self.effectiveCache.get('{0}.{1}'.format (scope, key))

        except:
            pass

        return result

    def upsertGlobalWithKey (self, key, data):
        print("cacheAccess.py: Upserting global with key '{}'.".format (key))
        self.effectiveCache.set ('Global.{}'.format (key), data)

    def upsertScopedWithKey (self, scope, key, data):
        print("cacheAccess.py: Upserting in scope '{0}' with key '{1}'.".format (scope, key))
        self.effectiveCache.set ('{0}.{1}'.format (scope, key), data)


