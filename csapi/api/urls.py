
from django.conf.urls import url
from ..services.eligibilityService import EligibilityService

urlpatterns = [
   url(r'^refreshEligibility', EligibilityService.as_view ()),
]