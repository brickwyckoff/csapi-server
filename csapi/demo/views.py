
from urllib import request
from urllib.error import URLError, HTTPError

from django.core import serializers
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_GET
from rest_framework import viewsets

from csapi.models import demdata
from csapi.serializers import DemdataSerializer
from csapi.models.current_amd_visits import CurrentAmdVisits
from csapi.models.current_amd_patients import CurrentAmdPatients
from csapi.models.current_amd_patient_insurances import CurrentAmdPatientInsurances

import xml.etree.ElementTree

@csrf_exempt
@require_GET
def verifyCredentials(request):
	if not request.apiUser:
		return JsonResponse ({})

	return JsonResponse (request.apiUser)


@csrf_exempt
@require_GET
def loadDemData(request, demid):
	id = int(demid)
	rec = demdata.Demdata.objects.get(pk=id)
	retVal = serializers.serialize('json', [rec])
	return JsonResponse (retVal, safe=False)
def getXML(url, xml_params, cookie_value = None):

	headers = {'Content-Length': '600', 'Content-Type': 'application/xml'}
	if cookie_value:
		headers['Cookie'] = cookie_value
	# 	JSON params
	# 	values = {
	#   "ppmdmsg": {
	#     "@action": "login",
	#     "@class": "login",
	#     "@msgtime": "7/23/2015 3:29:16 PM ",
	#     "@username": "STR-FUL",
	#     "@psw": "sdf900bb",
	#     "@officecode": "991147",
	#     "@appname": "API"
	#   }
	# }
	try:
        # Use the line bellow for JSON encoding
		# data = parse.urlencode(xml_request).encode("utf-8")
		xml_params =  xml_params.encode("utf-8")
		req = request.Request(url, headers=headers)
		response = request.urlopen(req, xml_params)
		response_text = response.read()
		print(response_text)

	except HTTPError as e:
		print('The server couldn\'t fulfill the request.')
		print('Error code: ', e.code)
	except URLError as e:
		print('We failed to reach a server.')
		print('Reason: ', e.reason)
	except Exception as e:
		print('Error :', e)
	return response_text.decode("utf-8")
def saveXML(xml):
# 	Parse result xml
	nodes_list = {'visitlist', 'patientlist', 'insurancelist'}


	# for top_node in nodes_list:
	try:
				# if 'visitlist' == nodes_list[0]:
		parent_node = xml.find('./Results/visitlist')
		for visit_node in parent_node:
						class_to_save = CurrentAmdVisits()
						class_to_save.visit_num = visit_node.attrib['id']
						class_to_save.columnheading = visit_node.attrib['columnheading']
						class_to_save.duration = visit_node.attrib ['duration']
						class_to_save.color = visit_node.attrib ['color']
						class_to_save.save()

				    #'patientlist' element:
						for patientlist_node in visit_node:
							# 'patient' element:
							for patient_node in patientlist_node:
								class_to_save = CurrentAmdPatients()
								class_to_save.mrn = patient_node.attrib['id']
								class_to_save.patient_name = patient_node.attrib['name']
								class_to_save.ssn = patient_node.attrib['ssn']
								class_to_save.change_date = patient_node.attrib['changedat']
								class_to_save.create_date = patient_node.attrib['createdat']
								class_to_save.visit_num_id = visit_node.attrib['id']
								class_to_save.save()
						   # 'insurancelist' element:
								for insurancelist_node in patient_node:
									# 'insurance' element:
									for insurance_node in insurancelist_node:
										class_to_save = CurrentAmdPatientInsurances()
										class_to_save.insurance_id = insurance_node.attrib['id']
										class_to_save.carname = insurance_node.attrib['carname']
										class_to_save.carcode = insurance_node.attrib['carcode']
										class_to_save.change_date = insurance_node.attrib['changedat']
										class_to_save.create_date = insurance_node.attrib['createdat']
										class_to_save.mrn = patient_node.attrib['id']
										class_to_save.visit_num = visit_node.attrib['id']
										class_to_save.save()
	except Exception as e:
				return str(e)



	return 'Success'

def loginAMD(request):
	result={}
	results = []
	import datetime
	current_date = datetime.datetime.now().strftime("%m/%d/%Y")
	current_datetime = datetime.datetime.now().strftime("%m/%d/%Y %I:%M:%S %p")
	resourceAccessSites = Resourceaccess.objects.all()
	url = 'https://partnerlogin.advancedmd.com/practicemanager/xmlrpc/processrequest.asp'
	# xml_params = b'<ppmdmsg action="login" class="login" msgtime="7/23/2015 3:29:16 PM" username="STR-FUL" psw="sdf900bb" officecode="991147" appname="API"/>'
	# Clean old data from tables

	CurrentAmdPatientInsurances.objects.all().delete()
	CurrentAmdPatients.objects.all().delete()
	CurrentAmdVisits.objects.all().delete()

	for resourceAccessSite in resourceAccessSites:

		username = resourceAccessSite.resourceusername
		psw = resourceAccessSite.resourcepassword
		appname = resourceAccessSite.resourceversion
		officecode = resourceAccessSite.resourceother
		result['username'] = username
		result['appname'] = appname
		result['officecode'] = officecode

		xml_params = '<ppmdmsg action="login" class="login" msgtime="' + current_datetime + '" username="'+ username +'" psw="'+psw +'" officecode="'+officecode+'" appname="' + appname + '"/>'
		try:
			response_text = getXML(url=url,xml_params=xml_params)
			response_xml = xml.etree.ElementTree.fromstring(response_text)
			usercontext_node= response_xml.find('./Results/usercontext')
			# usercontext_node = xml.find('usercontext')
			url = usercontext_node.attrib['webserver']
			url += '/xmlrpc/processrequest.asp'

			response_text = getXML(url=url, xml_params=xml_params)
			response_xml = xml.etree.ElementTree.fromstring(response_text)
			token = response_xml.find('./Results/usercontext').text

			resourceAccessSite.resourcetoken = token
			resourceAccessSite.save()

			xml_params = '<ppmdmsg action="getdatevisits" class="api" msgtime="' + current_datetime + '" visitdate="' + current_date +'"><visit columnheading="ColumnHeading" duration="Duration" color="Color" /><patient name="Name" ssn="SSN" changedat="ChangedAt" createdat="CreatedAt" /><insurance carname="CarName" carcode="CarCode" changedat="ChangedAt" createdat="CreatedAt"/></ppmdmsg>'
			response_text = getXML(url=url,xml_params=xml_params, cookie_value= 'token=' + token)
			response_xml = xml.etree.ElementTree.fromstring(response_text)
			response_text = saveXML(response_xml)
			if response_text == 'Success':
				result['Success'] = 'Success'
			else:
				result['Error'] = response_text
			results.append(result)
		except Exception as e:
			print ('Error :' + response_text)
			result['Error'] = response_text
			results.append(result)

	import json
	test = json.dumps(results)
	return JsonResponse (test, safe=False)

class DemdataViewSet(viewsets.ModelViewSet):
    queryset = demdata.Demdata.objects.all().order_by('ptlastname')[:5]
    serializer_class = DemdataSerializer
