
# csapi.demo.urls.py

from django.conf.urls import url, include
from . import views

urlpatterns = [
    url(r'^verify/$', views.verifyCredentials),
    url(r'^demdatamanual/(?P<demid>\d+)/$', views.loadDemData),
    url(r'^loginamd/$', views.loginAMD),
]